import { Component, OnInit } from '@angular/core';

import { PageService } from './../page.service';
import { LocalstorageService } from '../localstorage.service';
import { CompaniesService } from './../services/companies.service';
import { EmployeesService } from '../services/employees.service';
import { AttendancesService } from '../services/attendances.service';
import { PayrollsService } from '../services/payrolls.service';
import { LeavesService } from '../services/leaves.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  company : any = {};

  total_count_emp = 0;
  birthdays = [];
  newEmployees : any = {};
  attendances = [];
  params_attendance = {
		filter_date: 'today',
    limit: 100
  };
  today = {
    absent : 0,
    present: 0,
    late : 0,
  };
  
  month = {
    absent : 0,
    present: 0,
    late : 0,
    undertime: 0
  };

  payrolls = [];
  params_payroll = {
    w: 'total_payroll'
  };
  total_payroll = 0;

  leaveTypes = [];
  leaves = [];
  params_leaves = {
		limit: 10,
		page: 1,
		w: 'upcoming_leaves'
  };
  
  latest_payroll = [];
  params_latest_payroll = {
    w: 'latest_payroll'
  };

  empLoader = true;
  dtrLoader = true;
  payrollLoader = true;
  newEmpLoader = true;
  birthdaysLoader = true;
  leavesLoader = true;
  latestPayrollLoader = true;

  percent = {
    absent: 0,
    present: 0,
    late: 0,
    undertime: 0
  };

  constructor(
    private PageService: PageService,
    private storage: LocalstorageService,
    private CompaniesService: CompaniesService,
    private EmployeesService: EmployeesService,
    private AttendancesService: AttendancesService,
    private PayrollsService: PayrollsService,
    private LeavesService: LeavesService
  ) { }

  ngOnInit() {
    console.log('DAHS BOARD INIT');
    this.company = JSON.parse( localStorage.getItem('company') );

    // TODO: Put logic to call only if company attendance updated at is not the same with current date.
    // if ( this.company.attd_updtd_at ) {
    //   console.log('oksssssssss');
    // } else {
    //   console.log('noooooooooo');
    // }
    this.PageService.pageName.next('Dashboard');
    this.updateAttandances();
    this.count_employees();
    this.count_attendance_today();
    this.get_payroll();
    this.getAttendaceCurrentMonth();
    this.getBirthdays();
    this.newEmpolyees();
    this.getLeaves();
    this.getLatestPayroll();
  }


  updateAttandances() {
    this.CompaniesService.updateAttendances( { company_id: this.company._id  }).subscribe( data => {
      // console.log('request sucessful for update Attandances', data);
    });
  }

  count_employees() {
    
    this.EmployeesService.count_employees( 'total_users' ).subscribe( data => {

			if ( !data.error ) {
        this.total_count_emp = data.results;
			}

			this.empLoader = false;
		});
  }

  count_attendance_today() {

    this.attendances = [];
    this.AttendancesService.get( this.params_attendance ).subscribe( data => {

      if ( !data.error ) {
        this.attendances = data.results;
      } 

      this.attendances.forEach(emp => {
        this.today.present += emp.status === 'Present' ? 1 : 0;
        this.today.absent += emp.status === 'Absent' ? 1 : 0;
        this.today.late += emp.is_late ? 1 : 0;
      });

      this.dtrLoader = false;
    });
  }

  getAttendaceCurrentMonth(){
    this.params_attendance.filter_date = 'current_month'
    this.attendances = [];
    this.AttendancesService.get( this.params_attendance ).subscribe( data => {

      if ( !data.error ) {
        this.attendances = data.results;
      } 

      this.attendances.forEach(emp => {
        this.month.present += emp.status === 'Present' ? 1 : 0;
        this.month.absent += emp.status === 'Absent' ? 1 : 0;
        this.month.late += emp.is_late ? 1 : 0;
        this.month.undertime += emp.is_undertime ? 1 : 0;
      });

      this.percent.present = Math.round( ( this.month.present / this.attendances.length ) * 100 ) || 0;
      this.percent.late = Math.round( ( this.month.late / this.attendances.length ) * 100 ) || 0;
      this.percent.absent = Math.round( ( this.month.absent / this.attendances.length ) * 100 ) || 0;
      this.percent.undertime = Math.round( ( this.month.undertime / this.attendances.length ) * 100 ) || 0;

      this.dtrLoader = false;
    });

    
  }

  get_payroll() {
    this.payrolls = [];
		this.PayrollsService.get( this.params_payroll ).subscribe( data => {

			if ( !data.error ) {
        this.payrolls = data.results;
      }

      this.payrolls.forEach(payroll => {
        this.total_payroll += parseInt(payroll.overall_total_netpay);
      });
      
			this.payrollLoader = false;
		});
  }

  getBirthdays() {
    this.EmployeesService.count_employees( 'birthdays' ).subscribe( data => {

			if ( !data.error ) {
        this.birthdays = data.results;
			}

			this.birthdaysLoader = false;
		});    
  }


  newEmpolyees() {

    let conditions  = {
      s: '',
      limit: 7,
      page: 1,
      sort: 'createdAt:-1'
    };

    this.EmployeesService.get( conditions ).subscribe( data => {

      if ( !data.error ) {
				this.newEmployees = data.results;
				this.newEmployees.total_count = data.results_count;
			}

			this.newEmpLoader = false;
    });
  }

  getLeaves() {
    this.leaveTypes['SL'] = 'Sick Leave';
		this.leaveTypes['VL'] = 'Vacation Leave';
		this.leaveTypes['EL'] = 'Emergency Leave';
		this.leaveTypes['PL'] = 'Paternity Leave';
		this.leaveTypes['ML'] = 'Maternity Leave';

		this.leaves = [];
		this.LeavesService.get( this.params_leaves ).subscribe( data => {

			if ( !data.error ) {
				this.leaves = data.results;
			}
      
      this.leavesLoader = false;
		});
  }

  getLatestPayroll() {

		this.PayrollsService.get( this.params_latest_payroll ).subscribe( data => {

			if ( !data.error ) {
        this.latest_payroll = data.results;
      }
      
			this.latestPayrollLoader = false;
		});
  }
}
