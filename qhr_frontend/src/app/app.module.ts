import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import {SuiModule} from 'ng2-semantic-ui';
import { NgxSpinnerModule } from 'ngx-spinner';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { DatePipe } from '@angular/common';

import { ClipboardModule } from 'ngx-clipboard';
import { ChecklistModule } from 'angular-checklist';


// THIRD PARTY MODULES
    // import { MomentModule } from 'angular2-moment';
    import { MomentModule } from 'ngx-moment';
    // import { SuiModule } from 'ng2-semantic-ui';

// PIPES
    import { DurationPipe } from './pipes/duration.pipe';
    import { ImagesPipe } from './pipes/images.pipe';
    
// SERVICES
    // == General
    import { AuthService } from './auth.service';
    import { SignupService } from './services/signup.service';
    import { PageService } from './page.service';
    import { MainCompService } from './services/comp/main-comp.service';
    import { LocalstorageService } from './localstorage.service';
    import { TextHelperService } from './texthelper.service';
    import { PaginatorService } from './paginator.service';
    import { SharedDataEmployeeService } from './services/shared-data-employee.service';
    import { UserSettingsService } from './services/usersettings.service';

    // == Models
    import { EmployeesService } from './services/employees.service';
    import { DepartmentsService } from './services/departments.service';
    import { BranchesService } from './services/branches.service';
    import { AttendancesService } from './services/attendances.service';
    import { LeavesService } from './services/leaves.service';
    import { CompaniesService } from './services/companies.service';
    import { LogsService } from './services/logs.service';
    import { PayrollsService } from './services/payrolls.service';
    import { PayrollCalculationService } from './services/payrollcalculation.service';
    import { PayrollDetailsService } from './services/payroll-details.service';
    import { DeductionsService } from './services/deductions.service';
    import { EarningsService } from './services/earnings.service';
    import { NotificationsService } from './services/notifications.service';
    import { PermissionsService } from './services/permissions.service';
    import { UserPermissionsService } from './services/userpermissions.service';
    import { DocumentsService } from './services/documents.service';
    import { FreedomBoardService } from './services/freedomboard.service';
    import { RequestDemoService } from './services/requestdemo.service';
    import { QuotesService } from './services/quotes.service';
    import { HolidaysService } from './services/holidays.service';
    import { ForgotPasswordService } from './services/forgotpassword.service';
    import { PaymentTransactionsService } from './services/paymenttransactions.service';
    import { PayslipsService } from './services/payslips.service';
    import { LoansService } from './services/loans.service';
    import { TeamsService } from './services/teams.service';
    import { TeamMembersService } from './services/teammembers.service';
    import { SubscriptionsService } from './services/subscriptions.service';
    import { LoginRestrictionsService } from './services/loginrestrictions.service';
    import { LoanPaymentsService } from './services/loan-payments.service';
    import { PayrollApproversService } from './services/payrollapprovers.service';
    import { PayrollGroupsService } from './services/payrollgroups.service';
    import { InvoicesService } from './services/invoices.service';
    import { PayrollContributionsService } from './services/payrollcontributions.service';
    import { LeaveCreditsService } from './services/leavecredits.service';
    import { OvertimeService } from './services/overtime.service';
    import { OvertimeApproversService } from './services/overtimeapprovers.service';
    import { DesignationsService } from './services/designation.service';
    import { UserDesignationsService } from './services/userdesignation.service';
    import { WorkScheduleService } from './services/workschedule.service';
    import { UserLoginRestrictionsService } from './services/userloginrestrictions.service';

// COMPONENTS

    import { SignupComponent } from './signup/signup.component';
    import { VerifyCompanyComponent } from './verify-company/verify-company.component';

    // == Auth
    import { LoginComponent } from './login/login.component';
    import { LogoutComponent } from './logout/logout.component';
    // == General
    import { AppComponent } from './app.component';
    import { MainComponent } from './layouts/main/main.component';
    import { HeaderComponent } from './shared/header/header.component';
    import { FooterComponent } from './shared/footer/footer.component';
    import { SidebarComponent } from './shared/sidebar/sidebar.component';
    // Dashboard
    import { DashboardComponent } from './dashboard/dashboard.component';
    // == Employees
    import { EmployeesIndexComponent } from './employees/employees-index/employees-index.component';
    import { EmployeesShowComponent } from './employees/employees-show/employees-show.component';
    import { EmployeesCreateComponent } from './employees/employees-create/employees-create.component';
    import { EmployeesUpdateComponent } from './employees/employees-update/employees-update.component';

    import { EmployeesUpdatePersonalComponent } from './employees/employees-update-personal/employees-update-personal.component';
    import { EmployeesUpdateWorkComponent } from './employees/employees-update-work/employees-update-work.component';
    import { EmployeesUpdateBenefitsComponent } from './employees/employees-update-benefits/employees-update-benefits.component';
    import { EmployeesUpdateSocialComponent } from './employees/employees-update-social/employees-update-social.component';
    import { UpdateMainMenuComponent } from './shared/employees/update-main-menu/update-main-menu.component';
    import { EmployeesUpdateDailyWorkSchedulesComponent } from './employees/employees-update-daily-work-schedules/employees-update-daily-work-schedules.component';
    import { EmployeesUpdateLeavesComponent } from './employees/employees-update-leaves/employees-update-leaves.component';
    import { EmployeesUpdateSalaryComponent } from './employees/employees-update-salary/employees-update-salary.component';
    import { EmployeesUpdatePasswordComponent } from './employees/employees-update-password/employees-update-password.component';
    import { EmployeesUpdatePermissionsComponent } from './employees/employees-update-permissions/employees-update-permissions.component';

    // == Departments
    import { DepartmentsIndexComponent } from './departments/departments-index/departments-index.component';
    import { DepartmentsCreateComponent } from './departments/departments-create/departments-create.component';
    import { DepartmentsUpdateComponent } from './departments/departments-update/departments-update.component';
    import { DepartmentsShowComponent } from './departments/departments-show/departments-show.component';
    // == Branches
    import { BranchesShowComponent } from './branches/branches-show/branches-show.component';
    import { BranchesCreateComponent } from './branches/branches-create/branches-create.component';
    import { BranchesUpdateComponent } from './branches/branches-update/branches-update.component';
    import { BranchesIndexComponent } from './branches/branches-index/branches-index.component';
    // == Attendances
    import { AttendancesIndexComponent } from './attendances/attendances-index/attendances-index.component';
    import { AttendancesUpdateComponent } from './attendances/attendances-update/attendances-update.component';
    import { AttendancesShowComponent } from './attendances/attendances-show/attendances-show.component';
    import { AttendanceCreateComponent } from './attendances/attendance-create/attendance-create.component';
    // == Leaves
    import { LeavesIndexComponent } from './leaves/leaves-index/leaves-index.component';
    import { LeavesShowComponent } from './leaves/leaves-show/leaves-show.component';
    import { LeavesCreateComponent } from './leaves/leaves-create/leaves-create.component';
    import { LeavesUpdateComponent } from './leaves/leaves-update/leaves-update.component';
    //  == Users
    import { UsersIndexComponent } from './users/users-index/users-index.component';
    import { UsersShowComponent } from './users/users-show/users-show.component';
    import { UsersCreateComponent } from './users/users-create/users-create.component';
    import { UsersUdpateComponent } from './users/users-udpate/users-udpate.component';
    // == Settings
    import { SettingsShowComponent } from './settings/settings-show/settings-show.component';
    import { SettingsUpdateComponent } from './settings/settings-update/settings-update.component';
    // == Companies Profile
    import { CompaniesUpdateComponent } from './companies/companies-update/companies-update.component';
    import { CompaniesShowComponent } from './companies/companies-show/companies-show.component';
    import { CompaniesCreateComponent } from './companies/companies-create/companies-create.component';
    // ==Subscriptions
    import { SubscriptionsComponent } from './subscriptions/subscriptions/subscriptions.component';
    import { SubscriptionsCreateComponent } from './subscriptions/subscriptions-create/subscriptions-create.component';
    import { SubscriptionsShowComponent } from './subscriptions/subscriptions-show/subscriptions-show.component';

    // == Logs
    import { LogsIndexComponent } from './logs/logs-index/logs-index.component';

    // == Notifications
    import { NotifsIndexComponent } from './notifications/notifs-index/notifs-index.component';

    // == Payrolls
    import { PayrollsIndexComponent } from './payrolls/payrolls-index/payrolls-index.component';
    import { PayrollsCreateComponent } from './payrolls/payrolls-create/payrolls-create.component';
    import { PayrollsUpdateComponent } from './payrolls/payrolls-update/payrolls-update.component';
    
    import { PayrollsUpdateMainMenuComponent } from './shared/payrolls/payrolls-update-main-menu/payrolls-update-main-menu.component';
    import { PayrollsUpdateInfoComponent } from './payrolls/payrolls-update-info/payrolls-update-info.component';
    import { PayrollsUpdateDetailsComponent } from './payrolls/payrolls-update-details/payrolls-update-details.component';
    import { PayrollsShowComponent } from './payrolls/payrolls-show/payrolls-show.component';
    
    // Employee Deductions
    import { DeductionsCreateComponent } from './employees/deductions/deductions-create/deductions-create.component';
    import { DeductionsUpdateComponent } from './employees/deductions/deductions-update/deductions-update.component'; 
    import { DeductionsIndexComponent } from './employees/deductions/deductions-index/deductions-index.component'; 

    // Employee Earnings
    import { EarningsCreateComponent } from './employees/earnings/earnings-create/earnings-create.component';
    import { EarningsIndexComponent } from './employees/earnings/earnings-index/earnings-index.component';
    import { EarningsUpdateComponent } from './employees/earnings/earnings-update/earnings-update.component';

    // == Modal
    import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';

    //Front
    import { FrontFooterComponent } from './shared/front-footer/front-footer.component';
    import { FrontMainBannerComponent } from './shared/front-main-banner/front-main-banner.component';
    import { FrontMainMenuComponent } from './shared/front-main-menu/front-main-menu.component';
    import { FrontWhatTheySayComponent } from './shared/front-what-they-say/front-what-they-say.component';
    import { AboutUsComponent } from './front/about-us/about-us.component';
    import { ContactUsComponent } from './front/contact-us/contact-us.component';
    import { PrivacyPolicyComponent } from './front/privacy-policy/privacy-policy.component';
    import { TermsAndConditionsComponent } from './front/terms-and-conditions/terms-and-conditions.component';
    import { QuickhrComponent } from './front/quickhr/quickhr.component';

    //Angular Google Map
    import { AgmCoreModule} from '@agm/core';

    //Documents
    import { DocumentsCreateComponent } from './employees/documents/documents-create/documents-create.component';
    import { DocumentsIndexComponent } from './employees/documents/documents-index/documents-index.component';

    //Freedom Board
    import { BoardIndexComponent } from './freedomboard/board-index/board-index.component';
    import { BoardCreateComponent } from './freedomboard/board-create/board-create.component';
    import { BoardShowComponent } from './freedomboard/board-show/board-show.component';

    // User Settings
    import { UserSettingsShowComponent } from './user-settings/user-settings-show/user-settings-show.component';
    import { UserSettingsUpdateComponent } from './user-settings/user-settings-update/user-settings-update.component';

    // Request Demo
    import { RequestDemoComponent } from './request-demo/request-demo.component';

    // Quotes
    import { QuoteCreateComponent } from './quotes/quote-create/quote-create.component';
    import { QuoteIndexComponent } from './quotes/quote-index/quote-index.component';
    import { QuoteUpdateComponent } from './quotes/quote-update/quote-update.component';

    // Holidays
    import { HolidayCreateComponent } from './holidays/holiday-create/holiday-create.component';
    import { HolidayIndexComponent } from './holidays/holiday-index/holiday-index.component';

    // Forgot Password
    import { ForgotComponent } from './forgot-password/forgot/forgot.component';
    import { ResetComponent } from './forgot-password/reset/reset.component';

    // Payment History
    import { PaymentTransactionsIndexComponent } from './payment-transactions/payment-transactions-index/payment-transactions-index.component';
    import { PaymentTransactionsShowComponent } from './payment-transactions/payment-transactions-show/payment-transactions-show.component';
    import { PaymentTransactionsInvoiceComponent } from './payment-transactions/payment-transactions-invoice/payment-transactions-invoice.component';

    // Payslips
    import { PayslipsIndexComponent } from './payslips/payslips-index/payslips-index.component';
    import { PayslipsShowComponent } from './payslips/payslips-show/payslips-show.component';

    // Loans
    import { LoansCreateComponent } from './loans/loans-create/loans-create.component';
    import { LoansIndexComponent } from './loans/loans-index/loans-index.component';
    import { LoansShowComponent } from './loans/loans-show/loans-show.component';
    import { LoansUpdateComponent } from './loans/loans-update/loans-update.component';
    import { LoansPaymentComponent } from './loans/loans-payment/loans-payment.component';
    // Teams
    import { TeamsIndexComponent } from './teams/teams-index/teams-index.component';
    import { TeamsCreateComponent } from './teams/teams-create/teams-create.component';
    import { TeamsShowComponent } from './teams/teams-show/teams-show.component';
    import { TeamsUpdateComponent } from './teams/teams-update/teams-update.component';

    // Team Members
    import { TeamMembersIndexComponent } from './team-members/team-members-index/team-members-index.component';
    import { TeamMembersCreateComponent } from './team-members/team-members-create/team-members-create.component';

    // Login Restrictions
    import { LoginRestrictionsIndexComponent } from './login-restrictions/login-restrictions-index/login-restrictions-index.component';
    import { LoginRestrictionsCreateComponent } from './login-restrictions/login-restrictions-create/login-restrictions-create.component';
    import { LoginRestrictionsUpdateComponent } from './login-restrictions/login-restrictions-update/login-restrictions-update.component';
    import { LoginRestrictionsShowComponent } from './login-restrictions/login-restrictions-show/login-restrictions-show.component';

    // Payroll Approvers
    import { PayrollApproversIndexComponent } from './payroll-approvers/payroll-approvers-index/payroll-approvers-index.component';
    import { PayrollApproversCreateComponent } from './payroll-approvers/payroll-approvers-create/payroll-approvers-create.component';
    
    // Payroll Groups
    import { PayrollGroupsCreateComponent } from './payroll-groups/payroll-groups-create/payroll-groups-create.component';
    import { PayrollGroupsIndexComponent } from './payroll-groups/payroll-groups-index/payroll-groups-index.component';
    import { PayrollGroupsShowComponent } from './payroll-groups/payroll-groups-show/payroll-groups-show.component';
    import { PayrollGroupsAddComponent } from './payroll-groups/payroll-groups-add/payroll-groups-add.component';
    import { PayrollGroupsUpdateComponent } from './payroll-groups/payroll-groups-update/payroll-groups-update.component';

    // Payroll Contributions
    import { ContributionsComponent } from './payrolls/contributions/contributions.component';
    import { ContributionsEditComponent } from './payrolls/contributions-edit/contributions-edit.component';

    // Leave Credits
    import { CreditsCreateComponent } from './leave-credits/credits-create/credits-create.component';
    import { CreditsIndexComponent } from './leave-credits/credits-index/credits-index.component';
    import { CreditsUpdateComponent } from './leave-credits/credits-update/credits-update.component';

    // Overtime
    import { OvertimeCreateComponent } from './overtimes/overtime-create/overtime-create.component';
    import { OvertimeIndexComponent } from './overtimes/overtime-index/overtime-index.component';
    import { OvertimeUpdateComponent } from './overtimes/overtime-update/overtime-update.component';
    import { OvertimeShowComponent } from './overtimes/overtime-show/overtime-show.component';

    // Overtime Approvers
    import { OvertimeApproversCreateComponent } from './overtime-approvers/overtime-approvers-create/overtime-approvers-create.component';
    import { OvertimeApproversIndexComponent } from './overtime-approvers/overtime-approvers-index/overtime-approvers-index.component';
    import { OvertimeApproversShowComponent } from './overtime-approvers/overtime-approvers-show/overtime-approvers-show.component';
    import { OvertimeApproversCreateMembersComponent } from './overtime-approvers/overtime-approvers-create-members/overtime-approvers-create-members.component';

    // Designations
    import { DesignationsIndexComponent } from './designations/designations-index/designations-index.component';
    import { DesignationsCreateComponent } from './designations/designations-create/designations-create.component';
    import { DesignationsUpdateComponent } from './designations/designations-update/designations-update.component';
    import { DesignationsShowComponent } from './designations/designations-show/designations-show.component';

    // User Designations
    import { UserDesignationsCreateComponent } from './user-designations/user-designations-create/user-designations-create.component';
    import { UserDesignationsIndexComponent } from './user-designations/user-designations-index/user-designations-index.component';
    import { UserDesignationsShowComponent } from './user-designations/user-designations-show/user-designations-show.component';

    // Work Schedules
    import { WorkSchedCreateComponent } from './work-schedules/work-sched-create/work-sched-create.component';
    import { WorkSchedIndexComponent } from './work-schedules/work-sched-index/work-sched-index.component';
    import { WorkSchedUpdateComponent } from './work-schedules/work-sched-update/work-sched-update.component';

    // User Login Restrictions
    import { UserLoginRestrictionsCreateComponent } from './user-login-restrictions/user-login-restrictions-create/user-login-restrictions-create.component';
    import { UserLoginRestrictionsIndexComponent } from './user-login-restrictions/user-login-restrictions-index/user-login-restrictions-index.component';


export const ROUTES: Routes = [
    // { path: 'signup', component: SignupComponent },
    { path: 'companies/verify/:hash', component: VerifyCompanyComponent },
    { path: 'login', component: LoginComponent },
    { path: 'logout', component: LogoutComponent },
    { path: 'privacy-policy', component: PrivacyPolicyComponent },
    { path: 'terms-and-conditions', component: TermsAndConditionsComponent },
    { path: 'contact-us',component: ContactUsComponent},
    { path: 'about-us', component: AboutUsComponent },
    { path: 'quickhr', component: QuickhrComponent },
    { path: 'request-demo', component: RequestDemoComponent },
    { path: 'forgot-password', component: ForgotComponent },
    { path: 'forgot-password/reset/:hash', component: ResetComponent },
    { path: 'signup', component: CompaniesCreateComponent },
    // { path: '', component: DashboardComponent },
    { path: '', component: MainComponent,
        children: [
            { path: '', component: DashboardComponent },
            { path: 'employees/create', component: EmployeesCreateComponent },
            { path: 'employees/:id', component: EmployeesShowComponent},
            { path: 'employees/:id/edit', component: EmployeesUpdateComponent,
                children: [ 
                    { path: 'personal', component: EmployeesUpdatePersonalComponent },
                    { path: 'work', component: EmployeesUpdateWorkComponent },
                    { path: 'leaves', component: EmployeesUpdateLeavesComponent },
                    { path: 'benefits', component: EmployeesUpdateBenefitsComponent },
                    { path: 'social', component: EmployeesUpdateSocialComponent },
                    { path: 'daily-work-schedules', component: EmployeesUpdateDailyWorkSchedulesComponent },
                    { path: 'salary', component: EmployeesUpdateSalaryComponent },
                    { path: 'password', component: EmployeesUpdatePasswordComponent },
                    { path: 'deductions', component: DeductionsIndexComponent },
                    { path: 'deductions/create', component: DeductionsCreateComponent },
                    { path: 'deductions/:deduction_id/edit', component: DeductionsUpdateComponent },
                    { path: 'earnings', component: EarningsIndexComponent },
                    { path: 'earnings/create', component: EarningsCreateComponent },
                    { path: 'earnings/:earning_id/edit', component: EarningsUpdateComponent },
                    { path: 'permissions', component: EmployeesUpdatePermissionsComponent },
                    { path: 'documents', component: DocumentsIndexComponent},
                    { path: 'documents/create', component: DocumentsCreateComponent},
                ]
            },
            { path: 'employees', component: EmployeesIndexComponent },

            { path: 'departments/create', component: DepartmentsCreateComponent },
            { path: 'departments/:id', component: DepartmentsShowComponent},
            { path: 'departments/:id/edit', component: DepartmentsUpdateComponent },
            { path: 'departments', component: DepartmentsIndexComponent },

            { path: 'branches/create', component: BranchesCreateComponent },
            { path: 'branches/:id', component: BranchesShowComponent},
            { path: 'branches/:id/edit', component: BranchesUpdateComponent },
            { path: 'branches', component: BranchesIndexComponent },

            { path: 'attendances', component: AttendancesIndexComponent },
            { path: 'attendances/create', component: AttendanceCreateComponent },
            { path: 'attendances/:id/show', component: AttendancesShowComponent },
            { path: 'attendances/:id/edit', component: AttendancesUpdateComponent },

            { path: 'leaves/create', component: LeavesCreateComponent },
            { path: 'leaves/:id', component: LeavesShowComponent},
            { path: 'leaves/:id/edit', component: LeavesUpdateComponent },
            { path: 'leaves', component: LeavesIndexComponent },

            { path: 'settings', component: SettingsShowComponent },
            { path: 'settings/:id/edit', component: SettingsUpdateComponent },

            // { path: 'companies/create', component: CompaniesCreateComponent },
            { path: 'companies/:id', component: CompaniesShowComponent },
            { path: 'companies/:id/edit', component: CompaniesUpdateComponent },
            
            { path: 'logs', component: LogsIndexComponent },

            { path: 'notifications', component: NotifsIndexComponent },

            { path: 'payrolls', component: PayrollsIndexComponent },
            { path: 'payrolls/create', component: PayrollsCreateComponent },
            { path: 'payrolls/:id/show', component: PayrollsShowComponent},
            { path: 'payrolls/:id/edit', component: PayrollsUpdateComponent,
                children: [ 
                    { path: 'info', component: PayrollsUpdateInfoComponent },
                    { path: 'details', component: PayrollsUpdateDetailsComponent },
                ]
            },

            { path: 'freedom-board', component: BoardIndexComponent },
            { path: 'freedom-board/create', component: BoardCreateComponent },
            { path: 'freedom-board/:id/show', component: BoardShowComponent },

            { path: 'user-settings', component: UserSettingsShowComponent },
            { path: 'user-settings/:id/edit', component: UserSettingsUpdateComponent },

            { path: 'quotes', component: QuoteIndexComponent },
            { path: 'quotes/create', component: QuoteCreateComponent },
            { path: 'quotes/:id/edit', component: QuoteUpdateComponent },

            { path: 'holidays', component: HolidayIndexComponent },
            { path: 'holidays/create', component: HolidayCreateComponent },

            { path: 'subscriptions', component: SubscriptionsComponent },
            { path: 'subscriptions/:planType', component: SubscriptionsCreateComponent },
            { path: 'subscriptions/:planType/show', component: SubscriptionsShowComponent },

            { path: 'payment-transactions', component: PaymentTransactionsIndexComponent },
            { path: 'payment-transactions/:id/show', component: PaymentTransactionsShowComponent },
            { path: 'invoice/:id', component: PaymentTransactionsInvoiceComponent },

            { path: 'payslips', component: PayslipsIndexComponent },
            { path: 'payslips/:id/show', component: PayslipsShowComponent },
            { path: 'loans', component: LoansIndexComponent },
            { path: 'loans/create', component: LoansCreateComponent },
            { path: 'loans/:id', component: LoansShowComponent },
            { path: 'loans/:id/edit', component: LoansUpdateComponent },
            { path: 'loans/:id/payment', component: LoansPaymentComponent },
            
            { path: 'teams', component: TeamsIndexComponent },
            { path: 'teams/create', component: TeamsCreateComponent },
            { path: 'teams/:id/show', component: TeamsShowComponent },
            { path: 'teams/:id/edit', component: TeamsUpdateComponent },
            { path: 'teams/:id/edit/members', component: TeamMembersIndexComponent },
            { path: 'teams/:id/edit/members/create', component: TeamMembersCreateComponent },

            { path: 'login-restrictions', component: LoginRestrictionsIndexComponent },
            { path: 'login-restrictions/create', component: LoginRestrictionsCreateComponent },
            { path: 'login-restrictions/:id/show', component: LoginRestrictionsShowComponent },
            { path: 'login-restrictions/:id/edit', component: LoginRestrictionsUpdateComponent },

            { path: 'payroll-approvers', component: PayrollApproversIndexComponent },
            { path: 'payroll-approvers/create', component: PayrollApproversCreateComponent },

            { path: 'payroll-groups/create', component: PayrollGroupsCreateComponent },
            { path: 'payroll-groups/:id/show', component: PayrollGroupsShowComponent},
            { path: 'payroll-groups/:id/add', component: PayrollGroupsAddComponent },
            { path: 'payroll-groups', component: PayrollGroupsIndexComponent },
            { path: 'payroll-groups/:id/edit', component: PayrollGroupsUpdateComponent },

            { path: 'contributions', component: ContributionsComponent },
            { path: 'contributions/:id/edit', component: ContributionsEditComponent },

            { path: 'leave-credits', component: CreditsIndexComponent },
            { path: 'leave-credits/create', component: CreditsCreateComponent },
            { path: 'leave-credits/:id/edit', component: CreditsUpdateComponent },

            { path: 'overtimes', component: OvertimeIndexComponent },
            { path: 'overtimes/create', component: OvertimeCreateComponent },
            { path: 'overtimes/:id/show', component: OvertimeShowComponent },
            { path: 'overtimes/:id/edit', component: OvertimeUpdateComponent },

            { path: 'supervisors/create', component: OvertimeApproversCreateComponent },
            { path: 'supervisors', component: OvertimeApproversIndexComponent },
            { path: 'supervisors/:id/show', component: OvertimeApproversShowComponent },
            { path: 'supervisors/:id/create-member', component: OvertimeApproversCreateMembersComponent },

            { path: 'designations', component: DesignationsIndexComponent },
            { path: 'designations/create', component: DesignationsCreateComponent },
            { path: 'designations/:id/show', component: DesignationsShowComponent },
            { path: 'designations/:id/edit', component: DesignationsUpdateComponent },

            { path: 'user-designations', component: UserDesignationsIndexComponent },
            { path: 'user-designations/create', component: UserDesignationsCreateComponent },
            { path: 'user-designations/:id/show', component: UserDesignationsShowComponent },

            { path: 'work-schedules', component: WorkSchedIndexComponent },
            { path: 'work-schedules/create', component: WorkSchedCreateComponent },
            { path: 'work-schedules/:id/edit', component: WorkSchedUpdateComponent },

            { path: 'employees-login-restrictions', component: UserLoginRestrictionsIndexComponent },
            { path: 'employees-login-restrictions/create', component: UserLoginRestrictionsCreateComponent },
            // { path: 'employees-login-restrictions/:id/edit', component: LoginRestrictionsUpdateComponent },
        ]
    },
];


@NgModule({
    declarations: [
        AppComponent,
        SignupComponent,
        LoginComponent,
        LogoutComponent,
        MainComponent,

        HeaderComponent,
        FooterComponent,
        SidebarComponent,

        EmployeesIndexComponent,
        EmployeesShowComponent,
        EmployeesCreateComponent,
        EmployeesUpdateComponent,
        EmployeesUpdateWorkComponent,
        EmployeesUpdateBenefitsComponent,
        EmployeesUpdateSocialComponent,
        EmployeesUpdatePasswordComponent,
        UpdateMainMenuComponent,
        EmployeesUpdatePersonalComponent,
        EmployeesUpdateDailyWorkSchedulesComponent,
        DepartmentsIndexComponent,
        DepartmentsCreateComponent,
        DepartmentsUpdateComponent,
        DepartmentsShowComponent,
        BranchesShowComponent,
        BranchesCreateComponent,
        BranchesUpdateComponent,
        BranchesIndexComponent,
        AttendancesIndexComponent,
        AttendancesUpdateComponent,
        AttendancesShowComponent,
        AttendanceCreateComponent,

        DurationPipe,

        LeavesIndexComponent,
        LeavesShowComponent,
        LeavesCreateComponent,
        LeavesUpdateComponent,
        UsersIndexComponent,
        UsersShowComponent,
        UsersCreateComponent,
        UsersUdpateComponent,
        SettingsShowComponent,
        SettingsUpdateComponent,
        CompaniesUpdateComponent,
        CompaniesShowComponent,
        CompaniesCreateComponent,
        VerifyCompanyComponent,
        EmployeesUpdateLeavesComponent,
        EmployeesUpdateSalaryComponent,
        EmployeesUpdatePermissionsComponent,
        LogsIndexComponent,
        PayrollsIndexComponent,
        PayrollsCreateComponent,
        PayrollsUpdateComponent,
        PayrollsUpdateMainMenuComponent,
        PayrollsUpdateInfoComponent,
        PayrollsUpdateDetailsComponent,
        ConfirmModalComponent,
        PayrollsShowComponent,
        DeductionsCreateComponent,
        DeductionsUpdateComponent,
        DeductionsIndexComponent,
        EarningsCreateComponent,
        EarningsIndexComponent,
        EarningsUpdateComponent,
        DashboardComponent,
        NotifsIndexComponent,
        FrontFooterComponent,
        FrontMainBannerComponent,
        FrontMainMenuComponent,
        FrontWhatTheySayComponent,
        AboutUsComponent,
        ContactUsComponent,
        PrivacyPolicyComponent,
        TermsAndConditionsComponent,
        QuickhrComponent,
        
        ImagesPipe,
        
        DocumentsCreateComponent,
        DocumentsIndexComponent,
        BoardIndexComponent,
        BoardCreateComponent,
        BoardShowComponent,
        UserSettingsShowComponent,
        UserSettingsUpdateComponent,
        RequestDemoComponent,
        QuoteCreateComponent,
        QuoteIndexComponent,
        QuoteUpdateComponent,
        HolidayCreateComponent,
        HolidayIndexComponent,
        ForgotComponent,
        ResetComponent,
        SubscriptionsComponent,
        PaymentTransactionsIndexComponent,
        PaymentTransactionsShowComponent,
        PayslipsIndexComponent,
        PayslipsShowComponent,
        SubscriptionsShowComponent,
        LoansCreateComponent,
        LoansIndexComponent,
        LoansShowComponent,
        LoansUpdateComponent,
        TeamsIndexComponent,
        TeamsCreateComponent,
        TeamsShowComponent,
        TeamsUpdateComponent,
        LoginRestrictionsIndexComponent,
        LoginRestrictionsCreateComponent,
        LoginRestrictionsUpdateComponent,
        LoginRestrictionsShowComponent,
        SubscriptionsCreateComponent,
        TeamMembersIndexComponent,
        TeamMembersCreateComponent,
        LoansPaymentComponent,
        PayrollApproversIndexComponent,
        PayrollApproversCreateComponent,
        PayrollGroupsCreateComponent,
        PayrollGroupsIndexComponent,
        PayrollGroupsShowComponent,
        PayrollGroupsAddComponent,
        PayrollGroupsUpdateComponent,
        PaymentTransactionsInvoiceComponent,
        ContributionsComponent,
        ContributionsEditComponent,
        CreditsCreateComponent,
        CreditsIndexComponent,
        CreditsUpdateComponent,
        OvertimeCreateComponent,
        OvertimeIndexComponent,
        OvertimeUpdateComponent,
        OvertimeApproversCreateComponent,
        OvertimeApproversIndexComponent,
        OvertimeShowComponent,
        OvertimeApproversShowComponent,
        OvertimeApproversCreateMembersComponent,
        DesignationsIndexComponent,
        DesignationsCreateComponent,
        DesignationsUpdateComponent,
        UserDesignationsCreateComponent,
        UserDesignationsIndexComponent,
        DesignationsShowComponent,
        UserDesignationsShowComponent,
        WorkSchedCreateComponent,
        WorkSchedIndexComponent,
        WorkSchedUpdateComponent,
        UserLoginRestrictionsCreateComponent,
        UserLoginRestrictionsIndexComponent,
    ],
    imports: [
        BrowserModule,
        HttpModule,
        HttpClientModule,
        RouterModule.forRoot(ROUTES),
        FormsModule,
        BrowserAnimationsModule,
        MomentModule,
        ClipboardModule,
        ChecklistModule,
        
        SuiModule,
        NgxSpinnerModule,
        BrowserAnimationsModule,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory
        }),
        AgmCoreModule.forRoot({
            apiKey:'AIzaSyCNzkQNjzOZjckSgoOwwraoTem1qppKdsc'
          }),
    ],
    providers: [
        AuthService,
        SignupService,
        PageService,
        MainCompService,
        SharedDataEmployeeService,
        TextHelperService,
        LocalstorageService,
        PaginatorService,
        DatePipe,

        EmployeesService,
        BranchesService,
        DepartmentsService,
        AttendancesService,
        LeavesService,
        CompaniesService,
        LogsService,
        PayrollsService,
        PayrollCalculationService,
        PayrollDetailsService,
        DeductionsService,
        EarningsService,
        NotificationsService,
        PermissionsService,
        UserPermissionsService,
        DocumentsService,
        FreedomBoardService,
        UserSettingsService,
        RequestDemoService,
        QuotesService,
        HolidaysService,
        ForgotPasswordService,
        PaymentTransactionsService,
        PayslipsService,
        LoansService,
        TeamsService,
        TeamMembersService,
        SubscriptionsService,
        LoanPaymentsService,
        PayrollContributionsService,
        LeaveCreditsService,
        OvertimeService,
        OvertimeApproversService,
        PayrollApproversService,
        LoginRestrictionsService,
        PayrollGroupsService,
        InvoicesService,
        DesignationsService,
        UserDesignationsService,
        WorkScheduleService,
        UserLoginRestrictionsService
    ],
    entryComponents: [ConfirmModalComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
