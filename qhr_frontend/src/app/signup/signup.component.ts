import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TextHelperService } from '../texthelper.service';
import { SignupService } from '../services/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  user: any = {
    first_name: '',
    last_name: '',
    user_email: '',
    password: '',
    name: '',
    address: '',
    email: '',
    gender: 'male'
  }

  successMsg = '';
  errorMsg = '';
  isError: boolean;
  noticeMsg = '';
  
  signupProcessing = false;
  slug;
  
  constructor(
    private router: Router,
    private TextHelperService: TextHelperService,
    private SignupService: SignupService,
  ) { }

  ngOnInit() {

    // if ( this.storage.get('token') ) {
		// 	this.router.navigate(['/']);
    // }
    
    // console.log('called init on signup');
    // this.slug = localStorage.getItem('company_slug');
  }

  onSubmit() {

    this.signupProcessing = true;

    const fields = {
      first_name: this.user.first_name,
      last_name: this.user.last_name,
      gender: this.user.gender,
      user_email: this.user.user_email,
      password: this.user.password,
      name: this.user.name,
      address: this.user.address,
      email: this.user.email
    };
    

		this.successMsg = '';
		this.errorMsg = '';
    
    console.log(fields);

		this.SignupService.store( fields ).subscribe( data => {
      console.log('request sucessful', data)
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
      
      this.signupProcessing = false;
    });
	}

}
