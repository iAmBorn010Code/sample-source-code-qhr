import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../auth.service';
import { LocalstorageService } from '../localstorage.service';
import { TextHelperService } from '../texthelper.service';
import { CompaniesService } from '../services/companies.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	user: any = {
		username: '',
		password: '',
	}

	siteUrl: string = environment.siteUrl;

	company: any = {};
	
	isError: boolean;
	successMsg = '';
	errorMsg = '';
	noticeMsg = '';
	viewportHeight;
	hasToken: boolean;
	loading: boolean;
	isNew: boolean;

	mainLoader = true;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private storage: LocalstorageService,
		private AuthService: AuthService,
		private TextHelperService: TextHelperService,
		private CompaniesService: CompaniesService
	) { }

	ngOnInit() {
		
		// If has token, redirect to dashboard.
		// if ( this.storage.get('token') ) {
		// 	this.router.navigate(['/']);
		// }
		// this.getSlug();
		console.log(this.user);
		console.log('what');
	}
	getSlug() {
		
		// Get company slug from url
		this.route.params.subscribe( params => {
			this.user.slug = params['slug'];
		});
		
		console.log('slughere', this.user.slug);
		this.getCompany();
		
	}

	getCompany() {

		console.log('get company', this.user.slug);
		this.CompaniesService.getCompany( this.user.slug ).subscribe( data => {
			
			if ( !data.error ) {
				this.company = data.results;
				console.log(this.company);
			} else {
				this.isError = true;
				this.errorMsg = "No record found.";
			}

			this.mainLoader = false;
		});
	}


  	onSubmit() {

		// if ( this.storage.get('token') ) {
		// 	this.router.navigate(['/']);
		// }

		console.log(this.user);

		this.successMsg = '';
		this.errorMsg = '';

		this.loading = true;

		this.AuthService.login( this.user ).subscribe( data => {
			
			if ( !data.error ) {

				this.successMsg = 'Success! Logging in..';
				console.log( data );

				this.storage.set( 'token', data.token );
				this.storage.set( 'user', data.user );
				this.storage.set( 'perm', data.perm );
				this.storage.set( 'company', data.company );
				this.storage.set( 'currBranchId', data.branch._id );
				this.storage.set( 'currBranchName', data.branch.name );
				this.router.navigate(['/']);
			} else {
				const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
				this.errorMsg = msg;
			}

			this.loading = false;
			return;
		});
	}

}
