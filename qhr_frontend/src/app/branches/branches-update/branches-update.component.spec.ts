import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchesUpdateComponent } from './branches-update.component';

describe('BranchesUpdateComponent', () => {
  let component: BranchesUpdateComponent;
  let fixture: ComponentFixture<BranchesUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchesUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
