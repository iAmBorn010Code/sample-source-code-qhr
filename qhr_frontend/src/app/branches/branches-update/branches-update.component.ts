import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { BranchesService } from '../../services/branches.service';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';
import { LocalstorageService } from 'src/app/localstorage.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-branches-update',
  templateUrl: './branches-update.component.html',
  styleUrls: ['./branches-update.component.css']
})
export class BranchesUpdateComponent implements OnInit {

	branch: any = {};
	branch_id;

  	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
	saving = false;

	selectedFile: File = null;
	uploads_history = [];

	fileUploading = false;
	fileUploaded = false;
	fileUploadTriggered = false;
	fileUploadingMsg = '';

	perm: any;
	withPerm = false;

	currBranch;
	enableSwitch = true;

  	constructor(
		private PageService: PageService,
    	private TextHelperService: TextHelperService,
    	private BranchesService: BranchesService,
		private ActivatedRoute: ActivatedRoute,
		private storage: LocalstorageService,
		private modalService: SuiModalService,
  	) { }

  	ngOnInit() {
		
		this.PageService.pageName.next('Branches');
		this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(700) !== -1 ? true : false;

		this.ActivatedRoute.params.subscribe( params => {
			this.branch_id = params['id'];
		});

		this.currBranch = localStorage.getItem('currBranchId');
		this.enableSwitch = this.currBranch == this.branch_id ? false : true;
	
		this.BranchesService.show( this.branch_id ).subscribe( data => {
			if ( !data.error ) {
				this.branch = data.results;
			}
			this.mainLoader = !this.mainLoader;
		});
  	}

  	update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;

		this.saving = true;

		const fields = {
			name: this.branch.name,
			status: this.branch.status,
			address: this.branch.address,
		};

		console.log( fields );

 	 	this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.BranchesService.update( params['id'], fields ).subscribe( data => {
				this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
						this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
				}
			});
		});
	}

	onFileChanged(event) {

		this.selectedFile = <File>event.target.files[0];
		const uploadData = new FormData();
		
		uploadData.append('branch_id', this.branch._id);
		uploadData.append('logo', this.selectedFile, this.selectedFile.name);
		

		console.log(uploadData);

		this.fileUploading = true;
		this.fileUploadTriggered = true;
		
		const fileMaxSize = 1024 * 1024 * 2; // Max file size is 2mb.
		if ( this.selectedFile.size > fileMaxSize ) {
			this.fileUploading = false;
			this.fileUploaded = false;
			this.fileUploadingMsg = 'File size is too large.'
		}

		// Check file type
		if ( this.selectedFile.type == 'image/jpeg' || this.selectedFile.type == 'image/png' ) {
			this.BranchesService.logoUpload(uploadData).subscribe(data => {

	      		this.fileUploading = false;

				if (!data.error) {
				this.fileUploaded = true;
				this.branch.logo = data.logo;
				console.log(this.branch.logo);
				} else {
				this.fileUploaded = false;
				this.fileUploadingMsg = data.msg;
				console.log(this.fileUploadingMsg);
				}
		    });
		} else {
			this.fileUploading = false;
			this.fileUploaded = false;
			this.fileUploadingMsg = 'File type is not supported.'
		}
	
	}

	open() {

		this.modalService
			.open( new ConfirmModal('Are you sure?', 'Do you want to switch to this branch?') )
			.onApprove( () => this.switchBranch() )
			.onDeny( () => '' )
	}

	switchBranch() {
		this.storage.set( 'currBranchId', this.branch._id );
		this.storage.set( 'currBranchName', this.branch.name );
		this.enableSwitch = false;
		this.PageService.currBranchName.next(this.branch.name);
		console.log('current', localStorage.getItem('currBranchId') );
		console.log('current', localStorage.getItem('currBranchName') );
	}

}
