import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-branches-show',
  templateUrl: './branches-show.component.html',
  styleUrls: ['./branches-show.component.css']
})
export class BranchesShowComponent implements OnInit {

  constructor(
    private PageService: PageService,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Branches');
  }

}
