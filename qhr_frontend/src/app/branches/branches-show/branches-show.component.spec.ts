import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchesShowComponent } from './branches-show.component';

describe('BranchesShowComponent', () => {
  let component: BranchesShowComponent;
  let fixture: ComponentFixture<BranchesShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchesShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchesShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
