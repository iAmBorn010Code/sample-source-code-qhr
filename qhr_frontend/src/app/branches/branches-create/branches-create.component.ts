import { Component, OnInit } from '@angular/core';
import { BranchesService } from '../../services/branches.service';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-branches-create',
  templateUrl: './branches-create.component.html',
  styleUrls: ['./branches-create.component.css']
})
export class BranchesCreateComponent implements OnInit {

  branch: any = {
    status: 'active'
  };

  uploads_history = [];
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private BranchesService: BranchesService,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Branches');
    console.log('init called');
  }

  getFields() {

    const fields = {
        name: this.branch.name,
        status: this.branch.status,
        address: this.branch.address,
    };
    
    return fields;
  }

  save() {
    console.log('clicked save');
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;

    this.saving = true;

    console.log( this.getFields() );

    this.BranchesService.store( this.getFields() ).subscribe( data => {
      console.log('request sucessful', data)
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
