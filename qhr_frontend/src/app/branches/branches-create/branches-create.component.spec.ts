import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchesCreateComponent } from './branches-create.component';

describe('BranchesCreateComponent', () => {
  let component: BranchesCreateComponent;
  let fixture: ComponentFixture<BranchesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchesCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
