import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { BranchesService } from '../../services/branches.service';

@Component({
  selector: 'app-branches-index',
  templateUrl: './branches-index.component.html',
  styleUrls: ['./branches-index.component.css']
})
export class BranchesIndexComponent implements OnInit {

  	branches = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
		s: ''
	};

	results_count = 0;
	mainLoader = true;
	errorMsg = '';

	perm: any;
	withPerm = false;

  	constructor(
  		private PageService: PageService,
  		private PaginatorService: PaginatorService,
      private BranchesService: BranchesService,
  	) { }

  	ngOnInit() {

		this.PageService.pageName.next('Branches');
		this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(700) !== -1 ? true : false;
		this.get();
	}
	  

	get() {
		  
		this.branches = [];
		this.BranchesService.get( this.params ).subscribe( data => {

			console.log('BRANCHES', data);
			if ( !data.error ) {
				this.branches = data.results;
				this.results_count = data.results_count;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( this.paginator );
			} else {
				this.errorMsg = data.msg;
				console.log('this.errorMsg', this.errorMsg );
			}

			this.mainLoader = false;
		});
	}
	  

	// THIS IS FOR PAGINATIONS
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}
}