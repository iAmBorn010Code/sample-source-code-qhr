import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchesIndexComponent } from './branches-index.component';

describe('BranchesIndexComponent', () => {
  let component: BranchesIndexComponent;
  let fixture: ComponentFixture<BranchesIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchesIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchesIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
