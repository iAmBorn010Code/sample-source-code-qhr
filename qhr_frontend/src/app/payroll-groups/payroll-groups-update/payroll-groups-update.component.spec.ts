import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollGroupsUpdateComponent } from './payroll-groups-update.component';

describe('PayrollGroupsUpdateComponent', () => {
  let component: PayrollGroupsUpdateComponent;
  let fixture: ComponentFixture<PayrollGroupsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollGroupsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollGroupsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
