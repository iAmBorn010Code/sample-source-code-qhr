import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { PayrollGroupsService } from 'src/app/services/payrollgroups.service';
import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-payroll-groups-update',
  templateUrl: './payroll-groups-update.component.html',
  styleUrls: ['./payroll-groups-update.component.css']
})
export class PayrollGroupsUpdateComponent implements OnInit {

  payroll_group: any = {};

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;

  saving = false;
  payroll_group_id = '';
  constructor(
    private TextHelperService: TextHelperService,
    private PayrollGroupsService: PayrollGroupsService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.payroll_group_id = params['id'];
      console.log('id', this.payroll_group_id);
      this.show();
    });
  }

  show() {
    this.PayrollGroupsService.show( this.payroll_group_id ).subscribe( data => {
      if ( !data.error ) {
        this.payroll_group = data.results;
        this.mainLoader = false;
      }
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			name: this.payroll_group.name,
		};
    this.PayrollGroupsService.update( this.payroll_group_id, fields ).subscribe( data => {
      
      this.saving = false;

      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }

    });
  }

}
