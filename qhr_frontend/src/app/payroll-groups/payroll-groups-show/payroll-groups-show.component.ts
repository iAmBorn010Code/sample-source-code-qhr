import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PayrollGroupsService } from 'src/app/services/payrollgroups.service';

@Component({
  selector: 'app-payroll-groups-show',
  templateUrl: './payroll-groups-show.component.html',
  styleUrls: ['./payroll-groups-show.component.css']
})
export class PayrollGroupsShowComponent implements OnInit {

  payroll_group: any = {};
  id;

  params = {
    team_id: ''
	};

  mainLoader = true; 

  perm: any;
	withPerm = false;

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private PayrollGroupsService: PayrollGroupsService
  ) { }

  ngOnInit() {

    this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(1200) !== -1 ? true : false;

    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.params.team_id = this.id = params['id'];
      this.show();
    });

  }

  show() {
    this.PayrollGroupsService.show( this.id ).subscribe( data => {
      if ( !data.error ) {
        this.payroll_group = data.results;
        this.mainLoader = false; 
      }
    });
  }

}
