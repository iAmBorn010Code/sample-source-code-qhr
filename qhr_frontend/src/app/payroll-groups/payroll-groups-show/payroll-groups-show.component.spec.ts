import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollGroupsShowComponent } from './payroll-groups-show.component';

describe('PayrollGroupsShowComponent', () => {
  let component: PayrollGroupsShowComponent;
  let fixture: ComponentFixture<PayrollGroupsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollGroupsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollGroupsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
