import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollGroupsAddComponent } from './payroll-groups-add.component';

describe('PayrollGroupsAddComponent', () => {
  let component: PayrollGroupsAddComponent;
  let fixture: ComponentFixture<PayrollGroupsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollGroupsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollGroupsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
