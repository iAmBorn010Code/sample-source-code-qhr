import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { TextHelperService } from '../../texthelper.service';
import { PayrollGroupsService } from 'src/app/services/payrollgroups.service';

@Component({
  selector: 'app-payroll-groups-index',
  templateUrl: './payroll-groups-index.component.html',
  styleUrls: ['./payroll-groups-index.component.css']
})
export class PayrollGroupsIndexComponent implements OnInit {

  payroll_groups = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
	};
	
  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  perm: any;
	withPerm = false;

  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private PayrollGroupsService: PayrollGroupsService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Payroll Groups');
    this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(500) !== -1 ? true : false;
    this.get();
  }

  get() {
		  
		this.payroll_groups = [];
		this.PayrollGroupsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.payroll_groups = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			} else {
        this.errorMsg = data.msg;
      }
			this.mainLoader = false;
		});
  }


  
  //pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
