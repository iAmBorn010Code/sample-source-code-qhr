import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollGroupsIndexComponent } from './payroll-groups-index.component';

describe('PayrollGroupsIndexComponent', () => {
  let component: PayrollGroupsIndexComponent;
  let fixture: ComponentFixture<PayrollGroupsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollGroupsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollGroupsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
