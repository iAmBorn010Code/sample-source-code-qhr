import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollGroupsCreateComponent } from './payroll-groups-create.component';

describe('PayrollGroupsCreateComponent', () => {
  let component: PayrollGroupsCreateComponent;
  let fixture: ComponentFixture<PayrollGroupsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollGroupsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollGroupsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
