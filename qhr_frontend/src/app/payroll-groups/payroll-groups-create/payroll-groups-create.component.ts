import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { PayrollGroupsService } from 'src/app/services/payrollgroups.service';

@Component({
  selector: 'app-payroll-groups-create',
  templateUrl: './payroll-groups-create.component.html',
  styleUrls: ['./payroll-groups-create.component.css']
})
export class PayrollGroupsCreateComponent implements OnInit {

  payroll_group: any = {};

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private PayrollGroupsService: PayrollGroupsService
  ) { }

  ngOnInit() {

  }

  getFields() {

    const fields = {
      name: this.payroll_group.name,
    };
    
    return fields;
  }

  save() {

    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.PayrollGroupsService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }
}
