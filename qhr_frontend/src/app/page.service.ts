import { Injectable } from '@angular/core';
import { Subject } from 'rxjs'

@Injectable()
export class PageService {

  pageName = new Subject();
  companyLogo = new Subject();
  profilePhoto = new Subject();
  currBranchName = new Subject();
  companyName = new Subject();

  constructor() { }

}
