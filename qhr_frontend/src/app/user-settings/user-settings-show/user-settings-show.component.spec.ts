import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSettingsShowComponent } from './user-settings-show.component';

describe('UserSettingsShowComponent', () => {
  let component: UserSettingsShowComponent;
  let fixture: ComponentFixture<UserSettingsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSettingsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSettingsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
