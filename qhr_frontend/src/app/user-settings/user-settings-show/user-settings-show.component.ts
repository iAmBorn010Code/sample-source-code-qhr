import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserSettingsService } from '../../services/usersettings.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-user-settings-show',
  templateUrl: './user-settings-show.component.html',
  styleUrls: ['./user-settings-show.component.css']
})
export class UserSettingsShowComponent implements OnInit {

  setting: any;
  id;

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;

  constructor(
    private PageService: PageService,
    private UserSettingsService: UserSettingsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    console.log('called init on user settings');
    this.PageService.pageName.next('User Settings');
    this.show();
  }

  show() {

    this.UserSettingsService.show().subscribe( data => {

      if ( !data.error ) {
       this.setting = data.results;
       console.log(this.setting);
      } else {

        console.log( data.msg );
      }
      this.mainLoader = false;
    });

  }

}
