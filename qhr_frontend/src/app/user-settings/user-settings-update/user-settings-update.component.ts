import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserSettingsService } from '../../services/usersettings.service';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-user-settings-update',
  templateUrl: './user-settings-update.component.html',
  styleUrls: ['./user-settings-update.component.css']
})
export class UserSettingsUpdateComponent implements OnInit {

  setting: any = {};

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
  saving = false;
  
  constructor(
    private TextHelperService: TextHelperService,
    private UserSettingsService: UserSettingsService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.UserSettingsService.show( ).subscribe( data => {
      if ( !data.error ) {
        this.setting = data.results;
      }
      this.mainLoader = !this.mainLoader;
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			field1: this.setting.field1,
			field2: this.setting.field2,
			field3: this.setting.field3,
			field4: this.setting.field4
		};

		console.log( fields );

 	 	this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.UserSettingsService.update( params['id'], fields ).subscribe( data => {
				this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
						this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
				}
			});
		});
	}

}
