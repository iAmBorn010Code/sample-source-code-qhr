import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSettingsUpdateComponent } from './user-settings-update.component';

describe('UserSettingsUpdateComponent', () => {
  let component: UserSettingsUpdateComponent;
  let fixture: ComponentFixture<UserSettingsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSettingsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSettingsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
