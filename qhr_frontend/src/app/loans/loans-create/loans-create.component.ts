import { Component, OnInit } from '@angular/core';
import { LoansService } from '../../services/loans.service';
import { Router } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { EmployeesService } from '../../services/employees.service';
import { PageService } from '../../page.service';
import { AuthService } from '../../auth.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-loans-create',
  templateUrl: './loans-create.component.html',
  styleUrls: ['./loans-create.component.css']
})
export class LoansCreateComponent implements OnInit {

  userPermissions = [];
  loggedUser;

  employees = [];

  loan: any = { };

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;
  canCreateForOthers = false;

  nextPayment: any = {
    amount: 0,
    date: ''
  }

  totalPayable = 0;

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private LoansService: LoansService,
    private AuthService: AuthService,
    private EmployeesService: EmployeesService,
  ) { 
    
  }

  ngOnInit() {
    this.PageService.pageName.next('Loans');

    this.userPermissions = localStorage.perm;
    this.loggedUser = this.AuthService.getLoggedUser();

    this.canCreateForOthers = this.userPermissions.indexOf(1400) !== -1;

    if ( this.canCreateForOthers ) {
      this.getEmployees();
    } 

    this.loan = {
      employeeId: this.loggedUser._id,
      amount: 0,
      payment_type: 'weekly',
      with_interest: false,
      status: 'pending',
      start_date: '',
      end_date: '',
      payment_rcv: '',
      interest_type: '',
      percentage: 0,
      interest: 0
    };
  }

  getFields() {
    const fields = {
        employee_id: this.loan.employeeId,
        amount: this.loan.amount,
        payment_type: this.loan.payment_type,
        period: this.loan.period,
        with_interest: this.loan.with_interest,
        interest: this.loan.interest,
        interest_type: this.loan.interest_type,
        status: this.loan.status,
        start_date: this.loan.start_date,
        payment_rcv: this.loan.payment_rcv,
        percentage: this.loan.percentage
        
    };
    
    return fields;
  }

  save() {
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.LoansService.store( this.getFields() ).subscribe( data => {

      this.saving = true;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
        console.log('data', data);
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
      this.saving = false;
    });
  }

  getEmployees() {

		this.EmployeesService.get({}).subscribe( data => {
			if ( !data.error ) {
				this.employees = data.results;
			}
		});

  }

  changeInterestType( type ) {

    if ( type == 'percentage' ) {
      this.loan.interest = (this.loan.amount * this.loan.percentage)/100;
      this.calculateLoan();
    } else {
      this.loan.percentage = 0;
      this.loan.interest = this.loan.interest;
    }

  }

  calculateLoan() {

    let oneDay=1000*60*60*24;
    this.nextPayment.amount = parseInt(this.loan.amount)/parseInt(this.loan.period);
    this.totalPayable = parseInt(this.loan.amount) + parseInt(this.loan.interest);

    if ( this.loan.payment_type == 'weekly' ) {

      let startDate = new Date(this.loan.start_date).getTime();
      let oneWeek = oneDay * 7;
      let paymentDate = startDate + oneWeek;
      this.nextPayment.date = Math.round(paymentDate); 

    } else {

      let startDate = new Date(this.loan.start_date).getTime();
      let oneMonth = oneDay * 30;
      let paymentDate = startDate + oneMonth;
      this.nextPayment.date = Math.round(paymentDate); 
    }

    if ( this.loan.with_interest ) {

      this.nextPayment.amount = (parseInt(this.loan.amount)/parseInt(this.loan.period)) + ( parseInt(this.loan.interest)/parseInt(this.loan.period) );
    }

  }
  

}
