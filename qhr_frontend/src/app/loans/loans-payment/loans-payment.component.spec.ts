import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoansPaymentComponent } from './loans-payment.component';

describe('LoansPaymentComponent', () => {
  let component: LoansPaymentComponent;
  let fixture: ComponentFixture<LoansPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoansPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoansPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
