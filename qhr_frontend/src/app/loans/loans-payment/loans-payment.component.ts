import { Component, OnInit } from '@angular/core';
import { LoanPaymentsService } from '../../services/loan-payments.service';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';

import { PageService } from '../../page.service';


@Component({
  selector: 'app-loans-payment',
  templateUrl: './loans-payment.component.html',
  styleUrls: ['./loans-payment.component.css']
})
export class LoansPaymentComponent implements OnInit {

  loanpayment: any = { };
  loanId = '';
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;


  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private LoanPaymentsService: LoanPaymentsService,
    private ActivatedRoute: ActivatedRoute,
  ) { 
    
  }

  ngOnInit() {
    this.PageService.pageName.next('Leaves');
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.loanId = params['id'];
    });
   
  }

  getFields() {

    const fields = {
        loan_id: this.loanId,
        amount: this.loanpayment.amount,
    };
    
    return fields;
  }

  save() {
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.LoanPaymentsService.store( this.getFields() ).subscribe( data => {

      this.saving = true;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
      this.saving = false;
    });
  }

}
