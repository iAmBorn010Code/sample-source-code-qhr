import { Component, OnInit } from '@angular/core';
import { LoansService } from '../../services/loans.service';
import { TextHelperService } from '../../texthelper.service';
import { EmployeesService } from '../../services/employees.service';
import { PageService } from '../../page.service';
import { AuthService } from '../../auth.service';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SharedDataEmployeeService } from '../../services/shared-data-employee.service';

@Component({
  selector: 'app-loans-update',
  templateUrl: './loans-update.component.html',
  styleUrls: ['./loans-update.component.css']
})
export class LoansUpdateComponent implements OnInit {

  userPermissions = [];
  loggedUser;

  employees = [];

  loan: any = { 
    employeeId: '',
    amount: 0,
    payment_type: 'daily',
    with_interest: false,
    status: 'pending',
    start_date: '',
    end_date: '',
    payment_rcv: ''
  };

  loanId = '';

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  mainLoader = true;

  nextPayment: any = {
    amount: 0,
    date: ''
  }

  totalPayable = 0;

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private LoansService: LoansService,
    private ActivatedRoute: ActivatedRoute,
    private AuthService: AuthService,
    private EmployeesService: EmployeesService,
  ) { 
    
  }

  ngOnInit() {
    this.PageService.pageName.next('Loans');
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.loanId = params['id'];
      this.show( this.loanId );
			
    });
    
  }

  show( id ){
    this.LoansService.show( id ).subscribe( data => {
      if ( !data.error ) {
        this.loan = data.results;
        this.calculateLoan();
        console.log('this loan', this.loan);
      } else {
        this.errorMsg = data.msg;
      }
      this.mainLoader = false;
    });
  }

  update() {
    
    this.successMsg = '';
		this.errorMsg = '';
    this.isCloseMsg = false;

    const fields = {
      employee_id: this.loan.employeeId,
      amount: this.loan.amount,
      payment_type: this.loan.payment_type,
      with_interest: this.loan.with_interest,
      interest_type: this.loan.interest_type,
      interest: this.loan.interest,
      status: this.loan.status,
      start_date: this.loan.start_date,
      period: this.loan.period,
      percentage: this.loan.percentage
    };
    
    this.LoansService.update( this.loanId, fields ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
        this.loan = data.results;
        this.show(this.loanId);
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

  }

  getEmployees() {

		this.EmployeesService.get({}).subscribe( data => {
      console.log( data );
			if ( !data.error ) {
        console.log( this.employees );
        this.employees = data.results;
        this.loan.employeeId = this.loan.employee._id;
        this.mainLoader = false;
			}
		});

  }

  changeInterestType( type ) {

    if ( type == 'percentage' ) {
      this.loan.interest = (this.loan.amount * this.loan.percentage)/100;
      this.calculateLoan();
    } else {
      this.loan.percentage = 0;
      this.loan.interest = this.loan.interest;
    }

  }

  calculateLoan() {

    let oneDay=1000*60*60*24;
    this.nextPayment.amount = parseInt(this.loan.amount)/parseInt(this.loan.period);
    this.totalPayable = parseInt(this.loan.amount) + parseInt(this.loan.interest);

    if ( this.loan.payment_type == 'weekly' ) {

      let startDate = new Date(this.loan.start_date).getTime();
      let oneWeek = oneDay * 7;
      let paymentDate = startDate + oneWeek;
      this.nextPayment.date = Math.round(paymentDate); 

    } else {

      let startDate = new Date(this.loan.start_date).getTime();
      let oneMonth = oneDay * 30;
      let paymentDate = startDate + oneMonth;
      this.nextPayment.date = Math.round(paymentDate); 
    }

    if ( this.loan.with_interest ) {
      this.nextPayment.amount = (parseInt(this.loan.amount)/parseInt(this.loan.period)) + ( parseInt(this.loan.interest)/parseInt(this.loan.period) );
    }

  }

}
