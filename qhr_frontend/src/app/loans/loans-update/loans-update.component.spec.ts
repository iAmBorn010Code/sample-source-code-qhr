import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoansUpdateComponent } from './loans-update.component';

describe('LoansEditComponent', () => {
  let component: LoansUpdateComponent;
  let fixture: ComponentFixture<LoansUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoansUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoansUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
