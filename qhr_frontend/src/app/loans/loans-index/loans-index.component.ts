import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { LoansService } from '../../services/loans.service';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-loans-index',
  templateUrl: './loans-index.component.html',
  styleUrls: ['./loans-index.component.css']
})
export class LoansIndexComponent implements OnInit {

	loans = [];
	employees = [];	
	errorMsg = '';
	paginator: any;
	params = {
		limit: 8,
		page: 1,
		emp_id: ''
	};

	params_emp = {
		s: ''
	};

	mainLoader = true;
	empLoader = true;
	searching = false;

	perm: any;
	user: any = {};
	withPerm = false;

	constructor(
		private PageService: PageService,
		private PaginatorService: PaginatorService,
		private LoansService: LoansService,
		private EmployeesService: EmployeesService
	) { }

  ngOnInit() {

		this.PageService.pageName.next('Loans');
		this.perm = localStorage.getItem('perm');
		this.user = JSON.parse( localStorage.getItem('user') );
		this.withPerm = this.perm.indexOf(1400) !== -1 ? true : false;

		if ( this.withPerm ) {
			this.employees = [];
			this.EmployeesService.get( this.params_emp ).subscribe( data => {
				if ( !data.error ) {
					this.employees = data.results;
				} else {
					this.errorMsg = data.msg;
				}
				this.empLoader = false;
			});
		}

		this.get();
	}
	  

	get() {
		this.mainLoader = true;

		this.loans = [];
		this.LoansService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.loans = data.results;
				console.log(this.loans)
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			this.mainLoader = false;
		});
	}

	getEmp() {

		this.mainLoader = true;
		this.searching = true;
		this.loans = [];

		this.LoansService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.loans = data.results;
				
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			this.mainLoader = false;
			this.searching = false;
		});

	}

	//pagination
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
