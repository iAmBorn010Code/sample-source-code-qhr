import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoansIndexComponent } from './loans-index.component';

describe('LoansIndexComponent', () => {
  let component: LoansIndexComponent;
  let fixture: ComponentFixture<LoansIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoansIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoansIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
