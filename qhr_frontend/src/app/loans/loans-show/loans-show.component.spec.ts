import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoansShowComponent } from './loans-show.component';

describe('LoansShowComponent', () => {
  let component: LoansShowComponent;
  let fixture: ComponentFixture<LoansShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoansShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoansShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
