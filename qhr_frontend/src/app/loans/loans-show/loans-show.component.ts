import { Component, OnInit } from '@angular/core';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { PageService } from '../../page.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LoansService } from '../../services/loans.service';
import { TextHelperService } from '../../texthelper.service';
import { LoanPaymentsService } from '../../services/loan-payments.service';
import { PaginatorService } from '../../paginator.service';

@Component({
  selector: 'app-loans-show',
  templateUrl: './loans-show.component.html',
  styleUrls: ['./loans-show.component.css']
})
export class LoansShowComponent implements OnInit {

  loan: any = { };
  status: any;
  id;

  loan_payments = [];
	paginator: any;
	params = {
		limit: 10,
    	page: 1,
  };

  results_count = 0;
  
  tableLoader = true; 
  
  mainLoader = true; 

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  modal_msg = '';
  success_msg = '';

  perm: any;
	withPerm = false;

  constructor(
    private PageService: PageService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private LoansService: LoansService,
    private LoanPaymentsService: LoanPaymentsService,
    private ActivatedRoute: ActivatedRoute,
    private PaginatorService: PaginatorService,
  ) { }

  ngOnInit() {
    
    this.perm = localStorage.getItem('perm');
    this.withPerm = this.perm.indexOf(1400) !== -1 ? true : false;
    
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.id = params['id'];
      this.show( this.id );
     
    });
    this.getLoanPayments();

  }

  getLoanPayments() {
			
		this.tableLoader = true;
		this.loan_payments = []; 
		this.LoanPaymentsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
        this.loan_payments = data.results;
				this.results_count = data.results_count;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( this.paginator );
			}
			this.tableLoader = false;
		});
  }

  show( id ) {

    this.mainLoader = true;

     this.LoansService.show( this.id ).subscribe( data => {
        if ( !data.error ) {
          this.loan = data.results;
        }

        this.mainLoader = false;
      });
  }

  open( status ) {
    console.log('stats', status); 

    if ( status == 'approved' ) {
      this.modal_msg = 'Are you sure you want to approve this loan?';
      this.success_msg = 'Loan was approved.';
    } else if ( status == 'disapproved' ) {
      this.modal_msg = 'Are you sure you want to disapprove this loan?';
      this.success_msg = 'Loan was disapproved.';
    } else if ( status == 'deleted' ) {
      this.modal_msg = 'Are you sure you want to delete this loan?';
      this.success_msg = 'Loan was deleted.';
    }

    this.modalService
        .open( new ConfirmModal('Are you sure?', this.modal_msg) )
        .onApprove( () => this.update(status) )
        .onDeny( () => '' )
  }

  update(status) {

    console.log('status in update', status);
    
    const fields = {
      status: status
    };
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    
    this.LoansService.update( this.id, fields ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = this.success_msg;
        this.loan = data.results;
        this.show(this.id);
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

  }

  //pagination
	nextPage() {
		this.tableLoader = true;
		this.params.page = this.paginator.nextPage;
		this.getLoanPayments();
	}

	prevPage() {
		this.tableLoader = true;
		this.params.page = this.paginator.prevPage;
		this.getLoanPayments();
	}
	
	goToPage( page ) {
		this.tableLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.getLoanPayments();
	}

}
