import { Injectable } from '@angular/core';

@Injectable()
export class TextHelperService {

    constructor() { }

    formatErrors(msgs, form_errors, seperator = '<br />') 
    {
        let msg = '';

        if ( typeof form_errors !== 'undefined' && form_errors != null && typeof form_errors.errors !== 'undefined' && form_errors.errors != null ) {
            const list = Object.keys(form_errors.errors);
            if ( list.length > 0) {
                list.forEach(function (key, i) {
                    msg += form_errors.errors[key].message + seperator;
                });
                return msg;
            }
        } else if ( typeof form_errors !== 'undefined' && form_errors != null  && typeof form_errors.message !== 'undefined' && form_errors.message != null && form_errors.message ) {
            return form_errors.message;
        } else if ( typeof form_errors !== 'undefined' && form_errors != null && typeof form_errors[0] !== 'undefined' ) {
            
            form_errors.forEach(function (value, key) {
                msg += value.msg + seperator;
            });

            return msg;
        } else {
            return msgs;
        }
    }

    serialize(obj: any) {
        // Remove all null values in an object
        Object.keys(obj).forEach((key) => (obj[key] === undefined || obj[key] === null) && delete obj[key]);

        // Minified solution from http://stackoverflow.com/questions/1714786/querystring-encoding-of-a-javascript-object
        return Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    }

    ucWords(str) {
        return (str + '')
            .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                return $1.toUpperCase()
            })
    }

}
