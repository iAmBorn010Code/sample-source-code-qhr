import { Component, OnInit } from '@angular/core';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { CompaniesService } from 'src/app/services/companies.service';
import { SubscriptionsService } from 'src/app/services/subscriptions.service';
import { PageService } from 'src/app/page.service';
import { TextHelperService } from '../../texthelper.service';


@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.css']
})
export class SubscriptionsComponent implements OnInit {


  successMsg = '';
  errorMsg = '';
  isError: boolean;
  isCloseMsg: boolean;

  // subscription: any = {};
  currentSub: any = {};
  awaitingSub: any = {};
  company: any = {};
  company_id;

  subscriptions = [];
  sub: any = {};
  subs = [];

  mainLoader = true;

  isExpired = false;

  constructor(
    private modalService: SuiModalService,
    private CompaniesService: CompaniesService,
    private SubscriptionsService: SubscriptionsService,
    private TextHelperService: TextHelperService,
    private PageService: PageService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Subscriptions');
    this.company = JSON.parse( localStorage.getItem('company') );
    this.company_id = this.company._id;
    this.showCurrent(this.company_id);
    
    console.log('main', this.mainLoader);
    this.getSubs();
  }

  showCurrent( id ) {
    this.mainLoader = true;
    let paid = true;
    this.SubscriptionsService.show( id, paid).subscribe( data => {
      if ( !data.error ) {
        this.currentSub = data.results;
        this.isExpired = data.isExpired;
      } else {
        this.mainLoader = false;
        this.awaitingSub = null;
        this.errorMsg = "No record found.";
      }
      this.showAwaiting(this.company_id);
    });
  }

  showAwaiting( id ) {
    this.mainLoader = true;
    let paid = false;
    this.SubscriptionsService.show( id, paid ).subscribe( data => {
      console.log('awaiting', data);
      if ( !data.error ) {
       this.awaitingSub = data.results;
      //  console.log('awaiting', this.awaitingSub);
      } else {
        this.awaitingSub = null;
        console.log(this.awaitingSub);
        this.errorMsg = "No record found.";
      }

      this.mainLoader = false;

      
    });
  }

  open( sub ) {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you want to subscribe to this plan?') )
        .onApprove( () => this.subscribe( sub ) )
        .onDeny( () => '' )
  }

  subscribe( sub ) {

    // this.mainLoader = true;
    console.log('sub', sub);
    this.SubscriptionsService.store( sub ).subscribe( data => {

      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
        this.awaitingSub = data.result;
        this.showCurrent(this.awaitingSub.company_id);
        this.mainLoader = false;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

  getSubs() {

    this.mainLoader = true;
    this.SubscriptionsService.getSubs().subscribe( data => {
      if ( !data.error ) {
        this.subscriptions = data.results;
        // this.mainLoader = false;
        Object.keys(this.subscriptions).forEach(key=> {
          this.sub = {
            plan_type: key,
            price: this.subscriptions[key].price,
            desc: this.subscriptions[key].desc
          }    
          this.subs.push(this.sub);
        });
        this.successMsg = data.msg;
      } else {
        this.errorMsg = data.msg;
      }
    });

  }

  cancel( sub ) {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you want to cancel this plan?') )
        .onApprove( () => this.remove( sub ) )
        .onDeny( () => '' )
  }

  remove( sub ) {

    this.mainLoader = true;
    console.log('sub', sub);
    this.SubscriptionsService.cancel( sub ).subscribe( data => {

      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
        console.log('cancel', data);
        this.showCurrent(data.result.company_id);
        // this.mainLoader = false;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
