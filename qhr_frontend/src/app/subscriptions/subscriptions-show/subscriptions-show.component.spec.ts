import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionsShowComponent } from './subscriptions-show.component';

describe('SubscriptionsShowComponent', () => {
  let component: SubscriptionsShowComponent;
  let fixture: ComponentFixture<SubscriptionsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriptionsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
