import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { CompaniesService } from 'src/app/services/companies.service';

@Component({
  selector: 'app-subscriptions-show',
  templateUrl: './subscriptions-show.component.html',
  styleUrls: ['./subscriptions-show.component.css']
})
export class SubscriptionsShowComponent implements OnInit {

  plan_type = '';
  saving = false;
  subscribed = false;

  successMsg = '';
  errorMsg = '';

  

  constructor(
    private route: ActivatedRoute,
    private modalService: SuiModalService,
    private CompaniesService: CompaniesService
  ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      // this.id = params['id'];
      console.log('plantype', params['planType'] );
      this.plan_type = params['planType'];
    });
    
  }

  open() {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you want to subscribe to this plan?') )
        .onApprove( () => this.subscribe( this.plan_type ) )
        .onDeny( () => '' )
  }

  subscribe( type ) {

    this.saving = true;
    this.CompaniesService.subscribe( type ).subscribe( data => {
      if ( !data.error ) {
        this.successMsg = data.msg;
        this.subscribed = true;
      } else {
        this.errorMsg = data.msg;
      }
      this.saving = false;
    });

  }
}
