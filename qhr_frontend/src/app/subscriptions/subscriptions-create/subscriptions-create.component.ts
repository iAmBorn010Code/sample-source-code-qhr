import { Component, OnInit } from '@angular/core';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { CompaniesService } from 'src/app/services/companies.service';
import { SubscriptionsService } from 'src/app/services/subscriptions.service';
import { PageService } from 'src/app/page.service';
import { TextHelperService } from '../../texthelper.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-subscriptions-create',
  templateUrl: './subscriptions-create.component.html',
  styleUrls: ['./subscriptions-create.component.css']
})
export class SubscriptionsCreateComponent implements OnInit {

  successMsg = '';
  errorMsg = '';
  isError: boolean;
  isCloseMsg: boolean;

  saving: boolean = false;

  subscription: any = {};
  company: any = {};
  company_id;

  subscriptions = [];
  sub: any = {};
  subs = [];
  interval_count = '3';
  payment_method = 'bank-transfer';

  mainLoader = true;
  plan_type ='';

  constructor(
    private modalService: SuiModalService,
    private CompaniesService: CompaniesService,
    private SubscriptionsService: SubscriptionsService,
    private TextHelperService: TextHelperService,
    private PageService: PageService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Subscriptions');
    this.route.params.subscribe( params => {
      // this.id = params['id'];
      console.log('plantype', params['planType'] );
      this.plan_type = params['planType'];
    });
    this.getSubs( this.plan_type );
  }


  open( sub ) {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you want to subscribe to this plan?') )
        .onApprove( () => this.subscribe( sub ) )
        .onDeny( () => '' )
  }

  subscribe( sub ) {

    // this.mainLoader = true;
    
    this.subscription = {
        plan_type: this.sub.plan_type,
        price: this.sub.price,
        desc: this.sub.desc,
        interval_count: this.interval_count,
        payment_method: this.payment_method
    };

    console.log('sub', this.subscription);
    this.SubscriptionsService.store( this.subscription ).subscribe( data => {

      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
        this.subscription = data.result;
        this.mainLoader = false;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

  getSubs( plantype ) {

    this.mainLoader = true;
    this.SubscriptionsService.getSubs().subscribe( data => {
      if ( !data.error ) {
        this.subscriptions = data.results;
        this.mainLoader = false;
        this.sub = {
              plan_type: plantype,
              price: this.subscriptions[plantype].price,
              desc: this.subscriptions[plantype].desc,
            }  
        this.successMsg = data.msg;
      } else {
        this.errorMsg = data.msg;
      }
    });

  }

}
