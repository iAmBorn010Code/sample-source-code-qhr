import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { ActivatedRoute } from '@angular/router';
import { PaymentTransactionsService } from 'src/app/services/paymenttransactions.service';

@Component({
  selector: 'app-payment-transactions-show',
  templateUrl: './payment-transactions-show.component.html',
  styleUrls: ['./payment-transactions-show.component.css']
})
export class PaymentTransactionsShowComponent implements OnInit {

  transaction: any = {};
  id: string = null;
	
  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';
  
  mainLoader = true;
  planTypes = [];

  constructor(
    private route: ActivatedRoute,
    private TextHelperService: TextHelperService,
    private PaymentTransactionsService: PaymentTransactionsService
  ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.id = params['id'];
      this.show( this.id );
    });
  }

  show( id ) {

    this.PaymentTransactionsService.show( id ).subscribe( data => {
      if ( !data.error ) {
        this.transaction = data.results;
        console.log('transaction', this.transaction );
      } else {
        console.log( data.msg );
      }

      this.mainLoader = false;

    });

  }

}
