import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentTransactionsShowComponent } from './payment-transactions-show.component';

describe('PaymentTransactionsShowComponent', () => {
  let component: PaymentTransactionsShowComponent;
  let fixture: ComponentFixture<PaymentTransactionsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentTransactionsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentTransactionsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
