import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentTransactionsInvoiceComponent } from './payment-transactions-invoice.component';

describe('PaymentTransactionsInvoiceComponent', () => {
  let component: PaymentTransactionsInvoiceComponent;
  let fixture: ComponentFixture<PaymentTransactionsInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentTransactionsInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentTransactionsInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
