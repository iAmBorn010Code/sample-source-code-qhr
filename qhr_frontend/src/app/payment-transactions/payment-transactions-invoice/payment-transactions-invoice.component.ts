import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { InvoicesService } from 'src/app/services/invoices.service';

@Component({
  selector: 'app-payment-transactions-invoice',
  templateUrl: './payment-transactions-invoice.component.html',
  styleUrls: ['./payment-transactions-invoice.component.css']
})
export class PaymentTransactionsInvoiceComponent implements OnInit {

  invoice: any = {};
  payment_trans_id = '';
  invoice_uid= '';

  mainLoader = true; 

  items = [];

  perm: any;
	withPerm = false;

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private InvoicesService: InvoicesService
  ) { }

  ngOnInit() {

    this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(1700) !== -1 ? true : false;

    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.invoice_uid = params['id'];
      this.ActivatedRoute.queryParams.subscribe( params => {
        this.payment_trans_id = params['payment_trans_id'];
  
        this.show();
      });  
    });
  }

  show() {
    this.InvoicesService.show( this.invoice_uid, this.payment_trans_id ).subscribe( data => {
      if ( !data.error ) {
        this.invoice = data.results;
        this.items = this.invoice.items; 
        console.log('data', this.items);
        this.mainLoader = false; 
      }
    });
  }

}
