import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentTransactionsIndexComponent } from './payment-transactions-index.component';

describe('PaymentTransactionsIndexComponent', () => {
  let component: PaymentTransactionsIndexComponent;
  let fixture: ComponentFixture<PaymentTransactionsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentTransactionsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentTransactionsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
