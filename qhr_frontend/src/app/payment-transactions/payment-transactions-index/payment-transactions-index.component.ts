import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { PaymentTransactionsService } from 'src/app/services/paymenttransactions.service';

@Component({
  selector: 'app-payment-transactions-index',
  templateUrl: './payment-transactions-index.component.html',
  styleUrls: ['./payment-transactions-index.component.css']
})
export class PaymentTransactionsIndexComponent implements OnInit {

  payment_transactions = [];
	paginator: any;
	params = {
		limit: 10,
    	page: 1,
    	// type: 'credit'
	};

  mainLoader = true;
	results_count = 0;

	successMsg = '';
	errorMsg = '';

  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private PaymentTransactionsService: PaymentTransactionsService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Payment History');
		this.get();
  }

  get() {
			
		this.mainLoader = true;
		this.payment_transactions = []; 
		this.PaymentTransactionsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
        this.payment_transactions = data.results;
				this.results_count = data.results_count;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( this.paginator );
			}
			this.mainLoader = false;
		});
  }

//   filter( type ) {

// 		this.params.type = type;
// 		this.get();
// 	}
  
  //pagination
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
