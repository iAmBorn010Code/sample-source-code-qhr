import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ForgotPasswordService } from '../../services/forgotpassword.service';
import { EmployeesService } from '../../services/employees.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {

  employee= {
    username: ''
  }

  results_count;
  loading: boolean;

  successMsg = '';
  errorMsg = '';

  constructor(
    private EmployeesService: EmployeesService
  ) { }

  ngOnInit() {
  }

  onSubmit() {

    this.successMsg = '';
    this.errorMsg = '';
    this.loading = true;

    this.EmployeesService.forgot_password( this.employee ).subscribe( data => {

			if ( !data.error ) {
				this.employee = data.results;
        this.results_count = data.results_count;
        this.successMsg = data.msg;
			} else {
				this.errorMsg = data.msg;
				console.log('this.errorMsg', this.errorMsg );
			}

			this.loading = false;
		});
  }

}
