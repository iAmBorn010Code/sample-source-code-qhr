import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ForgotPasswordService } from 'src/app/services/forgotpassword.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

  hash = '';
  new_password: string;
  confirm_password: string;

  loading: boolean;
  successMsg = '';
  errorMsg = '';

  password_changed = false;

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private ForgotPasswordService: ForgotPasswordService
  ) { }

  ngOnInit() {
    this.ActivatedRoute.params.subscribe( params => {
      this.hash = params['hash'];
      console.log('hash', this.hash);
    });
  }

  onSubmit() {
    this.successMsg = '';
		this.errorMsg = '';
    this.loading = true;

		const fields = {
      new_password: this.new_password,
      confirm_password: this.confirm_password,
    };
    
    console.log( fields );

    this.ForgotPasswordService.reset_password( this.hash, fields ).subscribe( data => {
      if ( !data.error ) {
        this.successMsg = data.msg;
        this.password_changed = true;
      } else {
        this.errorMsg = data.msg;
      }
      this.loading = false;
    });
  }

}
