import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkSchedIndexComponent } from './work-sched-index.component';

describe('WorkSchedIndexComponent', () => {
  let component: WorkSchedIndexComponent;
  let fixture: ComponentFixture<WorkSchedIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkSchedIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkSchedIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
