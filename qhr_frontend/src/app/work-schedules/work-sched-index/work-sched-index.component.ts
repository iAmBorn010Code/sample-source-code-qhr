import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { TextHelperService } from 'src/app/texthelper.service';
import { WorkScheduleService } from 'src/app/services/workschedule.service';
import { EmployeesService } from 'src/app/services/employees.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-work-sched-index',
  templateUrl: './work-sched-index.component.html',
  styleUrls: ['./work-sched-index.component.css']
})
export class WorkSchedIndexComponent implements OnInit {

  	work_schedules = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
		user_id: ''
	};

	employees = [];	
	params_emp = {
		s: ''
	};

	mainLoader = true;
	empLoader = true;
  
  	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  	errorMsg = '';

	perm: any;
	withPerm = false;

	constructor(
		private PageService: PageService,
		private PaginatorService: PaginatorService,
		private TextHelperService: TextHelperService,
		private WorkSchedService: WorkScheduleService,
		private EmployeesService: EmployeesService,
		private modalService: SuiModalService,
	) { }

  	ngOnInit() {
		this.PageService.pageName.next('Work Schedules');
		this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(3400) !== -1 ? true : false;

		if ( this.withPerm ) {
			this.EmployeesService.get( this.params_emp ).subscribe( data => {
				if ( !data.error ) {
					this.employees = data.results;
				}
				this.empLoader = false;
			});
		}

		this.get();
	}

  	get() {

		this.mainLoader = true;
		this.work_schedules = [];
		
		this.WorkSchedService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.work_schedules = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			
			this.mainLoader = false;
		});
	}
	  
	open( id ) {

		this.modalService
			.open( new ConfirmModal('Are you sure?', 'Are you sure to delete this?') )
			.onApprove( () => this.delete( id ) )
			.onDeny( () => '' )
	}

	delete( id ) {

		this.WorkSchedService.destroy( id ).subscribe( data => {
			if ( !data.error ) {
				this.isError = false;
				this.successMsg = data.msg;
			} else {
				this.isError = true;
				const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
				this.errorMsg = msg;
			}

			this.get();
		});

	}

	// THIS IS FOR PAGINATIONS
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}


}
