import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { WorkScheduleService } from 'src/app/services/workschedule.service';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-work-sched-create',
  templateUrl: './work-sched-create.component.html',
  styleUrls: ['./work-sched-create.component.css']
})
export class WorkSchedCreateComponent implements OnInit {

  work_sched: any = {
    with_break: 0,
    mon_enabled: 1,
    tue_enabled: 1,
    wed_enabled: 1,
    thu_enabled: 1,
    fri_enabled: 1,
    sat_enabled: 0,
    sun_enabled: 0,
  };

  params = {
		s: '',
  };

  employees = [];

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private WorkSchedService: WorkScheduleService,
    private EmployeesService: EmployeesService
  ) { }

  ngOnInit() {
    this.EmployeesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
        this.employees = data.results;
			} else {
				this.errorMsg = data.msg;
      }
      
		});
  }

  getFields() {

    const fields = {
      user_id: this.work_sched.employee_id,
      rate: this.work_sched.rate,
      with_break: this.work_sched.with_break,
      day: this.work_sched.day,
      time_in: this.work_sched.time_in,
      time_out: this.work_sched.time_out,
      time_in2: this.work_sched.time_in2,
      time_out2: this.work_sched.time_out2,
      nd_start: this.work_sched.nd_start,
      nd_end: this.work_sched.nd_end,
      mon_enabled: this.work_sched.mon_enabled,
      tue_enabled: this.work_sched.tue_enabled,
      wed_enabled: this.work_sched.wed_enabled,
      thu_enabled: this.work_sched.thu_enabled,
      fri_enabled: this.work_sched.fri_enabled,
      sat_enabled: this.work_sched.sat_enabled,
      sun_enabled: this.work_sched.sun_enabled
    };
    
    return fields;
  }

  save() {
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.WorkSchedService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
