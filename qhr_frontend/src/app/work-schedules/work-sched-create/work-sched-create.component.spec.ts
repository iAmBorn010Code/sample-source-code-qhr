import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkSchedCreateComponent } from './work-sched-create.component';

describe('WorkSchedCreateComponent', () => {
  let component: WorkSchedCreateComponent;
  let fixture: ComponentFixture<WorkSchedCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkSchedCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkSchedCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
