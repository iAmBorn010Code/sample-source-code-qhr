import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { WorkScheduleService } from 'src/app/services/workschedule.service';

@Component({
  selector: 'app-work-sched-update',
  templateUrl: './work-sched-update.component.html',
  styleUrls: ['./work-sched-update.component.css']
})
export class WorkSchedUpdateComponent implements OnInit {

  work_sched: any = {};
  work_sched_id;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
	saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private WorkSchedService: WorkScheduleService
  ) { }

  ngOnInit() {
    this.ActivatedRoute.params.subscribe( params => {
      this.work_sched_id = params['id'];
    });
    
    this.WorkSchedService.show( this.work_sched_id ).subscribe( data => {
      if ( !data.error ) {
        this.work_sched = data.results;
        console.log('work_sched', this.work_sched);
      }
      
      this.mainLoader = false;
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
      enabled: this.work_sched.enabled,
      with_break: this.work_sched.with_break,
      rate: this.work_sched.rate,
			time_in: this.work_sched.time_in,
      time_out: this.work_sched.time_out,
      time_in2: this.work_sched.time_in2,
      time_out2: this.work_sched.time_out2,
      nd_start: this.work_sched.nd_start,
      nd_end: this.work_sched.nd_end,
      mon_enabled: this.work_sched.mon_enabled,
      tue_enabled: this.work_sched.tue_enabled,
      wed_enabled: this.work_sched.wed_enabled,
      thu_enabled: this.work_sched.thu_enabled,
      fri_enabled: this.work_sched.fri_enabled,
      sat_enabled: this.work_sched.sat_enabled,
      sun_enabled: this.work_sched.sun_enabled
		};

    this.WorkSchedService.update( this.work_sched_id, fields ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
	}

}
