import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkSchedUpdateComponent } from './work-sched-update.component';

describe('WorkSchedUpdateComponent', () => {
  let component: WorkSchedUpdateComponent;
  let fixture: ComponentFixture<WorkSchedUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkSchedUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkSchedUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
