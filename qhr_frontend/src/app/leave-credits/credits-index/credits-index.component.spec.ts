import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditsIndexComponent } from './credits-index.component';

describe('CreditsIndexComponent', () => {
  let component: CreditsIndexComponent;
  let fixture: ComponentFixture<CreditsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
