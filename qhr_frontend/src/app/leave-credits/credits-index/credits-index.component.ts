import { Component, OnInit } from '@angular/core';
import { PaginatorService } from '../../paginator.service';
import { ActivatedRoute } from '@angular/router';
import { LeaveCreditsService } from 'src/app/services/leavecredits.service';
import { EmployeesService } from 'src/app/services/employees.service';
import { PageService } from 'src/app/page.service';


@Component({
  selector: 'app-credits-index',
  templateUrl: './credits-index.component.html',
  styleUrls: ['./credits-index.component.css']
})
export class CreditsIndexComponent implements OnInit {

  leaves = [];
  leaveTypes = [];
  employees = [];
  paginator: any;
	params = {
		limit: 10,
    page: 1,
    emp_id: ''
  };
  
  emp_params = {
    limit: 100,
    page: 1,
  }

  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  perm: any;
	withPerm = false;

  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private ActivatedRoute: ActivatedRoute,
    private LeaveCreditsService: LeaveCreditsService,
    private EmployeesService: EmployeesService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Employee Leave Credits');
    this.leaveTypes['SL'] = 'Sick Leave';
		this.leaveTypes['VL'] = 'Vacation Leave';
		this.leaveTypes['EL'] = 'Emergency Leave';
		this.leaveTypes['PL'] = 'Paternity Leave';
    this.leaveTypes['ML'] = 'Maternity Leave';
    
    this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(400) !== -1 ? true : false;

		if ( this.withPerm ) {
      this.getEmployees();
		}

    this.get();
    
  }

  get() {

    this.mainLoader = true;
    this.leaves = [];

    this.LeaveCreditsService.get( this.params ).subscribe( data => {

      if ( !data.error ) {

        let result = data.results;

        result.forEach(details => {
          details.leave_credits.forEach(credit => {
            
            let obj = {
              employee: details,
              _id: credit._id,
              type: credit.type,
              credit: credit.credit,
              used: credit.used,
              pending: credit.pending
            }
          
            this.leaves.push( obj );
          });
        });
      
        this.paginator = 
          this.PaginatorService.paginate( 
          data.total_count, 
          this.params.limit, 
          this.params.page, 
          5 
          );
      } else {
        this.isError = true;
        this.errorMsg = data.msg;
      }

      this.mainLoader = false;

    });

  }

  getEmployees() {
    this.EmployeesService.get( this.emp_params ).subscribe( data => {

			if ( !data.error ) {
				this.employees = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			} else {
				this.errorMsg = data.msg;
			}

			this.mainLoader = false;
		});
  }

}
