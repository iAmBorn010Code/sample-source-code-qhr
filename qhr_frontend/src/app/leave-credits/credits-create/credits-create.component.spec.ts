import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditsCreateComponent } from './credits-create.component';

describe('CreditsCreateComponent', () => {
  let component: CreditsCreateComponent;
  let fixture: ComponentFixture<CreditsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
