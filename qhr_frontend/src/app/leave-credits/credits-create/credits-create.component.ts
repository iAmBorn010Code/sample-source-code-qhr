import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { Router } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { LeaveCreditsService } from 'src/app/services/leavecredits.service';

@Component({
  selector: 'app-credits-create',
  templateUrl: './credits-create.component.html',
  styleUrls: ['./credits-create.component.css']
})
export class CreditsCreateComponent implements OnInit {

  leave_credit: any = {};
  employees = [];

  params = {
		s: '',
  };

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;
  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private EmployeesService: EmployeesService,
    private router: Router,
    private LeaveCreditsService: LeaveCreditsService
  ) { }

  ngOnInit() {
    this.EmployeesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.employees = data.results;
			} else {
				this.errorMsg = data.msg;
			}

			this.mainLoader = false;
		});
  }

  getFields() {

    const fields = {
      user_id: this.leave_credit.user_id,
      type: this.leave_credit.type,
      credit: this.leave_credit.credit,
    };
    
    return fields;
  }

  save() {
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.LeaveCreditsService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
