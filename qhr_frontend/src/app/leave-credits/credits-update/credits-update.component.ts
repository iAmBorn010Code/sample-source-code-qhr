import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { ActivatedRoute, Params } from '@angular/router';
import { LeaveCreditsService } from 'src/app/services/leavecredits.service';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-credits-update',
  templateUrl: './credits-update.component.html',
  styleUrls: ['./credits-update.component.css']
})
export class CreditsUpdateComponent implements OnInit {

  leave_credit: any = {};
  employee: any = {};
  emp_id = '';
  credit_id = '';
  leaveTypes = [];
  
	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';
	
	mainLoader = true;
	saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private LeaveCreditsService: LeaveCreditsService,
    private EmployeesService: EmployeesService
  ) { }

  ngOnInit() {

    this.leaveTypes['SL'] = 'Sick Leave';
		this.leaveTypes['VL'] = 'Vacation Leave';
		this.leaveTypes['EL'] = 'Emergency Leave';
		this.leaveTypes['PL'] = 'Paternity Leave';
		this.leaveTypes['ML'] = 'Maternity Leave';

    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.credit_id = params['id'];
    });

    this.ActivatedRoute.queryParams.subscribe( params => {
      this.emp_id = params['emp_id'];

      let emp_params = {
        f: '+leave_credits'
      };
      
      this.EmployeesService.show( this.emp_id, emp_params ).subscribe( data => {
        if ( !data.error ) {
          this.employee = data.results
          
          this.employee.leave_credits.forEach(details => {
            if ( details._id == this.credit_id ){
              this.leave_credit = details;
            }
          });

          console.log('leave credit', this.leave_credit);
          console.log('employee', this.employee );
				} else {
          this.isError = true;
          this.errorMsg = data.msg;
        }
        this.mainLoader = false;
      });
    });
  }

  getFields() {

    const fields = {
      _id: this.leave_credit._id,
      credit: this.leave_credit.credit,
    };

    return fields;
    
  }

  

  update() {

    this.saving = true;

    this.LeaveCreditsService.update( this.emp_id, this.getFields() ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }

      this.saving = false;
    });
  }

}
