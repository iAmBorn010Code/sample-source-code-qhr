import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditsUpdateComponent } from './credits-update.component';

describe('CreditsUpdateComponent', () => {
  let component: CreditsUpdateComponent;
  let fixture: ComponentFixture<CreditsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
