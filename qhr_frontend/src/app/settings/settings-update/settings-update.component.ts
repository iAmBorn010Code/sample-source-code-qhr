import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SettingsService } from '../../services/settings.service';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-settings-update',
  templateUrl: './settings-update.component.html',
  styleUrls: ['./settings-update.component.css']
})
export class SettingsUpdateComponent implements OnInit {

  setting: any = {};

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
	saving = false;

  constructor(
		private PageService: PageService,
    private TextHelperService: TextHelperService,
    private SettingsService: SettingsService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.PageService.pageName.next('Settings');
    console.log('called init personal');

    this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.SettingsService.show( ).subscribe( data => {
				if ( !data.error ) {
          this.setting = data.results;
				}
				this.mainLoader = !this.mainLoader;
			});
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			field1: this.setting.field1,
			field2: this.setting.field2,
			field3: this.setting.field3,
			field4: this.setting.field4
		};

		console.log( fields );

 	 	this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.SettingsService.update( params['id'], fields ).subscribe( data => {
				this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
						this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
				}
			});
		});
	}

}
