import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SettingsService } from '../../services/settings.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-settings-show',
  templateUrl: './settings-show.component.html',
  styleUrls: ['./settings-show.component.css']
})
export class SettingsShowComponent implements OnInit {

  setting: any;
  id;

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;

  constructor(
    private PageService: PageService,
    private SettingsService: SettingsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    console.log('called init on settings');
    this.PageService.pageName.next('Company Settings');
    this.show();
  }

  show() {

    this.SettingsService.show().subscribe( data => {

      if ( !data.error ) {
       this.setting = data.results;
       console.log(this.setting);
      } else {

        console.log( data.msg );
      }
      this.mainLoader = false;
    });

  }

}
