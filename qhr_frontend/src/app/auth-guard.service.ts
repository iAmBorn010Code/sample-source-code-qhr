import { Injectable } from '@angular/core';
import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

	constructor( private router: Router ) { }

	canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {

		if ( !localStorage.getItem( 'token' ) ) {

			this.router.navigate(['/login']);
			return false;
		} else {

			const user = JSON.parse( localStorage.getItem('user') );
			return true;
		}
	}
}
