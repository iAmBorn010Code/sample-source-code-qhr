import { Component, OnInit } from '@angular/core';
import { PayrollsService } from '../../services/payrolls.service';
import { Router, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';
import { PayrollGroupsService } from 'src/app/services/payrollgroups.service';

@Component({
  selector: 'app-payrolls-create',
  templateUrl: './payrolls-create.component.html',
  styleUrls: ['./payrolls-create.component.css']
})
export class PayrollsCreateComponent implements OnInit {

  payroll: any = {
    name: '',
		pay_period_from: '',
    pay_period_to: '',
    payroll_group_id: null,
    salary_type: 'bi-monthly'
  };

  payroll_groups = [];
  
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private PayrollsService: PayrollsService,
    private PayrollGroupsService: PayrollGroupsService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Payrolls');
    console.log('init called');
    this.PayrollGroupsService.get( ).subscribe( data => {

      if ( !data.error ) {
        this.payroll_groups = data.results;
      } else {
        this.errorMsg = data.msg;
      }
      // this.mainLoader = false;
    });
  }

  getFields() {

    const fields = {
      name: this.payroll.name,
      salary_type: this.payroll.salary_type,
			pay_period_from: this.payroll.pay_period_from,
      pay_period_to: this.payroll.pay_period_to,
      payroll_group_id: this.payroll.payroll_group_id
    };
    
    return fields;
  }

  save() {
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.PayrollsService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
