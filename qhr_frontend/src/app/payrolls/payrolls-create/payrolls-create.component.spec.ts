import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollsCreateComponent } from './payrolls-create.component';

describe('PayrollsCreateComponent', () => {
  let component: PayrollsCreateComponent;
  let fixture: ComponentFixture<PayrollsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
