import { Component, OnInit } from '@angular/core';
import { PaginatorService } from '../../paginator.service';
import { ActivatedRoute } from '@angular/router';
import { PayrollContributionsService } from 'src/app/services/payrollcontributions.service';

@Component({
  selector: 'app-contributions',
  templateUrl: './contributions.component.html',
  styleUrls: ['./contributions.component.css']
})
export class ContributionsComponent implements OnInit {

  contributions = [];
  employees = [];
  paginator: any;
	params = {
		limit: 10,
    page: 1,
    contributions: true,
    payroll_id: '',
    emp_id: ''
  };
  
  emp_params = {
    limit: 100,
    page: 1,
    payroll_id: '',
  }

  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  perm: any;
  withPerm = false;


  constructor(
    private PaginatorService: PaginatorService,
    private ActivatedRoute: ActivatedRoute,
    private PayrollContributionsService: PayrollContributionsService
  ) { }

  ngOnInit() {

    this.ActivatedRoute.queryParams.subscribe( params => {
      this.params.payroll_id = params['payroll_id'];
      this.emp_params.payroll_id = params['payroll_id'];
      this.get();
      this.getEmployees();
    });  

  }

  get(){

    this.contributions = [];
    this.mainLoader = true;

    console.log('here', this.params);

    this.PayrollContributionsService.get( this.params ).subscribe( data => {

      if ( !data.error ) {

        let payroll_details = data.results;

        payroll_details.forEach(details => {
          details.contributions.forEach(contributions => {
            
            let obj = {
              payroll_detail_id: details._id,
              employee: details.employee,
              name: contributions.name,
              ee_amt: contributions.ee_amt,
              er_amt: contributions.er_amt,
              ec_amt: contributions.ec_amt,
              ee_status: contributions.ee_status,
              er_status: contributions.er_status,
              ec_status: contributions.ec_status,
              total: contributions.total,
              specify_other: contributions.specify_other,
              _id: contributions._id
            }
          
            this.contributions.push( obj );
          });
        });
      
        this.paginator = 
          this.PaginatorService.paginate( 
          data.total_count, 
          this.params.limit, 
          this.params.page, 
          5 
          );
      } else {
        this.isError = true;
        this.errorMsg = data.msg;
      }

      this.mainLoader = false;

    });
  }

  getEmployees(){

    this.PayrollContributionsService.get( this.emp_params ).subscribe( data => {

      if ( !data.error ) {

        let details = data.results;

        details.forEach(details => {
          this.employees.push(details.employee);
        });

        console.log('details', details);
        console.log('employees', this.employees);
        
      } else {
        this.isError = true;
        this.errorMsg = data.msg;
      }

      this.mainLoader = false;

    });
  }


}
