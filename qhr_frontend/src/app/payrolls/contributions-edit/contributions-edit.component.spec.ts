import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributionsEditComponent } from './contributions-edit.component';

describe('ContributionsEditComponent', () => {
  let component: ContributionsEditComponent;
  let fixture: ComponentFixture<ContributionsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributionsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributionsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
