import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { ActivatedRoute, Params } from '@angular/router';
import { PayrollContributionsService } from 'src/app/services/payrollcontributions.service';

@Component({
  selector: 'app-contributions-edit',
  templateUrl: './contributions-edit.component.html',
  styleUrls: ['./contributions-edit.component.css']
})
export class ContributionsEditComponent implements OnInit {

  contribution: any = {};
  payroll_detail_id = '';
  contribution_id = '';
  payroll_id= '';
  
	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';
	
	mainLoader = true;
	saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private PayrollContributionsService: PayrollContributionsService
  ) { }

  ngOnInit() {

    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.contribution_id = params['id'];
    });

    this.ActivatedRoute.queryParams.subscribe( params => {
      this.payroll_detail_id = params['payroll_detail_id'];
      this.payroll_id = params['payroll_id'];

      this.PayrollContributionsService.show( this.payroll_detail_id ).subscribe( data => {
        if ( !data.error ) {
          let payroll_details = data.results
          
          payroll_details.contributions.forEach(details => {
            if ( details._id == this.contribution_id ){
              this.contribution = details;
            }
          });

				}
        this.mainLoader = false;
      });
    });

    
  }

  getFields() {

    const fields = {
      _id: this.contribution._id,
      ee_status: this.contribution.ee_status,
      er_status: this.contribution.er_status,
      ec_status: this.contribution.ec_status,
      name: this.contribution.name,
    };

    return fields;
    
  }

  

  update() {

    this.saving = true;

    this.PayrollContributionsService.update( this.payroll_detail_id, this.getFields() ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }

      this.saving = false;
    });
  }



}
