import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollsUpdateDetailsComponent } from './payrolls-update-details.component';

describe('PayrollsUpdateDetailsComponent', () => {
  let component: PayrollsUpdateDetailsComponent;
  let fixture: ComponentFixture<PayrollsUpdateDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollsUpdateDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollsUpdateDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
