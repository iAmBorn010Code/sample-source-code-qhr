import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { EmployeesService } from '../../services/employees.service';
import { PayrollsService } from '../../services/payrolls.service';
import { PayrollDetailsService } from '../../services/payroll-details.service';
import { DeductionsService } from '../../services/deductions.service';
import { EarningsService } from '../../services/earnings.service';
import { TextHelperService } from '../../texthelper.service';
import { DepartmentsService } from 'src/app/services/departments.service';
import { DatePipe } from '@angular/common';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';

import { PayrollCalculationService } from '../../services/payrollcalculation.service';


@Component({
	selector: 'app-payrolls-update-details',
	templateUrl: './payrolls-update-details.component.html',
	styleUrls: ['./payrolls-update-details.component.css']
})
export class PayrollsUpdateDetailsComponent implements OnInit {


	PayrollDetail : any;


	employees = [];
	department: any;
	selectedEmployee : any;
    payrollId = '';
    payrollDetailId = null;
	payroll : any = [];
	deductions : any = [];
	earnings : any = [];
	contributions : any = [];

	payrollStatus = '';

	addtlDeduction : any = {
		desc : '',
		amount : 0,
	};
	addtlIncome : any = {
		desc : '',
		amount : 0
	};
	contribution : any = {
		name: '',
		ee_amt: 0,
		er_amt: 0,
		other_specify: ''
	};

	paginator: any;
	params = {
		limit: 1000,
		page: 1,
		payroll_id: ''
	};

	emp_params = {
		limit: 1000,
		page: 1,
		salary_type: ''
	}
   
	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	errorDeductionMsg = '';
	errorIncomeMsg = '';
	errorContributionMsg = '';

	// isTransactionError: boolean = false;
	isCloseTransactionMsg:boolean;
	errorTransactionMsg = '';

    // --- LOADERS
	employeesLoader = true;
	deductionsLoader = true;
	earningsLoader = true;
    contributionsLoader = true;
	payrollDetailsLoader = true;
	mainLoader = true;
	addingToPayrollLoader = false;
    saving = false;

	employeeIsSelected = false;

	isAllowed = true;
	isAdded = false;

	index: number;
	user_id: string;
	type: any = [];

	withholdingTax = 0;
	withTax: boolean;
    withBenefits: boolean;
    withNightDiff: boolean;
    withOvertime: boolean;

	payrollParams: any;
	pay_period_from = '';
	pay_period_to = '';

	getEmployeesFromApprovedPayroll: any;


    // --- INCOMES
    updatingAddtlIncome: boolean = false;

    // --- DEDUCTIONS
    updatingAddtlDeduction: boolean = false;

    // --- CONTRIBUTIONS
    updatingContribution: boolean = false;
    
    constructor(
        private PageService: PageService,
        private PaginatorService: PaginatorService,
        private EmployeesService: EmployeesService,
        private PayrollDetailsService: PayrollDetailsService,
        private PayrollsService: PayrollsService,
        private DeductionsService: DeductionsService,
        private EarningsService: EarningsService,
        private ActivatedRoute: ActivatedRoute,
        private TextHelperService: TextHelperService,
        private DepartmentsService: DepartmentsService,
        private DatePipe: DatePipe,
        private modalService: SuiModalService,
        private PayrollCalculationService: PayrollCalculationService,

    ) { }

    ngOnInit() {
        this.PageService.pageName.next('Payroll Details');
        this.ActivatedRoute.parent.params.forEach(( params: Params ) => {

            // Initialize Payroll Calculation
            this.PayrollDetail = this.PayrollCalculationService
                .initialize()
                .getPayroll();

            this.payrollId = params['id'];
            this.showPayrollDetails();
            this.getEmployees();
        });
    }

    // Only allow user to edit or add employee to payroll details if payroll.status is pending
    showPayrollDetails(){
        this.PayrollsService.show( this.payrollId ).subscribe( data => {
            if ( !data.error ) {
                this.payroll = data.results;
                // console.log('this.payroll', this.payroll);
                this.pay_period_from = this.DatePipe.transform(this.payroll.pay_period_from,'yyyy-MM-dd');
                this.pay_period_to = this.DatePipe.transform(this.payroll.pay_period_to,'yyyy-MM-dd');
                this.payrollStatus = this.payroll.status;
                this.isAllowed = this.payrollStatus == 'pending';
            }
            this.mainLoader = !this.mainLoader;
        });
    }

    getPayrollDetails( employee : any ) {

        // Reset Payroll Calculation and Initialize
        this.PayrollCalculationService.initialize();
        
        this.deductions = [];
        this.earnings = [];
        this.contributions = [];
        this.payrollDetailsLoader = true;
        this.earningsLoader = true;
        this.deductionsLoader = true;
        this.isAdded = false;
        this.selectedEmployee = employee;
        // this.isTransactionError = false;
        this.contributionsLoader = true;

        this.DepartmentsService.show( this.selectedEmployee.department_id ).subscribe( data => {
            if ( !data.error ) {
                this.department = data.results;
            } else {
                console.log( data.msg );
            }
        });

        this.PayrollDetailsService.getEmployeePayroll( { user_id: this.selectedEmployee._id, payroll_id: this.payrollId } ).subscribe( data => {

            if ( !data.error ) {
                let payrollDetailResult = data.results;
                // console.log('payrollDetailResult:', payrollDetailResult);
                if ( payrollDetailResult )  {
                    this.PayrollDetail = this.PayrollCalculationService
                        .setDetails(payrollDetailResult)
                        .calculatePayroll()
                        .getPayroll();
                    this.isAdded = true;
                    this.payrollDetailId = payrollDetailResult._id;
                    // console.log('NEW DETAILS', this.PayrollDetail );
                    this.earningsLoader = false;
                    this.deductionsLoader = false;
                    this.contributionsLoader = false;
                }
            } else {
                // User not yet added in the payrol. Processing getting payroll details instead.
                this.calculateEmployeePayroll();
                console.log('EMPLOYEE NOT YET ADDED SHOW DEFAULT VALUES INSTEAD' );
            }
        });

        this.employeeIsSelected = true;
    }

    calculateEmployeePayroll() {
        let calcParams = { 
            from: this.pay_period_from, 
            to: this.pay_period_to,
            salary_type: this.payroll.salary_type,
            payroll_id: this.payrollId
        };


        // console.log('calcParams', calcParams);
        this.PayrollsService.calculatePayroll( this.selectedEmployee._id, calcParams ).subscribe( data => {
            if ( !data.error ) {
                let payrollDetailResult = data.results;
                // console.log('No Payroll Details Found: Calculate insteaed', payrollDetailResult );
                this.PayrollDetail = this.PayrollCalculationService
                    .setDetails(payrollDetailResult)
                    .calculatePayroll()
                    .getPayroll();
                // alert('Not found in payroll yet.');
                // console.log('OK ang data NEW:', this.PayrollDetail);
            } else {
                alert('Unable to calculate payroll at this time. Please try again later.');
                console.log('ERROR: ', data.msg );
            }
            
            this.earningsLoader = false;
            this.deductionsLoader = false;
            this.contributionsLoader = false;
            
        });
    }

	getEmployees() {
		
		this.employees = [];
		this.employeesLoader = true;

		if ( this.payroll.status != 'approved' ) {

			this.emp_params.salary_type = this.payroll.type; 
			this.EmployeesService.get( this.emp_params ).subscribe( data => {

				if ( !data.error ) {
                    this.employees = data.results;

                    this.employees.forEach(employee => {
                        employee.added = false;
                        this.PayrollDetailsService.getEmployeePayroll( { user_id: employee._id, payroll_id: this.payrollId } ).subscribe( data => {

                            let payrollDetailResult = data.results;
                            if ( payrollDetailResult.employee == employee._id )  {
                                employee.added = true;
                            } 
                            
                        });

                    });

					this.paginator = 
						this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
						);
				}
                this.employeesLoader = false;
			});
		} else {

			this.params.payroll_id = this.payrollId;
			
			this.PayrollDetailsService.get( this.params ).subscribe( data => {

				if ( !data.error ) {
					this.getEmployeesFromApprovedPayroll = data.results;

					this.getEmployeesFromApprovedPayroll.forEach(emp => {
						this.employees.push(emp.employee);
					});

				
					this.paginator = 
						this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
						);
				}
				this.employeesLoader = false;
			});
		}
	}

	deSelectEmployee() {
		this.selectedEmployee = null;
        this.employeeIsSelected = false;
        this.successMsg = '';
	    this.errorMsg = '';
        this.getEmployees();
    }
    
    // Add new deduction or earning
    updateAddtl( what : string, type: string = 'add' ){

        if ( what === 'deduction' ) {
            this.updatingAddtlDeduction = true;
        } else if ( what === 'income' ) {
            this.updatingAddtlIncome = true;
        }  else if ( what === 'contributions' ) {
            this.updatingContribution = true;
        }
    }

    cancelUpdateAddtl( what : string ){

        if ( what === 'deduction' ) {
            this.updatingAddtlDeduction = false;
        } else if ( what === 'income' ) {
            this.updatingAddtlIncome = false;
        } else if ( what === 'contributions' ) {
            this.updatingContribution = false;
        }

        this.errorDeductionMsg = '';
        this.errorIncomeMsg = '';
        this.errorContributionMsg = '';
        
    }

    removeAddtl( what : string, desc : string, other_specify: string ){
        if ( what === 'deduction' ) {
            this.PayrollDetail = this.PayrollCalculationService
                .removeAddtlDeduction(desc)
                .calculatePayroll()
                .getPayroll();
        } else if ( what === 'income' ) {
            this.PayrollDetail = this.PayrollCalculationService
                .removeAddtlIncome(desc)
                .calculatePayroll()
                .getPayroll();
        } else if ( what === 'contribution' ) {
            this.PayrollDetail = this.PayrollCalculationService
                .removeContribution(desc, other_specify )
                .calculatePayroll()
                .getPayroll();
        }
    }
 
// =*****************************************************************************************
// ************************************* INCOMES *****************************************
// =*****************************************************************************************

	storeAddtlIncome() {

		this.errorIncomeMsg = '';

		if ( this.addtlIncome.desc.length < 2 ) {
			this.errorIncomeMsg = 'Description must have at least 2 characters.';
			return;
		} 

		if ( this.addtlIncome.amount <= 0 ) {
			this.errorIncomeMsg = 'Amount value is not valid.';
			return;
        } 

		this.PayrollDetail = this.PayrollCalculationService
			.addAddtlIncome(this.addtlIncome)
			.calculatePayroll()
			.getPayroll();

		this.addtlIncome = {
			desc: '',
			amount: 0
        }
        
        this.updatingAddtlIncome = false;
	}
// =*****************************************************************************************
// ************************************* DEDUCTIONS *****************************************
// =*****************************************************************************************

	storeAddtlDeduction() {

		this.errorDeductionMsg = '';

		if ( this.addtlDeduction.desc.length < 2 ) {
			this.errorDeductionMsg = 'Description must have at least 2 characters.';
			return;
		} 

		if ( this.addtlDeduction.amount <= 0 ) {
			this.errorDeductionMsg = 'Amount value is not valid.';
			return;
		} 

		this.PayrollDetail = this.PayrollCalculationService
			.addAddtlDeduction(this.addtlDeduction)
			.calculatePayroll()
			.getPayroll();

		this.addtlDeduction = {
			desc: '',
			amount: 0
        }
        
        this.updatingAddtlDeduction = false;
    }

// =*****************************************************************************************
// ************************************* CONTRIBUTIONS *****************************************
// =*****************************************************************************************

    storeContribution() {

        this.errorContributionMsg = '';

        // TODO: Allow only sepcified contribution. ie: sss, pagibig, etc.
		if ( this.contribution.name.length < 2 ) {
			this.errorContributionMsg = 'Name must have at least 2 characters.';
			return;
        } 
        
        if ( this.contribution.name === 'others' && this.contribution.other_specify.length < 2 ) {
            this.errorContributionMsg = 'Other Specify must have at least 2 characters.';
			return;
        }

		if ( this.contribution.ee_amt <= 0 ) {
			this.errorContributionMsg = 'EE value is not valid.';
			return;
        }
        
        if ( this.contribution.er_amt < 0 ) {
			this.errorContributionMsg = 'ER value is not valid.';
			return;
		} 

		this.PayrollDetail = this.PayrollCalculationService
			.addContribution(this.contribution)
			.calculatePayroll()
			.getPayroll();

        this.contribution = {
            name: '',
            other_specify: '',
            ee_amt: 0,
            er_amt: 0
        }

        this.updatingContribution = false;

    }

    changeGeneralOptions( e ) {

		this.PayrollDetail = this.PayrollCalculationService
			.set( e.target.name, e.target.checked )
            .calculatePayroll()
            .getPayroll();
	}



    addPayrollDetails(e) {

        this.PayrollDetail = this.PayrollCalculationService
            .calculatePayroll()
            .getPayroll();

        this.PayrollDetail.payrollId = this.payrollId;
        this.PayrollDetail.employeeId = this.selectedEmployee._id;
        this.PayrollDetail.from = this.pay_period_from;
        this.PayrollDetail.to = this.pay_period_to;

        // console.log('this.PayrollDetail', this.PayrollDetail);

        this.successMsg = '';
        this.errorMsg = '';
        this.isCloseMsg = false;
        this.saving = true;
        

        this.PayrollDetailsService.store( this.PayrollDetail ).subscribe( data => {
            this.saving = false;
            
            if ( !data.error ) {
                this.isError = false;
                this.successMsg = data.msg;
                // console.log(data);
            } else {
                this.isError = true;
                const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
                this.errorMsg = msg;
            }
        });
    }

    updatePayrollDetails(e) {

        alert('ok');

        this.PayrollDetail = this.PayrollCalculationService
            .calculatePayroll()
            .getPayroll();

        
        this.PayrollDetail.payrollId = this.payrollId;
        this.PayrollDetail.payrollDetailId = this.payrollDetailId;
        this.PayrollDetail.employeeId = this.selectedEmployee._id;
        this.PayrollDetail.from = this.pay_period_from;
        this.PayrollDetail.to = this.pay_period_to;

        // console.log('this.PayrollDetail', this.PayrollDetail);

        this.successMsg = '';
        this.errorMsg = '';
        this.isCloseMsg = false;
        this.saving = true;

        this.PayrollDetailsService.update( this.payrollDetailId, this.PayrollDetail ).subscribe( data => {
            this.saving = false;
            
            if ( !data.error ) {
                this.isError = false;
                this.successMsg = data.msg;
                this.PayrollDetail = this.PayrollCalculationService
                        .setDetails(data.results)
                        .calculatePayroll()
                        .getPayroll();
            } else {
                this.isError = true;
                const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
                this.errorMsg = msg;
            }
        });
    }

    onContributionChange(event): void {  // event will give you full breif of action
       
        this.contribution.ee_amt = 0;
        this.contribution.er_amt = 0;
        this.contribution.ec_amt = 0;
        this.contribution.type = 1;

        if ( this.contribution.name ==='sss' ) {
            
            this.contribution.ee_amt = this.PayrollDetail.sss_ee;
            this.contribution.er_amt = this.PayrollDetail.sss_er;
            this.contribution.ec_amt = this.PayrollDetail.totalBasicPay < 14750 ? 10 : 30;
   
        }
        
        if (  this.contribution.name ==='philhealth' ) {
            this.contribution.ee_amt = this.PayrollDetail.philhealth_ee;
            this.contribution.er_amt = this.PayrollDetail.philhealth_er;
        }

        if (  this.contribution.name ==='pagibig' ) {
            this.contribution.ee_amt = this.PayrollDetail.pagibig_ee;
            this.contribution.er_amt = this.PayrollDetail.pagibig_er;
        }

        if (  this.contribution.name ==='gsis' ) {
            this.contribution.ee_amt = this.PayrollDetail.gsis_ee;
            this.contribution.er_amt = this.PayrollDetail.gsis_er;
        }
    }

    onTermChange(event): void {  // event will give you full breif of action

        if ( this.contribution.name ==='sss' ) {
            this.contribution.ee_amt = this.PayrollDetail.sss_ee / this.contribution.term;
            this.contribution.er_amt = this.PayrollDetail.sss_er / this.contribution.term;
            this.contribution.ec_amt = (this.PayrollDetail.totalBasicPay < 14750 ? 10 : 30 / this.contribution.term);
        }
        
        if (  this.contribution.name ==='philhealth' ) {
            this.contribution.ee_amt = this.PayrollDetail.philhealth_ee / this.contribution.term;
            this.contribution.er_amt = this.PayrollDetail.philhealth_er / this.contribution.term ;
        }

        if (  this.contribution.name ==='pagibig' ) {
            this.contribution.ee_amt = this.PayrollDetail.pagibig_ee / this.contribution.term;
            this.contribution.er_amt = this.PayrollDetail.pagibig_er / this.contribution.term;
        }

        if (  this.contribution.name ==='gsis' ) {
            this.contribution.ee_amt = this.PayrollDetail.gsis_ee / this.contribution.term;
            this.contribution.er_amt = this.PayrollDetail.gsis_er / this.contribution.term;
        }
    }
}
