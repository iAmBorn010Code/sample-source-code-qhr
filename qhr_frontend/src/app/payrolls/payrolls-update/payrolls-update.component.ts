import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PayrollsService } from '../../services/payrolls.service';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-payrolls-update',
  templateUrl: './payrolls-update.component.html',
  styleUrls: ['./payrolls-update.component.css']
})
export class PayrollsUpdateComponent implements OnInit {

  payroll: any = {
    name: '',
		pay_period_from: '',
		pay_period_to: '',
  };
  
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
		private PayrollsService: PayrollsService,
		private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.PageService.pageName.next('Payrolls');
    this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.PayrollsService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
          this.payroll = data.results;
				}
			});
    });

  }

}
