import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollsUpdateComponent } from './payrolls-update.component';

describe('PayrollsUpdateComponent', () => {
  let component: PayrollsUpdateComponent;
  let fixture: ComponentFixture<PayrollsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
