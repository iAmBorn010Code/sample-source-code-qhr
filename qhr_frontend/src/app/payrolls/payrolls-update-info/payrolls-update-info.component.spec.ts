import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollsUpdateInfoComponent } from './payrolls-update-info.component';

describe('PayrollsUpdateInfoComponent', () => {
  let component: PayrollsUpdateInfoComponent;
  let fixture: ComponentFixture<PayrollsUpdateInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollsUpdateInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollsUpdateInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
