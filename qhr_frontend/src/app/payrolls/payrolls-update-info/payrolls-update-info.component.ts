import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PayrollsService } from '../../services/payrolls.service';
import { TextHelperService } from '../../texthelper.service';
import { PayrollGroupsService } from 'src/app/services/payrollgroups.service';

@Component({
  selector: 'app-payrolls-update-info',
  templateUrl: './payrolls-update-info.component.html',
  styleUrls: ['./payrolls-update-info.component.css']
})
export class PayrollsUpdateInfoComponent implements OnInit {

	payroll: any = {};
	
	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';
	
	payroll_groups = [];

	mainLoader = true;
	saving = false;

	constructor(
		private TextHelperService: TextHelperService,
		private PayrollsService: PayrollsService,
		private ActivatedRoute: ActivatedRoute,
		private PayrollGroupsService: PayrollGroupsService,
	) { }

	ngOnInit() {
		this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.PayrollsService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
					this.payroll = data.results;
					console.log('payroll', this.payroll);
				}
				this.mainLoader = !this.mainLoader;
			});
		});

		this.PayrollGroupsService.get( ).subscribe( data => {

			if ( !data.error ) {
			  this.payroll_groups = data.results;
			} else {
			  this.errorMsg = data.msg;
			}
			// this.mainLoader = false;
		  });
	}


    update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			name: this.payroll.name,
			salary_type: this.payroll.salary_type,
			pay_period_from: this.payroll.pay_period_from,
			pay_period_to: this.payroll.pay_period_to,
			payroll_group_id: this.payroll.payroll_group_id,
		};

 	 	this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.PayrollsService.update( params['id'], fields ).subscribe( data => {
				this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
					this.isError = true;
					const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
					this.errorMsg = msg;
				}
			});
		});
	}

}
