import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollsIndexComponent } from './payrolls-index.component';

describe('PayrollsIndexComponent', () => {
  let component: PayrollsIndexComponent;
  let fixture: ComponentFixture<PayrollsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
