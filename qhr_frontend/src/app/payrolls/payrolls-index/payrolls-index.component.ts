import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { PayrollsService } from '../../services/payrolls.service';
import { PayrollGroupsService } from '../../services/payrollgroups.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-payrolls-index',
  templateUrl: './payrolls-index.component.html',
  styleUrls: ['./payrolls-index.component.css']
})
export class PayrollsIndexComponent implements OnInit {

  	payrolls = [];
	paginator: any;
	params = {
		limit: 8,
		page: 1,
		status: '',
		payroll_group_id: ''
	};

	mainLoader = true;

	payroll_group: any = {};

	hasGroup = false;
  constructor(
    private PageService: PageService,
  	private PaginatorService: PaginatorService,
	private PayrollsService: PayrollsService,
	private PayrollGroupsService: PayrollGroupsService,
	private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Payrolls');
	this.ActivatedRoute.queryParams.subscribe( params => {
		var group_id = params['payroll_group_id'];
		if ( group_id ){
			this.params.payroll_group_id = group_id;
			this.getGroup(group_id);
			this.hasGroup = true;
		} else {
			this.params.payroll_group_id = '';
			this.payroll_group = {};
			this.hasGroup = false;
		}
		this.get();
	});
	
  }

  get() {
		this.mainLoader = true;
		this.payrolls = [];
		this.PayrollsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.payrolls = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( this.paginator );
			}
			this.mainLoader = false;
		});
	}
	  
	change_status(status) {
		this.params.status = status;
		this.get();
	}

	getGroup( id ){
		this.PayrollGroupsService.show( id ).subscribe( data => {
			console.log('data',data);
			if ( !data.error ) {
				this.payroll_group = data.results;
				console.log(this.payroll_group);
				this.mainLoader = false; 
			}
		});
	}

	//pagination
  nextPage() {
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
