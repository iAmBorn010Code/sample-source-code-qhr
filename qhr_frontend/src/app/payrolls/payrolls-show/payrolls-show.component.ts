import { Component, OnInit } from '@angular/core';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { PageService } from '../../page.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PayrollsService } from '../../services/payrolls.service';
import { PayrollGroupsService } from '../../services/payrollgroups.service';
import { TextHelperService } from '../../texthelper.service';
import { PayrollApproversService } from 'src/app/services/payrollapprovers.service';

@Component({
  selector: 'app-payrolls-show',
  templateUrl: './payrolls-show.component.html',
  styleUrls: ['./payrolls-show.component.css']
})
export class PayrollsShowComponent implements OnInit {

  payroll: any = {
    name: '',
    pay_period_from: '',
    pay_period_to: '',
  };
  status: any;
  id;
  
  mainLoader = true; 

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  modal_msg = '';
  success_msg = '';

  payroll_group_id = '';
  withPerm = false;

  params = {
		limit: 10,
    page: 1,
    w: 'payroll_approver'
	};

  constructor(
    private PageService: PageService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private PayrollsService: PayrollsService,
    private PayrollGroupsService: PayrollGroupsService,
    private ActivatedRoute: ActivatedRoute,
    private PayrollApproversService: PayrollApproversService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Payrolls');
    this.checkPermission();
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.id = params['id'];
      this.show( this.id );
    });

  }

  show( id ) {

    this.mainLoader = true;

     this.PayrollsService.show( this.id ).subscribe( data => {
        if ( !data.error ) {
          this.payroll = data.results;
        }
        this.mainLoader = false;
      });
  }

  checkPermission() {
    this.PayrollApproversService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
        this.withPerm = data.total_count > 0 ? true : false;
			} else {
        this.errorMsg = data.msg;
      }
			this.mainLoader = false;
    });
    
  }

  open( status ) {

    if ( status == 'approved' ) {
      this.modal_msg = 'Are you sure you want to approve this payroll?';
      this.success_msg = 'Payroll was approved.';
    } else if ( status == 'disapproved' ) {
      this.modal_msg = 'Are you sure you want to disapprove this payroll?';
      this.success_msg = 'Payroll was disapproved.';
    } else if ( status == 'deleted' ) {
      this.modal_msg = 'Are you sure you want to delete this payroll?';
      this.success_msg = 'Payroll was deleted.';
    }

    this.modalService
        .open( new ConfirmModal('Are you sure?', this.modal_msg) )
        .onApprove( () => this.update(status) )
        .onDeny( () => '' )
  }

  update(status) {

    const fields = {
      status: status
    };
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    
    this.PayrollsService.update( this.id, fields ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = this.success_msg
        this.payroll = data.results;
        this.show(this.id);

        if ( status == 'approved' && this.payroll.payroll_group_id) {
          this.calculateGroupTotal();
        }
       
        
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

  }

  calculateGroupTotal(){

    this.payroll_group_id = this.payroll.payroll_group_id;
    const fields = {
      payroll_id: this.payroll._id
    };

    this.PayrollGroupsService.addToGroup( this.payroll_group_id, fields ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = this.success_msg
        // this.payroll = data.results;
        // this.show(this.id);
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }
    

 
}
