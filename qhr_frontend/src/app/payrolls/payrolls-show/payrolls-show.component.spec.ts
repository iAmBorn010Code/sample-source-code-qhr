import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollsShowComponent } from './payrolls-show.component';

describe('PayrollsShowComponent', () => {
  let component: PayrollsShowComponent;
  let fixture: ComponentFixture<PayrollsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
