import { Pipe } from '@angular/core';
import { environment } from '../../environments/environment';

@Pipe({
  name:"images"
})

export class ImagesPipe { 

    company = JSON.parse( localStorage.getItem('company') );

    transform( filename: string, params : any): string {

        let imageBaseURL = environment.bucketFileUrl;
        let imgSrc = '';

        // console.log( params );

        let imageDirs = {
            document : 'companies/' + this.company._id + '/users/' + params.id + '/documents/' + filename,
            user_avatar : 'companies/' + this.company._id + '/users/' + params.id + '/avatar/' + filename,
            company_logo : 'companies/' + params.id + '/logo/' + filename,
            branch_logo: 'companies/' + this.company._id + '/branches/' + params.id + '/logo/' + filename,
            qr_code: 'companies/' + this.company._id + '/users/' + params.id + '/qr/' + filename,
        };

        if (! filename ) {
            if ( params.type === 'company_logo' )
                imgSrc = '/assets/images/QloudHR-logo.png';
            else if ( params.type === 'branch_logo') 
                imgSrc = '/assets/images/defaults/branch-default.png';
            else if (params.type === 'user_avatar') {
                imgSrc = params.gender === 'female'  ?  
                    '/assets/images/avatar/female-avatar.png' : '/assets/images/avatar/male-avatar.png';
            } else 
                imgSrc = '/assets/images/defaults/photo.png';
        } else {
            imgSrc = imageBaseURL + imageDirs[params.type];
        }
        
        return imgSrc;
    }

}