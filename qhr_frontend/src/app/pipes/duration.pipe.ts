import { Pipe } from '@angular/core';

import * as moment from 'moment';

@Pipe({
  name:"duration"
})


export class DurationPipe { 
    transform(num: number, format: string, base: string = 'sec'): any {

      let H = 0, m = 0, s = 0;

      if ( base === 'sec' ) {
        H = num > 3600 ? Math.floor( num / 3600 ) : 0;
        m = Math.floor( num / 60 );
        s = num % 60;
        m = m - (H * 60 );
      } else if ( base === 'min' ) {
        H = num > 60 ? Math.floor( num / 60 ) : 0;
        m = num;
        s = ( num * 60 ) % 60;
        m = m - (H * 60 );
      }

      format = format.replace("HH", (H+'').padStart(2, '0') );
      format = format.replace("mm", (m+'').padStart(2, '0'));
      format = format.replace("ss", (s+'').padStart(2, '0'));

      return format;
    }

}