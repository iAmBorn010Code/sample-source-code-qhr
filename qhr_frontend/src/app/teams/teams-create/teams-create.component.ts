import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { TeamsService } from 'src/app/services/teams.service';

@Component({
  selector: 'app-teams-create',
  templateUrl: './teams-create.component.html',
  styleUrls: ['./teams-create.component.css']
})
export class TeamsCreateComponent implements OnInit {

  team: any = {};

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private TeamsService: TeamsService
  ) { }

  ngOnInit() {

  }

  getFields() {

    const fields = {
      name: this.team.name,
      status: this.team.status,
    };
    
    return fields;
  }

  save() {

    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.TeamsService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
