import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TeamsService } from 'src/app/services/teams.service';
import { TeamMembersService } from 'src/app/services/teammembers.service';


@Component({
  selector: 'app-teams-show',
  templateUrl: './teams-show.component.html',
  styleUrls: ['./teams-show.component.css']
})
export class TeamsShowComponent implements OnInit {

  team: any = {};
  members: any = {};
  id;

  params = {
    team_id: ''
	};

  mainLoader = true; 

  perm: any;
	withPerm = false;

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private TeamsService: TeamsService,
    private TeamMembersService: TeamMembersService
  ) { }

  ngOnInit() {

    this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(1200) !== -1 ? true : false;

    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.params.team_id = this.id = params['id'];
      this.show();
    });

  }

  show() {

    this.TeamsService.show( this.id ).subscribe( data => {
      if ( !data.error ) {
        this.team = data.results;
      }
    });

    this.TeamMembersService.get( this.params ).subscribe( data => {
      if ( !data.error ) {
        this.members = data.results;
      }
      this.mainLoader = false;
    });
  }

}
