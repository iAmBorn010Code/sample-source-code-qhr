import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from '../../texthelper.service';
import { TeamsService } from 'src/app/services/teams.service';

@Component({
  selector: 'app-teams-index',
  templateUrl: './teams-index.component.html',
  styleUrls: ['./teams-index.component.css']
})
export class TeamsIndexComponent implements OnInit {

  teams = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
	};
	
  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  perm: any;
	withPerm = false;

  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private TeamsService: TeamsService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Teams');
    this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(1200) !== -1 ? true : false;
    this.get();
  }

  get() {
		  
		this.teams = [];
		this.TeamsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.teams = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			} else {
        this.errorMsg = data.msg;
      }
			this.mainLoader = false;
		});
  }

  //Delete
  open( id ) {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure to delete this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {
    
    this.TeamsService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

    this.mainLoader = true;
    this.get();

  }
  
  //pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}


}
