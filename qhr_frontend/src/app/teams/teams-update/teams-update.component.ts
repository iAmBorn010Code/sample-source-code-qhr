import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { TeamsService } from 'src/app/services/teams.service';

@Component({
  selector: 'app-teams-update',
  templateUrl: './teams-update.component.html',
  styleUrls: ['./teams-update.component.css']
})
export class TeamsUpdateComponent implements OnInit {

  team: any = {};
  id;
  
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
	errorMsg = '';
	
	mainLoader = true;
  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private TeamsService: TeamsService,
  ) { }

  ngOnInit() {

    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.id = params['id']

      this.TeamsService.show( this.id ).subscribe( data => {
        if ( !data.error ) {
          this.team = data.results;
          this.mainLoader = false;
        } 
      });
    });

  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			name: this.team.name,
			status: this.team.status,
    };
    
    this.TeamsService.update( this.id, fields ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }


}
