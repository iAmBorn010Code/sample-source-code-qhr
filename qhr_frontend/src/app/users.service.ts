import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../environments/environment';
import { TextHelperService } from './texthelper.service';
import 'rxjs/add/operator/map';
import { AuthService } from './auth.service';

@Injectable()
export class UsersService {

 	apiBaseUrl: string = environment.apiBaseUrl;
    url: string;

    token = '&token=' + ( localStorage.getItem('token') || '' ) ;

  	constructor(
        private http: Http,
        private helper: TextHelperService,
        private AuthService: AuthService,
	) { 
	}

    get( params: any = {} ) {

        this.token = '&token=' + localStorage.getItem('token');
        const data = this.helper.serialize( params );
        this.url = this.apiBaseUrl + 'users?' + data + this.token;

        console.log( this.url );
        
        return this.http.get( this.url ).map( res => {
            const x = res.json();
            this.AuthService.isStillLogged(x);
            return x;
        });
    }

  	show( id ) {

        this.token = '&token=' + localStorage.getItem('token');
        this.url = this.apiBaseUrl + 'users/'+ id + '?' + this.token;

        console.log('HERE: ', this.url);
        
        return this.http.get( this.url ).map( res => {
            const x = res.json();
            this.AuthService.isStillLogged(x);
            return x;
        });
    }

    store( params ) {

        this.token = '&token=' + localStorage.getItem('token');
        this.url = this.apiBaseUrl + 'users?' + this.token;
        const body = JSON.stringify(params);
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(this.url, body, options).map(res => {
            const x = res.json();
            this.AuthService.isStillLogged(x);
            return x;
        });
    }

    destroy() {
      
    }

    update( serviceId: string, fields = {} ) {
        
        this.token = '&token=' + localStorage.getItem('token');
        fields['_method'] = 'PUT';
        const url = this.apiBaseUrl + 'users/' + serviceId + '?' + this.token;
        const body = JSON.stringify( fields );
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(url, body, options).map(res => {
            const x = res.json();
            this.AuthService.isStillLogged(x);
            return x;
        });
    }

}
