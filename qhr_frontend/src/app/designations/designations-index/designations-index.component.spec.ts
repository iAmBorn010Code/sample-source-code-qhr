import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignationsIndexComponent } from './designations-index.component';

describe('DesignationsIndexComponent', () => {
  let component: DesignationsIndexComponent;
  let fixture: ComponentFixture<DesignationsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignationsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignationsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
