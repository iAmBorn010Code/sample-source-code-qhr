import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { DesignationsService } from 'src/app/services/designation.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from 'src/app/texthelper.service';

@Component({
  selector: 'app-designations-index',
  templateUrl: './designations-index.component.html',
  styleUrls: ['./designations-index.component.css']
})
export class DesignationsIndexComponent implements OnInit {

  designations = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
		emp_id: ''
	};

	mainLoader = true;
  
  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

	perm: any;
	withPerm = false;

  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private DesignationsService: DesignationsService,
	private modalService: SuiModalService,
	private TextHelperService: TextHelperService
  ) { }

  ngOnInit() {

    this.PageService.pageName.next('Designations');
		this.perm = localStorage.getItem('perm');
    this.withPerm = this.perm.indexOf(3100) !== -1 ? true : false;
    this.get();
  }

  get() {
		this.mainLoader = true;

		this.designations = [];
		this.DesignationsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.designations = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			
			this.mainLoader = false;
		});
  }

  open( id ) {
    console.log('clicked delete!');

    console.log('ID', id); 

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure to delete this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {
    console.log('confirmed delete');

    this.DesignationsService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }

      this.get();
    });

  }
	
	// THIS IS FOR PAGINATIONS
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}


}
