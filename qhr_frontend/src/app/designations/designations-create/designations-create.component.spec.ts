import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignationsCreateComponent } from './designations-create.component';

describe('DesignationsCreateComponent', () => {
  let component: DesignationsCreateComponent;
  let fixture: ComponentFixture<DesignationsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignationsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignationsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
