import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { DesignationsService } from 'src/app/services/designation.service';

@Component({
  selector: 'app-designations-create',
  templateUrl: './designations-create.component.html',
  styleUrls: ['./designations-create.component.css']
})
export class DesignationsCreateComponent implements OnInit {

  designation: any = {
    with_time: 0,
    with_break: 0
  };

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private DesignationsService: DesignationsService
  ) { }

  ngOnInit() {
  }

  getFields() {

    const fields = {
      title: this.designation.title,
      rate: this.designation.rate,
      ot_rate: this.designation.ot_rate,
      with_time: this.designation.with_time,
      with_break: this.designation.with_break,
      time_in: this.designation.time_in,
      time_out: this.designation.time_out,
      time_in2: this.designation.time_in2,
      time_out2: this.designation.time_out2
    };
    
    return fields;
  }

  save() {
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.DesignationsService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
