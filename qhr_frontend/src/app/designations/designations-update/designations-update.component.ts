import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { DesignationsService } from 'src/app/services/designation.service';

@Component({
  selector: 'app-designations-update',
  templateUrl: './designations-update.component.html',
  styleUrls: ['./designations-update.component.css']
})
export class DesignationsUpdateComponent implements OnInit {

  designation: any = {};
  designation_id;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
	saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private DesignationsService: DesignationsService
  ) { }

  ngOnInit() {

    this.ActivatedRoute.params.subscribe( params => {
      this.designation_id = params['id'];
    });
    
    this.DesignationsService.show( this.designation_id ).subscribe( data => {
      if ( !data.error ) {
        this.designation = data.results;
        console.log('designation', this.designation);
      }
      
      this.mainLoader = false;
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
      title: this.designation.title,
      rate: this.designation.rate,
      ot_rate: this.designation.ot_rate,
      with_time: this.designation.with_time,
      with_break: this.designation.with_break,
			time_in: this.designation.time_in,
      time_out: this.designation.time_out,
      time_in2: this.designation.time_in2,
      time_out2: this.designation.time_out2,
		};

    this.DesignationsService.update( this.designation_id, fields ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
	}

}
