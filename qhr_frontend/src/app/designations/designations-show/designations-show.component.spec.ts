import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignationsShowComponent } from './designations-show.component';

describe('DesignationsShowComponent', () => {
  let component: DesignationsShowComponent;
  let fixture: ComponentFixture<DesignationsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignationsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignationsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
