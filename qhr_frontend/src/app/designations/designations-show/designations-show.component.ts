import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { DesignationsService } from 'src/app/services/designation.service';

@Component({
  selector: 'app-designations-show',
  templateUrl: './designations-show.component.html',
  styleUrls: ['./designations-show.component.css']
})
export class DesignationsShowComponent implements OnInit {

  designation: any = {};
  id;

  mainLoader = true; 

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  constructor(
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private DesignationService: DesignationsService
  ) { }

  ngOnInit() {
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.id = params['id'];
      this.show();
    });
  }

  show() {

    this.mainLoader = true;

    this.DesignationService.show( this.id ).subscribe( data => {
      if ( !data.error ) {
        this.designation = data.results;
        console.log('designation: ', this.designation);
      }
      this.mainLoader = false;
    });
  }

}
