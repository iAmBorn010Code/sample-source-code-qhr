import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeIndexComponent } from './overtime-index.component';

describe('OvertimeIndexComponent', () => {
  let component: OvertimeIndexComponent;
  let fixture: ComponentFixture<OvertimeIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
