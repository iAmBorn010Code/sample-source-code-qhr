import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { EmployeesService } from 'src/app/services/employees.service';
import { OvertimeService } from 'src/app/services/overtime.service';

@Component({
  selector: 'app-overtime-index',
  templateUrl: './overtime-index.component.html',
  styleUrls: ['./overtime-index.component.css']
})
export class OvertimeIndexComponent implements OnInit {

	overtimes = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
		emp_id: ''
	};

	employees = [];	
	params_emp = {
		s: ''
	};

	mainLoader = true;
	empLoader = true;
	searching = false;

	perm: any;
	withPerm = false;

	constructor(
		private PageService: PageService,
		private PaginatorService: PaginatorService,
		private EmployeesService: EmployeesService,
		private OvertimeService: OvertimeService
	) { }

  	ngOnInit() {

    	this.PageService.pageName.next('Overtimes');
		this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(1900) !== -1 ? true : false;

		if ( this.withPerm ) {
			this.EmployeesService.get( this.params_emp ).subscribe( data => {
				if ( !data.error ) {
					this.employees = data.results;
				}
				this.empLoader = false;
			});
		}

		this.get();
	}

	get() {
		this.mainLoader = true;

		this.overtimes = [];
		this.OvertimeService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.overtimes = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			
			this.mainLoader = false;
		});
	}
	
	// THIS IS FOR PAGINATIONS
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
