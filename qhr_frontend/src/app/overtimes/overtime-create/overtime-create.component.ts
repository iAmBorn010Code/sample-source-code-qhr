import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';
import { OvertimeService } from 'src/app/services/overtime.service';

@Component({
  selector: 'app-overtime-create',
  templateUrl: './overtime-create.component.html',
  styleUrls: ['./overtime-create.component.css']
})
export class OvertimeCreateComponent implements OnInit {

  overtime: any = {
    with_break: 0,
    with_ot_rate: 0
  };

  params = {
		s: '',
  };
  employees = [];

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;
  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private EmployeesService: EmployeesService,
    private OvertimeService: OvertimeService
  ) { }

  ngOnInit() {
    this.EmployeesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.employees = data.results;
			} else {
				this.errorMsg = data.msg;
			}

			this.mainLoader = false;
		});
  }

  getFields() {

    const fields = {
      employee_id: this.overtime.employee_id,
      time_in: this.overtime.time_in,
      time_out: this.overtime.time_out,
      with_ot_rate: this.overtime.with_ot_rate,
      ot_rate: this.overtime.ot_rate
    };
    
    return fields;
  }

  save() {
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.OvertimeService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
