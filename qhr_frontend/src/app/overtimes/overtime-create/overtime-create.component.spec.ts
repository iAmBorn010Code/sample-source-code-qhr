import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeCreateComponent } from './overtime-create.component';

describe('OvertimeCreateComponent', () => {
  let component: OvertimeCreateComponent;
  let fixture: ComponentFixture<OvertimeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
