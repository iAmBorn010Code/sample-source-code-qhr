import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeUpdateComponent } from './overtime-update.component';

describe('OvertimeUpdateComponent', () => {
  let component: OvertimeUpdateComponent;
  let fixture: ComponentFixture<OvertimeUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
