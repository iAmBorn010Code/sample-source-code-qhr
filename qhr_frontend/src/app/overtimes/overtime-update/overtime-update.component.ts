import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { OvertimeService } from 'src/app/services/overtime.service';

@Component({
  selector: 'app-overtime-update',
  templateUrl: './overtime-update.component.html',
  styleUrls: ['./overtime-update.component.css']
})
export class OvertimeUpdateComponent implements OnInit {

  overtime: any = {};
  overtime_id;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
	saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private OvertimeService: OvertimeService
  ) { }

  ngOnInit() {

    this.ActivatedRoute.params.subscribe( params => {
      this.overtime_id = params['id'];
    });
    
    this.OvertimeService.show( this.overtime_id ).subscribe( data => {
      if ( !data.error ) {
        this.overtime = data.results;
      }
      
      this.mainLoader = false;
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			time_in: this.overtime.time_in,
      time_out: this.overtime.time_out,
      with_ot_rate: this.overtime.with_ot_rate,
      ot_rate: this.overtime.ot_rate
		};

    this.OvertimeService.update( this.overtime_id, fields ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
	}

}
