import { Component, OnInit } from '@angular/core';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { OvertimeService } from 'src/app/services/overtime.service';

@Component({
  selector: 'app-overtime-show',
  templateUrl: './overtime-show.component.html',
  styleUrls: ['./overtime-show.component.css']
})
export class OvertimeShowComponent implements OnInit {

  overtime: any = {};
  id;

  mainLoader = true; 

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  modal_msg = '';
  success_msg = '';

  constructor(
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private OvertimeService: OvertimeService
  ) { }

  ngOnInit() {
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.id = params['id'];
      this.show();
    });
  }

  show() {

    this.mainLoader = true;

    this.OvertimeService.show( this.id ).subscribe( data => {
      if ( !data.error ) {
        this.overtime = data.results;
      }
      this.mainLoader = false;
    });
  }

  open( status ) {

    if ( status == 'approved' ) {
      this.modal_msg = 'Are you sure you want to approve this overtime?';
      this.success_msg = 'Overtime was approved.';
    } else if ( status == 'disapproved' ) {
      this.modal_msg = 'Are you sure you want to disapprove this overtime?';
      this.success_msg = 'Overtime was disapproved.';
    }

    this.modalService
      .open( new ConfirmModal('Are you sure?', this.modal_msg) )
      .onApprove( () => this.update(status) )
      .onDeny( () => '' )
  }

  update(status) {

    const fields = {
      status: status,
      employee: this.overtime.employee._id
    };
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    
    this.OvertimeService.update( this.id, fields ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = this.success_msg;
        this.show();
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
      
    });

  }

}
