import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeShowComponent } from './overtime-show.component';

describe('OvertimeShowComponent', () => {
  let component: OvertimeShowComponent;
  let fixture: ComponentFixture<OvertimeShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
