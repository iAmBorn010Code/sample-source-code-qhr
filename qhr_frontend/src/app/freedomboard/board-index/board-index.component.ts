import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { FreedomBoardService } from '../../services/freedomboard.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from '../../texthelper.service';
import { DepartmentsService } from 'src/app/services/departments.service';

@Component({
  selector: 'app-board-index',
  templateUrl: './board-index.component.html',
  styleUrls: ['./board-index.component.css']
})
export class BoardIndexComponent implements OnInit {

  ratings = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
  };
  
  departments = [];
  params_dept = {
    limit: 5,
		w: 'get_ratings'
	};
	
  deptRatingLoader = true;
  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  perm: any;
	withPerm = false;
  
  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private FreedomBoardService: FreedomBoardService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private DepartmentsService: DepartmentsService
  ) { }

  ngOnInit() {

    this.PageService.pageName.next('Freedom Board');
    this.perm = localStorage.getItem('perm');
    this.withPerm = this.perm.indexOf(1500) !== -1 ? true : false;
    
    this.get();
    this.departmentRatings();
  }

  get() {
		  
		this.ratings = [];
		this.FreedomBoardService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.ratings = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			this.mainLoader = false;
		});
  }

  departmentRatings() {
    this.DepartmentsService.get( this.params_dept ).subscribe( data => {

			if ( !data.error ) {
        this.departments = data.results;
        console.log('departments rating', this.departments);
			}
			this.deptRatingLoader = false;
		});
  }

  //Delete
  open( id ) {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure to delete this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {

    this.FreedomBoardService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

    this.mainLoader = true;
    this.get();

  }
  
  //pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
