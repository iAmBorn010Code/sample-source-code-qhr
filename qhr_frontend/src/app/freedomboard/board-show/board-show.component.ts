import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FreedomBoardService } from '../../services/freedomboard.service';

@Component({
  selector: 'app-board-show',
  templateUrl: './board-show.component.html',
  styleUrls: ['./board-show.component.css']
})
export class BoardShowComponent implements OnInit {
	
  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';
  
  mainLoader = true;
  rating: null;

  constructor(
    private route: ActivatedRoute,
    private FreedomBoardService: FreedomBoardService,
  ) { }

  ngOnInit() {

    this.route.params.subscribe( params => {
      console.log('id', params['id'])
      this.show( params['id'] );
    });

  }

  show( id ) {

    console.log( 'called show' );

    this.FreedomBoardService.show( id ).subscribe( data => {
      if ( !data.error ) {
        this.rating = data.results;
        console.log('rating', this.rating );
      } else {
        console.log( data.msg );
      }

      this.mainLoader = false;

    });

  }

}
