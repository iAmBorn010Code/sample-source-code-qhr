import { Component, OnInit } from '@angular/core';
import { FreedomBoardService } from '../../services/freedomboard.service';
import { DepartmentsService } from '../../services/departments.service';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-board-create',
  templateUrl: './board-create.component.html',
  styleUrls: ['./board-create.component.css']
})
export class BoardCreateComponent implements OnInit {

  rating: any = {};
  departments = [];

  results_count = 0;
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;
  mainLoader = true;

  params = {
		s: ''
  };
  
  ratings;


  constructor(
    private TextHelperService: TextHelperService,
    private FreedomBoardService: FreedomBoardService,
    private DepartmentsService: DepartmentsService,
  ) { }

  ngOnInit() {

    this.ratings= [
      {id: 1},
      {id: 2},
      {id: 3},
      {id: 4},
      {id: 5},
      {id: 6},
      {id: 7},
      {id: 8},
      {id: 9},
      {id: 10}
    ];

    this.departments = [];
		this.DepartmentsService.get( this.params ).subscribe( data => {

			console.log('departments', data);
			if ( !data.error ) {
				this.departments = data.results;
				this.results_count = data.results_count;
			} else {
				this.errorMsg = data.msg;
				console.log('this.errorMsg', this.errorMsg );
			}

			this.mainLoader = false;
		});

  }

  getFields() {

    const fields = {
      department_id: this.rating.department_id,
      rating: this.rating.rating,
      comment: this.rating.comment,
    };
    
    return fields;
  }

  save() {
    console.log('clicked save');
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;

    this.saving = true;

    console.log( this.getFields() );

    this.FreedomBoardService.store( this.getFields() ).subscribe( data => {
      console.log('request sucessful', data)
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
