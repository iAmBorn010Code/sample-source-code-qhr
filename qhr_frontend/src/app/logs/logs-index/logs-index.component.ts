import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { LogsService } from '../../services/logs.service';

@Component({
  selector: 'app-logs-index',
  templateUrl: './logs-index.component.html',
  styleUrls: ['./logs-index.component.css']
})
export class LogsIndexComponent implements OnInit {

  	logs = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
	};
	
	mainLoader = true;

	constructor(
		private PageService: PageService,
		private PaginatorService: PaginatorService,
		private LogsService: LogsService
	) { }

	ngOnInit() {
		this.PageService.pageName.next('Logs');
		this.get();
	}

  	get() {
		  
		this.logs = [];
		this.LogsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.logs = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( this.paginator );
			}
			console.log(this.logs);
			this.mainLoader = !this.mainLoader;
		});
  	}	
  
	//pagination
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}
  
}
