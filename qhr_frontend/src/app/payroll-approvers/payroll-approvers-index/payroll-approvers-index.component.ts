import { Component, OnInit } from '@angular/core';
import { PaginatorService } from '../../paginator.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from 'src/app/page.service';
import { PayrollApproversService } from 'src/app/services/payrollapprovers.service';

@Component({
  selector: 'app-payroll-approvers-index',
  templateUrl: './payroll-approvers-index.component.html',
  styleUrls: ['./payroll-approvers-index.component.css']
})
export class PayrollApproversIndexComponent implements OnInit {

  approvers = [];
  paginator: any;
	params = {
		limit: 10,
    page: 1,
    team_id: ''
	};
	
  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  constructor(
    private PaginatorService: PaginatorService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private PageService: PageService,
    private PayrollApproversService: PayrollApproversService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Payroll Approvers');
    this.get();
  }

  get() {
		  
		this.approvers = [];
		this.PayrollApproversService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.approvers = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			} else {
        this.errorMsg = data.msg;
      }
			this.mainLoader = false;
		});
  }

  //Delete
  open( id ) {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure you want to remove this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {
    
    this.PayrollApproversService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

    this.mainLoader = true;
    this.get();

  }
  
  //pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
