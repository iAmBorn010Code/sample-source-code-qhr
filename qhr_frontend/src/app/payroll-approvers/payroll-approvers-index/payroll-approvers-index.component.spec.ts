import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollApproversIndexComponent } from './payroll-approvers-index.component';

describe('PayrollApproversIndexComponent', () => {
  let component: PayrollApproversIndexComponent;
  let fixture: ComponentFixture<PayrollApproversIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollApproversIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollApproversIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
