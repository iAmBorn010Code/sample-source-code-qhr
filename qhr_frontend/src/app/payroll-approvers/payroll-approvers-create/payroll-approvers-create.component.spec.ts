import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollApproversCreateComponent } from './payroll-approvers-create.component';

describe('PayrollApproversCreateComponent', () => {
  let component: PayrollApproversCreateComponent;
  let fixture: ComponentFixture<PayrollApproversCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollApproversCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollApproversCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
