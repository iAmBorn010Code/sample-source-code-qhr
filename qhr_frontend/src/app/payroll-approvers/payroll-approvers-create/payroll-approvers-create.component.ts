import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { Router } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { PayrollApproversService } from 'src/app/services/payrollapprovers.service';

@Component({
  selector: 'app-payroll-approvers-create',
  templateUrl: './payroll-approvers-create.component.html',
  styleUrls: ['./payroll-approvers-create.component.css']
})
export class PayrollApproversCreateComponent implements OnInit {

  payroll_approver: any = {};

  params = {
		s: '',
  };

  employees = [];

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;
  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private EmployeesService: EmployeesService,
    private router: Router,
    private PayrollApproversService: PayrollApproversService
  ) { }

  ngOnInit() {

    this.EmployeesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.employees = data.results;
			} else {
				this.errorMsg = data.msg;
			}

			this.mainLoader = false;
		});
  }

  getFields() {

    const fields = {
      user_id: this.payroll_approver.user_id,
    };
    
    return fields;
  }

  save() {
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.PayrollApproversService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
