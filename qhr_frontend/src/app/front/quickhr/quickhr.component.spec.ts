import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickhrComponent } from './quickhr.component';

describe('QuickhrComponent', () => {
  let component: QuickhrComponent;
  let fixture: ComponentFixture<QuickhrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickhrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickhrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
