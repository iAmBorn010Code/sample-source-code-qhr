import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  latitude = 10.7124;
  longitude= 122.5603;
  zoom= 18;
  
  
  constructor() { }

  ngOnInit() {
  }

}
