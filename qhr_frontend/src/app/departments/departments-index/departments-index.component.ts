import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { DepartmentsService } from '../../services/departments.service';

@Component({
  selector: 'app-departments-index',
  templateUrl: './departments-index.component.html',
  styleUrls: ['./departments-index.component.css']
})
export class DepartmentsIndexComponent implements OnInit {

  departments = [];
	paginator: any;
	params = {
		limit: 5,
		page: 1,
		s: ''
	};

	results_count = 0;
	mainLoader = true;

	perm: any;
	withPerm = false;

	constructor(
		private PageService: PageService,
		private PaginatorService: PaginatorService,
		private DepartmentsService: DepartmentsService,
	) { }

	ngOnInit() {

		this.PageService.pageName.next('Departments');
		this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(600) !== -1 ? true : false;
		this.get();
	}
	  

	get() {
		  
		this.departments = []; 
		this.DepartmentsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.departments = data.results;
				this.results_count = data.results_count;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			this.mainLoader = false;
		});
	}
	  

	//pagination
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}
}

