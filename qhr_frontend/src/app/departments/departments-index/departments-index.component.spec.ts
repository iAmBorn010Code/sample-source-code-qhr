import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentsIndexComponent } from './departments-index.component';

describe('DepartmentsIndexComponent', () => {
  let component: DepartmentsIndexComponent;
  let fixture: ComponentFixture<DepartmentsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
