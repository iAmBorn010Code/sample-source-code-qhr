import { Component, OnInit } from '@angular/core';
import { DepartmentsService } from '../../services/departments.service';
import { Router } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-departments-create',
  templateUrl: './departments-create.component.html',
  styleUrls: ['./departments-create.component.css']
})
export class DepartmentsCreateComponent implements OnInit {

  department: any = {
    status: 'active'
  };
  uploads_history = [];
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private DepartmentsService: DepartmentsService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Departments');
    console.log('init called');
  }

  getFields() {

    const fields = {
        name: this.department.name,
        status: this.department.status,
        description: this.department.description
    };
    
    return fields;
  }

  save() {
    console.log('clicked save');
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    console.log( this.getFields() );

    this.DepartmentsService.store( this.getFields() ).subscribe( data => {
      console.log('request sucessful', data)
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
