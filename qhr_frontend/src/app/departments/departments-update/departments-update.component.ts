import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DepartmentsService } from '../../services/departments.service';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-departments-update',
  templateUrl: './departments-update.component.html',
  styleUrls: ['./departments-update.component.css']
})
export class DepartmentsUpdateComponent implements OnInit {

	department: any = {};
	
	department_id;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
	saving = false;

  constructor(
		private PageService: PageService,
    private TextHelperService: TextHelperService,
    private DepartmentsService: DepartmentsService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
		
		this.PageService.pageName.next('Departments');
		console.log('called init personal');
		
		this.ActivatedRoute.params.subscribe( params => {
			this.department_id = params['id'];
		});

		console.log('ID', this.department_id);
		
		this.DepartmentsService.show( this.department_id ).subscribe( data => {
			if ( !data.error ) {
				this.department = data.results;
				console.log('department', this.department);
			}
			this.mainLoader = !this.mainLoader;
		});

  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			name: this.department.name,
			status: this.department.status
		};

		console.log( fields );

 	 	this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.DepartmentsService.update( params['id'], fields ).subscribe( data => {
				this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
						this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
				}
			});
		});
	}

}
