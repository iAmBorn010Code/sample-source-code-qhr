import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-departments-show',
  templateUrl: './departments-show.component.html',
  styleUrls: ['./departments-show.component.css']
})
export class DepartmentsShowComponent implements OnInit {

  constructor(
    private PageService: PageService,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Departments');
  }

}
