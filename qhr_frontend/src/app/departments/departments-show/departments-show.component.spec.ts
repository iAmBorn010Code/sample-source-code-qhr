import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentsShowComponent } from './departments-show.component';

describe('DepartmentsShowComponent', () => {
  let component: DepartmentsShowComponent;
  let fixture: ComponentFixture<DepartmentsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
