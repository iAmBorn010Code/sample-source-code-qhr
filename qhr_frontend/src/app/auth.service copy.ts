import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';
import { LocalstorageService } from './localstorage.service';


@Injectable()
export class AuthService {
    apiBaseUrl: string = environment.apiBaseUrl;
    resourceUrl: string;

    constructor(
        private router: Router,
        private http: Http,
        private storage: LocalstorageService,
    ) {
        this.resourceUrl = this.apiBaseUrl + 'auth';
    }

    login( params: any ) {

        const url = this.resourceUrl + '/login';
        const body = JSON.stringify( params );
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        console.log(url, 'HERE', body);

        return this.http.post(url, body, options).pipe(
            map( res => {
                const x = res.json();
                return x;
            })
        );
    }

    logout() {

        console.log('logged out');
        const url = this.resourceUrl + '/logout?token=' + localStorage.getItem('token');

       
        this.storage.set( 'token', '' );
        this.storage.set( 'user', {} );
        this.storage.set( 'company', {} );
        this.storage.set( 'currBranchId', '' );
        this.storage.clear();
        localStorage.clear();

        this.router.navigate(['/login']);

        // try {
        //     console.log( url );
        //     this.http.get(url).pipe(
        //         map( res => {
        //             this.storage.clear();
        //             localStorage.clear();
        //             this.router.navigate(['/login']);
        //         })
        //     );
        // } catch ( err ) {
        //     console.log('Error logging out.');
        //     this.handleError( err );
        //     this.storage.clear();
        //     localStorage.clear();
        //     this.router.navigate(['/login']);
        // }
    }

    getLoggedUser() {

        let user = localStorage.getItem('user');

        try {
            user = JSON.parse( user );
        } catch( e ) {
            user = null;
        }

		if ( typeof user !== 'object' || !user ) {
            this.logout();
		}
        
        return user;
	}

    isStillLogged( res:any ) {

        
        if ( res.forced_login || res.token === null || res.token === 'null' || !res.token ) {
            console.log( res, 'WOOOPSPS');

            // alert('Session expired.');
            this.router.navigate(['/logout']);
			this.storage.clear();
			return;
		}
        
        this.saveLocalStorage( res.token, res.user );
	}

	private saveLocalStorage( token, user = {} ) {

    	if ( token ) {
    		this.storage.set( 'token', token );
    	}

    	if ( user ) {

	    	if ( Object.keys(user).length !== 0 ) {
				this.storage.set( 'user', user );
	    	}
    	}
    }

    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
