import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDesignationsCreateComponent } from './user-designations-create.component';

describe('UserDesignationsCreateComponent', () => {
  let component: UserDesignationsCreateComponent;
  let fixture: ComponentFixture<UserDesignationsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDesignationsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDesignationsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
