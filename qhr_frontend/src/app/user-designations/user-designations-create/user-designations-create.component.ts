import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';
import { UserDesignationsService } from 'src/app/services/userdesignation.service';
import { DesignationsService } from 'src/app/services/designation.service';

@Component({
  selector: 'app-user-designations-create',
  templateUrl: './user-designations-create.component.html',
  styleUrls: ['./user-designations-create.component.css']
})
export class UserDesignationsCreateComponent implements OnInit {

  user_designation: any = {
    status: 'pending'
  };
  employees = [];
  designations = [];

  days = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' ];

  params = {
		s: '',
  };
  

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  empLoader = true;
  designationLoader = true;
  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private EmployeesService: EmployeesService,
    private UserDesignationsService: UserDesignationsService,
    private DesignationsService: DesignationsService
  ) { }

  ngOnInit() {

    this.EmployeesService.get( this.params ).subscribe( data => {
			if ( !data.error ) {
				this.employees = data.results;
			} else {
				this.errorMsg = data.msg;
      }
      
      this.empLoader = false;
    });
    
    this.DesignationsService.get( this.params ).subscribe( data => {
			if ( !data.error ) {
				this.designations = data.results;
			} else {
				this.errorMsg = data.msg;
      }

      this.designationLoader = false;
    });

  }

  getFields() {

    const fields = {
      user_id: this.user_designation.user_id,
      designation_id: this.user_designation.designation_id,
      status: this.user_designation.status,
      day: this.user_designation.day,
    };
    
    return fields;
  }

  save() {
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.UserDesignationsService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
