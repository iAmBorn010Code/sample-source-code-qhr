import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDesignationsShowComponent } from './user-designations-show.component';

describe('UserDesignationsShowComponent', () => {
  let component: UserDesignationsShowComponent;
  let fixture: ComponentFixture<UserDesignationsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDesignationsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDesignationsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
