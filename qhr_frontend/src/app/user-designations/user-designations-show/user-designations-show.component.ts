import { Component, OnInit } from '@angular/core';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { UserDesignationsService } from 'src/app/services/userdesignation.service';

@Component({
  selector: 'app-user-designations-show',
  templateUrl: './user-designations-show.component.html',
  styleUrls: ['./user-designations-show.component.css']
})
export class UserDesignationsShowComponent implements OnInit {

  user_designation: any = {};
  id;

  mainLoader = true; 

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  modal_msg = '';
  success_msg = '';

  constructor(
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private UserDesignationService: UserDesignationsService
  ) { }

  ngOnInit() {
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.id = params['id'];
      this.show();
    });
  }

  show() {

    this.mainLoader = true;

    this.UserDesignationService.show( this.id ).subscribe( data => {
      if ( !data.error ) {
        this.user_designation = data.results;
        console.log('user_designation: ', this.user_designation);
      }
      this.mainLoader = false;
    });
  }

  open( status ) {

    if ( status == 'approved' ) {
      this.modal_msg = 'Are you sure you want to approve this employee designation?';
      this.success_msg = 'Employee designation was approved.';
    } else if ( status == 'disapproved' ) {
      this.modal_msg = 'Are you sure you want to disapprove this employee designation?';
      this.success_msg = 'Employee designation was disapproved.';
    }

    this.modalService
      .open( new ConfirmModal('Are you sure?', this.modal_msg) )
      .onApprove( () => this.update(status) )
      .onDeny( () => '' )
  }

  update(status) {

    const fields = {
      status: status
    };
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    
    this.UserDesignationService.update( this.id, fields ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = this.success_msg;
        this.show();
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
      
    });

  }

}
