import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDesignationsIndexComponent } from './user-designations-index.component';

describe('UserDesignationsIndexComponent', () => {
  let component: UserDesignationsIndexComponent;
  let fixture: ComponentFixture<UserDesignationsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDesignationsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDesignationsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
