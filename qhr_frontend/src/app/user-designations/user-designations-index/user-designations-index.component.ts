import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { DesignationsService } from 'src/app/services/designation.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from 'src/app/texthelper.service';
import { UserDesignationsService } from 'src/app/services/userdesignation.service';

@Component({
  selector: 'app-user-designations-index',
  templateUrl: './user-designations-index.component.html',
  styleUrls: ['./user-designations-index.component.css']
})
export class UserDesignationsIndexComponent implements OnInit {

  designations = [];
  user_designations = [];
	paginator: any;
	params = {
		limit: 10,
    page: 1,
    designation_id: '',
    day: ''
  };
  
  designation_params = {
    s: ''
  }

  days = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' ];

  mainLoader = true;
  designationLoader = true;
  
  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

	perm: any;
	withPerm = false;

  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private DesignationsService: DesignationsService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService  ,
    private UserDesignationsService: UserDesignationsService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Employee Designations');
		this.perm = localStorage.getItem('perm');
    this.withPerm = this.perm.indexOf(3300) !== -1 ? true : false;

    this.DesignationsService.get( this.designation_params ).subscribe( data => {
			if ( !data.error ) {
				this.designations = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
      }
      
      this.designationLoader = false;
    });
    

    this.get();
  }

  get() {

    this.mainLoader = true;
		this.user_designations = [];
		this.UserDesignationsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.user_designations = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			
			this.mainLoader = false;
		});
  }

  open( id ) {
    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure to remove this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {

    this.UserDesignationsService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }

      this.get();
    });
  }
	
	// THIS IS FOR PAGINATIONS
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}


}
