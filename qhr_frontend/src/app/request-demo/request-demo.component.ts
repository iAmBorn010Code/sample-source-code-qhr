import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TextHelperService } from '../texthelper.service';
import { RequestDemoService } from '../services/requestdemo.service';

@Component({
  selector: 'app-request-demo',
  templateUrl: './request-demo.component.html',
  styleUrls: ['./request-demo.component.css']
})
export class RequestDemoComponent implements OnInit {

  user: any = {
    full_name: '',
    company_name: '',
    office_email: '',
    company_size: '',
    contact_no: ''
  }

  successMsg = '';
  errorMsg = '';
  isError: boolean;
  noticeMsg = '';
  
  saving = false;
  slug;

  constructor(
    private router: Router,
    private TextHelperService: TextHelperService,
    private RequestDemoService: RequestDemoService
  ) { }

  ngOnInit() {
  }

  onSubmit() {

    this.saving = true;

    const fields = {
      full_name: this.user.full_name,
      company_name: this.user.company_name,
      email: this.user.email,
      company_size: this.user.company_size,
      contact_no: this.user.contact_no,
    };
    

		this.successMsg = '';
		this.errorMsg = '';
    
    console.log(fields);

		this.RequestDemoService.store( fields ).subscribe( data => {
      console.log('request sucessful', data)
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
      
      this.saving = false;
    });
	}

}
