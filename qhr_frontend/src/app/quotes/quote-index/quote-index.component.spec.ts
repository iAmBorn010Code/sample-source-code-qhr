import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteIndexComponent } from './quote-index.component';

describe('QuoteIndexComponent', () => {
  let component: QuoteIndexComponent;
  let fixture: ComponentFixture<QuoteIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuoteIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
