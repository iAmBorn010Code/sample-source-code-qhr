import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { QuotesService } from '../../services/quotes.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-quote-index',
  templateUrl: './quote-index.component.html',
  styleUrls: ['./quote-index.component.css']
})
export class QuoteIndexComponent implements OnInit {

  quotes = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
	};
	
  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  perm: any;
	withPerm = false;

  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private QuotesService: QuotesService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService
  ) { }

  ngOnInit() {

    this.PageService.pageName.next('Quotes');
    this.perm = localStorage.getItem('perm');
    this.withPerm = this.perm.indexOf(900) !== -1 ? true : false;
    this.get();

  }

  get() {
		  
		this.quotes = [];
		this.QuotesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.quotes = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( this.paginator );
			}
			console.log(this.quotes);
			this.mainLoader = false;
		});
  }

  //Delete
  open( id ) {
    console.log('clicked delete!');

    console.log('ID', id); 

    this.modalService
      .open( new ConfirmModal('Are you sure?', 'Are you sure to delete this?') )
      .onApprove( () => this.delete( id ) )
      .onDeny( () => '' )
  }

  delete( id ) {
    console.log('confirmed delete');

    this.QuotesService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

    this.mainLoader = true;
    this.get();

  }

}
