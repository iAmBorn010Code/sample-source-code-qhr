import { Component, OnInit } from '@angular/core';
import { QuotesService } from '../../services/quotes.service';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-quote-create',
  templateUrl: './quote-create.component.html',
  styleUrls: ['./quote-create.component.css']
})
export class QuoteCreateComponent implements OnInit {

  quote = '';

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private QuotesService: QuotesService,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Quotes');
  }

  getFields() {

    const fields = {
      quote: this.quote,
    };
    
    return fields;
  }

  save() {

    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;

    this.saving = true;

    console.log( this.getFields() );

    this.QuotesService.store( this.getFields() ).subscribe( data => {
      console.log('request sucessful', data)
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
