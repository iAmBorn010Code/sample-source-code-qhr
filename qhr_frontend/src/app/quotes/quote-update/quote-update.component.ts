import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { QuotesService } from '../../services/quotes.service';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-quote-update',
  templateUrl: './quote-update.component.html',
  styleUrls: ['./quote-update.component.css']
})
export class QuoteUpdateComponent implements OnInit {

  quote: any = {};
	quote_id;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
	saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private QuotesService: QuotesService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.ActivatedRoute.params.subscribe( params => {
			this.quote_id = params['id'];
		});
		
		this.QuotesService.show( this.quote_id ).subscribe( data => {
			if ( !data.error ) {
				this.quote = data.results;
			}
			this.mainLoader = false;
		});
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;

		this.saving = true;

		const fields = {
			quote: this.quote.quote,
		};

    this.QuotesService.update( this.quote_id, fields ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
	}

}
