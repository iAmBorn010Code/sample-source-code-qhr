import { Component, OnInit } from '@angular/core';
import { LeavesService } from '../../services/leaves.service';
import { Router } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { EmployeesService } from '../../services/employees.service';
import { PageService } from '../../page.service';
import { AuthService } from '../../auth.service';
import { Subject } from 'rxjs/Subject';

// import {SuiModule} from 'ng2-semantic-ui';
import { CalendarEvent } from 'angular-calendar';
import {
  isSameDay,
  isSameMonth,
} from 'date-fns';


@Component({
  selector: 'app-leaves-create',
  templateUrl: './leaves-create.component.html',
  styleUrls: ['./leaves-create.component.css']
})
export class LeavesCreateComponent implements OnInit {

  leave: any = {};
  employees = [];
  leaveTypes = [
    {name: 'Sick Leave', type: 'SL'},
    {name: 'Vacation Leave', type: 'VL'},
    {name: 'Emergency Leave', type: 'EL'},
    {name: 'Paternity Leave', type: 'PL'},
    {name: 'Maternity Leave', type: 'ML'},
  ];

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  clickedDate = new Date();
  view = 'month';
  refresh: Subject<any> = new Subject();
  activeDayIsOpen = false;
  viewDate: Date = new Date();

  userPermissions = [];
  loggedUser;

  params = {
    calendar: 1,
    month: this.viewDate.getMonth() + 1,
    year: (new Date).getFullYear(),
  };

  canCreateForOthers = false;

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private LeavesService: LeavesService,
    private AuthService: AuthService,
    private EmployeesService: EmployeesService,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Leaves');

    this.userPermissions = localStorage.perm;
    this.loggedUser = this.AuthService.getLoggedUser();

    this.canCreateForOthers = this.userPermissions.indexOf(400) !== -1;

    if (! this.canCreateForOthers ) {
      this.leave.employeeId = this.loggedUser._id;
    } else {
      this.getEmployees();
    }
  }

  getFields() {

    const fields = {
        user_id: this.leave.employeeId,
        type: this.leave.type,
        date: this.clickedDate,
        reason: this.leave.reason,
    };
    
    return fields;
  }

  save() {
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.LeavesService.store( this.getFields() ).subscribe( data => {

      this.saving = true;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
        this.resetFields();
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
      this.saving = false;
    });
  }

  getEmployees() {

    // TODO: Get only employee under that company.
		this.EmployeesService.get({}).subscribe( data => {
      console.log( data );
			if ( !data.error ) {
        console.log( this.employees );
				this.employees = data.results;
			}
		});

  }

  resetFields() {
    this.leave.type = null;
    this.leave.employeeId = null;
    this.clickedDate = new Date(),
    this.leave.reason = '';
  }
  

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {

    console.log('Day Clicked: ', date);

    this.clickedDate = date;

    if (isSameMonth(date, this.viewDate)) {

      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventClicked({ event }: { event: CalendarEvent }): void {

    console.log( this.viewDate.getMonth() );


    // console.log('bookings: ', this.bookings );
    this.refresh.next();

    console.log('Event clicked', event);
  }

  getEventTitle( events ) {
    if ( !events ) {
      return '';
    }

    return events.map( event => event.title ).join(', ');
  }


  nextMonth(e) {
    // alert(JSON.stringify(e.getMonth()));
    
    this.params.month = e.getMonth() + 1;
    this.params.year = e.getYear() - 100;
  }


}
