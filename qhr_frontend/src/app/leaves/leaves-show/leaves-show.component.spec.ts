import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavesShowComponent } from './leaves-show.component';

describe('LeavesShowComponent', () => {
  let component: LeavesShowComponent;
  let fixture: ComponentFixture<LeavesShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavesShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavesShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
