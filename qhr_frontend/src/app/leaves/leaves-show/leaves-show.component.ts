import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LeavesService } from '../../services/leaves.service';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-leaves-show',
  templateUrl: './leaves-show.component.html',
  styleUrls: ['./leaves-show.component.css']
})
export class LeavesShowComponent implements OnInit {

  leaveTypes = [];
  leave: any = {};
  status: any;
  id: string = '';
	
  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';
  
  mainLoader = true;
  
  constructor(
    private PageService: PageService,
    private route: ActivatedRoute,
    private LeavesService: LeavesService,
    private TextHelperService: TextHelperService,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Leaves');
    this.route.params.subscribe( params => {

      this.show( params['id'] );
      this.id = params['id'];
    });
    console.log(this.id);
  }
  
  show( id ) {

    console.log( 'called show' );

    this.leaveTypes['SL'] = 'Sick Leave';
		this.leaveTypes['VL'] = 'Vacation Leave';
    this.leaveTypes['EL'] = 'Emergency Leave';
    this.leaveTypes['PL'] = 'Paternity Leave';
		this.leaveTypes['ML'] = 'Maternity Leave';

    this.LeavesService.show( id ).subscribe( data => {
      if ( !data.error ) {
        this.leave = data.results;
        console.log('LEAVE', this.leave );
      } else {
        console.log( data.msg );
      }

      this.mainLoader = !this.mainLoader;

    });

  }

}
