import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavesIndexComponent } from './leaves-index.component';

describe('LeavesIndexComponent', () => {
  let component: LeavesIndexComponent;
  let fixture: ComponentFixture<LeavesIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavesIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavesIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
