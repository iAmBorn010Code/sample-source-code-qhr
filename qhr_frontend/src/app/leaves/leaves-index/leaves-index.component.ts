import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { LeavesService } from '../../services/leaves.service';
import { EmployeesService } from 'src/app/services/employees.service';
import { headersToString } from 'selenium-webdriver/http';

@Component({
  selector: 'app-leaves-index',
  templateUrl: './leaves-index.component.html',
  styleUrls: ['./leaves-index.component.css']
})
export class LeavesIndexComponent implements OnInit {

	leaveTypes = [];
	leaves = [];
	employees = [];	
	errorMsg = '';
	paginator: any;
	params = {
		limit: 8,
		page: 1,
		emp_id: '',
	};

	params_emp = {
		s: ''
	};

	perm: any;
	user: any = {};
	withPerm = false;

	mainLoader = true;
	empLoader = true;
	searching = false;

	leave_credits = {
		SL: 0,
		VL: 0,
		EL: 0,
		PL: 0,
		ML: 0
	}

	constructor(
		private PageService: PageService,
		private PaginatorService: PaginatorService,
		private LeavesService: LeavesService,
		private EmployeesService: EmployeesService
	) { }

  ngOnInit() {

		this.PageService.pageName.next('Leaves');
		this.perm = localStorage.getItem('perm');
		this.user = JSON.parse( localStorage.getItem('user') );
		this.withPerm = this.perm.indexOf(400) !== -1 ? true : false;

		if ( this.withPerm ) {
			this.employees = [];
			this.EmployeesService.get( this.params_emp ).subscribe( data => {

				if ( !data.error ) {
					this.employees = data.results;
				} else {
					this.errorMsg = data.msg;
				}

				this.empLoader = false;
			});
		} else {
			this.EmployeesService.show( this.user._id ).subscribe( data => {

				if ( !data.error ) {
					this.user = data.results;
					this.user.leave_credits.forEach(leave => {
						if ( leave.type == 'VL' ){
							this.leave_credits.VL = leave.credit	
						}
						if ( leave.type == 'EL' ){
							this.leave_credits.EL = leave.credit
						}
						if ( leave.type == 'SL' ){
							this.leave_credits.SL = leave.credit
						}
						if ( leave.type == 'ML' ){
							this.leave_credits.ML = leave.credit
						}
						if ( leave.type == 'PL' ){
							this.leave_credits.PL = leave.credit
						}
					});
				} else {
					this.errorMsg = data.msg;
				}
			});
		}

		this.get();

	}
	  

	get() {

		this.mainLoader = true;

		this.leaveTypes['SL'] = 'Sick Leave';
		this.leaveTypes['VL'] = 'Vacation Leave';
		this.leaveTypes['EL'] = 'Emergency Leave';
		this.leaveTypes['PL'] = 'Paternity Leave';
		this.leaveTypes['ML'] = 'Maternity Leave';

		this.leaves = [];
		this.LeavesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.leaves = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			this.mainLoader = false;
		});
	}

	emp() {

		this.mainLoader = true;
		this.searching = true;
		this.leaves = [];

		this.LeavesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.leaves = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			this.mainLoader = false;
			this.searching = false;
		});

	}
	  

	//pagination
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}
}
