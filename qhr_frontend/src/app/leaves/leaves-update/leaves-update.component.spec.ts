import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavesUpdateComponent } from './leaves-update.component';

describe('LeavesUpdateComponent', () => {
  let component: LeavesUpdateComponent;
  let fixture: ComponentFixture<LeavesUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavesUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
