import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { ActivatedRoute } from '@angular/router';
import { LeavesService } from '../../services/leaves.service';
import { TextHelperService } from '../../texthelper.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-leaves-update',
  templateUrl: './leaves-update.component.html',
  styleUrls: ['./leaves-update.component.css']
})
export class LeavesUpdateComponent implements OnInit {

  leaveTypes = [];
  leave: any = {};
  status: any;
  id;
  
  mainLoader = true; 

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  modal_msg = '';
  
  constructor(
    private PageService: PageService,
    private route: ActivatedRoute,
    private LeavesService: LeavesService,
    private TextHelperService: TextHelperService,
    private modalService: SuiModalService,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Leaves');
    this.route.params.subscribe( params => {

      this.show( params['id'] );
      this.id = params['id'];
    });
  }

  show( id ) {

    this.mainLoader = true;

    this.leaveTypes['SL'] = 'Sick Leave';
		this.leaveTypes['VL'] = 'Vacation Leave';
    this.leaveTypes['EL'] = 'Emergency Leave';
    this.leaveTypes['PL'] = 'Paternity Leave';
		this.leaveTypes['ML'] = 'Maternity Leave';

    this.LeavesService.show( id ).subscribe( data => {
      if ( !data.error ) {
        this.leave = data.results;
      } else {
        console.log( data.msg );
      }
      
      this.mainLoader = !this.mainLoader;

    });

  }

  open( status ) {

    if ( status == 'approved' ) {
      this.modal_msg = 'Are you sure you want to approve this leave?';
    } else {
      this.modal_msg = 'Are you sure you want to disapprove this leave?';
    }

    this.modalService
        .open( new ConfirmModal('Are you sure?', this.modal_msg) )
        .onApprove( () => this.update(status) )
        .onDeny( () => '' )
  }

  update(status) {

    const fields = {
      status: status,
      user_id: this.leave.user_id,
      date: this.leave.date,
      type: this.leave.type
		};
    
    this.successMsg = '';
		this.errorMsg = '';
    this.isCloseMsg = false;
    
    this.LeavesService.update( this.id, fields ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
        this.leave = data.results;
        this.show(this.id);
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

  }

}
