import { Component, OnInit } from '@angular/core';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { OvertimeApproversService } from 'src/app/services/overtimeapprovers.service';
import { PageService } from 'src/app/page.service';

@Component({
  selector: 'app-overtime-approvers-show',
  templateUrl: './overtime-approvers-show.component.html',
  styleUrls: ['./overtime-approvers-show.component.css']
})
export class OvertimeApproversShowComponent implements OnInit {

  approver: any;
  members = [];
  approver_id;

  params = {
		limit: 10,
    page: 1,
    supervisor_id: ''
	};

  mainLoader = true; 

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  modal_msg = '';
  success_msg = '';

  constructor(
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private OvertimeApproversService: OvertimeApproversService,
    private PageService: PageService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Supervisor Members');
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.approver_id = params['id'];

      this.OvertimeApproversService.show( this.approver_id ).subscribe( data => {
        if ( !data.error ) {
          this.approver = data.results;
          this.params.supervisor_id = this.approver.employee._id;
          this.show();
        } else {
          this.errorMsg = data.msg;
        }
      });

      
    });
  }

  show() {

    this.mainLoader = true;

    this.OvertimeApproversService.getMembers( this.params ).subscribe( data => {
      if ( !data.error ) {
        this.members = data.results;
      }
      this.mainLoader = false;
    });
  }

  //Delete
  open( id ) {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure you want to remove this member?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {
    
    this.OvertimeApproversService.destroyMember( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

    this.mainLoader = true;
    this.show();

  }

}
