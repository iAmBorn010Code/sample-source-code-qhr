import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeApproversShowComponent } from './overtime-approvers-show.component';

describe('OvertimeApproversShowComponent', () => {
  let component: OvertimeApproversShowComponent;
  let fixture: ComponentFixture<OvertimeApproversShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeApproversShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeApproversShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
