import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeApproversCreateComponent } from './overtime-approvers-create.component';

describe('OvertimeApproversCreateComponent', () => {
  let component: OvertimeApproversCreateComponent;
  let fixture: ComponentFixture<OvertimeApproversCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeApproversCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeApproversCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
