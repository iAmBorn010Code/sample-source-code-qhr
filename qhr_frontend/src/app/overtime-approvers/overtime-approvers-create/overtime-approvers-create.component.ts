import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';
import { OvertimeApproversService } from 'src/app/services/overtimeapprovers.service';

@Component({
  selector: 'app-overtime-approvers-create',
  templateUrl: './overtime-approvers-create.component.html',
  styleUrls: ['./overtime-approvers-create.component.css']
})
export class OvertimeApproversCreateComponent implements OnInit {

  overtime_approver: any = {};

  params = {
		s: '',
  };

  employees = [];

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;
  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private EmployeesService: EmployeesService,
    private OvertimeApproversService: OvertimeApproversService
  ) { }

  ngOnInit() {

    this.EmployeesService.get( this.params ).subscribe( data => {
			if ( !data.error ) {
				this.employees = data.results;
			} else {
				this.errorMsg = data.msg;
			}
			this.mainLoader = false;
		});
  }

  getFields() {

    const fields = {
      user_id: this.overtime_approver.user_id,
    };
    
    return fields;
  }

  save() {
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.OvertimeApproversService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
