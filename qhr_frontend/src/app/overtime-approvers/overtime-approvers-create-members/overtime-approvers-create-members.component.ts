import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';
import { OvertimeApproversService } from 'src/app/services/overtimeapprovers.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-overtime-approvers-create-members',
  templateUrl: './overtime-approvers-create-members.component.html',
  styleUrls: ['./overtime-approvers-create-members.component.css']
})
export class OvertimeApproversCreateMembersComponent implements OnInit {

  member: any = {};
  approver: any = {};
  approver_id;

  params = {
		s: '',
  };

  employees = [];

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;
  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private EmployeesService: EmployeesService,
    private OvertimeApproversService: OvertimeApproversService,
    private ActivatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {

    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.approver_id = params['id'];
    });

    this.OvertimeApproversService.show( this.approver_id ).subscribe( data => {
			if ( !data.error ) {
				this.approver = data.results;
			} else {
				this.errorMsg = data.msg;
			}
		});

    this.EmployeesService.get( this.params ).subscribe( data => {
			if ( !data.error ) {
				this.employees = data.results;
			} else {
				this.errorMsg = data.msg;
			}
			this.mainLoader = false;
		});
  }

  getFields() {

    const fields = {
      user_id: this.member.user_id,
      supervisor_id: this.approver.employee._id
    };
    
    return fields;
  }

  save() {
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.OvertimeApproversService.createMember( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
