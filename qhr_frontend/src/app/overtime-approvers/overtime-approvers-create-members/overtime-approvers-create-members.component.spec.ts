import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeApproversCreateMembersComponent } from './overtime-approvers-create-members.component';

describe('OvertimeApproversCreateMembersComponent', () => {
  let component: OvertimeApproversCreateMembersComponent;
  let fixture: ComponentFixture<OvertimeApproversCreateMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeApproversCreateMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeApproversCreateMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
