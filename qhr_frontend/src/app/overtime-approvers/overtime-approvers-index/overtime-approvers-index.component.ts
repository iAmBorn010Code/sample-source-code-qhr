import { Component, OnInit } from '@angular/core';
import { PaginatorService } from '../../paginator.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from 'src/app/page.service';
import { OvertimeApproversService } from 'src/app/services/overtimeapprovers.service';

@Component({
  selector: 'app-overtime-approvers-index',
  templateUrl: './overtime-approvers-index.component.html',
  styleUrls: ['./overtime-approvers-index.component.css']
})
export class OvertimeApproversIndexComponent implements OnInit {

  approvers = [];
  paginator: any;
	params = {
		limit: 10,
    page: 1,
	};
	
  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  constructor(
    private PaginatorService: PaginatorService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private PageService: PageService,
    private OvertimeApproversService: OvertimeApproversService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Supervisors'); 
    this.get();
  }

  get() {
		  
		this.approvers = [];
		this.OvertimeApproversService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.approvers = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			} else {
        this.errorMsg = data.msg;
      }
			this.mainLoader = false;
		});
  }

  //Delete
  open( id ) {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure you want to remove this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {
    
    this.OvertimeApproversService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

    this.mainLoader = true;
    this.get();

  }
  
  //pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
