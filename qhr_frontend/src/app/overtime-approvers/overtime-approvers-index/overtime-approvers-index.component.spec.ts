import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeApproversIndexComponent } from './overtime-approvers-index.component';

describe('OvertimeApproversIndexComponent', () => {
  let component: OvertimeApproversIndexComponent;
  let fixture: ComponentFixture<OvertimeApproversIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeApproversIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeApproversIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
