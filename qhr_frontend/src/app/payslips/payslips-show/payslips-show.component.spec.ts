import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayslipsShowComponent } from './payslips-show.component';

describe('PayslipsShowComponent', () => {
  let component: PayslipsShowComponent;
  let fixture: ComponentFixture<PayslipsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayslipsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayslipsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
