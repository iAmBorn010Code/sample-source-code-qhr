import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { PayrollDetailsService } from 'src/app/services/payroll-details.service';
import { PayslipsService } from 'src/app/services/payslips.service';

@Component({
  selector: 'app-payslips-show',
  templateUrl: './payslips-show.component.html',
  styleUrls: ['./payslips-show.component.css']
})
export class PayslipsShowComponent implements OnInit {

  payslip: any = {};
  incomes: any = {};
  deductions: any = {};
  
  mainLoader = true; 

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  constructor(
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private PayslipsService: PayslipsService
  ) { }

  ngOnInit() {
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.show(params['id']);
    });
  }

  show( id ) {

    this.mainLoader = true;

     this.PayslipsService.show( id ).subscribe( data => {
        if ( !data.error ) {
          this.payslip = data.results;
          this.deductions = this.payslip.deductions;
          this.incomes = this.payslip.incomes;
          console.log('payslip: ', this.payslip );
          console.log('deductions: ', this.deductions );
          console.log('incomes: ', this.incomes );
        }

        this.mainLoader = false;
      });
  }

}
