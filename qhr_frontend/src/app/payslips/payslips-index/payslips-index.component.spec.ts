import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayslipsIndexComponent } from './payslips-index.component';

describe('PayslipsIndexComponent', () => {
  let component: PayslipsIndexComponent;
  let fixture: ComponentFixture<PayslipsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayslipsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayslipsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
