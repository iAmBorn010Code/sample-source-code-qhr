import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { PayslipsService } from 'src/app/services/payslips.service';
import { AuthService } from 'src/app/auth.service';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-payslips-index',
  templateUrl: './payslips-index.component.html',
  styleUrls: ['./payslips-index.component.css']
})
export class PayslipsIndexComponent implements OnInit {

  payslips = [];
	paginator: any;
	params = {
		limit: 10,
    page: 1,
    user_id: ''
	};

  empLoader = false;
  mainLoader = true;
  results_count = 0;
  
  canViewOthers = false;
  userPermissions = [];
  loggedUser;

  employees = [];	
  params_emp = {
		s: ''
	};

  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private PayslipsService: PayslipsService,
    private AuthService: AuthService,
    private EmployeesService: EmployeesService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Payslips');

    this.userPermissions = localStorage.perm;
    this.canViewOthers = this.userPermissions.indexOf(1100) !== -1;

    this.loggedUser = this.AuthService.getLoggedUser();

		if ( !this.canViewOthers ) {
      this.params.user_id = this.loggedUser._id;
    } else {
      this.empLoader = true;
      this.EmployeesService.get( this.params_emp ).subscribe( data => {

        console.log('employees', data);
        if ( !data.error ) {
          this.employees = data.results;
          console.log('emps', this.employees);
        }
  
        this.empLoader = false;
      });
    }

    this.get();
  }

  get() {
			
		this.mainLoader = true;
		this.payslips = []; 
		this.PayslipsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
        this.payslips = data.results;
				this.results_count = data.results_count;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( this.paginator );
			}
			this.mainLoader = false;
		});
  }
  
  //pagination
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
