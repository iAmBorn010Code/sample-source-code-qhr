import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolidayIndexComponent } from './holiday-index.component';

describe('HolidayIndexComponent', () => {
  let component: HolidayIndexComponent;
  let fixture: ComponentFixture<HolidayIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolidayIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolidayIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
