import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { HolidaysService } from '../../services/holidays.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-holiday-index',
  templateUrl: './holiday-index.component.html',
  styleUrls: ['./holiday-index.component.css']
})
export class HolidayIndexComponent implements OnInit {

  holidays = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
	};
	
  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  perm: any;
	withPerm = false;

  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private HolidaysService: HolidaysService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Holidays');
    this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(1000) !== -1 ? true : false;

    this.get();
  }

  get() {
		  
		this.holidays = [];
		this.HolidaysService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.holidays = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( this.paginator );
			}
			console.log(this.holidays);
			this.mainLoader = false;
		});
  }

  //Delete
  open( id ) {
    console.log('clicked delete!');

    console.log('ID', id); 

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure to delete this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {
    console.log('confirmed delete');

    this.HolidaysService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

    this.mainLoader = true;
    this.get();

  }
  
  //pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
