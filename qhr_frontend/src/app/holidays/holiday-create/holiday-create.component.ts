import { Component, OnInit } from '@angular/core';
import { HolidaysService } from '../../services/holidays.service';
import { Router } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';
import { AuthService } from '../../auth.service';
import { Subject } from 'rxjs/Subject';

// import {SuiModule} from 'ng2-semantic-ui';
import { CalendarEvent } from 'angular-calendar';
import {
  isSameDay,
  isSameMonth,
} from 'date-fns';

@Component({
  selector: 'app-holiday-create',
  templateUrl: './holiday-create.component.html',
  styleUrls: ['./holiday-create.component.css']
})
export class HolidayCreateComponent implements OnInit {

  holiday: any = {};

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  clickedDate = new Date();
  view = 'month';
  refresh: Subject<any> = new Subject();
  activeDayIsOpen = false;
  viewDate: Date = new Date();

  params = {
    calendar: 1,
    month: this.viewDate.getMonth() + 1,
    year: (new Date).getFullYear(),
  };

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private HolidaysService: HolidaysService,
    private AuthService: AuthService,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Holidays');
  }

  getFields() {

    const fields = {
      name: this.holiday.name,
      type: this.holiday.type,
      date: this.clickedDate,
      description: this.holiday.description,
    };
    
    return fields;
  }

  save() {
    
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.HolidaysService.store( this.getFields() ).subscribe( data => {

      this.saving = true;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
        this.resetFields();
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
      this.saving = false;
    });
  }

  resetFields() {
    this.holiday.type = null;
    this.holiday.name = null;
    this.clickedDate = new Date(),
    this.holiday.description = '';
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {

    console.log('Day Clicked: ', date);

    this.clickedDate = date;

    if (isSameMonth(date, this.viewDate)) {

      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventClicked({ event }: { event: CalendarEvent }): void {
    
    this.refresh.next();

    console.log('Event clicked', event);
  }

  getEventTitle( events ) {
    if ( !events ) {
      return '';
    }

    return events.map( event => event.title ).join(', ');
  }

  nextMonth(e) {
    
    this.params.month = e.getMonth() + 1;
    this.params.year = e.getYear() - 100;
  }

}
