import { Component, OnInit } from '@angular/core';
import { PageService } from './../../page.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

	pageName = '';
	mainBG  = '#fff';
	constructor(
		private pageService: PageService,
	) { }

	ngOnInit() {
		
		this.pageService.pageName.subscribe(
      (pageName) => {
				this.pageName = <string>pageName;
				this.mainBG = this.pageName === 'Dashboard' ? '#f6f7fb' : '#fff';
      }
    );
	}

	

}
