import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamMembersIndexComponent } from './team-members-index.component';

describe('TeamMembersIndexComponent', () => {
  let component: TeamMembersIndexComponent;
  let fixture: ComponentFixture<TeamMembersIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamMembersIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamMembersIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
