import { Component, OnInit } from '@angular/core';
import { PaginatorService } from '../../paginator.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from '../../texthelper.service';
import { TeamMembersService } from 'src/app/services/teammembers.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-team-members-index',
  templateUrl: './team-members-index.component.html',
  styleUrls: ['./team-members-index.component.css']
})
export class TeamMembersIndexComponent implements OnInit {

  members = [];
  paginator: any;
	params = {
		limit: 10,
    page: 1,
    team_id: ''
	};
	
  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  constructor(
    private PaginatorService: PaginatorService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private TeamMembersService: TeamMembersService,
    private ActivatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {

    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.params.team_id = params['id'];
      this.get();
    });
  }

  get() {

    this.TeamMembersService.get( this.params ).subscribe( data => {

      if ( !data.error ) {
        this.members = data.results;
        this.paginator = 
          this.PaginatorService.paginate( 
            data.total_count, 
            this.params.limit, 
            this.params.page, 
            5 
          );
      } else {
        this.errorMsg = data.msg;
      }
      this.mainLoader = false;
    });

  }

  open( id ) {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure to delete this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {
    
    this.TeamMembersService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

    this.mainLoader = true;
    this.get();

  }
  
  //pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
