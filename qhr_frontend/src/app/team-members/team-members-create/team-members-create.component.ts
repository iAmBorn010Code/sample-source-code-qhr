import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { TeamMembersService } from 'src/app/services/teammembers.service';
import { ActivatedRoute, Params } from '@angular/router';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-team-members-create',
  templateUrl: './team-members-create.component.html',
  styleUrls: ['./team-members-create.component.css']
})
export class TeamMembersCreateComponent implements OnInit {

  employees: any = {};

  employee_id;
  team_id;

  mainLoader = true;
  saving = false;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  constructor(
    private TextHelperService: TextHelperService,
    private TeamMembersService: TeamMembersService,
    private ActivatedRoute: ActivatedRoute,
    private EmployeesService: EmployeesService
  ) { }

  ngOnInit() {

    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.team_id = params['id'];
    });
    this.EmployeesService.get( ).subscribe( data => {

      if ( !data.error ) {
        this.employees = data.results;
      } else {
        this.errorMsg = data.msg;
      }
      this.mainLoader = false;
    });
  }

  getFields() {

    const fields = {
      user_id: this.employee_id,
      team_id: this.team_id
    };
    
    return fields;
  }

  save() {

    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    this.TeamMembersService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
