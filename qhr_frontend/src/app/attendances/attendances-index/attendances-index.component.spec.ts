import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendancesIndexComponent } from './attendances-index.component';

describe('AttendancesIndexComponent', () => {
  let component: AttendancesIndexComponent;
  let fixture: ComponentFixture<AttendancesIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendancesIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendancesIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
