import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { AttendancesService } from '../../services/attendances.service';
import { EmployeesService } from 'src/app/services/employees.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from 'src/app/texthelper.service';

@Component({
  selector: 'app-attendances-index',
  templateUrl: './attendances-index.component.html',
  styleUrls: ['./attendances-index.component.css']
})
export class AttendancesIndexComponent implements OnInit {

  	attendances = [];
	paginator: any;
	params = {
		limit: 20,
		page: 1,
		filter_date: 'today',
		date_from: '',
		date_to: '',
		emp_id: '',
		w: '',
		ot: ''
	};

	employees = [];	
	params_emp = {
		s: ''
	};

	w = {
		lates: 0,
		absents: 0,
		undertime: 0,
		on_time: 0
	}

	mainLoader = true;
	empLoader = true;
	statsLoader = true;	

	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	perm: any;
	user: any = {};
	withPerm = false;

  	constructor(
  		private PageService: PageService,
		private PaginatorService: PaginatorService,
		private AttendancesService: AttendancesService,
		private EmployeesService: EmployeesService  ,
		private modalService: SuiModalService,
		private TextHelperService: TextHelperService,
  	) { }

  	ngOnInit() {

		this.PageService.pageName.next('Attendances');
		this.perm = localStorage.getItem('perm');
		this.user = JSON.parse( localStorage.getItem('user') );
		this.withPerm = this.perm.indexOf(300) !== -1 ? true : false;

		if ( this.withPerm ) {
			this.EmployeesService.get( this.params_emp ).subscribe( data => {
				if ( !data.error ) {
					this.employees = data.results;
				}
				this.empLoader = false;
			});
		}

		this.get();
	}
	  

	get() {
		this.mainLoader = true;
		this.attendances = [];
		this.AttendancesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.attendances = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			}
			
			this.mainLoader = false;
			this.statistics();
		});
	}

	statistics() {

		this.statsLoader = true;
		let w = [ 'lates', 'absents', 'undertime', 'on_time' ];

		this.params.emp_id = !this.withPerm ? this.user._id : '' ;

		for ( var i = 0; i < w.length; i++ ) {
			this.params.w = w[i];
			this.AttendancesService.getStatistics( this.params ).subscribe( data => {
				if ( !data.error ) {
					let count = data.results;

					if ( data.w == 'lates' ) {
						this.w.lates = count;
					} else if ( data.w == 'absents' ) {
						this.w.absents = count;
					} else if ( data.w == 'undertime' ) {
						this.w.undertime = count;
					} else if ( data.w == 'on_time' ) {
						this.w.on_time = count;
					}
				}
			});

		}

		this.statsLoader = false;

	}

	getOT() {
		this.params.ot = this.params.ot == 'getOT' ? '' : 'getOT';
		this.get();
	}

	open( id ) {

	this.modalService
		.open( new ConfirmModal('Are you sure?', 'Are you sure you want to delete this attendance?') )
		.onApprove( () => this.delete( id ) )
		.onDeny( () => '' )
	}

	delete( id ) {
	
		this.AttendancesService.destroy( id ).subscribe( data => {
			if ( !data.error ) {
			this.isError = false;
			this.successMsg = data.msg;
			} else {
			this.isError = true;
			const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
			this.errorMsg = msg;
			}
		});

		this.mainLoader = true;
		this.get();

	}

	// THIS IS FOR PAGINATIONS
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

	filter( type ) {

		this.params.filter_date = type;
		this.params.page = 1;

		if ( this.params.filter_date != 'custom' ) {
			this.params.date_from = '';
			this.params.date_to = '';
		}

		this.get();
	}
}
