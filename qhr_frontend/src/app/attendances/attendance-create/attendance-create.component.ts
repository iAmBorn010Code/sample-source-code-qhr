import { Component, OnInit } from '@angular/core';
import { AttendancesService } from '../../services/attendances.service';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-attendance-create',
  templateUrl: './attendance-create.component.html',
  styleUrls: ['./attendance-create.component.css']
})
export class AttendanceCreateComponent implements OnInit {

  attendance: any = {
    status: 0,
    with_ot: 0,
    with_break: 0,
    on_leave: 0,
    with_schedule: 1,
  };

  params = {
		s: '',
  };
  employees = [];
  show_emp: any = {};
  status = 'Present';

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;
  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private AttendancesService: AttendancesService,
    private EmployeesService: EmployeesService,
  ) { }

  ngOnInit() {
    this.EmployeesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.employees = data.results;
			} else {
				this.errorMsg = data.msg;
			}

			this.mainLoader = false;
		});
  }

  getFields() {

    this.status = this.attendance.status == 1 ? 'Absent' : 'Present';

    const fields = {
      user_id: this.attendance.employee_id,
      status: this.status,
      with_break: this.attendance.with_break,
      with_ot: this.attendance.with_ot,
      with_schedule: this.attendance.with_schedule,
      with_night_differential: this.attendance.with_night_differential,
      on_leave: this.attendance.on_leave,

      leave_type: this.attendance.leave_type,
      salary_type: this.attendance.salary_type,
      daily_rate: this.attendance.daily_rate,
      ot_rate: this.attendance.ot_rate,
      
      time_in: this.attendance.time_in,
      time_in_ref: this.attendance.time_in_ref,
      time_out: this.attendance.time_out,
      time_out_ref: this.attendance.time_out_ref,  
      
      time_in2: this.attendance.time_in2,
      time_in2_ref: this.attendance.time_in2_ref,
      time_out2: this.attendance.time_out2,
      time_out2_ref: this.attendance.time_out2_ref,

      ot_in: this.attendance.ot_in,
      ot_in_ref: this.attendance.ot_in_ref,
      ot_out: this.attendance.ot_out,
      ot_out_ref: this.attendance.ot_out_ref,

      nd_in_ref: this.attendance.nd_in_ref,
      nd_out_ref: this.attendance.nd_out_ref,
      nd_rate: this.attendance.nd_rate
      
    };

    return fields;
  }

  save() {
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    console.log('check: ', this.getFields());

    if ( !this.attendance.employee_id ){
      this.isError = true;
      let msg = "Please select employee.";
      this.errorMsg = msg;
      this.saving = false;
      return;
    };    

    this.AttendancesService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        let msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
