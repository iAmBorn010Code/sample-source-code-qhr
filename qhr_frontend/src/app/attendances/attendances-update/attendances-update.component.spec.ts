import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendancesUpdateComponent } from './attendances-update.component';

describe('AttendancesUpdateComponent', () => {
  let component: AttendancesUpdateComponent;
  let fixture: ComponentFixture<AttendancesUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendancesUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendancesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
