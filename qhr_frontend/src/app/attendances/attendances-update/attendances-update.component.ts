import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { ActivatedRoute, Params } from '@angular/router';
import { AttendancesService } from '../../services/attendances.service';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-attendances-update',
  templateUrl: './attendances-update.component.html',
  styleUrls: ['./attendances-update.component.css']
})
export class AttendancesUpdateComponent implements OnInit {

  	attendance: any = {
		status: 0,
		with_ot: 0
	};
	attendance_id;
	status;

  	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
	saving = false;

  	withBreak;

	constructor(
		private PageService: PageService,
		private TextHelperService: TextHelperService,
		private AttendancesService: AttendancesService,
		private ActivatedRoute: ActivatedRoute,
	) { }

	ngOnInit() {
		this.PageService.pageName.next('Attendances');

		this.ActivatedRoute.params.subscribe( params => {
			this.attendance_id = params['id'];
		});
	
		this.AttendancesService.show( this.attendance_id ).subscribe( data => {
			if ( !data.error ) {
				this.attendance = data.results;
				console.log('attendance', this.attendance);
				this.attendance.status = this.attendance.status == 'Absent' ? 1 : 0;
			}
			
			this.withBreak = this.attendance.with_break;
			this.mainLoader = false;

		});
	}
	
	update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;

		// this.saving = true;

		this.status = this.attendance.status == 1 ? 'Absent' : 'Present';

		const fields = {
			status: this.status,
			on_leave: this.attendance.is_onleave,
			with_break: this.attendance.with_break,
			with_ot: this.attendance.with_ot,
			with_schedule: this.attendance.with_schedule,
			with_night_differential: this.attendance.with_night_differential,

			time_in: this.attendance.time_in,
			time_out: this.attendance.time_out,
			time_in_ref: this.attendance.time_in_ref,
			time_out_ref: this.attendance.time_out_ref,

			time_in2: this.attendance.time_in2,
			time_out2: this.attendance.time_out2,
			time_in2_ref: this.attendance.time_in2_ref,
			time_out2_ref: this.attendance.time_out2_ref,

			ot_in: this.attendance.ot_in,
			ot_out: this.attendance.ot_out,
			ot_in_ref: this.attendance.ot_in_ref,
			ot_out_ref: this.attendance.ot_out_ref,

			nd_in_ref: this.attendance.nd_in_ref,
			nd_out_ref: this.attendance.nd_out_ref,
			nd_rate: this.attendance.nd_rate,

			reason_for_update: this.attendance.reason_for_update,
			leave_type: this.attendance.leave_type,
			salary_type: this.attendance.salary_type,
			daily_rate: this.attendance.daily_rate,
			ot_rate: this.attendance.ot_rate,
		};

 	 	this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.AttendancesService.update( params['id'], fields ).subscribe( data => {
				this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
					this.isError = true;
					const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
					this.errorMsg = msg;
				}
			});
		});
	}

}
