import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { AttendancesService } from '../../services/attendances.service';
import { TextHelperService } from '../../texthelper.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-attendances-show',
  templateUrl: './attendances-show.component.html',
  styleUrls: ['./attendances-show.component.css']
})
export class AttendancesShowComponent implements OnInit {

  attendance: any = {};
  id: string = null;
	
  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';
  
  mainLoader = true;

  constructor(
    private PageService: PageService,
    private route: ActivatedRoute,
    private AttendancesService: AttendancesService,
    private TextHelperService: TextHelperService,
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Attendances');
    this.route.params.subscribe( params => {
      this.id = params['id'];
      this.show( this.id );
    });
  }

  show( id ) {

    console.log( 'called show' );

    this.AttendancesService.show( id ).subscribe( data => {
      if ( !data.error ) {
        this.attendance = data.results;
        console.log('attendance', this.attendance );
      } else {
        console.log( data.msg );
      }

      this.mainLoader = false;

    });

  }

}
