import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendancesShowComponent } from './attendances-show.component';

describe('AttendancesShowComponent', () => {
  let component: AttendancesShowComponent;
  let fixture: ComponentFixture<AttendancesShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendancesShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendancesShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
