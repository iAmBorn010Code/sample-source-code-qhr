import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesUpdateBenefitsComponent } from './employees-update-benefits.component';

describe('EmployeesUpdateBenefitsComponent', () => {
  let component: EmployeesUpdateBenefitsComponent;
  let fixture: ComponentFixture<EmployeesUpdateBenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesUpdateBenefitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesUpdateBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
