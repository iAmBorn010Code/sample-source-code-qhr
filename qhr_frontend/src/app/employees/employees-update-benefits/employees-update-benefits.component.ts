import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { SharedDataEmployeeService } from '../../services/shared-data-employee.service';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';


@Component({
  selector: 'app-employees-update-benefits',
  templateUrl: './employees-update-benefits.component.html',
  styleUrls: ['./employees-update-benefits.component.css']
})
export class EmployeesUpdateBenefitsComponent implements OnInit {

  employee: any = {};

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';
	
	mainLoader = true;
	saving = false;
  
  constructor(
    private TextHelperService: TextHelperService,
    private SharedDataEmployeeService: SharedDataEmployeeService,
    private EmployeesService: EmployeesService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    
    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
        console.log('here personal' + params['id'] );
				if ( !data.error ) {
					this.employee = data.results;
					this.SharedDataEmployeeService.employee.next( this.employee );
				} else {
					this.errorMsg = data.msg;
				}
				this.mainLoader = !this.mainLoader;
			});
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			sss: this.employee.sss,
			philhealth: this.employee.philhealth,
			pagibig: this.employee.pagibig,
			tin: this.employee.tin,
		};

		console.log( fields );

 	 	this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.update( params['id'], fields ).subscribe( data => {
				this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
						this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
				}
			});
		});
	}


}

