import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { SharedDataEmployeeService } from '../../services/shared-data-employee.service';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';
import { DepartmentsService } from '../../services/departments.service';
import { BranchesService } from '../../services/branches.service';

@Component({
  selector: 'app-employees-update-work',
  templateUrl: './employees-update-work.component.html',
  styleUrls: ['./employees-update-work.component.css']
})

export class EmployeesUpdateWorkComponent implements OnInit {
  
	employee: any = {};
	departments = [];
	branches = [];

	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
	saving = false;

	date: any;
  
	constructor(
		private TextHelperService: TextHelperService,
		private SharedDataEmployeeService: SharedDataEmployeeService,
		private EmployeesService: EmployeesService,
		private ActivatedRoute: ActivatedRoute,
		private DepartmentsService: DepartmentsService,
		private BranchesService: BranchesService,
	) { }

	ngOnInit() {
		
		this.date = new Date();
			
		this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
			
				if ( !data.error ) {
					this.employee = data.results;
					this.SharedDataEmployeeService.employee.next( this.employee );
				} else {
					this.errorMsg = data.msg;
				}
				this.mainLoader = !this.mainLoader;
			});
		});
			
		this.getDepartments();
		this.getBranches();
	}

  	update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			emp_ID: this.employee.emp_ID,
			job_title: this.employee.job_title,
			job_description: this.employee.job_description,
			department_id: this.employee.department_id,
			designation: this.employee.designation,
			branch_id: this.employee.branch_id,
			employment_status: this.employee.employment_status,
			status: this.employee.status,
			date_joined: this.employee.date_joined
		};

 	 	this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.update( params['id'], fields ).subscribe( data => {
				console.log( data );
				this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
						this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
				}
			});
		});
	}
	
	getDepartments() {

		this.DepartmentsService.get( {} ).subscribe( data => {

			if ( !data.error ) {
				this.departments = data.results;
			}
		});

	}

	getBranches() {

		this.BranchesService.get( {} ).subscribe( data => {

			if ( !data.error ) {
				this.branches = data.results;
			}
		});

	}


}

