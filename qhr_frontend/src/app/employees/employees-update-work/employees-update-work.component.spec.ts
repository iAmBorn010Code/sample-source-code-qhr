import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesUpdateWorkComponent } from './employees-update-work.component';

describe('EmployeesUpdateWorkComponent', () => {
  let component: EmployeesUpdateWorkComponent;
  let fixture: ComponentFixture<EmployeesUpdateWorkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesUpdateWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesUpdateWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
