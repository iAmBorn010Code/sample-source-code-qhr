import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { EmployeesService } from '../../services/employees.service';

@Component({
  selector: 'app-employees-index',
  templateUrl: './employees-index.component.html',
  styleUrls: ['./employees-index.component.css']
})
export class EmployeesIndexComponent implements OnInit {

	employees = [];
	paginator: any;
	params = {
		s: '',
		limit: 12,
		page: 1,
		sort: 'first_name:1',
		w: '',
		status: 'active'
	};
	
	status = 'active';
	search: any = {};
	keyword;
	results_count = 0;

	mainLoader = true;

	search_txt = false;
	errorMsg = '';

  	constructor(
  		private PageService: PageService,
  		private PaginatorService: PaginatorService,
      private EmployeesService: EmployeesService,
  	) { }

  	ngOnInit() {

		this.PageService.pageName.next('Employees');
		this.get();
	}
	  

	get() {

		if ( this.search.keyword ) {
			if ( this.search.keyword.length >= 3 && /\S/.test(this.search.keyword) ){
				this.search_txt = true;
				this.params.s = this.search.keyword;
			} else {
				this.errorMsg = 'Please input atleast 3 characters';
				this.mainLoader = false;
				return;
			}
		}
		
		this.mainLoader = true;
		this.employees = [];
		this.errorMsg = '';

		this.EmployeesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.employees = data.results;
				this.results_count = data.results_count;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
			} else {
				this.errorMsg = data.msg;
			}

			this.mainLoader = false;
		});
	}

	clearQuery() {
		this.search.keyword = '';
		this.search_txt = false;
		this.params.s = this.search.keyword;
		this.get();
	}

	sort( sort ) {
		this.params.sort = sort;
		this. get();
	}

	change_status( get_status ) {
		console.log('change status', get_status);
		this.params.status = get_status;
		this. get();
	}

	get_birthdays() {
		this.params.w === 'upcoming_birthdays' ? this.params.w = '' : this.params.w = 'upcoming_birthdays';
		this.get();
	}
	  

	// THIS IS FOR PAGINATIONS
	nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}
}
