import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesUpdateDailyWorkSchedulesComponent } from './employees-update-daily-work-schedules.component';

describe('EmployeesUpdateDailyWorkSchedulesComponent', () => {
  let component: EmployeesUpdateDailyWorkSchedulesComponent;
  let fixture: ComponentFixture<EmployeesUpdateDailyWorkSchedulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesUpdateDailyWorkSchedulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesUpdateDailyWorkSchedulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
