import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { SharedDataEmployeeService } from '../../services/shared-data-employee.service';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-employees-update-daily-work-schedules',
  templateUrl: './employees-update-daily-work-schedules.component.html',
  styleUrls: ['./employees-update-daily-work-schedules.component.css']
})
export class EmployeesUpdateDailyWorkSchedulesComponent implements OnInit {

  employee: any = {};

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  isError2: boolean;
	isCloseMsg2: boolean;
	successMsg2 = '';
  errorMsg2 = '';

  mainLoader = true;
  saving = false;
  
  constructor(
    private TextHelperService: TextHelperService,
    private SharedDataEmployeeService: SharedDataEmployeeService,
    private EmployeesService: EmployeesService,
    private ActivatedRoute: ActivatedRoute,
  ) {

    // Set default value while loading.
    this.employee.daily_work_schedules = {
      Monday: {enabled: 0, in: null, with_break: false, in2: null, out: null, out2: null}, 
      Tueday: {enabled: 0, in: null, with_break: false, in2: null, out: null, out2: null}, 
      Wednesday: {enabled: 0, in: null, with_break: false, in2: null, out: null, out2: null}, 
      Thursday: {enabled: 0, in: null, with_break: false, in2: null, out: null, out2: null}, 
      Friday: {enabled: 0, in: null, with_break: false, in2: null, out: null, out2: null}, 
      Saturday: {enabled: 0, in: null, with_break: false, in2: null, out: null, out2: null}, 
      Sunday: {enabled: 0, in: null, with_break: false, in2: null, out: null, out2: null}
    };
   }

  ngOnInit() {
    
    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
          this.employee = data.results;
          this.SharedDataEmployeeService.employee.next( this.employee );
        } else {
					this.errorMsg = data.msg;
				}
        this.mainLoader = !this.mainLoader;
			});
    });
  }

  update(type) {

		this.successMsg = '';
		this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    let fields = {};

    if ( type == 'otherConfig' ) {

      fields = {
        allow_ot_late: this.employee.allow_ot_late,
      };

    } else {

      // TODO: Need to validate values before saving it into DB.
      fields = {
        daily_work_schedules: this.employee.daily_work_schedules,
      };

    }

    console.log('fields', fields );
    this.saving = false;

 	 	this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.update( params['id'], fields ).subscribe( data => {
        this.saving = false;
				if ( !data.error ) {

          if ( type == 'otherConfig' ) {
            this.isError2 = false;
					  this.successMsg2 = data.msg;
          } else {
            this.isError = false;
					  this.successMsg = data.msg;  
          }
					
				} else {

          if ( type == 'otherConfig' ) {
            this.isError2 = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg2 = msg;
          } else {
            this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
          }
						
				}
			});
		});
	}


}
