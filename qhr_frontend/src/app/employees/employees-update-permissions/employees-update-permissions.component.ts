import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SharedDataEmployeeService } from '../../services/shared-data-employee.service';
import { EmployeesService } from '../../services/employees.service';
import { PermissionsService } from '../../services/permissions.service';
import { UserPermissionsService } from '../../services/userpermissions.service';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-employees-update-permissions',
  templateUrl: './employees-update-permissions.component.html',
  styleUrls: ['./employees-update-permissions.component.css']
})
export class EmployeesUpdatePermissionsComponent implements OnInit {

  employee: any = {
    basic_salary: 15000, 
  }

  permissions: any;
  currModule: any;

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;
  saving = false;

  userPermissions = []

  constructor(
    private TextHelperService: TextHelperService,
    private SharedDataEmployeeService: SharedDataEmployeeService,
    private EmployeesService: EmployeesService,
    private PermissionsService: PermissionsService,
    private UserPermissionsService: UserPermissionsService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    
    console.log('called init update permissons');

    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
          this.employee = data.results;
          this.SharedDataEmployeeService.employee.next( this.employee );
          this.getPermissions();
          this.getUserPermissions();
        }  else {
          this.errorMsg = data.msg;
        }

        this.mainLoader = false;
			});
    });
  }

  getPermissions() {

    this.PermissionsService.get().subscribe( data => {
      
      if ( !data.error ) {
        this.permissions = data.results;
        this.currModule = this.permissions[0];
        console.log('this.permissions.',this.permissions);
        console.log('this.currModule.',this.currModule);
      }
    });
  }

  getUserPermissions() {

    this.UserPermissionsService.show(this.employee._id).subscribe( data => {
      
      if ( !data.error ) {
        this.userPermissions = data.results.perm;
        console.log('this.userPermissions.',this.userPermissions);
      }
    });
  }

  update() {

    console.log('will update the permissions', this.userPermissions);
		this.successMsg = '';
		this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

		const fields = {
      perm: this.userPermissions.join(','),
      user_id: this.employee._id,
		};

		console.log( fields );
    // TODO: user permission_id instead.
    this.UserPermissionsService.update( this.employee._id, fields ).subscribe( data => {
      
      this.saving = false;
      console.log( data );
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
          this.isError = true;
          const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
          this.errorMsg = msg;
      }
    });
    
  }

  onChangeCurrentModule( module_name: string ) {

    for( let i = 0; i < this.permissions.length; i++ ) {
      
      if ( this.permissions[i].module_name === module_name ) {
        this.currModule = this.permissions[i];
        break;
      }
    }
  }

  
  onPermissionChange( event: any ) {

    console.log('BEFORE: ', this.userPermissions);
    let value = parseInt( event.target.value );
    
    if ( event.target.checked ) {

      // Add from user permissions.
      if ( this.userPermissions.indexOf( value ) === -1 ) {
        this.userPermissions.push( value );
      }
    } else {

      // Remove from user permissions.
      if ( this.userPermissions.indexOf( value ) > -1 ) {

        const index: number = this.userPermissions.indexOf( value );
        if ( index !== -1 ) {
            this.userPermissions.splice(index, 1);
        }   
      }
    }

    console.log('AFTER: ', this.userPermissions);
  }

}
