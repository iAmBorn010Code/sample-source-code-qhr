import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesUpdatePermissionsComponent } from './employees-update-permissions.component';

describe('EmployeesUpdatePermissionsComponent', () => {
  let component: EmployeesUpdatePermissionsComponent;
  let fixture: ComponentFixture<EmployeesUpdatePermissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesUpdatePermissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesUpdatePermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
