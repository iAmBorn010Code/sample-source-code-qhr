import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesUpdatePasswordComponent } from './employees-update-password.component';

describe('EmployeesUpdatePasswordComponent', () => {
  let component: EmployeesUpdatePasswordComponent;
  let fixture: ComponentFixture<EmployeesUpdatePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesUpdatePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesUpdatePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
