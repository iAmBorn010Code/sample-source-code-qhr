import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { SharedDataEmployeeService } from '../../services/shared-data-employee.service';
import { Router } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-employees-update-password',
  templateUrl: './employees-update-password.component.html',
  styleUrls: ['./employees-update-password.component.css']
})
export class EmployeesUpdatePasswordComponent implements OnInit {

  current_password: string;
  new_password: string;
  confirm_password: string;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';
  localUser: any = {};
  employeeId = null;

  mainLoader = true;
  saving = false;

  userPermissions = [];
  loggedUser;

  ownProfile = false;

  constructor(
    private router: Router,
    private EmployeesService: EmployeesService,
    private ActivatedRoute: ActivatedRoute,
    private SharedDataEmployeeService: SharedDataEmployeeService,
    private AuthService: AuthService,
    private TextHelperService: TextHelperService
  ) { }

  ngOnInit() {

    this.userPermissions = localStorage.perm;
    this.loggedUser = this.AuthService.getLoggedUser();
    
    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {

      this.employeeId = params['id'];

      // Is own profile?
      this.ownProfile = this.loggedUser._id === this.employeeId;

			this.EmployeesService.show( this.employeeId ).subscribe( data => {
				if ( !data.error ) {
          this.SharedDataEmployeeService.employee.next( data.results );
        } else {
          this.errorMsg = data.msg;
        }
        this.mainLoader = !this.mainLoader;
			});
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

		const fields = {
      current_password: this.current_password,
      new_password: this.new_password,
      confirm_password: this.confirm_password,
    };
    
    console.log( fields );

    this.EmployeesService.updatePassword( this.employeeId, fields ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
        // this.new_password = '';
      } else {
          this.isError = true;
          const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
          this.errorMsg = msg;
      }
    });

	}

}
