import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesUpdateSocialComponent } from './employees-update-social.component';

describe('EmployeesUpdateSocialComponent', () => {
  let component: EmployeesUpdateSocialComponent;
  let fixture: ComponentFixture<EmployeesUpdateSocialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesUpdateSocialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesUpdateSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
