import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { SharedDataEmployeeService } from '../../services/shared-data-employee.service';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-employees-update-social',
  templateUrl: './employees-update-social.component.html',
  styleUrls: ['./employees-update-social.component.css']
})
export class EmployeesUpdateSocialComponent implements OnInit {

  employee: any = {};

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';
	
	mainLoader = true;
	saving = false;
  
  constructor(
    private TextHelperService: TextHelperService,
    private SharedDataEmployeeService: SharedDataEmployeeService,
    private EmployeesService: EmployeesService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    
    console.log('called init personal');

    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
					this.employee = data.results;
					this.SharedDataEmployeeService.employee.next( this.employee );
				} else {
					this.errorMsg = data.msg;
				}
				this.mainLoader = !this.mainLoader;
			});
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			linkedin: this.employee.linkedin,
			facebook: this.employee.facebook,
			instagram: this.employee.instagram,
			twitter: this.employee.twitter,
			skype: this.employee.skype,
		};

		console.log( fields );

 	 	this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.update( params['id'], fields ).subscribe( data => {
				this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
						this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
				}
			});
		});
	}


}

