import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SharedDataEmployeeService } from '../../services/shared-data-employee.service';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-employees-update-salary',
  templateUrl: './employees-update-salary.component.html',
  styleUrls: ['./employees-update-salary.component.css']
})
export class EmployeesUpdateSalaryComponent implements OnInit {

  employee: any = {}

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;
  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private SharedDataEmployeeService: SharedDataEmployeeService,
    private EmployeesService: EmployeesService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    
    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
          this.employee = data.results;
          this.employee.salary_type = this.employee.salary_type || 'monthly-fixed';
          this.SharedDataEmployeeService.employee.next( this.employee );
        } else {
          this.errorMsg = data.msg;
        }
        
        this.mainLoader = !this.mainLoader;
			});
    });
  }

  update() {
		this.successMsg = '';
		this.errorMsg = '';
    this.isCloseMsg = true;
    this.saving = true;

		const fields = {
      basic_salary: this.employee.basic_salary,
      salary_type: this.employee.salary_type,
      with_cola: this.employee.with_cola,
      cola_rate: this.employee.with_cola ? this.employee.cola_rate : 0,
		};

 	 	this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.update( params['id'], fields ).subscribe( data => {
        this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
						this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
        }
        
        this.isCloseMsg = false;
			});
		});
	}

}
