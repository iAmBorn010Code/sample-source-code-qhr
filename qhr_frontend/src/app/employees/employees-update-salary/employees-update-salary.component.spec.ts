import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesUpdateSalaryComponent } from './employees-update-salary.component';

describe('EmployeesUpdateSalaryComponent', () => {
  let component: EmployeesUpdateSalaryComponent;
  let fixture: ComponentFixture<EmployeesUpdateSalaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesUpdateSalaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesUpdateSalaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
