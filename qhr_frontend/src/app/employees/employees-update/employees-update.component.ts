
import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';
import { SharedDataEmployeeService } from '../../services/shared-data-employee.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-employees-edit',
  templateUrl: './employees-update.component.html',
  styleUrls: ['./employees-update.component.css'],
})
export class EmployeesUpdateComponent implements OnInit {

	employee: any = {
		first_name: 0,
		middle_name: '',
		last_name: '',
		suffix: '',
		email: '',
		address: '',
		contact_nos: '',
		job_title: '',
		job_description: '',
		photo:'',
		file:'',
		mood:''
		};

	newMood : string;

	mainLoader = true;  

	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	isHovering : boolean;

	selectedFile: File = null;
	uploads_history = [];

	fileUploading = false;
	fileUploaded = false;
	fileUploadTriggered = false;
	fileUploadingMsg = '';

	correctFormat: boolean;
	correctSize: boolean;
  	constructor(
		private PageService: PageService,
		private TextHelperService: TextHelperService,
		private EmployeesService: EmployeesService,
		private SharedDataEmployeeService: SharedDataEmployeeService,
		private ActivatedRoute: ActivatedRoute,
	) { }

	ngOnInit() {

		this.PageService.pageName.next('Employees');
		// this.ActivatedRoute.params.forEach(( params: Params ) => {
		// 	console.log( params );
		// });

		this.SharedDataEmployeeService.employee.subscribe( (employee) => {
			this.employee = <string>employee;
			this.newMood = this.employee.mood;
			
		});

		this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
					this.employee = data.results;
					this.SharedDataEmployeeService.employee.next( this.employee );
				}
				this.mainLoader = !this.mainLoader;
			});
		});


	}

	mouseHovering(){
		this.isHovering = true;
	}

	mouseLeft(){
		this.isHovering = false;
		this.newMood = this.employee.mood;
	}

	onFileChanged(event) {
		
		this.selectedFile = <File>event.target.files[0];
		const uploadData = new FormData();
		
		uploadData.append('user_id', this.employee._id);
		uploadData.append('avatar', this.selectedFile, this.selectedFile.name);


		this.fileUploading = true;
		this.fileUploadTriggered = true;

		const fileMaxSize = 1024 * 1024 * 2; // Max file size is 2mb.
		if ( this.selectedFile.size > fileMaxSize ) {
			this.fileUploading = false;
			this.fileUploaded = false;
			this.fileUploadingMsg = 'File size is too large.'
		}

		try {

			// Check file type
			if ( this.selectedFile.type == 'image/jpeg' || this.selectedFile.type == 'image/png' ) {
				this.EmployeesService.avatarUpload(uploadData).subscribe(data => {

					this.fileUploading = false;
					if (! data.error ) {
						this.fileUploaded = true;
						this.employee.photo = data.photo;
						this.PageService.profilePhoto.next( this.employee.photo );
					} else {
						this.fileUploaded = false;
						this.fileUploadingMsg = data.msg;
					}
				});
			} else {
				this.fileUploading = false;
				this.fileUploaded = false;
				this.fileUploadingMsg = 'File type is not supported.'
			}
		} catch(err) {
			this.fileUploading = false;
			this.fileUploaded = false;
			this.fileUploadingMsg = 'Unable to upload image at this time. Please try again later.'
		}
	
	  }

	update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;

		const fields = {
			mood: this.newMood,
		};

 	 	this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.EmployeesService.update( params['id'], fields ).subscribe( data => {
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
					this.employee.mood = this.newMood;
					this.SharedDataEmployeeService.employee.next( this.employee );
				} else {
						this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
				}
			});
		});
	}
}
