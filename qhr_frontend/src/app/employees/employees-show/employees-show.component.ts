import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeesService } from '../../services/employees.service';
import { PageService } from '../../page.service';
import { BranchesService } from 'src/app/services/branches.service';
import { DepartmentsService } from 'src/app/services/departments.service';

@Component({
  selector: 'app-employees-show',
  templateUrl: './employees-show.component.html',
  styleUrls: ['./employees-show.component.css']
})
export class EmployeesShowComponent implements OnInit {

  employee: any;
  branch: any;
  department: any;

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  perm: any;
	withPerm = false;

  mainLoader = true;

  constructor(
    private PageService: PageService,
    private route: ActivatedRoute,
    private EmployeesService: EmployeesService,
    private BranchesService: BranchesService,
    private DepartmentsService: DepartmentsService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Employees');
    this.perm = localStorage.getItem('perm');
		this.withPerm = this.perm.indexOf(200) !== -1 ? true : false;

    this.route.params.subscribe( params => {

      this.show( params['id'] )
      .then( res => {

        if ( !this.employee.branch_id ){
          return new Promise((resolve, reject) => {
            resolve();
          });
        } else {
          return this.getBranch();          
        }
        
      })
      .then( res => {
        if ( !this.employee.department ){
          return new Promise((resolve, reject) => {
            this.mainLoader = false;
            resolve();
          });
        } else {
          return this.getDepartment();          
        }
        
      })
      .catch(function(error){
        this.isError = true;
        this.errorMsg = !("message" in error) ? '' : error.message;
      });

    });
  }

  show( id ) {

    return new Promise((resolve, reject) => {
      this.EmployeesService.show( id ).subscribe( data => {

        if ( !data.error ) {
         this.employee = data.results;
         resolve();
        } else {
          this.mainLoader = false;
          this.errorMsg = data.msg;
          reject();
          return;
        }

      });
    }); 

  }

  getBranch() {
    return new Promise((resolve, reject) => {
      this.BranchesService.show( this.employee.branch_id ).subscribe( data => {

        if ( !data.error ) {
         this.branch = data.results;
         resolve();
        } else {
          this.mainLoader = false;
          this.errorMsg = data.msg;
          reject();
          return;
        }

      });
    }); 
  }

  getDepartment() {
    return new Promise((resolve, reject) => {
      this.DepartmentsService.show( this.employee.department_id ).subscribe( data => {

        this.mainLoader = false;
        if ( !data.error ) {
          this.department = data.results;
          resolve();
        } else {
          this.mainLoader = false;
          this.errorMsg = data.msg;
          reject();
          return;
        }

      });
    }); 
  }

}
