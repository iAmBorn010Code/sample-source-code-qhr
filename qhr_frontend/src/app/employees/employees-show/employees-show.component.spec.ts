import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesShowComponent } from './employees-show.component';

describe('EmployeesShowComponent', () => {
  let component: EmployeesShowComponent;
  let fixture: ComponentFixture<EmployeesShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
