import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from 'src/app/texthelper.service';


@Component({
  selector: 'app-employees-update-leaves',
  templateUrl: './employees-update-leaves.component.html',
  styleUrls: ['./employees-update-leaves.component.css']
})
export class EmployeesUpdateLeavesComponent implements OnInit {

  emp_id;
  employee: any = {};
  
  leaves = [];

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';
	
	mainLoader = true;
	saving = false;

  constructor(
    private route: ActivatedRoute,
    private EmployeesService: EmployeesService,
		private TextHelperService: TextHelperService
  ) { 

    // Set default value while loading.
    this.employee.leaves = {
      SL: {credit: 10, used: null, pending: null}, 
      VL: {credit: 10, used: null, pending: null}, 
      EL: {credit: 10, used: null, pending: null}, 
    };

  }

  ngOnInit() {
    
    this.route.parent.params.forEach(( params: Params ) => {

			this.emp_id = params['id'];

    });

    this.leaves = [];
		this.EmployeesService.show( this.emp_id ).subscribe( data => {

      if ( !data.error ) {
        this.employee = data.results;
      }  else {
        this.errorMsg = data.msg;
      }

      this.mainLoader = false;
    });

  }

  update() {
    this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		// const fields = {
		// 	SL: this.leaves['SL'].credit,
		// 	VL: this.leaves['VL'].credit,
    //   EL: this.leaves['EL'].credit,
    //   update_leave: 1
    // };
    
    const fields = {
      leaves: this.employee.leaves,
		};

    console.log( fields );
    
    console.log( 'id', this.emp_id );

    this.EmployeesService.update( this.emp_id, fields ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
          this.isError = true;
          const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
          this.errorMsg = msg;
      }
    });
  }

}
