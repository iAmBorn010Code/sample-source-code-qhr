import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesUpdateLeavesComponent } from './employees-update-leaves.component';

describe('EmployeesUpdateLeavesComponent', () => {
  let component: EmployeesUpdateLeavesComponent;
  let fixture: ComponentFixture<EmployeesUpdateLeavesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesUpdateLeavesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesUpdateLeavesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
