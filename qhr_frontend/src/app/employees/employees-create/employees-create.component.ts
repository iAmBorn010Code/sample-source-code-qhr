import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { Router, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-employees-create',
  templateUrl: './employees-create.component.html',
  styleUrls: ['./employees-create.component.css']
})
export class EmployeesCreateComponent implements OnInit {

  employee: any = {
		first_name: '',
		middle_name: '',
		last_name: '',
		suffix: '',
		email: '',
		address: '',
    contact_nos: '',
    birthdate:''
  };

  date;
  saving = false;
  
  uploads_history = [];
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private EmployeesService: EmployeesService,
    private router: Router,
  ) { }

  ngOnInit() {

    this.date = new Date();
    console.log(this.date);
    
    this.PageService.pageName.next('Employees');
    console.log('init called');
  }

  getFields() {

    const fields = {
      first_name: this.employee.first_name,
			middle_name: this.employee.middle_name,
			last_name: this.employee.last_name,
      suffix: this.employee.suffix,
      gender: this. employee.gender,
      email: this.employee.email,
			address: this.employee.address,
      contact_nos: this.employee.contact_nos,
      birthdate: this.employee.birthdate,
      username: this.employee.username,
    };
    
    return fields;
  }

  save() {
    console.log('clicked save!');
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    console.log( this.getFields() );

    this.EmployeesService.store( this.getFields() ).subscribe( data => {
      console.log('request sucessful', data)
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
