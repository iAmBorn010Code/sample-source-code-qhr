import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeductionsIndexComponent } from './deductions-index.component';

describe('DeductionsIndexComponent', () => {
  let component: DeductionsIndexComponent;
  let fixture: ComponentFixture<DeductionsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeductionsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeductionsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
