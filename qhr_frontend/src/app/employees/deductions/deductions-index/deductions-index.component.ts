import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SharedDataEmployeeService } from '../../../services/shared-data-employee.service';
import { EmployeesService } from '../../../services/employees.service';
import { DeductionsService } from '../../../services/deductions.service';

import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../../confirm-modal/confirm-modal.component';
import { TextHelperService } from '../../../texthelper.service';
import { PaginatorService } from '../../../paginator.service';


@Component({
  selector: 'app-deductions-index',
  templateUrl: './deductions-index.component.html',
  styleUrls: ['./deductions-index.component.css']
})
export class DeductionsIndexComponent implements OnInit {

  employee: any = {
		food_allowance: '',
    travel_allowance: '',
    _id: null
  };

  deductions = [];

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';
  
  paginator: any;
	params = {
		limit: 5,
    page: 1,
    user_id: 0,
	};

	mainLoader = true;

  constructor(
    private SharedDataEmployeeService: SharedDataEmployeeService,
    private EmployeesService: EmployeesService,
    private DeductionsService: DeductionsService,
    private ActivatedRoute: ActivatedRoute,
    private PaginatorService: PaginatorService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
  ) { }

  ngOnInit() {
    
    console.log('called init deductions');

    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {

      this.params.user_id = params['id'];

			this.EmployeesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
          this.employee = data.results;
          this.SharedDataEmployeeService.employee.next( this.employee );
          console.log('employee data updated.');
				} else {
          this.errorMsg = data.msg;
        }
      });
      
      this.get();
    });
  }

  get() {
    
    this.mainLoader = true;
    this.deductions = [];
		this.DeductionsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
        this.deductions = data.results;
        console.log(this.deductions);
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( this.paginator );
      } else {
        this.errorMsg = data.msg;
      }
      
			this.mainLoader = false;

		});
  }
  
  open( id ) {
    console.log('clicked delete!');

    console.log('ID', id); 

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure to delete this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {
    console.log('confirmed delete');

    this.DeductionsService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }

      this.get();
    });

  }

  //pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}