import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SharedDataEmployeeService } from '../../../services/shared-data-employee.service';
import { EmployeesService } from '../../../services/employees.service';
import { DeductionsService } from '../../../services/deductions.service';
import { TextHelperService } from '../../../texthelper.service';

@Component({
  selector: 'app-deductions-update',
  templateUrl: './deductions-update.component.html',
  styleUrls: ['./deductions-update.component.css']
})
export class DeductionsUpdateComponent implements OnInit {

  deduction: any = {
		description: '',
    amount: '',
    type: '',
	};
	
  mainLoader = true;
  saving = false;
  deduction_id;

  employee: any = {};

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';
  
  constructor(
    private TextHelperService: TextHelperService,
    private SharedDataEmployeeService: SharedDataEmployeeService,
    private EmployeesService: EmployeesService,
    private ActivatedRoute: ActivatedRoute,
    private DeductionsService: DeductionsService,
  ) { }

  ngOnInit() {
    console.log('called init deduction');

    this.ActivatedRoute.params.subscribe( params => {
      this.deduction_id = params['deduction_id'];

      this.DeductionsService.show( this.deduction_id ).subscribe( data => {
				if ( !data.error ) {
          this.deduction = data.results;
				} else {
          this.errorMsg = data.msg;
        }
				this.mainLoader = !this.mainLoader;
			});
    });

    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
          this.employee = data.results;
          this.SharedDataEmployeeService.employee.next( this.employee );
          console.log('employee data updated.');
				} else {
          this.errorMsg = data.msg;
        }
			});
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

		const fields = {
      description: this.deduction.description,
      type: this.deduction.type,
			amount: this.deduction.amount,
		};

    console.log( fields );
    console.log( this.deduction_id );

    this.DeductionsService.update( this.deduction_id, fields ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
          this.isError = true;
          const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
          this.errorMsg = msg;
      }
    });
 	
	}

}
