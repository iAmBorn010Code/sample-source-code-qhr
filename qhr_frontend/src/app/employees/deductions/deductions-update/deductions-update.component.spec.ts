import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeductionsUpdateComponent } from './deductions-update.component';

describe('DeductionsUpdateComponent', () => {
  let component: DeductionsUpdateComponent;
  let fixture: ComponentFixture<DeductionsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeductionsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeductionsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
