import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeductionsCreateComponent } from './deductions-create.component';

describe('DeductionsCreateComponent', () => {
  let component: DeductionsCreateComponent;
  let fixture: ComponentFixture<DeductionsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeductionsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeductionsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
