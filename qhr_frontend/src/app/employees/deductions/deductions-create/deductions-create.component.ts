import { Component, OnInit } from '@angular/core';
import { DeductionsService } from '../../../services/deductions.service';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../../texthelper.service';

@Component({
  selector: 'app-deductions-create',
  templateUrl: './deductions-create.component.html',
  styleUrls: ['./deductions-create.component.css']
})
export class DeductionsCreateComponent implements OnInit {

  deduction: any = {
		description: '',
    type: '',
    amount: '',
  };

  id;
  
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private DeductionsService: DeductionsService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
      this.id = params['id'];
    });

    console.log('ID', this.id);

  }

  getFields() {

    const fields = {
      user_id: this.id,
      description: this.deduction.description,
      type: this.deduction.type,
      amount: this.deduction.amount,
    };
    
    return fields;
  }

  save() {
    console.log('clicked save!');
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    console.log( this.getFields() );

    this.DeductionsService.store( this.getFields() ).subscribe( data => {
      console.log('request sucessful', data)
      this.saving =   false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
