import { Component, OnInit } from '@angular/core';
import { EarningsService } from '../../../services/earnings.service';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../../texthelper.service';

@Component({
  selector: 'app-earnings-create',
  templateUrl: './earnings-create.component.html',
  styleUrls: ['./earnings-create.component.css']
})
export class EarningsCreateComponent implements OnInit {

  earning: any = {
		description: '',
    type: '',
    amount: '',
  };

  userId = 0;
  saving = false;
  
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  constructor(
    private TextHelperService: TextHelperService,
    private EarningsService: EarningsService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
      this.userId = params['id'];
    });

    console.log('userID', this.userId);
  }

  getFields() {

    const fields = {
      user_id: this.userId,
      description: this.earning.description,
      type: this.earning.type,
      amount: this.earning.amount,
    };
    
    return fields;
  }

  save() {
    console.log('clicked save!');
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    console.log( this.getFields() );

    this.EarningsService.store( this.getFields() ).subscribe( data => {
      console.log('request sucessful', data)
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
