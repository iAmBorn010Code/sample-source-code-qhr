import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarningsIndexComponent } from './earnings-index.component';

describe('EarningsIndexComponent', () => {
  let component: EarningsIndexComponent;
  let fixture: ComponentFixture<EarningsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarningsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarningsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
