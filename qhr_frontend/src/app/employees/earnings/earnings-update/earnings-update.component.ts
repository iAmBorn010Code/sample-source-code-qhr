import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SharedDataEmployeeService } from '../../../services/shared-data-employee.service';
import { EmployeesService } from '../../../services/employees.service';
import { EarningsService } from '../../../services/earnings.service';
import { TextHelperService } from '../../../texthelper.service';

@Component({
  selector: 'app-earnings-update',
  templateUrl: './earnings-update.component.html',
  styleUrls: ['./earnings-update.component.css']
})
export class EarningsUpdateComponent implements OnInit {

  earning: any = {
		description: '',
    amount: '',
    type: '',
	};
	
  mainLoader = true;
  saving = false;
  earning_id;

  employee: any = {};

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  constructor(
    private TextHelperService: TextHelperService,
    private SharedDataEmployeeService: SharedDataEmployeeService,
    private EmployeesService: EmployeesService,
    private ActivatedRoute: ActivatedRoute,
    private EarningsService: EarningsService,
  ) { }

  ngOnInit() {
    console.log('called init update earning');

    this.ActivatedRoute.params.subscribe( params => {
      this.earning_id = params['earning_id'];
      console.log(this.earning_id);

      this.EarningsService.show( this.earning_id ).subscribe( data => {
        if ( !data.error ) {
          this.earning = data.results;
        } else {
          this.errorMsg = data.msg;
        }
        this.mainLoader = !this.mainLoader;
      });
    });
    

    this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
        console.log('here personal' + params['id'] );
				if ( !data.error ) {
          this.employee = data.results;
          this.SharedDataEmployeeService.employee.next( this.employee );
          console.log('employee data updated.');
				} else {
          this.errorMsg = data.msg;
        }
			});
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

		const fields = {
      description: this.earning.description,
      type: this.earning.type,
			amount: this.earning.amount,
		};

    console.log( fields );
    console.log( this.earning_id );

    this.EarningsService.update( this.earning_id, fields ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
          this.isError = true;
          const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
          this.errorMsg = msg;
      }
    });
 	
	}

}
