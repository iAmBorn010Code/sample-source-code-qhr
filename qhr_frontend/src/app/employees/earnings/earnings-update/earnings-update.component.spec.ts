import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarningsUpdateComponent } from './earnings-update.component';

describe('EarningsUpdateComponent', () => {
  let component: EarningsUpdateComponent;
  let fixture: ComponentFixture<EarningsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarningsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarningsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
