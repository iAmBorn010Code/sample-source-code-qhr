import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsCreateComponent } from './documents-create.component';

describe('DocumentsCreateComponent', () => {
  let component: DocumentsCreateComponent;
  let fixture: ComponentFixture<DocumentsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
