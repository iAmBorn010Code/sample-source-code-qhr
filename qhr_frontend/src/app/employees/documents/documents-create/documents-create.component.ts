import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DocumentsService } from '../../../services/documents.service';
import { TextHelperService } from 'src/app/texthelper.service';

@Component({
  selector: 'app-documents-create',
  templateUrl: './documents-create.component.html',
  styleUrls: ['./documents-create.component.css']
})
export class DocumentsCreateComponent implements OnInit {

  emp_id;
  
  document: any = {
		name: '',
    filename: '',
  };

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  selectedFile: File = null;
  fileUploading = false;
  fileUploaded = false;
  fileUploadTriggered = false;
  fileUploadingMsg = '';

  constructor(
    private route: ActivatedRoute,
    private DocumentsService: DocumentsService,
    private TextHelperService: TextHelperService
  ) { }

  ngOnInit() {

    this.route.parent.params.forEach(( params: Params ) => {

			this.emp_id = params['id'];
			console.log('emp_id', this.emp_id);

		});

    console.log('emp_id', this.emp_id);

  }

  getFields() {

    const fields = {
      name: this.document.name,
      filename: this.document.filename,
      user_id: this.emp_id
    };
    
    return fields;
  }

  save() {
    console.log('clicked save!');
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    console.log( this.getFields() );

    this.DocumentsService.store( this.getFields() ).subscribe( data => {
      console.log('request sucessful', data)
      this.saving =   false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

  onFileChanged(event) {

    this.selectedFile = <File>event.target.files[0];
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name); 

    this.fileUploading = true;
    this.fileUploadTriggered = true;
    // this.document.filename = null;
    
    // if (! this.document.uploaded_filename ) this.document.uploaded_filename = this.selectedFile.name;
    
    this.DocumentsService.fileUpload( this.emp_id, uploadData ).subscribe( data => {

      this.fileUploading = false;
      
      if ( !data.error ) {
        this.fileUploaded = true;
        this.document.filename = data.filename;
        // this.document.uploaded_filename = data.orig_filename || this.selectedFile.name;
        console.log(this.document.filename);
      } else {
        this.fileUploaded = false;
        this.fileUploadingMsg = data.msg;
      }
    });
  }

}
