import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DocumentsService } from '../../../services/documents.service';
import { PageService } from '../../../page.service';
import { PaginatorService } from 'src/app/paginator.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../../confirm-modal/confirm-modal.component';
import { TextHelperService } from 'src/app/texthelper.service';
import { EmployeesService } from 'src/app/services/employees.service';
import { SharedDataEmployeeService } from 'src/app/services/shared-data-employee.service';

@Component({
  selector: 'app-documents-index',
  templateUrl: './documents-index.component.html',
  styleUrls: ['./documents-index.component.css']
})
export class DocumentsIndexComponent implements OnInit {

  emp_id : 0;
	documents = [];
	employee: any = {};
	paginator: any;
	params = {
		limit: 5,
		page: 1,
		emp_id: ''
	};

  mainLoader = true;
	
	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';
	
	results_count = 0;

  constructor(
    private PageService: PageService,
    private route: ActivatedRoute,
    private DocumentsService: DocumentsService,
		private PaginatorService: PaginatorService,
		private modalService: SuiModalService,
		private TextHelperService: TextHelperService,
		private EmployeesService: EmployeesService,
		private SharedDataEmployeeService: SharedDataEmployeeService
  ) { }

  ngOnInit() {
		
		this.route.parent.params.forEach(( params: Params ) => {

			this.params.emp_id = params['id'];
			console.log('emp_id', this.params.emp_id);

			this.EmployeesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
          this.employee = data.results;
          this.SharedDataEmployeeService.employee.next( this.employee );
        }
			});
		});
		
    this.PageService.pageName.next('Documents');
		this.get();

  }

  get() {

		this.mainLoader = true;
		  
		this.documents = [];
		this.DocumentsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.documents = data.results;
				this.results_count = data.results_count;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( 'length', this.documents.length );
			}
			this.mainLoader = false;
		});
	}

	open( id ) {
    console.log('clicked delete!');

    console.log('ID', id); 

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure to delete this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {
    console.log('confirmed delete');

    this.DocumentsService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
				this.successMsg = data.msg;
				this.get();	
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
			}
		});

  }
	  

	//pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}


}
