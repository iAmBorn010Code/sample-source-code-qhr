import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SharedDataEmployeeService } from '../../services/shared-data-employee.service';
import { EmployeesService } from '../../services/employees.service';
import { TextHelperService } from '../../texthelper.service';

@Component({
  selector: 'app-employees-update-personal',
  templateUrl: './employees-update-personal.component.html',
  styleUrls: ['./employees-update-personal.component.css']
})
export class EmployeesUpdatePersonalComponent implements OnInit {
  
  	employee: any = {
		first_name: '',
		middle_name: '',
		last_name: '',
		suffix: '',
		email: '',
		gender: '',
		birthdate: '',
		address: '',
		contact_nos: '',
		job_title: '',
		job_description: '',
		hash: '',
	};

	mainLoader = true;
	saving = false;
	generate = false;

  	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	constructor(
		private TextHelperService: TextHelperService,
		private SharedDataEmployeeService: SharedDataEmployeeService,
		private EmployeesService: EmployeesService,
		private ActivatedRoute: ActivatedRoute,
	) { }

	ngOnInit() {
		
		this.mainLoader = true;
		this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
					this.employee = data.results;
					this.employee.birthdate = this.employee.birth_year + '-' + this.employee.birth_month + '-' + this.employee.birth_day;
					this.SharedDataEmployeeService.employee.next( this.employee );
				} else {
					this.errorMsg = data.msg;
				}
				this.mainLoader = false;
			});
		});		
	}

	update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			first_name: this.employee.first_name,
			middle_name: this.employee.middle_name,
			last_name: this.employee.last_name,
			suffix: this.employee.suffix,
			email: this.employee.email,
			username: this.employee.username,
			gender: this.employee.gender,
			birthdate: this.employee.birthdate,
			address: this.employee.address,
			contact_nos: this.employee.contact_nos,
			hash: this.employee.hash
		};

		this.ActivatedRoute.parent.params.forEach(( params: Params ) => {
			this.EmployeesService.update( params['id'], fields ).subscribe( data => {
				
				this.saving = false;

				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;
				} else {
					this.isError = true;
					const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
					this.errorMsg = msg;
				}

			});
		});
	}

	
	gen_hash() {

		this.generate = true;

		this.EmployeesService.gen_hash( this.employee._id ).subscribe( data => {
			if ( !data.error ) {
				this.generate = false;
				this.employee.hash = data.results;
				this.SharedDataEmployeeService.employee.next( this.employee );
			}
		});

	}

}
