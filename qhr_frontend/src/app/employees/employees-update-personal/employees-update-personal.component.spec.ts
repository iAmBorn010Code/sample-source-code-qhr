import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesUpdatePersonalComponent } from './employees-update-personal.component';

describe('EmployeesUpdatePersonalComponent', () => {
  let component: EmployeesUpdatePersonalComponent;
  let fixture: ComponentFixture<EmployeesUpdatePersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesUpdatePersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesUpdatePersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
