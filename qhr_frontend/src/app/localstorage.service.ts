import { Injectable } from '@angular/core';

@Injectable()
export class LocalstorageService {
	token: string;
	user: any;
	company: any;
	branch: any;
	currBranchId: any;
	currBranchName: any;
	perm = [];
	constructor() {
		// this.token = this.get('token');
		// this.user = JSON.parse( this.get('user') );
	}

	get( key ) {
		return localStorage.getItem( key );
	}

	set( key, value ) {

		switch ( key ) {
			case "token":
				this.token = value;
				localStorage.setItem( 'token', value );
				break;
			case "user":
				this.user = value;
				localStorage.setItem( key, JSON.stringify( value ));
				break;
			case "company":
				this.company = value;
				localStorage.setItem( key, JSON.stringify( value ));
				break;
			case "currBranchId":
				this.currBranchId = value;
				localStorage.setItem( key, value );
				break;
			case "currBranchName":
				this.currBranchName = value;
				localStorage.setItem( key, value );
				break;
			case "perm":
				this.perm = value;
				localStorage.setItem( key, value );
				break;
			default:
				localStorage.setItem( key, value );
		}
	}

	clear() {

		localStorage.clear();

		this.token = '';
		this.user = {};
	}
}
