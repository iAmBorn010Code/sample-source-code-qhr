import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginRestrictionsShowComponent } from './login-restrictions-show.component';

describe('LoginRestrictionsShowComponent', () => {
  let component: LoginRestrictionsShowComponent;
  let fixture: ComponentFixture<LoginRestrictionsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginRestrictionsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginRestrictionsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
