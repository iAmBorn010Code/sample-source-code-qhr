import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { LoginRestrictionsService } from 'src/app/services/loginrestrictions.service';

@Component({
  selector: 'app-login-restrictions-show',
  templateUrl: './login-restrictions-show.component.html',
  styleUrls: ['./login-restrictions-show.component.css']
})
export class LoginRestrictionsShowComponent implements OnInit {

  restriction: any = {};
  id;

  mainLoader = true; 

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private LoginRestrictionsService: LoginRestrictionsService
  ) { }

  ngOnInit() {
    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.id = params['id'];
      console.log('show', this.id);
      this.show();
    });
  }

  show() {

    this.mainLoader = true;

    this.LoginRestrictionsService.show( this.id ).subscribe( data => {
      if ( !data.error ) {
        this.restriction = data.results;
      }
      this.mainLoader = false;
    });
  }

}
