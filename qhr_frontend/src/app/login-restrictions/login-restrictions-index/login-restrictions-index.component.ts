import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { SuiModalService } from 'ng2-semantic-ui';
import { ConfirmModal } from '../../confirm-modal/confirm-modal.component';
import { TextHelperService } from '../../texthelper.service';
import { LoginRestrictionsService } from 'src/app/services/loginrestrictions.service';

@Component({
  selector: 'app-login-restrictions-index',
  templateUrl: './login-restrictions-index.component.html',
  styleUrls: ['./login-restrictions-index.component.css']
})
export class LoginRestrictionsIndexComponent implements OnInit {

  restrictions = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
	};
	
  mainLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
  errorMsg = '';

  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private modalService: SuiModalService,
    private TextHelperService: TextHelperService,
    private LoginRestrictionsService: LoginRestrictionsService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Login Restrictions');
    this.get();
  }

  get() {
		  
		this.restrictions = [];
		this.LoginRestrictionsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.restrictions = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
          );
        console.log('restrictions', this.restrictions);
			} else {
        this.errorMsg = data.msg;
      }
			this.mainLoader = false;
		});
  }

  //Delete
  open( id ) {

    this.modalService
        .open( new ConfirmModal('Are you sure?', 'Are you sure to delete this?') )
        .onApprove( () => this.delete( id ) )
        .onDeny( () => '' )
  }

  delete( id ) {
    
    this.LoginRestrictionsService.destroy( id ).subscribe( data => {
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });

    this.mainLoader = true;
    this.get();

  }
  
  //pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
