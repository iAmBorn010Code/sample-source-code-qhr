import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginRestrictionsIndexComponent } from './login-restrictions-index.component';

describe('LoginRestrictionsIndexComponent', () => {
  let component: LoginRestrictionsIndexComponent;
  let fixture: ComponentFixture<LoginRestrictionsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginRestrictionsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginRestrictionsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
