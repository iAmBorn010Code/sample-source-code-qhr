import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { LoginRestrictionsService } from 'src/app/services/loginrestrictions.service';

@Component({
  selector: 'app-login-restrictions-create',
  templateUrl: './login-restrictions-create.component.html',
  styleUrls: ['./login-restrictions-create.component.css']
})
export class LoginRestrictionsCreateComponent implements OnInit {

  login_restriction: any = {};

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private LoginRestrictionsService: LoginRestrictionsService
  ) { }

  ngOnInit() {
  }

  getFields() {

    const fields = {
      name: this.login_restriction.name,
      type: this.login_restriction.type,
      value: this.login_restriction.value
    };
    
    return fields;
  }

  save() {
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    console.log('fields: ', this.getFields());

    this.LoginRestrictionsService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
