import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginRestrictionsCreateComponent } from './login-restrictions-create.component';

describe('LoginRestrictionsCreateComponent', () => {
  let component: LoginRestrictionsCreateComponent;
  let fixture: ComponentFixture<LoginRestrictionsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginRestrictionsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginRestrictionsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
