import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { LoginRestrictionsService } from 'src/app/services/loginrestrictions.service';

@Component({
  selector: 'app-login-restrictions-update',
  templateUrl: './login-restrictions-update.component.html',
  styleUrls: ['./login-restrictions-update.component.css']
})
export class LoginRestrictionsUpdateComponent implements OnInit {

  restriction: any = {};
  id;
  
  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
	errorMsg = '';
	
	mainLoader = true;
	saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private ActivatedRoute: ActivatedRoute,
    private LoginRestrictionsService: LoginRestrictionsService
  ) { }

  ngOnInit() {

    this.ActivatedRoute.params.forEach(( params: Params ) => {
      this.id = params['id']

      this.LoginRestrictionsService.show( this.id ).subscribe( data => {
        this.mainLoader = false;
        if ( !data.error ) {
          this.restriction = data.results;
        } else {
          this.errorMsg = data.msg;
        }
      });
    });
  }

  update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
      name: this.restriction.name,
      type: this.restriction.type,
      value: this.restriction.value
    };
    
    console.log('fields:', fields);

    this.LoginRestrictionsService.update( this.id, fields ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
