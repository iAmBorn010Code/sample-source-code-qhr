import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginRestrictionsUpdateComponent } from './login-restrictions-update.component';

describe('LoginRestrictionsUpdateComponent', () => {
  let component: LoginRestrictionsUpdateComponent;
  let fixture: ComponentFixture<LoginRestrictionsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginRestrictionsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginRestrictionsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
