import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersUdpateComponent } from './users-udpate.component';

describe('UsersUdpateComponent', () => {
  let component: UsersUdpateComponent;
  let fixture: ComponentFixture<UsersUdpateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersUdpateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersUdpateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
