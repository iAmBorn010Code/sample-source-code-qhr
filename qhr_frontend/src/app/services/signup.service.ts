import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { TextHelperService } from './../texthelper.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  apiBaseUrl: string = environment.apiBaseUrl;
  url: string;
  token:string;
  resourceUrl: string;

  constructor(
    private http: Http,
    private helper: TextHelperService,
  ) {
    this.resourceUrl = this.apiBaseUrl + 'users';
   }

   store( params ) {

    this.url = this.resourceUrl + '/signup';
    const body = JSON.stringify(params);
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(this.url, body, options).pipe(
        map( res => {
            const x = res.json();
            return x;
        })
    );
  }
}
