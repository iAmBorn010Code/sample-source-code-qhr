import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { TextHelperService } from './../texthelper.service';
import { map } from 'rxjs/operators';
import { LocalstorageService } from './../localstorage.service';
import { AuthService } from './../auth.service';

@Injectable()
export class NotificationsService {

  apiBaseUrl: string = environment.apiBaseUrl;
  url: string;
  token:string;
  resourceUrl: string;

  constructor(
    private http: Http,
    private helper: TextHelperService,
    private AuthService: AuthService,
  ) {

    this.resourceUrl = this.apiBaseUrl + 'notifications';

  }

  get( params: any = {} ) {

    this.token = '&token=' + localStorage.getItem('token');

    const data = this.helper.serialize( params );
    this.url = this.resourceUrl +'?' + data + this.token;
    console.log( this.url );

    return this.http.get( this.url ).pipe(
        map( res => {
          const x = res.json();
          this.AuthService.isStillLogged(x);
          return x;
        })
    );
  }
  
}
