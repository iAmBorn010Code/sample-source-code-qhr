import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { TextHelperService } from './../texthelper.service';
import { map } from 'rxjs/operators';
import { LocalstorageService } from './../localstorage.service';
import { AuthService } from './../auth.service';

@Injectable()
export class PayrollContributionsService {

  apiBaseUrl: string = environment.apiBaseUrl;
  url: string;
  token:string;
  resourceUrl: string;

  constructor(
    private storage: LocalstorageService,
    private http: Http,
    private helper: TextHelperService,
    private AuthService: AuthService,
  ) { 
    this.resourceUrl = this.apiBaseUrl + 'contributions';
  }

  get( params: any = {} ) {

    this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + localStorage.getItem('currBranchId');

    const data = this.helper.serialize( params );
    this.url = this.resourceUrl +'?' + data + this.token;
    console.log( this.url );

    return this.http.get( this.url ).pipe(
        map( res => {
            const x = res.json();
            console.log('XXX', x );
            this.AuthService.isStillLogged(x);
            return x;
        })
    );
  }

  show( id, params: any = {} ) {

    this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + localStorage.getItem('currBranchId');
    const data = this.helper.serialize( params );
    this.url = this.resourceUrl +'/'+ id + '?' + data + this.token;

    console.log( this.url );

    return this.http.get( this.url ).pipe(
        map( res => {
            const x = res.json();
            this.AuthService.isStillLogged(x);
            return x;
        })
    );
  }

  update( id: string, fields = {} ) {
        
    this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + localStorage.getItem('currBranchId');

    fields['_method'] = 'PUT';
    const url = this.resourceUrl +'/' + id + '?' + this.token;
    const body = JSON.stringify( fields );
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    console.log('url', url);

    return this.http.post(url, body, options).pipe(
        map( res => {
            const x = res.json();
            this.AuthService.isStillLogged(x);
            return x;
        })
    );
  }

}
