import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { TextHelperService } from '../texthelper.service';
import { map } from 'rxjs/operators';
import { LocalstorageService } from '../localstorage.service';
import { AuthService } from '../auth.service';

@Injectable()
export class PayrollCalculationService {

    Payroll: any = {};
    
  	constructor(
        private storage: LocalstorageService,
        private http: Http,
        private helper: TextHelperService,
        private AuthService: AuthService,
	) {

    }

    initialize() {

        this.Payroll = {
            
            _id: null,
            employeeId: null,

            // INITIAL
            salaryType : 'bi-monthly-fixed',
            rateMin : 0.0,
            totalWorkRenderedMin: 0,
            totalWorkToRenderMin: 0,
            withTaxes: true,
            withBenefits: false,
            withNightDiff: true,
            withOvertime: true,
            deductAbsences: true,
            deductLates: true,
            deductUndertime: true,
    
            // TOTALS
            totalBasicPay: 0,
            totalIncomes: 0,
            totalDeductions: 0,
            totalNetPay: 0,
    
            // INCOMES
            addtlIncomes : [],
            totalAddtlIncomes: 0,
    
            // DEDUCTIONS
            addtlDeductions : [],
            totalAddtlDeductions: 0,
    
            // CONTRIBUTIONS
            contributions: [],
            totalContributions: 0,
            
            // TARDINESS and ABSENCES
            totalLates: 0,
            totalLatesMin: 0,
            
            totalUndertime: 0,
            totalUnderMin: 0,

            totalTardiness: 0,
            totalTardinessMin: 0,
            
            totalAbsences: 0,
    
            // TAXES
            totalWithHoldingTax: 0,
            withtaxTotalContributions: 0,

            totalCola: 0,
            totalNightDiff: 0,
            totalOvertime: 0,

            // SSS
            sss_er: 0,
            sss_ee: 0,
            total_sss: 0

        };

        return this;
    }

    setDetails( data : any ) {
        
        this
            .set('_id', data._id || 0 )
            .set('salaryType', data.salary_type )
            .set('rateMin', data.rate_min )
            .set('totalWorkRendered', data.total_work_rendered )
            .set('totalWorkToRender', data.total_work_to_render )
            .set('totalWorkRenderedMin', data.total_work_rendered_min )
            .set('totalWorkToRenderMin', data.total_work_to_render_min )
            .set('withTaxes', data.with_taxes )
            .set('withBenefits', data.with_benefits )
            .set('withNightDiff', data.with_night_diff)
            .set('withOvertime', data.with_ot)
            .set('deductAbsences', data.deduct_absences )
            .set('deductLates', data.deduct_lates )
            .set('deductUndertime', data.deduct_undertime )
            
            .set('totalBasicPay', data.total_basic_pay )
            .set('totalIncomes', data.total_incomes )
            .set('totalDeductions', data.total_deductions ? data.total_deductions : 0 )
            // .set('totalDeductions', data.total_deductions ? data.total_deductions : 0 )
            .set('totalNetPay', data.total_netpay )
            
            // INCOMES
            .set('addtlIncomes', data.addtl_incomes ? data.addtl_incomes : [] )
            .set('totalAddtlIncomes', data.total_addtl_incomes )

            // DEDUCTIONS
            .set('addtlDeductions', data.addtl_deductions ? data.addtl_deductions : [] )
            .set('totalAddtlDeductions', data.total_addtl_deductions )

            .set('totalUnwork', data.total_unwork )
            .set('totalUnworkMin', data.total_unwork_min )

             // CONTRIBUTIONS
            .set('contributions', data.contributions ? data.contributions : [] )
            .set('totalContributions', data.total_contributions )

            // TARDINESS
            .set('totalLates', data.total_lates )
            .set('totalLatesMin', data.total_lates_min )

            .set('totalUndertime', data.total_undertime )
            .set('totalUndertimeMin', data.total_undertime_min )

            .set('totalTardiness', data.total_tardiness )
            .set('totalTardinessMin', data.total_tardiness_min )
            
            .set('totalAbsences', data.total_absences )
            .set('totalWithHoldingTax', data.total_withholding_tax )

            .set('totalCola', data.total_cola)
            .set('totalNightDiff', data.total_night_diff)
            .set('totalOvertime', data.total_ot)

            // PRE-CALCULATED SSS
            .set('sss_er', data.sss_er)
            .set('sss_ee', data.sss_ee)
            .set('total_sss', data.total_sss)

            // PRE-CALCULATED PHILHEALTH
            .set('philhealth_er', data.philhealth_er)
            .set('philhealth_ee', data.philhealth_ee)
            .set('total_philhealth', data.total_philhealth)

            // PRE-CALCULATED PAGIBIG
            .set('pagibig_er', data.pagibig_er)
            .set('pagibig_ee', data.pagibig_ee)
            .set('total_pagibig', data.total_pagibig)

            // PRE-CALCULATED GSIS
            .set('gsis_er', data.gsis_er)
            .set('gsis_ee', data.gsis_ee)
            .set('total_gsis', data.total_gsis)
            
            .calculatePayroll();

        return this;
    }

    set( col: string, value : any ) {
        this.Payroll[col] = value; // TODO: If key not exist, skip process.
        return this;
    }

    get( col: string ) {
        return this.Payroll[col]; // TODO: If key not exist, throw error.
    }

    getPayroll() {
        return this.Payroll;
    }

    calcAddtlIncomes() {

        let total = 0;

        if ( this.Payroll.addtlIncomes.length ) {
            let l = this.Payroll.addtlIncomes.length;
            for ( let i = 0; i < l; i++ ) {
                total += this.Payroll.addtlIncomes[i].amount;
            }
        }

        return this.Payroll.totalAddtlIncomes = total;
    }

    calcAddtlDeductions() {

        let total = 0;

        if ( this.Payroll.addtlDeductions.length ) {
            let l = this.Payroll.addtlDeductions.length;
            for ( let i = 0; i < l; i++ ) {
                total += this.Payroll.addtlDeductions[i].amount;
            }
        }

        return this.Payroll.totalAddtlDeductions = total;
    }

    calcContributions() {

        let total = 0;
        let withTaxTotal = 0;
        if ( this.Payroll.withBenefits ) {
            if ( this.Payroll.contributions.length ) {
                let l = this.Payroll.contributions.length;
                for ( let i = 0; i < l; i++ ) {
                    total += this.Payroll.contributions[i].ee_amt;

                    if ( ['sss', 'philhealth', 'pagibig'].indexOf(this.Payroll.contributions[i].name) !== -1 ) {
                        withTaxTotal += this.Payroll.contributions[i].ee_amt;
                    }
                }
            }
        } else {
            this.Payroll.contributions = [];
        }

        this.Payroll.withtaxTotalContributions = withTaxTotal;
        return this.Payroll.totalContributions = total;
    }

    calcTotalDeductions() {

        this.Payroll.totalDeductions = this.Payroll.totalAddtlDeductions;
        
        if ( this.Payroll.withTaxes ) {
            this.Payroll.totalDeductions += this.Payroll.totalWithHoldingTax;
        }

        if ( this.Payroll.withBenefits ) {
            this.Payroll.totalDeductions += this.Payroll.totalContributions
        }

        if ( this.Payroll.deductLates ) {
            this.Payroll.totalDeductions += this.Payroll.totalLates;
        }

        if ( this.Payroll.deductUndertime ) {
            this.Payroll.totalDeductions += this.Payroll.totalUndertime;
        }

        if ( this.Payroll.deductAbsences ) {
            this.Payroll.totalDeductions += this.Payroll.totalUnwork;
        }

        // Tardiness and Absences
        this.Payroll.totalTardiness = 
            + this.Payroll.totalLates
            + this.Payroll.totalUndertime;
    }

    calculatePayroll() {
        console.log('this', this.Payroll)
        // INCOME CALCULATIONS
            // addtl
            this.calcAddtlIncomes();

            // Total
            this.Payroll.totalIncomes = 
                + this.Payroll.totalBasicPay
                + this.Payroll.totalAddtlIncomes
                + this.Payroll.totalCola;

            if ( this.Payroll.withNightDiff ) {
                this.Payroll.totalIncomes += this.Payroll.totalNightDiff;
            }

            if ( this.Payroll.withOvertime ) {
                this.Payroll.totalIncomes += this.Payroll.totalOvertime;   
            }

        // DEDUCTIONS CALCULATIONS
            // addtl
            this.calcAddtlDeductions();
            this.calcContributions();
            this.calcTax();
            this.calcTotalDeductions();

            // GROSS and NET PAY;
            this.Payroll.totalNetPay = this.Payroll.totalIncomes - this.Payroll.totalDeductions;
        
        // CONTRIBUTIONS PRE CALCULATIONS
            this.calcSSS();
            this.calcPagibig();
            this.calcPhilhealth();
            this.calcGSIS() 

        return this;
    }

    // INCOMES
    addAddtlIncome( data ) {

        let existIndex = this.Payroll.addtlIncomes.findIndex(x => x.desc == data.desc);

        if ( existIndex !== -1 ) 
            this.Payroll.addtlIncomes.splice(existIndex, 1, data)
        else
            this.Payroll.addtlIncomes.push(data);

        return this;
    }

    removeAddtlIncome( desc: string ) {

        let index =  this.Payroll.addtlIncomes.findIndex(x => x.desc == desc);

        if ( index !== -1 ) {
            this.Payroll.addtlIncomes.splice(index, 1);
        }
        return this;
    }

    // DEDUCTIONS
    addAddtlDeduction( data ) {

        let existIndex = this.Payroll.addtlDeductions.findIndex(x => x.desc == data.desc);

        if ( existIndex !== -1 ) 
            this.Payroll.addtlDeductions.splice(existIndex, 1, data)
        else
            this.Payroll.addtlDeductions.push(data);

        return this;
    }

    removeAddtlDeduction( desc: string ) {

        let index =  this.Payroll.addtlDeductions.findIndex(x => x.desc == desc);

        if ( index !== -1 ) {
            this.Payroll.addtlDeductions.splice(index, 1);
        }
        return this;
    }

    // CONTRIBUTIONS
    addContribution( data ) {

        let existIndex = this.Payroll.contributions.findIndex(x => x.name == data.name);
        let existSpecifyIndex = this.Payroll.contributions.findIndex(x => x.other_specify == data.other_specify);

        if ( existIndex !== -1 && existSpecifyIndex !== -1 ) {
            this.Payroll.contributions.splice(existIndex, 1, data);
        } else
            this.Payroll.contributions.push(data);

        return this;
    }

    removeContribution( name: string, other_specify: string ) {

        let existIndex = this.Payroll.contributions.findIndex(x => x.name == name);
        let existSpecifyIndex = this.Payroll.contributions.findIndex(x => x.other_specify == other_specify);

        if ( existIndex !== -1 && existSpecifyIndex !== -1 ) {
            this.Payroll.contributions.splice(existIndex, 1);
        }
        
        return this;
    }

    calcTax() {

        let totalContributions = this.Payroll.withtaxTotalContributions;
		let totalBasicPay = this.Payroll.totalBasicPay;
		let salaryType = this.Payroll.salaryType;
		let taxableIncome = totalBasicPay - totalContributions;
		let withholdingTax = 0;
        let overCompensationLevel = 0;
        
        if ( salaryType === 'monthly-fixed' || salaryType === 'monthly' ) {

			if ( taxableIncome > 20833 && taxableIncome < 33333 ){

				overCompensationLevel = (taxableIncome - 20833) * .20;
				withholdingTax = overCompensationLevel;

			} else if ( taxableIncome > 33333 && taxableIncome < 66667 ) {

				overCompensationLevel = (taxableIncome - 33333) * .25;
				withholdingTax = 2500 + overCompensationLevel;

			} else if ( taxableIncome > 66667 && taxableIncome < 166667 ) {

				overCompensationLevel = (taxableIncome - 66667) * .30;
				withholdingTax = 10833 + overCompensationLevel;

			} else if ( taxableIncome > 166667 && taxableIncome < 666667 ) {

				overCompensationLevel = (taxableIncome - 166667) * .32;
				withholdingTax = 40833 + overCompensationLevel;

			} else if ( taxableIncome > 666667 ) {

				overCompensationLevel = (taxableIncome - 666667) * .35;
				withholdingTax = 200833 + overCompensationLevel;

			}

        }
        
        if ( salaryType === 'bi-monthly-fixed' || salaryType === 'bi-monthly' ) {

			if ( taxableIncome > 10417 && taxableIncome < 16667 ){

				overCompensationLevel = (taxableIncome - 10417) * .20;
				withholdingTax = overCompensationLevel;

			} else if ( taxableIncome > 16667 && taxableIncome < 33333 ) {

				overCompensationLevel = (taxableIncome - 16667) * .25;
				withholdingTax = 1250 + overCompensationLevel;

			} else if ( taxableIncome > 33333 && taxableIncome < 83333 ) {

				overCompensationLevel = (taxableIncome - 33333) * .30;
				withholdingTax = 5416 + overCompensationLevel;

			} else if ( taxableIncome > 83333 && taxableIncome < 333333 ) {

				overCompensationLevel = (taxableIncome - 83333) * .32;
				withholdingTax = 20416 + overCompensationLevel;

			} else if ( taxableIncome > 333333 ) {

				overCompensationLevel = (taxableIncome - 333333) * .35;
				withholdingTax = 100416 + overCompensationLevel;

			}

        }
        
        if ( salaryType === 'weekly-fixed' || salaryType === 'weekly' ) {

			if ( taxableIncome > 4808 && taxableIncome < 7692 ){

				overCompensationLevel = (taxableIncome - 4808) * .20;
				withholdingTax = overCompensationLevel;

			} else if ( taxableIncome > 7692 && taxableIncome < 15385 ) {

				overCompensationLevel = (taxableIncome - 7692) * .25;
				withholdingTax = 576 + overCompensationLevel;

			} else if ( taxableIncome > 15385 && taxableIncome < 38462 ) {

				overCompensationLevel = (taxableIncome - 15385) * .30;
				withholdingTax = 2500 + overCompensationLevel;

			} else if ( taxableIncome > 38462 && taxableIncome < 153846 ) {

				overCompensationLevel = (taxableIncome - 38462) * .32;
				withholdingTax = 9423 + overCompensationLevel;

			} else if ( taxableIncome > 153846 ) {

				overCompensationLevel = (taxableIncome - 153846) * .35;
				withholdingTax = 46346 + overCompensationLevel;

			}

        }
        
        return this.Payroll.totalWithHoldingTax = withholdingTax;

    }

    calcSSS() {
        let totalBasicPay = this.Payroll.totalBasicPay;
        let total_sss = 0;
        let salary_credit = 0;
        // var ec = totalBasicPay < 14750 ? 10 : 30;
        if (  totalBasicPay < 2250 ) {
            salary_credit = 2000;
        } else if ( totalBasicPay >= 19750 ) {
            salary_credit = 20000;
        } else {
            salary_credit = Math.round(totalBasicPay/500) * 500
        }

        this.Payroll.sss_er = salary_credit * .08;
        this.Payroll.sss_ee = salary_credit * .04;
        this.Payroll.total_sss = this.Payroll.sss_er + this.Payroll.sss_ee;

        return this.Payroll;
    }

    calcPhilhealth() {
        let totalBasicPay = this.Payroll.totalBasicPay;
        let total_philhealth = totalBasicPay * .03;

        if (  totalBasicPay < 10000 ) {
            total_philhealth = 300;
        }

        if ( totalBasicPay >= 60000 ) {
            total_philhealth = 1800;
        } 

        this.Payroll.philhealth_er = total_philhealth/2;
        this.Payroll.philhealth_ee = total_philhealth/2;
        this.Payroll.total_philhealth = total_philhealth;

        return this.Payroll;
    }

    calcPagibig() {
        let totalBasicPay = this.Payroll.totalBasicPay;
        let pagibig_er = 0;
        let pagibig_ee = 0;
        if ( 1500 <= totalBasicPay && totalBasicPay > 1000 ) {
            pagibig_er = totalBasicPay * .01;
            pagibig_ee = totalBasicPay * .02;
        }

        if ( totalBasicPay > 1500 ) {
            pagibig_er = totalBasicPay * .02;
            pagibig_ee = totalBasicPay * .02;
        } 

        if ( totalBasicPay > 5000 ) {
            pagibig_er = 5000 * .02;
            pagibig_ee = 5000 * .02;
        }

        this.Payroll.pagibig_er = pagibig_er;
        this.Payroll.pagibig_ee = pagibig_ee;
        this.Payroll.total_pagibig = pagibig_er + pagibig_ee;

        return this.Payroll;
    }

    calcGSIS() {
        let totalBasicPay = this.Payroll.totalBasicPay;
        let gsis_er = totalBasicPay * .12;
        let gsis_ee = totalBasicPay * .09;
        
        this.Payroll.gsis_er = gsis_er;
        this.Payroll.gsis_ee = gsis_ee;
        this.Payroll.total_gsis = gsis_er + gsis_ee;

        return this.Payroll;
    }
    
    


}
