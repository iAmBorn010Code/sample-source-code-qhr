import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { LocalstorageService } from '../localstorage.service';
import { map } from 'rxjs/operators';
import { Http, Headers, RequestOptions } from '@angular/http';
import { TextHelperService } from '../texthelper.service';
import { AuthService } from '../auth.service';


@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

    apiBaseUrl: string = environment.apiBaseUrl;
    url: string;
    token: string;
    resourceUrl: string;
    constructor(
        private strorage: LocalstorageService,
        private http: Http,
        private helper: TextHelperService,
        private AuthService: AuthService,
    ) {
        this.resourceUrl = this.apiBaseUrl + 'companies';
    }

    get( params: any = {} ) {

        this.token = '&token=' + localStorage.getItem('token');

        const data = this.helper.serialize( params );
        this.url = this.resourceUrl +'?' + data + this.token;
        console.log( this.url );

        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

  	show( id ) {

        this.token = '&token=' + localStorage.getItem('token');
        this.url = this.resourceUrl +'/'+ id + '?' + this.token;

        console.log( this.url );

        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    store( params ) {

        this.url = this.resourceUrl;
        const body = JSON.stringify(params);
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(this.url, body, options).pipe(
            map( res => {
                const x = res.json();
                return x;
            })
        );
    }

    destroy() {
      
    }

    update( serviceId: string, fields = {} ) {
        
        this.token = '&token=' + localStorage.getItem('token');

        fields['_method'] = 'PUT';
        const url = this.resourceUrl +'/' + serviceId + '?' + this.token;
        const body = JSON.stringify( fields );
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(url, body, options).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    fileUpload( uploadData ) {

        console.log( uploadData );

        this.token = '&token=' + localStorage.getItem('token');
        this.url = this.resourceUrl +'/file_upload?' + this.token;
        const headers = new Headers({ 'Content-Type': []});
        const options = new RequestOptions({ headers: headers });

        console.log(this.url);

        return this.http.post(this.url, uploadData, options).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    getAvatar( imagename ){

        const url = this.apiBaseUrl +'uploads/users/' + imagename + '?' + this.token;
        return url;

    }

    verify( hash ){

        this.url = this.resourceUrl +'/confirmation/'+ hash;

        console.log(this.url);

        return this.http.get( this.url ).pipe(

            map( res => {
                const x = res.json();
                return x;
            })

        );
        
    }

    getCompany( slug ) {

        this.url = this.resourceUrl +'/i/'+ slug;

        console.log( this.url );    

        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                return x;
            })
        );
    }

    gen_hash( id ) {

        this.token = '&token=' + localStorage.getItem('token');

        this.url = this.resourceUrl + '/' + id + '/generate_hash?' + this.token;
        console.log( this.url );

        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                console.log('XXX', x );
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    logoUpload( uploadData ) {

        console.log( uploadData );

        this.token = '&token=' + localStorage.getItem('token');
        this.url = this.apiBaseUrl +'uploads/company/logo?' + this.token;
        const headers = new Headers({ 'Content-Type': []});
        const options = new RequestOptions({ headers: headers });

        console.log(this.url);

        return this.http.post(this.url, uploadData, options).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }


    updateAttendances( params: any = {} ) {

        this.token = '&token=' + localStorage.getItem('token');

        const data = this.helper.serialize( params );
        this.url = this.apiBaseUrl +'crons/attendances/update?' + data + this.token;
        console.log( this.url );

        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                // this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    subscribe( type: string ) {
        this.token = '&token=' + localStorage.getItem('token');

        this.url = this.resourceUrl + '/subscribe/' + type + '?' + this.token;
        console.log( this.url );

        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                console.log('XXX', x );
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }


}
