import { TestBed, inject } from '@angular/core/testing';

import { TeamMembersService } from './teammembers.service';

describe('TeamMembersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeamMembersService]
    });
  });

  it('should be created', inject([TeamMembersService], (service: TeamMembersService) => {
    expect(service).toBeTruthy();
  }));
});
