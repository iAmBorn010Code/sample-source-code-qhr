import { TestBed, inject } from '@angular/core/testing';

import { PayrollContributionsService } from './payrollcontributions.service';

describe('PayrollContributionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PayrollContributionsService]
    });
  });

  it('should be created', inject([PayrollContributionsService], (service: PayrollContributionsService) => {
    expect(service).toBeTruthy();
  }));
});
