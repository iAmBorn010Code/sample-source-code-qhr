import { TestBed, inject } from '@angular/core/testing';

import { LeaveCreditsService } from './leavecredits.service';

describe('LeaveCreditsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LeaveCreditsService]
    });
  });

  it('should be created', inject([LeaveCreditsService], (service: LeaveCreditsService) => {
    expect(service).toBeTruthy();
  }));
});
