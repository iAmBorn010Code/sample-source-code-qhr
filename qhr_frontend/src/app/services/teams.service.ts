import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { TextHelperService } from '../texthelper.service';
import { map } from 'rxjs/operators';
import { LocalstorageService } from '../localstorage.service';
import { AuthService } from '../auth.service';

@Injectable()
export class TeamsService {

  apiBaseUrl: string = environment.apiBaseUrl;
  url: string;
  token:string;
  resourceUrl: string;

  constructor(
    private storage: LocalstorageService,
    private http: Http,
    private helper: TextHelperService,
    private AuthService: AuthService,
  ) { 
    this.resourceUrl = this.apiBaseUrl + 'teams';
  }

  get( params: any = {} ) {

    this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + localStorage.getItem('currBranchId');

    const data = this.helper.serialize( params );
    this.url = this.resourceUrl +'?' + data + this.token;
    console.log( this.url );

    return this.http.get( this.url ).pipe(
      map( res => {
        const x = res.json();
        this.AuthService.isStillLogged(x);
        return x;
      })
    );
  }

  show( id ) {

    this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + localStorage.getItem('currBranchId');
    this.url = this.resourceUrl +'/'+ id + '?' + this.token;

    console.log( this.url );

    return this.http.get( this.url ).pipe(
      map( res => {
        const x = res.json();
        this.AuthService.isStillLogged(x);
        return x;
      })
    );
  }

  store( params ) {

    this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + localStorage.getItem('currBranchId');
    this.url = this.resourceUrl +'?' + this.token;
    const body = JSON.stringify(params);
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(this.url, body, options).pipe(
      map( res => {
        const x = res.json();
        this.AuthService.isStillLogged(x);
        return x;
      })
    );
  }

  update( serviceId: string, fields = {} ) {
        
    this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + localStorage.getItem('currBranchId');

    fields['_method'] = 'PUT';
    const url = this.resourceUrl +'/' + serviceId + '?' + this.token;
    const body = JSON.stringify( fields );
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(url, body, options).pipe(
        map( res => {
            const x = res.json();
            this.AuthService.isStillLogged(x);
            return x;
        })
    );
  }

  destroy( id: string, fields = {} ) {

    this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + localStorage.getItem('currBranchId');

    fields['_method'] = 'DELETE';
    const url = this.resourceUrl +'/' + id + '?' + this.token;
    const body = JSON.stringify( fields );
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.delete(url, body).pipe(
        map( res => {
            const x = res.json();
            this.AuthService.isStillLogged(x);
            return x;
        })
      );
  }
}
