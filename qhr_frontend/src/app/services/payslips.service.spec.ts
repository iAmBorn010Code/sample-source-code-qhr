import { TestBed, inject } from '@angular/core/testing';

import { PayslipsService } from './payslips.service';

describe('PayslipsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PayslipsService]
    });
  });

  it('should be created', inject([PayslipsService], (service: PayslipsService) => {
    expect(service).toBeTruthy();
  }));
});
