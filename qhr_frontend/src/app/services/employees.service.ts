import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { TextHelperService } from './../texthelper.service';
import { map } from 'rxjs/operators';
import { LocalstorageService } from './../localstorage.service';
import { AuthService } from './../auth.service';

@Injectable()
export class EmployeesService {

	apiBaseUrl: string = environment.apiBaseUrl;
    url: string;
    token:string;
    hash: string;
    resourceUrl: string;
    currBranchId: string = '';
  	constructor(
        // private storage: LocalstorageService,
        private http: Http,
        private helper: TextHelperService,
        private AuthService: AuthService,
	) { 
        this.resourceUrl = this.apiBaseUrl + 'users';
    }

    get( params: any = {} ) {
        this.currBranchId = localStorage.getItem('currBranchId') || '';
        this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + this.currBranchId;
        let data = this.helper.serialize( params );
        this.url = this.resourceUrl +'?' + data + this.token;

        console.log('url', this.url);
        return this.http.get( this.url ).pipe(
            map( res => {
                let x = res.json();
                console.log('XXX', x );
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

  	show( id, params: any = {} ) {
        this.currBranchId = localStorage.getItem('currBranchId') || '';
        this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + this.currBranchId;
        // this.url = this.resourceUrl +'/'+ id + '?' + this.token;
        let data = this.helper.serialize( params );
        this.url = this.resourceUrl +'/'+ id +'?' + data + this.token;
        console.log(this.url);
        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    store( params ) {
        this.currBranchId = localStorage.getItem('currBranchId') || '';
        this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + this.currBranchId;
        this.url = this.resourceUrl +'?' + this.token;
        const body = JSON.stringify(params);
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(this.url, body, options).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    destroy() {
      
    }

    update( id: string, fields = {} ) {
        this.currBranchId = localStorage.getItem('currBranchId') || '';
        this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + this.currBranchId;
        fields['_method'] = 'PUT';
        const url = this.resourceUrl +'/' + id + '?' + this.token;
        const body = JSON.stringify( fields );
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(url, body, options).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    updatePassword( id: string, fields = {} ) {
        this.currBranchId = localStorage.getItem('currBranchId') || '';
        this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + this.currBranchId;
        fields['_method'] = 'PUT';
        const url = this.resourceUrl +'/' + id + '/update/user/password?' + this.token;
        const body = JSON.stringify( fields );
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(url, body, options).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    gen_hash( id ) {
        this.currBranchId = localStorage.getItem('currBranchId') || '';
        this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + this.currBranchId;
        this.url = this.resourceUrl + '/' + id + '/generate_hash?' + this.token;
        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                console.log('XXX', x );
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    avatarUpload( uploadData ) {

        this.currBranchId = localStorage.getItem('currBranchId') || '';
        this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + this.currBranchId;
        this.url = this.apiBaseUrl +'uploads/user/avatar?' + this.token;
        const headers = new Headers({ 'Content-Type': []});
        const options = new RequestOptions({ headers: headers });

        console.log(this.url, uploadData );

        return this.http.post(this.url, uploadData, options).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    count_employees( w ) {

        this.currBranchId = localStorage.getItem('currBranchId') || '';
        this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + this.currBranchId;
        this.url = this.apiBaseUrl + 'statistics/users?w=' + w + this.token;
        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    forgot_password( params ) {
        let data = this.helper.serialize( params );
        this.url = this.resourceUrl +'/forgot-password?' + data;
        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                console.log('forgot password x', x)
                return x;
            })
        );
    }

    gen_qr( hash: string, id ) {
        this.currBranchId = localStorage.getItem('currBranchId') || '';
        this.token = '&token=' + localStorage.getItem('token') + '&branch_id=' + this.currBranchId;
        this.url = this.apiBaseUrl + 'myqr/' + id + '?hash=' + hash + this.token;
        console.log('gen qr', this.url);

        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                console.log('XXX', x );
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }
    
}
