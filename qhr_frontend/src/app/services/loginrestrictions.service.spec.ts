import { TestBed, inject } from '@angular/core/testing';

import { LoginRestrictionsService } from './loginrestrictions.service';

describe('LoginrestrictionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginRestrictionsService]
    });
  });

  it('should be created', inject([LoginRestrictionsService], (service: LoginRestrictionsService) => {
    expect(service).toBeTruthy();
  }));
});
