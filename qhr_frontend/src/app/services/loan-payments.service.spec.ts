import { TestBed, inject } from '@angular/core/testing';

import { LoanPaymentsService } from './loan-payments.service';

describe('LoanPaymentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoanPaymentsService]
    });
  });

  it('should be created', inject([LoanPaymentsService], (service: LoanPaymentsService) => {
    expect(service).toBeTruthy();
  }));
});
