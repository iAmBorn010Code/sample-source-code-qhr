import { TestBed, inject } from '@angular/core/testing';

import { PayrollGroupsService } from './payroll-groups.service';

describe('PayrollGroupsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PayrollGroupsService]
    });
  });

  it('should be created', inject([PayrollGroupsService], (service: PayrollGroupsService) => {
    expect(service).toBeTruthy();
  }));
});
