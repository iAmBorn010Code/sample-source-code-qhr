import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { TextHelperService } from './../texthelper.service';
import { map } from 'rxjs/operators';
import { LocalstorageService } from './../localstorage.service';
import { AuthService } from './../auth.service';

@Injectable({
  providedIn: 'root'
})
export class InvoicesService {

  apiBaseUrl: string = environment.apiBaseUrl;
  url: string;
  token:string;
  resourceUrl: string;

  constructor(
    private storage: LocalstorageService,
    private http: Http,
    private helper: TextHelperService,
    private AuthService: AuthService,
  ) {
    this.resourceUrl = this.apiBaseUrl + 'invoices/';
  }

  show( invoice_uid, payment_trans_id ) {

    this.token = '&token=' + localStorage.getItem('token');
    this.url = this.resourceUrl +  invoice_uid + '?payment_trans_id='+ payment_trans_id + this.token;

    console.log( this.url );

    return this.http.get( this.url ).pipe(
        map( res => {
            const x = res.json();
            this.AuthService.isStillLogged(x);
            return x;
        })
    );
  }
}
