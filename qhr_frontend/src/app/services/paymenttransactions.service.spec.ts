import { TestBed, inject } from '@angular/core/testing';

import { PaymentTransactionsService } from './paymenttransactions.service';

describe('BillingtransactionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PaymentTransactionsService]
    });
  });

  it('should be created', inject([PaymentTransactionsService], (service: PaymentTransactionsService) => {
    expect(service).toBeTruthy();
  }));
});
