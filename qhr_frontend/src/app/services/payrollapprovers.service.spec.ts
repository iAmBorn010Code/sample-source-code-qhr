import { TestBed, inject } from '@angular/core/testing';

import { PayrollApproversService } from './payrollapprovers.service';

describe('PayrollApproversService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PayrollApproversService]
    });
  });

  it('should be created', inject([PayrollApproversService], (service: PayrollApproversService) => {
    expect(service).toBeTruthy();
  }));
});
