import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { LocalstorageService } from '../localstorage.service';
import { map } from 'rxjs/operators';
import { Http, Headers, RequestOptions } from '@angular/http';
import { TextHelperService } from '../texthelper.service';
import { AuthService } from '../auth.service';


@Injectable({
  providedIn: 'root'
})
export class SubscriptionsService {

    apiBaseUrl: string = environment.apiBaseUrl;
    url: string;
    token: string;
    resourceUrl: string;
    constructor(
        private storage: LocalstorageService,
        private http: Http,
        private helper: TextHelperService,
        private AuthService: AuthService,
    ) {
        this.resourceUrl = this.apiBaseUrl + 'subscriptions';
    }

    get( params: any = {} ) {

        this.token = '&token=' + localStorage.getItem('token');

        const data = this.helper.serialize( params );
        this.url = this.resourceUrl +'?' + data + this.token;
        console.log( this.url );

        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    show( id, paid ) {

        this.token = '&token=' + localStorage.getItem('token');
        this.url = this.resourceUrl +'/'+ id + '?paid='+ paid + this.token;
    
        console.log( this.url );
    
        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    store( params ) {

        this.token = '&token=' + localStorage.getItem('token');
        this.url = this.resourceUrl +'?' + this.token + '&branch_id=' + localStorage.getItem('currBranchId');
        const body = JSON.stringify(params);
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(this.url, body, options).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    destroy() {
      
    }

    update( serviceId: string, fields = {} ) {
        
        this.token = '&token=' + localStorage.getItem('token');

        fields['_method'] = 'PUT';
        const url = this.resourceUrl +'/' + serviceId + '?' + this.token;
        const body = JSON.stringify( fields );
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(url, body, options).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    getSubs() {

        this.token = 'token=' + localStorage.getItem('token');

        // const data = this.helper.serialize( );
        this.url = this.resourceUrl +'/get?' + this.token;
        console.log( this.url );

        return this.http.get( this.url ).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }

    cancel( params ) {

        this.token = 'token=' + localStorage.getItem('token');
        this.url = this.resourceUrl +'/cancel?' + this.token;
        const body = JSON.stringify(params);
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(this.url, body, options).pipe(
            map( res => {
                const x = res.json();
                this.AuthService.isStillLogged(x);
                return x;
            })
        );
    }
}
