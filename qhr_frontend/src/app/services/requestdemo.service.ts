import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { TextHelperService } from './../texthelper.service';
import { map } from 'rxjs/operators';


@Injectable()
export class RequestDemoService {

  apiBaseUrl: string = environment.apiBaseUrl;
  url: string;
  token:string;
  resourceUrl: string;

  constructor(
    private http: Http,
    private helper: TextHelperService,
  ) {
    this.resourceUrl = this.apiBaseUrl + 'request-demo';
  }

  store( params ) {

    this.url = this.resourceUrl +'?';
    const body = JSON.stringify(params);
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    console.log('this.url');

    return this.http.post(this.url, body, options).pipe(
      map( res => {
        const x = res.json();
        return x;
      })
    );
}
}
