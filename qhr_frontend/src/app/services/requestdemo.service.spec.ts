import { TestBed, inject } from '@angular/core/testing';

import { RequestdemoService } from './requestdemo.service';

describe('RequestdemoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequestdemoService]
    });
  });

  it('should be created', inject([RequestdemoService], (service: RequestdemoService) => {
    expect(service).toBeTruthy();
  }));
});
