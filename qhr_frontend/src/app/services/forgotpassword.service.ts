import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { TextHelperService } from './../texthelper.service';
import { map } from 'rxjs/operators';

@Injectable()
export class ForgotPasswordService {

  apiBaseUrl: string = environment.apiBaseUrl;
  url: string;
  token:string;
  resourceUrl: string;

  constructor(
    private http: Http,
    private helper: TextHelperService,
  ) {
    this.resourceUrl = this.apiBaseUrl + 'passwordreset';
  }

  reset_password( hash: string, fields = {} ) {
    this.token = '&token=' + localStorage.getItem('token');
    fields['_method'] = 'PUT';
    const url = this.resourceUrl +'/reset/' + hash;
    const body = JSON.stringify( fields );
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(url, body, options).pipe(
      map( res => {
        const x = res.json();
        return x;
      })
    );
  }
}
