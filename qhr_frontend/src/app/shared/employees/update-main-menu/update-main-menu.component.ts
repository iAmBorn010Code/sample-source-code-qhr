// import { Component, OnInit, Input } from '@angular/core';
import { Component, OnInit, Input, ViewChild, AfterViewInit, HostListener } from '@angular/core';
import { EmployeesService } from '../../../services/employees.service';
import { SharedDataEmployeeService } from '../../../services/shared-data-employee.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-update-main-menu',
  templateUrl: './update-main-menu.component.html',
  styleUrls: ['./update-main-menu.component.css']
})
export class UpdateMainMenuComponent implements OnInit {

  @Input() employee: any;

  sharedData: any;

  qr_code: string;
  qrLoader = true;

  isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';
  
  constructor(
    private EmployeesService: EmployeesService,
    private SharedDataEmployeeService: SharedDataEmployeeService,
    private ActivatedRoute: ActivatedRoute

  ) { }

  ngOnInit() {

    this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.EmployeesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
					let result = data.results;
					this.SharedDataEmployeeService.employee.next( result );
				} else {
					this.errorMsg = data.msg;
				}
			});
		});		

    this.SharedDataEmployeeService.employee.subscribe( (employee) => {
      this.sharedData = <string>employee;
      this.qr_code = this.sharedData.qr_code;
      this.qrLoader = false;
		});

  }

  gen_qr(){

    if ( !this.sharedData.hash ){

      this.isError = true;
      this.errorMsg = "Please generate an employee hash first.";
    } else {

      this.qrLoader = true;

      this.EmployeesService.gen_qr( this.sharedData.hash, this.sharedData._id ).subscribe( data => {
        if ( !data.error ) {
          this.qr_code = data.results.qr_code;
        } else {
          this.isError = true;
        }

        this.qrLoader = false;
      });

    }
	}

}
