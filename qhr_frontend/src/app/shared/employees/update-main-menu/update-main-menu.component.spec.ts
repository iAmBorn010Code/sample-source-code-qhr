import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMainMenuComponent } from './update-main-menu.component';

describe('UpdateMainMenuComponent', () => {
  let component: UpdateMainMenuComponent;
  let fixture: ComponentFixture<UpdateMainMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateMainMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMainMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
