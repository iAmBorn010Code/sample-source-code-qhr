import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontWhatTheySayComponent } from './front-what-they-say.component';

describe('FrontWhatTheySayComponent', () => {
  let component: FrontWhatTheySayComponent;
  let fixture: ComponentFixture<FrontWhatTheySayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontWhatTheySayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontWhatTheySayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
