import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollsUpdateMainMenuComponent } from './payrolls-update-main-menu.component';

describe('PayrollsUpdateMainMenuComponent', () => {
  let component: PayrollsUpdateMainMenuComponent;
  let fixture: ComponentFixture<PayrollsUpdateMainMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollsUpdateMainMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollsUpdateMainMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
