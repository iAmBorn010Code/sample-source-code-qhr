import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payrolls-update-main-menu',
  templateUrl: './payrolls-update-main-menu.component.html',
  styleUrls: ['./payrolls-update-main-menu.component.css']
})
export class PayrollsUpdateMainMenuComponent implements OnInit {

  @Input() payroll: any;

  constructor(private router : Router) {}

  menuInfo: boolean = false;
  ngOnInit() {
    let str = this.router.url;
    var n = str.indexOf( 'info' );

    if ( n >= 0) 
      this.menuInfo = true;
    else
      this.menuInfo = false;

  }

}
