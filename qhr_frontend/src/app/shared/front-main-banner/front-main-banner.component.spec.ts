import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontMainBannerComponent } from './front-main-banner.component';

describe('FrontMainBannerComponent', () => {
  let component: FrontMainBannerComponent;
  let fixture: ComponentFixture<FrontMainBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontMainBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontMainBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
