import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontMainMenuComponent } from './front-main-menu.component';

describe('FrontMainMenuComponent', () => {
  let component: FrontMainMenuComponent;
  let fixture: ComponentFixture<FrontMainMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontMainMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontMainMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
