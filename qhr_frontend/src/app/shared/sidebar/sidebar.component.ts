import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  menus: any;

  company = {};
  perm: any;

  constructor() {
  }

  ngOnInit() {
    this.company = JSON.parse( localStorage.getItem('company') );
    this.perm =  localStorage.getItem('perm');

    console.log('sidebar');
    console.log('this.perm', this.perm);

    this.checkAccessPermissions();
  }

  checkAccessPermissions() {

    // Branches
    this.menus = {
      dashboard: this.perm.indexOf(100) !== -1 ? true : false,
      employees: this.perm.indexOf(200) !== -1 ? true : false,
      attendances: this.perm.indexOf(300) !== -1 ? true : false,
      leave_management: this.perm.indexOf(400) !== -1 ? true : false,
      payroll: this.perm.indexOf(500) !== -1 ? true : false,
      departments: this.perm.indexOf(600) !== -1 ? true : false,
      branches: this.perm.indexOf(700) !== -1 ? true : false,
      reports: this.perm.indexOf(800) !== -1 ? true : false,
      quotes: this.perm.indexOf(900) !== -1 ? true : false,
      holidays: this.perm.indexOf(1000) !== -1 ? true : false,
      payslips: this.perm.indexOf(1100) !== -1 ? true : false,
      teams: this.perm.indexOf(1200) !== -1 ? true : false,
      team_members: this.perm.indexOf(1300) !== -1 ? true : false,
      loans: this.perm.indexOf(1400) !== -1 ? true : false,
      freedom_board: this.perm.indexOf(1500) !== -1 ? true : false,
      payroll_approvers: this.perm.indexOf(1800) !== -1 ? true : false,
      overtime_approvers: this.perm.indexOf(3000) !== -1 ? true : false,
      payroll_groups: this.perm.indexOf(500) !== -1 ? true : false,
      logs: this.perm.indexOf(2000) !== -1 ? true : false,
      company_management: this.perm.indexOf(2100) !== -1 ? true : false,
      setting: this.perm.indexOf(2200) !== -1 ? true : false,
      user_setting: this.perm.indexOf(2300) !== -1 ? true : false,
      login_restrictions: this.perm.indexOf(2400) !== -1 ? true : false,
      designations: this.perm.indexOf(3100) !== -1 ? true : false,
    };

    console.log( this.menus );

  }



}
