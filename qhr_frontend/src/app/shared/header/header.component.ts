import { Component, OnInit } from '@angular/core';
import { PageService } from './../../page.service';
import { AuthService } from '../../auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  pageName = '';
  loggedUser: any = {};
  company: any = {};
  companyLogo: string = '';
  profilePhoto: string = '';
  currBranchName: string = '';
  companyName: string = '';

  perm: any;
  withPerm: any;

  constructor(  
    private pageService: PageService,
    private AuthService: AuthService,
  ) {
    this.loggedUser = this.AuthService.getLoggedUser();
    this.profilePhoto = this.loggedUser.photo;
    this.company = JSON.parse( localStorage.getItem('company') );
    this.perm = localStorage.getItem('perm');
    if ( this.company ){
      this.companyName = this.company.name;
      if ( this.company.logo ) {
        this.companyLogo = this.company.logo + '?r=' + Math.floor((Math.random() * 1000) + 1);
      } else {
        this.companyLogo = '';
      }
    } else {
      this.AuthService.logout();
    }
      
  }

  ngOnInit() {

    this.currBranchName = localStorage.getItem('currBranchName') || '';

    this.pageService.pageName.subscribe(
      (pageName) => {
        this.pageName = <string>pageName;
        console.log('Change Page Name:  ' + this.pageName );
      }
    );

    this.pageService.companyLogo.subscribe(
      (companyLogo) => {
        this.companyLogo = <string>companyLogo;
        console.log('Change companyLogo:  ' + this.companyLogo );
      }
    );

    this.pageService.profilePhoto.subscribe(
      (profilePhoto) => {
        this.profilePhoto = <string>profilePhoto;
        console.log('Change profilePhoto:  ' + this.profilePhoto );
      }
    );

    this.pageService.currBranchName.subscribe(
      (currBranchName) => {
        this.currBranchName = <string>currBranchName;
        console.log('Change currBranchName:  ' + this.currBranchName );
      }
    );

    this.pageService.companyName.subscribe(
      (companyName) => {
        this.companyName = <string>companyName;
        console.log('Change companyName:  ' + this.companyName );
      }
    );

    this.withPerm = {
      subscriptions: this.perm.indexOf(1600) !== -1 ? true : false,
      payments_history: this.perm.indexOf(1700) !== -1 ? true : false,
    }

  }

    

}
