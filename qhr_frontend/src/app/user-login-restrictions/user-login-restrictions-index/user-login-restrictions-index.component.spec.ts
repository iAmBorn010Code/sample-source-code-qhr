import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLoginRestrictionsIndexComponent } from './user-login-restrictions-index.component';

describe('UserLoginRestrictionsIndexComponent', () => {
  let component: UserLoginRestrictionsIndexComponent;
  let fixture: ComponentFixture<UserLoginRestrictionsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLoginRestrictionsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLoginRestrictionsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
