import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLoginRestrictionsCreateComponent } from './user-login-restrictions-create.component';

describe('UserLoginRestrictionsCreateComponent', () => {
  let component: UserLoginRestrictionsCreateComponent;
  let fixture: ComponentFixture<UserLoginRestrictionsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLoginRestrictionsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLoginRestrictionsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
