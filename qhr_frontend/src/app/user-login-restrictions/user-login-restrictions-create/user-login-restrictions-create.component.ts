import { Component, OnInit } from '@angular/core';
import { TextHelperService } from '../../texthelper.service';
import { LoginRestrictionsService } from 'src/app/services/loginrestrictions.service';
import { EmployeesService } from 'src/app/services/employees.service';
import { UserLoginRestrictionsService } from 'src/app/services/userloginrestrictions.service';

@Component({
  selector: 'app-user-login-restrictions-create',
  templateUrl: './user-login-restrictions-create.component.html',
  styleUrls: ['./user-login-restrictions-create.component.css']
})
export class UserLoginRestrictionsCreateComponent implements OnInit {

  user_restriction: any = {};
  login_restrictions: any = {};
  employees: any = {};

  params = {
		s: '',
  };

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;
  saving = false;

  constructor(
    private TextHelperService: TextHelperService,
    private LoginRestrictionsService: LoginRestrictionsService,
    private EmployeesService: EmployeesService,
    private UserLoginRestrictionsService: UserLoginRestrictionsService
  ) { }

  ngOnInit() {

    this.EmployeesService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.employees = data.results;
			} else {
				this.errorMsg = data.msg;
			}

    });
    
    this.LoginRestrictionsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.login_restrictions = data.results;
			} else {
				this.errorMsg = data.msg;
			}

			this.mainLoader = false;
		});

  }

  getFields() {

    const fields = {
      emp_id: this.user_restriction.emp_id,
      restriction_id: this.user_restriction.restriction_id,
    };
    
    return fields;
  }

  save() {
    this.successMsg = '';
    this.errorMsg = '';
    this.isCloseMsg = false;
    this.saving = true;

    console.log('fields: ', this.getFields());

    this.UserLoginRestrictionsService.store( this.getFields() ).subscribe( data => {
      this.saving = false;
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
    });
  }

}
