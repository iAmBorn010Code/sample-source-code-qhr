import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page.service';
import { PaginatorService } from '../../paginator.service';
import { NotificationsService } from '../../services/notifications.service';

@Component({
  selector: 'app-notifs-index',
  templateUrl: './notifs-index.component.html',
  styleUrls: ['./notifs-index.component.css']
})
export class NotifsIndexComponent implements OnInit {

  notifs = [];
	paginator: any;
	params = {
		limit: 10,
		page: 1,
	};
	
  mainLoader = true;
  
  constructor(
    private PageService: PageService,
    private PaginatorService: PaginatorService,
    private NotifsService: NotificationsService
  ) { }

  ngOnInit() {
    this.PageService.pageName.next('Notifications');
    this.get();
  }

  get() {
		  
		this.notifs = [];
		this.NotifsService.get( this.params ).subscribe( data => {

			if ( !data.error ) {
				this.notifs = data.results;
				this.paginator = 
					this.PaginatorService.paginate( 
						data.total_count, 
						this.params.limit, 
						this.params.page, 
						5 
					);
				console.log( this.paginator );
			}
			console.log('notifs', this.notifs);
			this.mainLoader = !this.mainLoader;
		});
  }
  
  //pagination
  nextPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.nextPage;
		this.get();
	}

	prevPage() {
		this.mainLoader = true;
		this.params.page = this.paginator.prevPage;
		this.get();
	}
	
	goToPage( page ) {
		this.mainLoader = true;
		if ( page === this.paginator.currrentPage ) return;
		this.params.page = page;
		this.get();
	}

}
