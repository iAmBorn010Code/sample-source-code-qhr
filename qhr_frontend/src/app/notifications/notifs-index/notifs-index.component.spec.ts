import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifsIndexComponent } from './notifs-index.component';

describe('NotifsIndexComponent', () => {
  let component: NotifsIndexComponent;
  let fixture: ComponentFixture<NotifsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
