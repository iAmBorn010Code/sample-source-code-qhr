import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CompaniesService } from '../../services/companies.service';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';
import { LocalstorageService } from '../../localstorage.service';

@Component({
  selector: 'app-companies-update',
  templateUrl: './companies-update.component.html',
  styleUrls: ['./companies-update.component.css']
})
export class CompaniesUpdateComponent implements OnInit {

	company: any = {
		name: '',
		address: '',
		email: '',
		contact_nos: '',
		short_description: '',
		long_description: '',
		hash: '',
		logo: '',
	};
	  
	isError: boolean;
	isCloseMsg: boolean;
	successMsg = '';
	errorMsg = '';

	mainLoader = true;
	saving = false;
	generating = false;

	selectedFile: File = null;
	uploads_history = [];

	fileUploading = false;
	fileUploaded = false;
	fileUploadTriggered = false;
	fileUploadingMsg = '';

  	constructor(
		private PageService: PageService,
		private TextHelperService: TextHelperService,
		private CompaniesService: CompaniesService,
		private ActivatedRoute: ActivatedRoute,
		private storage: LocalstorageService,
	) { }

	ngOnInit() {

		this.PageService.pageName.next('Companies');

		// this.ActivatedRoute.params.forEach(( params: Params ) => {
		// 	console.log( params );
		// });

		this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.CompaniesService.show( params['id'] ).subscribe( data => {
				if ( !data.error ) {
          		this.company = data.results;
				}
				this.mainLoader = !this.mainLoader;
				console.log( 'company', this.company );
			});
    	});

	}

	update() {

		this.successMsg = '';
		this.errorMsg = '';
		this.isCloseMsg = false;
		this.saving = true;

		const fields = {
			name: this.company.name,
			address: this.company.address,
			email: this.company.email,
			contact_nos: this.company.contact_nos,
			short_description: this.company.short_description,
			long_description: this.company.long_description,
			hash: this.company.hash
		};

		console.log( fields );

 	 	this.ActivatedRoute.params.forEach(( params: Params ) => {
			this.CompaniesService.update( params['id'], fields ).subscribe( data => {
				this.saving = false;
				if ( !data.error ) {
					this.isError = false;
					this.successMsg = data.msg;

					this.PageService.companyName.next( this.company.name );
				} else {
						this.isError = true;
						const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
						this.errorMsg = msg;
				}
			});
		});
	}

	gen_hash() {

		this.generating = true;
		this.CompaniesService.gen_hash( this.company._id ).subscribe( data => {
			if ( !data.error ) {
				this.company.hash = data.results;
				console.log('hash', this.company.hash);
			}
			this.generating = false;
		});

	}

	onFileChanged(event) {

		this.selectedFile = <File>event.target.files[0];
		const uploadData = new FormData();
		
		uploadData.append('company_id', this.company._id);
		uploadData.append('logo', this.selectedFile, this.selectedFile.name);
		

		console.log(uploadData);

		this.fileUploading = true;
		this.fileUploadTriggered = true;

		const fileMaxSize = 1024 * 1024 * 2; // Max file size is 2mb.
		if ( this.selectedFile.size > fileMaxSize ) {
			this.fileUploading = false;
			this.fileUploaded = false;
			this.fileUploadingMsg = 'File size is too large.'
		}

		// Check file type
		if ( this.selectedFile.type == 'image/jpeg' || this.selectedFile.type == 'image/png' ) {
			this.CompaniesService.logoUpload(uploadData).subscribe(data => {

	      	this.fileUploading = false;

					if (!data.error) {
						this.fileUploaded = true;
						this.company.logo = data.logo;
						this.storage.set( 'company', this.company );
						
						// Update company logo.
						let companyLogo = this.company.logo + '?r=' + Math.floor((Math.random() * 1000) + 1);
						this.PageService.companyLogo.next( companyLogo );

					} else {
						this.fileUploaded = false;
						this.fileUploadingMsg = data.msg;
					}
		    });
		} else {
			this.fileUploading = false;
			this.fileUploaded = false;
			this.fileUploadingMsg = 'File type is not supported.'
		}
		
	  }

}
