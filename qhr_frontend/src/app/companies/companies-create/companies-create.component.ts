import { Component, OnInit } from '@angular/core';
import { CompaniesService } from '../../services/companies.service';
import { Router, Params } from '@angular/router';
import { TextHelperService } from '../../texthelper.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-companies-create',
  templateUrl: './companies-create.component.html',
  styleUrls: ['./companies-create.component.css']
})
export class CompaniesCreateComponent implements OnInit {

    user: any = {
    first_name: '',
    last_name: '',
    user_email: '',
    username: '',
    password: '',
    name: '',
    address: '',
    email: '',
    gender: 'male'
  }


  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';
  loading: boolean = false;

  saving = false;

  constructor(
    private PageService: PageService,
    private TextHelperService: TextHelperService,
    private CompaniesService: CompaniesService,
    private router: Router,
  ) { }

  ngOnInit() {

    this.PageService.pageName.next('Companies');
    console.log('init called');
  }

  onSubmit() {

    this.saving = true;

    const fields = {
      first_name: this.user.first_name,
      last_name: this.user.last_name,
      gender: this.user.gender,
      username: this.user.username,
      user_email: this.user.user_email,
      password: this.user.password,
      name: this.user.name,
      address: this.user.address,
      email: this.user.email
    };
    

    this.successMsg = '';
    this.errorMsg = '';
    
    console.log(fields);

    this.CompaniesService.store( fields ).subscribe( data => {
      console.log('request sucessful', data)
      if ( !data.error ) {
        this.isError = false;
        this.successMsg = data.msg;
      } else {
        this.isError = true;
        const msg = this.TextHelperService.formatErrors( data.msg, data.form_errors );
        this.errorMsg = msg;
      }
      
      this.saving = false;
    });
  }
}
