import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompaniesService } from '../../services/companies.service';
import { PageService } from '../../page.service';

@Component({
  selector: 'app-companies-show',
  templateUrl: './companies-show.component.html',
  styleUrls: ['./companies-show.component.css'],
  providers: [CompaniesService]
})
export class CompaniesShowComponent implements OnInit {

  company: any;
  id;

  isError = false;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';

  mainLoader = true;

  constructor(
    private PageService: PageService,
    private CompaniesService: CompaniesService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.PageService.pageName.next('Companies');

    this.route.params.subscribe( params => {

      this.show( params['id'] );
      this.id = params['id'];
    });
  }

  show( id ) {

    this.isError = false;

    this.CompaniesService.show( id ).subscribe( data => {

      if ( !data.error ) {
       this.company = data.results;
      } else {
        this.isError = true;
        this.errorMsg = "No record found.";
      }
      this.mainLoader = !this.mainLoader;
    });

  }

}
