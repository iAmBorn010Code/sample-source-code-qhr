import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyCompanyComponent } from './verify-company.component';

describe('VerifyCompanyComponent', () => {
  let component: VerifyCompanyComponent;
  let fixture: ComponentFixture<VerifyCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
