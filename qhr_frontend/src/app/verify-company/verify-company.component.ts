import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompaniesService } from '../services/companies.service';

@Component({
  selector: 'app-verify-company',
  templateUrl: './verify-company.component.html',
  styleUrls: ['./verify-company.component.css']
})
export class VerifyCompanyComponent implements OnInit {

  company: any;
  hash;

  isError: boolean;
  isCloseMsg: boolean;
  successMsg = '';
  errorMsg = '';
  loading = '';

  constructor(
    private route: ActivatedRoute,
    private CompaniesService: CompaniesService
  ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.loading = 'true';
      this.verify( params['hash'] );
      this.hash = params['hash'];
    });
  }

  verify( hash ) {


    this.CompaniesService.verify( hash ).subscribe( data => {

      if ( !data.error ) {
        this.isError = true;
        this.errorMsg = data.msg;
      } else {
        this.isError = false;
        this.successMsg = data.msg;
      }
    });

  }

}
