import { Injectable } from '@angular/core';
import { filter, map, catchError } from 'rxjs/operators';

@Injectable()
export class PaginatorService {

    // Author: RYAN DEJANDO BINAS
    paginate( totalCount, take, page, pagesLimit ) {

      // var offset = offset >= totalCount ? totalCount - 1: offset;
      var pages = Math.ceil( totalCount / take );
      var currrentPage = page >= pages ? pages : page;
      var pagesBatch = Math.ceil( currrentPage / pagesLimit );

      var pagesFrom = (pagesBatch - 1)  * pagesLimit + 1;
      var pagesTo = pagesBatch * pagesLimit >= pages ? pages :  pagesBatch * pagesLimit;

      if ( 1 === pagesTo && pagesFrom === pagesTo ) {
        pagesTo = 0;
      }

      var prevPage = 1, nextPage = 2;
      var hasNext = false, hasPrev = false;

      prevPage = ( currrentPage - 1 ) <= 0 ? 1 : ( currrentPage - 1 );
      nextPage = ( currrentPage + 1 ) >= 1 ? ( currrentPage + 1 ) : 2;

      if ( currrentPage < pages ) {
        hasNext = true;
      }

      if ( currrentPage >= 2 ) {
        hasPrev = true;
      }

      let displayPages = [];
      for ( let x = pagesFrom; x <= pagesTo; x++ ) {
        displayPages.push( x );
      }

      return {
        'pages' : pages,
        'currrentPage' : currrentPage,
        'take' : take,
        'pagesLimit' : pagesLimit,
        'pagesBatch' : pagesBatch,
        'pagesFrom' : pagesFrom,
        'pagesTo' : pagesTo,
        'prevPage' : prevPage,
        'nextPage' : nextPage,
        'hasPrev' : hasPrev,
        'hasNext' : hasNext,
        'displayPages': displayPages
      };
    }
}
