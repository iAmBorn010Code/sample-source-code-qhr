export const environment = {
  production: true,
  apiBaseUrl: 'https://api-staging.qloudhr.com/',
  bucketUrl: 'https://s3-ap-southeast-1.amazonaws.com/qloudhr/staging/',
  bucketFileUrl :'https://s3-ap-southeast-1.amazonaws.com/qloudhr/staging/f/',
  siteUrl: 'https://staging.qloudhr.com/',
};