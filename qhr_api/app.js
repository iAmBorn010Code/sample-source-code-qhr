// Dependencies

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const path    = require("path");
const port = process.env.PORT || 3000;

var config = require('./config/config');

var moment = require('moment-timezone');
moment.tz.setDefault("Asia/Manila");

var cors = require('cors');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // support encoded bodies

app.use(cors());

app.use(expressValidator());
app.use(require('trim-request-body'));

const mongoose = require('mongoose');
mongoose.connect( config.database.mongoDB.connectionString );
mongoose.Promise = global.Promise;

const aws = require('aws-sdk');
const s3 = new aws.S3();
aws.config.update(config.aws);

// Routes
const users = require('./controllers/users');
app.use('/users', users);

const departments = require('./controllers/departments');
app.use('/departments', departments);

const branches = require('./controllers/branches');
app.use('/branches', branches);

// DTR
const dtr = require('./controllers/dailytimerecords');
app.use('/daily-time-records', dtr);

const leaves = require('./controllers/leaves');
app.use('/leaves', leaves);

const companies = require('./controllers/companies');
app.use('/companies', companies);

const settings = require('./controllers/settings');
app.use('/settings', settings);

const auth = require('./controllers/auth');
app.use('/auth', auth);

const payrolls = require('./controllers/payrolls');
app.use('/payrolls', payrolls);

const payrolldetails = require('./controllers/payrolldetails');
app.use('/payrolldetails', payrolldetails);

const deductions = require('./controllers/deductions');
app.use('/deductions', deductions);

const systemlogs = require('./controllers/systemlogs');
app.use('/systemlogs', systemlogs);

const earnings = require('./controllers/earnings');
app.use('/earnings', earnings);

const notifications = require('./controllers/notifications');
app.use('/notifications', notifications);

const userpermissions = require('./controllers/userpermissions');
app.use('/user-permissions', userpermissions);

const permissions = require('./controllers/permissions');
app.use('/permissions', permissions);

const uploads = require('./controllers/uploads');
app.use('/uploads', uploads);

const documents = require('./controllers/documents');
app.use('/documents', documents);

const freedomboard = require('./controllers/freedomboard');
app.use('/freedomboard', freedomboard);

const crons = require('./controllers/crons');
app.use('/crons', crons);

const usersettings = require('./controllers/usersettings');
app.use('/user-settings', usersettings);

const requestdemo = require('./controllers/requestdemo');
app.use('/request-demo', requestdemo);

const statistics = require('./controllers/statistics');
app.use('/statistics', statistics);

const quotes = require('./controllers/quotes');
app.use('/quotes', quotes);

const holidays = require('./controllers/holidays');
app.use('/holidays', holidays);

const passwordreset = require('./controllers/passwordreset');
app.use('/passwordreset', passwordreset);

const payments = require('./controllers/payments');
app.use('/payments', payments);

const paymenttransactions = require('./controllers/paymenttransactions');
app.use('/payment-transactions', paymenttransactions);

const subscriptions = require('./controllers/subscriptions');
app.use('/subscriptions', subscriptions);

const qrcode = require('./controllers/qrcode');
app.use('/myqr', qrcode);

const payslips = require('./controllers/payslips');
app.use('/payslips', payslips);

const signup = require('./controllers/signup');
app.use('/signup', signup);

const teams = require('./controllers/teams');
app.use('/teams', teams);

const loans = require('./controllers/loans');
app.use('/loans', loans);

const loginrestrictions = require('./controllers/loginrestrictions');
app.use('/login-restrictions', loginrestrictions);

const teammembers = require('./controllers/teammembers');
app.use('/team-members', teammembers);

const billing = require('./controllers/billing');
app.use('/billing', billing);

const loanpayments = require('./controllers/loanpayments');
app.use('/loan-payments', loanpayments);

const payrollapprovers = require('./controllers/payrollapprovers');
app.use('/payroll-approvers', payrollapprovers);

const payrollgroups = require('./controllers/payrollgroups');
app.use('/payroll-groups', payrollgroups);

const invoices = require('./controllers/invoices');
app.use('/invoices', invoices);

const contributions = require('./controllers/payrollcontributions');
app.use('/contributions', contributions);

const leavecredits = require('./controllers/leavecredits');
app.use('/leave-credits', leavecredits);

const overtimes = require('./controllers/overtimes');
app.use('/overtimes', overtimes);

const supervisors = require('./controllers/supervisors');
app.use('/supervisors', supervisors);

const supervisormembers = require('./controllers/supervisormembers');
app.use('/supervisor-members', supervisormembers);

const attendancestatistics = require('./controllers/attendancestatistics');
app.use('/stats/attendance', attendancestatistics);

const designations = require('./controllers/designations');
app.use('/designations', designations);

const userdesignations = require('./controllers/userdesignations');
app.use('/user-designations', userdesignations);

const workschedules = require('./controllers/workschedules');
app.use('/work-schedules', workschedules);

const userloginrestrictions = require('./controllers/userloginrestrictions');
app.use('/user-login-restrictions', userloginrestrictions);

app.listen(port, function () {
	console.log('\x1b[33m%s\x1b[0m', '** Express Development is listening on localhost:'+port+', open your browser on http://localhost:'+port+' **');
});



// UPDATER
const updater = require('./controllers/updater');
app.use('/updater', updater);
