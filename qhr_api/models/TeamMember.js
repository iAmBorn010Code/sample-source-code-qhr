const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];

// Schema
var schema = mongoose.Schema({

	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
	},

	employee: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		ref: 'User'
	},
	
	team_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},

	
}, { timestamps: true, "strict": true  });

 
const TeamMember = module.exports = mongoose.model('Team_Member', schema);

