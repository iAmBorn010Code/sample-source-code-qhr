const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];


// Schema
var schema = mongoose.Schema({

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	supervisor: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	employee: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'User'
	},


}, { timestamps: true, "strict": true  });

 
const SupervisorMember = module.exports = mongoose.model('supervisor_member', schema);

