const mongoose = require('mongoose');
const validate = require('mongoose-validator');

var config = require('../config/config');

// Validators
var nameValidator = [
  
];
 

// Schema
var schema = mongoose.Schema({
	
	branch_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Branch',
        required: true
    },    

    company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},

	name: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 30],
			  message: 'Payroll Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
		]
	},

    total_incomes: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Gross Income cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Gross Income cannot be less than 0.'
			}),
		]
    },

    total_deductions: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Total Deductions cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Total Deductions cannot be less than 0.'
			}),
		]
    },

	total_taxes: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Total Deductions cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Total Deductions cannot be less than 0.'
			}),
		]
    },

    total_contributions: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Total Deductions cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Total Deductions cannot be less than 0.'
			}),
		]
    },	

	overall_total: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Withholding tax cannot be less than 0.'
			}),
		]
    },
    
}, { timestamps: true, "strict": true  });

const PayrollGroup = module.exports = mongoose.model('Payroll_Group', schema);

