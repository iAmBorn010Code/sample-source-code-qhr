const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];


// Schema
var schema = mongoose.Schema({

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	employee: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'User'
	},


}, { timestamps: true, "strict": true  });

 
const Supervisor = module.exports = mongoose.model('supervisor', schema);

