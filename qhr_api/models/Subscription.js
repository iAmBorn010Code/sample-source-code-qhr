const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];
 

// Schema
var schema = mongoose.Schema({

	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
		validate: {
			validator: function(v) {
				return mongoose.Types.ObjectId.isValid(v)
			},
			message: 'Invalid ID.'
		}
	},

	total_amount: {
		type: Number,
		default: 0,
    },

    plan_type: {
		type: String,
		default: 'A',
		required: true,
		passIfEmpty: false,
    },

    company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
		ref: 'Company'
	},

    interval_count: {
		type: Number,
		default: 0,
    },

    interval: {
		type: String,
		default: 'monthly',
    },

    paid: { 
		type: Boolean, 
		default: false 
	},
	
	payment_method: {
		required: true,
		type: String,
		default: 'bank-transfer', // bank transfer, cash, cc, 
	},

	// TODO: Add start_at
	start_at: {
		type: Date,
		required: false,
		default: null
	},	

	expired_at: {
		type: Date,
		required: false,
		default: null
	},	

    
}, { timestamps: true, "strict": true  });

const Subscription = module.exports = mongoose.model('Subscription', schema);

