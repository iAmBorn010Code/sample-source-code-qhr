const mongoose = require('mongoose');
const validate = require('mongoose-validator')

var salaryTypes = [
	'monthly-fixed',
	'bi-monthly-fixed',
	'weekly-fixed',
	'monthly',
	'bi-monthly',
	'weekly'
];

// Schema
var schema = mongoose.Schema({
	
	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
		validate: {
			validator: function(v) {
				return mongoose.Types.ObjectId.isValid(v)
			},
			message: 'Invalid ID.'
		}
	},
	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},
	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},
	employee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    
    is_absent: { type: Number, default: 0 },
    is_onleave: { type: Number, default: 0 },
    is_holiday: { type: Boolean, default: false },

	leave_type: { type: String, default: null },    

    for_date: {type:Date, default:Date.now},
    with_schedule: { type: Boolean, default: true },
    with_break: { type: Boolean, default: false },
    with_ot: { type: Boolean, default: false },
    with_night_differential: { type: Boolean, default: false },

    time_in: {type:Date, default:null},
	time_in_unix: { type: Number, default: 0 },
	time_in_status: { type: String, default: null },
	time_in_diff: { type: Number, default: 0 },
	time_in_ref: {type:Date, default:null},
	time_in_ref_unix: { type: Number, default: 0 },
	is_late: { type: Number, default: 0 },

    time_out: {type:Date, default:null},
	time_out_unix: { type: Number, default: 0 },
	time_out_status: { type: String, default: null  },
	time_out_diff: { type: Number, default: 0 },
	time_out_ref: {type:Date, default:null},
	time_out_ref_unix: { type: Number, default: 0 },
	is_undertime: { type: Number, default: 0 },

    time_in2: {type:Date, default:null},
	time_in2_unix: { type: Number, default: 0 },
	time_in2_status: { type: String, default: null },
	time_in2_diff: { type: Number, default: 0 },
	time_in2_ref: {type:Date, default:null},
	time_in2_ref_unix: { type: Number, default: 0 },
	is_late2: { type: Number, default: 0 },

	time_out2: {type:Date, default:null},
	time_out2_unix: { type: Number, default: 0 },
	time_out2_status: { type: String, default: null },
	time_out2_diff: { type: Number, default: 0 },
	time_out2_ref: {type:Date, default:null},
	time_out2_ref_unix: { type: Number, default: 0 },
	is_undertime2: { type: Number, default: 0 },

	ot_in: {type:Date, default:null},
	ot_in_unix: { type: Number, default: 0 },
	ot_in_status: { type: String, default: null },
	ot_in_diff: { type: Number, default: 0 },
	ot_in_ref: {type:Date, default:null},
	ot_in_ref_unix: { type: Number, default: 0 },
	ot_is_late: { type: Number, default: 0 },

	ot_out: {type:Date, default:null},
	ot_out_unix: { type: Number, default: 0 },
	ot_out_status: { type: String, default: null },
	ot_out_diff: { type: Number, default: 0 },
	ot_out_ref: {type:Date, default:null},
	ot_out_ref_unix: { type: Number, default: 0 },
	ot_is_undertime: { type: Number, default: 0 },


	ot_late: { type: Number, default: 0 },
	ot_undertime: { type: Number, default: 0 },
	total_late: { type: Number, default: 0 },
	total_undertime: { type: Number, default: 0 },

	nd_in_ref: {type:Date, default:null},
	nd_out_ref: {type:Date, default:null},
	nd_in_ref_unix: {type: Number, default:0},
	nd_out_ref_unix: {type: Number, default:0},
	nd_late: { type: Number, default: 0 },
	nd_undertime: { type: Number, default: 0 },
	nd_total_time: { type: Number, default: 0 },
	nd_rate: { type: Number, default: 0 },

	work_rendered: { type: Number, default: 0 },
	work_to_render: { type: Number, default: 0 },
	ot_work_rendered: { type: Number, default: 0 },

	salary_type: {
    	type: String,
    	required: true,
    	validate: {
			validator: function(v) {
				return ( salaryTypes.indexOf(v) < 0 ) ? false : true;
			},
			message: 'Invalid salary type.'
		}
    },

    daily_rate: { type: Number, default: 0.00 },
    ot_rate: { type: Number, default: 0.00 },

	reason_for_update: {
        type: String,
		default: null,
		required: false,
        validate: [
			validate({
				validator: 'isLength',
				arguments: [5, 100],
				message: 'Reason should be between {ARGS[0]} and {ARGS[1]} characters',
				passIfEmpty: true,
			  	allowNull: true
			}),
		]
    },
    status: {
    	type: String,
    	required: false,
    	passIfEmpty: true,
    	default: null,
    	validate: [
			validate({
				validator: 'isLength',
				arguments: [4, 8],
				message: 'Status should be between {ARGS[0]} and {ARGS[1]} characters',
				passIfEmpty: true,
			  	allowNull: true
			}),
		]
    },

	designation: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Designation',
        required: false,
        passIfEmpty: true,
        allowNull: true
    },
    // designation: {
    // 	type: String,
    // 	required: false,
    // 	passIfEmpty: true,
    // 	default: null,
    // }
	
}, { timestamps: true, "strict": true  });

const DailyTimeRecord = module.exports = mongoose.model('DailyTimeRecord', schema);

