const mongoose = require('mongoose');
const validate = require('mongoose-validator')

var chargeTypes = [
	'subscription',
	'refund',
];

var planTypes = [
	'A',
	'B',
	'C',
	'D'
];

// Schema
var schema = mongoose.Schema({
	
	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
	},

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
	},

	stripe_charge_id: {
		type: String,
		default: null,
		required: true,
	},

	charge_type: {
		type: String,
		required: true,
		validate: {
			validator: function(v) {
				return ( chargeTypes.indexOf(v) < 0 ) ? false : true;
			},
			message: 'Invalid charge type.'
		}
	},

	plan_type: {
		required: true,
		type: String,
		validate: {
			validator: function(v) {
				return ( planTypes.indexOf(v) < 0 ) ? false : true;
			},
			message: 'Invalid plan type.'
		}
	},

	paid_amount: {
		type: Number,
		required: true,
		validate: {
			validator: function(v) {
				return v > 0;
			},
 	 		message: 'Invalid amount. Amount cannot be 0 or less.'
		} 
	},

	received_amount: {
		type: Number,
		required: true,
		validate: {
			validator: function(v) {
				return v > 0;
			},
 	 		message: 'Invalid amount. Amount cannot be 0 or less.'
		} 
	}
	
}, { timestamps: true, "strict": true  });

const Payment = module.exports = mongoose.model('Payment', schema);

