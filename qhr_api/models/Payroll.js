const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];

var salaryTypes = [
	'monthly-fixed', 'bi-monthly-fixed', 'weekly-fixed',
	'monthly', 'bi-monthly', 'weekly'
];
 

// Schema
var schema = mongoose.Schema({

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	payroll_group_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: false
	},
	
	name: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: 'Payroll Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
		]
	},
	
	pay_period_from: { 
		required: true,
		type: Date
    },
    
    pay_period_to: { 
		required: true,
		type: Date
	},

	status: {
		type: String,
		default: 'pending'
	},

	total_incomes: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Gross Income cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Gross Income cannot be less than 0.'
			}),
		]
    },

     total_deductions: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Total Deductions cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Total Deductions cannot be less than 0.'
			}),
		]
    },

    total_taxes: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Total Deductions cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Total Deductions cannot be less than 0.'
			}),
		]
    },

    total_contributions: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Total Deductions cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Total Deductions cannot be less than 0.'
			}),
		]
    },

    total_netpay: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Withholding tax cannot be less than 0.'
			}),
		]
    },	
    salary_type: {
    	type: String,
    	required: true,
    	default: 'bi-monthly-fixed',
    	validate: {
			validator: function(v) {
				return ( salaryTypes.indexOf(v) < 0 ) ? false : true;
			},
			message: 'Invalid type.'
		}
    }
}, { timestamps: true, "strict": true  });

 
// schema.plugin( mongoosePaginate );

const Payroll = module.exports = mongoose.model('Payroll', schema);