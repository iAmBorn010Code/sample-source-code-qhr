const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];
 

// Schema
var schema = mongoose.Schema({
	
	field1: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 50],
			  message: ' Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: ' Name should contain alpha-numeric characters and spaces only',
			}),
		]
	},
	
	field2: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 50],
			  message: 'field2 should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: 'field2 should contain alpha-numeric characters and spaces only',
			}),
		]
	},

	field3: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 50],
			  message: 'field3 should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: 'field3 should contain alpha-numeric characters and spaces only',
			}),
		]
	},

  field4: {
		type: String,
		default: null,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: 'field4 should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
		]
	},
		
  company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},
    
	
}, { timestamps: true, "strict": true  });

 
// schema.plugin( mongoosePaginate );

const Setting = module.exports = mongoose.model('Setting', schema);

