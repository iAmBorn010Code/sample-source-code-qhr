const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const validate = require('mongoose-validator')

var PayrollDetailAdditionalDeduction
	= require('../models/childs/PayrollDetailAdditionalDeduction');
var PayrollDetailAdditionalIncome
	= require('../models/childs/PayrollDetailAdditionalIncome');
var PayrollDetailContribution
	= require('../models/childs/PayrollDetailContribution');



// Validators
var nameValidator = [
  
];

var salaryType = [
	'monthly-fixed',
	'bi-monthly-fixed',
	'weekly-fixed',
	'monthly',
	'bi-monthly',
	'weekly'
];
 

// Schema
var schema = mongoose.Schema({
	
	payroll: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Payroll',
        required: true
    },

	employee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

	branch: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Branch',
        required: true
    },    

    company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},


	salary_type: {
		type: String,
		default: null,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return ( salaryType.indexOf(v) < 0 ) ? false : true;
				},
				message: 'Invalid salary type'
			})
		]
	},

	rate_min: {type: Number, default: 0.00, required: true },

	with_taxes: {
		type: Boolean,
		default: true,
		required: true
	},

	with_benefits: {
		type: Boolean,
		default: true,
		required: true
	},

	with_night_diff: {
		type: Boolean,
		default: true,
		required: true
	},

	deduct_absences: {
		type: Boolean,
		default: true,
		required: true
	},

	deduct_lates: {
		type: Boolean,
		default: false,
		required: true
	},

	deduct_undertime: {
		type: Boolean,
		default: true,
		required: true
	},

	total_basic_pay: { type: Number, default: 0.00, required: true },
	total_incomes: { type: Number, default: 0.00, required: true },
	total_deductions: { type: Number, default: 0.00, required: true },

    total_netpay: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Net Pay cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v > 0
				},
				message: 'Invalid value. Net Pay cannot be less than 0.'
			}),
		]
    },

    addtl_incomes: [PayrollDetailAdditionalIncome.schema],
	total_addtl_incomes: { type: Number, default: 0.00, required: true },

	addtl_deductions: [PayrollDetailAdditionalDeduction.schema],
	total_addtl_deductions: {type: Number, default: 0.00, required: true },

	contributions: [PayrollDetailContribution.schema],
	total_contributions: { type: Number, default: 0.00, required: true },

	total_contributions_ee: { type: Number, default: 0.00, required: true },
	total_contributions_er: { type: Number, default: 0.00, required: true },
	total_contributions_ec: { type: Number, default: 0.00, required: true },


	total_work_to_render_min: {type: Number, default: 0.00, required: true },
	total_work_rendered_min: {type: Number, default: 0.00, required: true },

	total_work_rendered: {type: Number, default: 0, required: true },
	total_work_to_render: {type: Number, default: 0, required: true },

	total_unwork: {type: Number, default: 0.00 },
	total_unwork_min: {type: Number, default: 0 },

	total_lates: {type: Number, default: 0.00, required: true },
	total_lates_min: {type: Number, default: 0.00, required: true },


	total_undertime: {type: Number, default: 0.00, required: true },
	total_undertime_min: {type: Number, default: 0.00, required: true },

	total_tardiness: {type: Number, default: 0.00, required: true },
	total_tardiness_min: {type: Number, default: 0.00, required: true },
	
	total_absences: {type: Number, default: 0, required: true },
	total_present: {type: Number, default: 0, required: true },

	total_withholding_tax: {type: Number, default: 0, required: true },

	total_cola: {type: Number, default: 0 },
	total_night_diff: {type: Number, default: 0 },

	daily: [mongoose.Schema.Types.Mixed],

}, { timestamps: true, "strict": true  });

 
// schema.plugin( mongoosePaginate );

const Payroll_Detail = module.exports = mongoose.model('Payroll_Detail', schema);