const mongoose = require('mongoose');
const validate = require('mongoose-validator')


// Schema
var schema = mongoose.Schema({	

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
	},
	
	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},
	
	name: { 
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 30],
			  message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
		],
	},	

	date: { 
		required: true,
		type: Date
	},

	type: {
		required: true,
		type: String,
		passIfEmpty: false,
		allowNull: false
	},

	description: {
		type: String,
		default: null,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: 'Description should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
		],
	},

	
}, { timestamps: true, "strict": true  });

 
const Holiday = module.exports = mongoose.model('Holiday', schema);

