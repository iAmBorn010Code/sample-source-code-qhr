const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];
 
var paymentType = [
	'weekly',
	'monthly',
];

// Schema
var schema = mongoose.Schema({
	
	employee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company'
    },

    branch: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Branch'
    },

	amount: {
		type: Number,
		default: 0.00,
		required: true,
    },

    balance: {
		type: Number,
		default: 0.00,
		required: true,
    },

    payment_type: {
		type: String,
		default: null,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return ( paymentType.indexOf(v) < 0 ) ? false : true;
				},
				message: 'Invalid payment type.'
			})
			
		]
	},

	period: {
		type: Number,
		default: 0,
    },

    percentage: {
		type: Number,
		default: 0,
		required: false
    },

	with_interest: {
    	type: Boolean,
    	default: false,
    	required: true
    },

    interest: {
		type: Number,
		default: 0.00,
		required: false,
    },

    interest_type: {
		type: String,
		default: null,
		required: false,
    },

    status: {
		type: String,
		default: 'pending'
	},

	start_date: { 
		required: true,
		type: Date
	},

	payment_rcv: { 
		required: false,
		type: Date
	},
	
}, { timestamps: true, "strict": true  });

 
// schema.plugin( mongoosePaginate );

const Loan = module.exports = mongoose.model('Loan', schema);