const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];

var companySize = [
	'A','B','C','D','E','F'
];
 

// Schema
var schema = mongoose.Schema({
	
	full_name: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: ' Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: ' Name should contain alpha-numeric characters and spaces only',
			}),
		]
	},

	email: {
		required: true,
		type: String,
		default: null,
		trim: true,
		lowercase: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: 'Email is too few characters.',
			  passIfEmpty: true,
			  allowNull: true
			}),
			validate({
			  validator: 'isEmail',
			  message: 'Please specify a valid email.',
			  passIfEmpty: false,
			  allowNull: false
			}),
		]
	},

	company_name: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [3, 50],
			  message: 'Company Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\.\-\ \s]+$/i,
			  message: 'Company Name should contain alpha-numeric characters and spaces only',
			}),
		]
	},

  	contact_no: {
		type: String,
		default: null,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: 'Contact Number should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
			validate({
				validator: 'matches',
				arguments: /^[\+\d\-]+$/i,
				message: 'Contact Number should contain dashes, plus sign, and digits only.',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},

	company_size: {
		type: String,
		default: null,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return ( companySize.indexOf(v) < 0 ) ? false : true;
				},
				message: 'Invalid company size.'
			})
			
		]
	},
    
	
}, { timestamps: true, "strict": true  });

 
// schema.plugin( mongoosePaginate );

const RequestDemo = module.exports = mongoose.model('Request_Demo', schema);

