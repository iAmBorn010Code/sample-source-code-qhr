const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];

var loginTypes = [ 'ip', 'device' ];

// Schema
var schema = mongoose.Schema({

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	name: { type: String, required: true },
	value: { type: String, required: true },

	type: {
		required: true,
		type: String,
		validate: {
			validator: function(v) {
				return ( loginTypes.indexOf(v) < 0 ) ? false : true;
			},
			message: 'Invalid login restriction type.'
		}
	}
	
}, { timestamps: true, "strict": true  });

 
const LoginRestriction = module.exports = mongoose.model('login_restrictions', schema);

