const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];
 

// Schema
var schema = mongoose.Schema({


	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,	
	},
		
	ref_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,	
	},

	type: {
		type: String,
		default: null,
		required: true,
		passIfEmpty: false,
	},

	description: {
		type: String,
		default: null,
		required: true,
		passIfEmpty: false,
	},
	
	read: {
		type: Boolean,
		default: 0,
		required: true,
		passIfEmpty: false,
	}
    
}, { timestamps: true, "strict": true  });

const Notification = module.exports = mongoose.model('Notification', schema);

