const mongoose = require('mongoose');
const validate = require('mongoose-validator');

var config = require('../config/config');

var InvoiceItem
	= require('../models/childs/InvoiceItem');

// Validators
var nameValidator = [
  
];
 

// Schema
var schema = mongoose.Schema({
	
	invoice_uid: {
		required: true,
		type: String,
		unique: true
	},

	payment_method: {
		required: true,
		type: String,
		default: 'bank-transfer', // bank transfer, cash, cc, 
	},	

	payment_date: {
		type: Date,
		required: true,
	},

	items: [InvoiceItem.schema],

    total_amount: {
		type: Number,
		default: 0.00,
    },

    company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
	},

	payment_trans_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},
    
}, { timestamps: true, "strict": true  });

const Invoice = module.exports = mongoose.model('Invoice', schema);

