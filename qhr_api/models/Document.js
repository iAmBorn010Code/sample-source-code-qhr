const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Schema
var schema = mongoose.Schema({
	
	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
		validate: {
			validator: function(v) {
				return mongoose.Types.ObjectId.isValid(v)
			},
			message: 'Invalid ID.'
		}
	},

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
	},

	name: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 30],
			  message: 'Document Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: 'Document Name should contain alpha-numeric characters and spaces only',
			}),
		]
	},


	filename: {
		type: String,
		required: true,		
	},
	
}, { timestamps: true, "strict": true  });

const Document = module.exports = mongoose.model('Document', schema);

