const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];

// Schema
var schema = mongoose.Schema({

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},
	
	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		default: null
	},

	employee: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'User'
	},

	time_in: {type:Date, required: true, default:null},
	time_out: {type:Date, required: true, default:null},

	time_in_unix: { type: Number, default: 0 },
	time_out_unix: { type: Number, default: 0 },

	approved_by: { type: String, default: null },
	for_date: { type:Date, default: null },

	with_ot_rate: { type: Boolean, default: false },
	ot_rate: { type:Number, default: 0 },
	
	status: {
		type: String,
		default: 'pending'
	}

	
}, { timestamps: true, "strict": true  });

 
const Overtime = module.exports = mongoose.model('Overtime', schema);

