const mongoose = require('mongoose');
const validate = require('mongoose-validator');

var config = require('../config/config');

// Validators
var nameValidator = [
  
];
 

// Schema
var schema = mongoose.Schema({

	transaction: {
		required: true,
		type: String,
		default: 'subscription',
	},

    trans_type: {
		required: true,
		type: String,
		default: 'credit', //debit or credit
	},

	payment_method: {
		required: true,
		type: String,
		default: 'bank-transfer', // bank transfer, cash, cc, 
	},	

	payment_date: {
		type: Date,
		required: true,
	},

    amount: {
		type: Number,
		default: 0.00,
    },

    company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
	},

	ref_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},

	invoice_uid: {
		required: true,
		type: String,
		unique: true
	},
    
}, { timestamps: true, "strict": true  });

const PaymentTransaction = module.exports = mongoose.model('Payment_Transaction', schema);

