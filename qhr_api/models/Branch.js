const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Schema
var schema = mongoose.Schema({
	
	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},

	name: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 20],
			  message: 'Branch Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: 'Branch Name should contain alpha-numeric characters and spaces only',
			}),
		]
	},

	status: {
		type: String,
		default: 'active'
	},

	logo: {
		type: String,
		default: null,
		passIfEmpty: true,
		allowNull: true,
		
	},
	address: {
		type: String,
		required: false,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 200],
			  message: 'Address should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
		]
	},
	
}, { timestamps: true, "strict": true  });

const Branch = module.exports = mongoose.model('Branch', schema);

