const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];
 

// Schema
var schema = mongoose.Schema({

	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
		validate: {
			validator: function(v) {
				return mongoose.Types.ObjectId.isValid(v)
			},
			message: 'Invalid ID.'
		}
	},

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},
	
	employee: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
    
    ref_id: {
		type: String,
		default: null,
		required: true,
		passIfEmpty: false,
    },

    type: {
		type: String,
		default: null,
		required: true,
		passIfEmpty: false,
    },

    company_id: {
		type: String,
		default: null,
		required: true,
		passIfEmpty: false,
    },

    action: {
		type: String,
		default: null,
		required: true,
		passIfEmpty: false,
    },
    
}, { timestamps: true, "strict": true  });

const SystemLog = module.exports = mongoose.model('SystemLog', schema);

