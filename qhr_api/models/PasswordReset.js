const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Schema
var schema = mongoose.Schema({
	
	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
	},

	hash: {
		type: String,
		default: null,
		required: true
	},

	expire_at: {
		type: Date,
		required: true,
	},
	
}, { timestamps: true, "strict": true  });

const Password_Reset = module.exports = mongoose.model('Password_Reset', schema);

