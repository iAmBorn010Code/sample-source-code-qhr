const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Schema
var schema = mongoose.Schema({

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},
	
	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
	},

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
	},

	quote: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [10, 100],
			  message: 'The quote should be between {ARGS[0]} and {ARGS[1]} characters.',
			}),
		]
	},


}, { timestamps: true, "strict": true  });

const Quote = module.exports = mongoose.model('Quotes', schema);

