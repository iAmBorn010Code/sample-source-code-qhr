const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Schema
var schema = mongoose.Schema({

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},
	
	department: {
		required: true,
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Department'
	},

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
	},

	rating: {
		type: Number,
		default: null,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v > 0 && v < 11
				},
				message: 'Allowed rating is from 1 - 10 only.'
			})
		]
	},

	comment: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 255],
			  message: 'Comment should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
		]
	},
	
}, { timestamps: true, "strict": true  });

const Freedom_Board = module.exports = mongoose.model('Freedom_Board', schema);

