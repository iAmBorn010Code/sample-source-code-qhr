const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];

// Schema
var schema = mongoose.Schema({

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	restriction: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'login_restrictions'
	},

	employee: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'User'
	},
	
}, { timestamps: true, "strict": true  });

 
const UserLoginRestriction = module.exports = mongoose.model('user_login_restrictions', schema);

