const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Schema
var schema = mongoose.Schema({

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},
	
	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		default: null
	},

	employee: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'User'
	},

	mon_enabled: { type: Boolean, default: false },
	tue_enabled: { type: Boolean, default: false },
	wed_enabled: { type: Boolean, default: false },
	thu_enabled: { type: Boolean, default: false },
	fri_enabled: { type: Boolean, default: false },
	sat_enabled: { type: Boolean, default: false },
	sun_enabled: { type: Boolean, default: false },

	rate: { type: Number, required: false, default: 0.00 },
	enabled: { type: Boolean, default: true },
	with_break: { type: Boolean, required: false, default: false },

	time_in: { type: String, required: false, default: null },
	time_out: { type: String, required: false, default: null },
	time_in2: { type: String, default: null },
	time_out2: { type: String, default: null },

	nd_start: { type: String, required: false, default: null },
	nd_end: { type: String, required: false, default: null },

	
}, { timestamps: true, "strict": true  });

 
const WorkSchedule = module.exports = mongoose.model('work_schedule', schema);

