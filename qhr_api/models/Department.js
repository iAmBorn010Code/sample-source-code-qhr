const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Schema
var schema = mongoose.Schema({
	
	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},
	
	name: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 50],
			  message: 'Department Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: 'Department Name should contain alpha-numeric characters and spaces only',
			}),
		]
	},

	status: {
		type: String,
		default: null,
		required: true
	},

	description: {
		type: String,
		default: null,
		required: false
	},

	rating: {
		type: Number,
		default: 0,
		required: false
	},

	no_of_raters: {
		type: Number,
		default: 0,
		required: false
	},

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},

}, { timestamps: true, "strict": true  });

const Department = module.exports = mongoose.model('Department', schema);

