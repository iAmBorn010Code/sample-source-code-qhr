const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];
 
var paymentType = [
	'weekly',
	'monthly',
];

// Schema
var schema = mongoose.Schema({
	
	loan: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Loan'
    },

    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company'
    },

    branch: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Branch'
    },

	amount: {
		type: Number,
		default: 0.00,
		required: true,
    },

	payment_date: { 
		required: false,
		type: Date
	},
	
}, { timestamps: true, "strict": true  });

 
// schema.plugin( mongoosePaginate );

const LoanPayment = module.exports = mongoose.model('Loan_Payment', schema);