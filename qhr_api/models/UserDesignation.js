const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];

var days = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' ];

// Schema
var schema = mongoose.Schema({

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},

	employee: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'User'
	},

	designation: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'Designation'
	},

	day: {
		type: String,
		required: true,
		validate: {
			validator: function(v) {
				return ( days.indexOf(v) < 0 ) ? false : true;
			},
			message: 'Invalid day.'
		}		
	},

	by: { type: String, default: null },
	by_id: { type: mongoose.Schema.Types.ObjectId, default: null },
	status: { type: String, default: 'pending' }


}, { timestamps: true, "strict": true  });

 
const UserDesignation = module.exports = mongoose.model('user_designation', schema);

