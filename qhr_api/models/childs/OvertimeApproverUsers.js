const mongoose = require('mongoose');
const validate = require('mongoose-validator');

// Schema
var schema = mongoose.Schema({
	
	employee: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'User'
	},

}, { timestamps: true, "strict": true  });

const OvertimeApproverUsers = module.exports = mongoose.model('OvertimeApproverUsers.js', schema);

