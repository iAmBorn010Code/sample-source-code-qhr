const mongoose = require('mongoose');
const validate = require('mongoose-validator');
// const uniqueValidator = require('mongoose-unique-validator');
// Schema
var schema = mongoose.Schema({
	
	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		validate: {
			validator: function(v) {
				return mongoose.Types.ObjectId.isValid(v)
			},
			message: 'Invalid User ID.'
		}
	},

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		validate: {
			validator: function(v) {
				return mongoose.Types.ObjectId.isValid(v)
			},
			message: 'Invalid Company ID.'
		}
	},

	name: {
		type: String,
		required: true,
		// unique: true,
		// uniqueCaseInsensitive: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 20],
			  message: 'Contribution Type should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: 'Contribution Type should contain alpha-numeric characters and spaces only',
			}),
		]
	},


	ee_amt: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 100000
				},
				message: 'Invalid value. Cannot be more than a hundred thousand.'
			}),
			validate( {
				validator: function(v) {
					return v > 0
				},
				message: 'Invalid value. Cannot be less than 0.'
			}),
		]
    },

	er_amt: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 100000
				},
				message: 'Invalid value. Cannot be more than a hundred thousand.'
			}),
			validate( {
				validator: function(v) {
					return v > 0
				},
				message: 'Invalid value. Cannot be less than 0.'
			}),
		]
    },

    ec_amt: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 100000
				},
				message: 'Invalid value. Cannot be more than a hundred thousand.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Cannot be less than 0.'
			}),
		]
    },

	ee_status: {
		type: String,
		default: 'pending',
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return  v === 'pending' || v === 'paid' || v === 'na';
				},
				message: 'Employee Contribution Status Invalid value.'
			}),
		]
    },

	er_status: {
		type: String,
		default: 'pending',
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return  v === 'pending' || v === 'paid' || v === 'na';
				},
				message: 'Employer Contribution Status Invalid value.'
			}),
		]
    },

    ec_status: {
		type: String,
		default: 'pending',
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return  v === 'pending' || v === 'paid' || v === 'na';
				},
				message: 'Employer Contribution Status Invalid value.'
			}),
		]
    },

    total: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v > 0
				},
				message: 'Invalid value. Cannot be less than 0.'
			}),
		]
    },

    other_specify: {
    	type: String,
    	default: null,
    	required: false,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 20],
			  message: '\'Other Specify\' field should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: 'Other Specify should contain alpha-numeric characters and spaces only',
			  passIfEmpty: true,
			  allowNull: true
			}),
		],
		passIfEmpty: true,
		allowNull: true
    }

}, { timestamps: true, "strict": true  });

// schema.pre('save', function (doc) {
 	
// });

// schema.plugin(uniqueValidator, {message: 'Contribution Name must be unique.'});

const PayrollDetailAdditionalDeduction = module.exports = mongoose.model('PayrollDetailContribution.js', schema);

