const mongoose = require('mongoose');
const validate = require('mongoose-validator');

var leaveTypes = [
	'SL',
	'VL',
	'EL',
	'PL',
	'ML'
];

// Schema
var schema = mongoose.Schema({
	
	type: {
		required: true,
		type: String,
		validate: {
			validator: function(v) {
				return ( leaveTypes.indexOf(v) < 0 ) ? false : true;
			},
			message: 'Invalid leave type.'
		}
	},

	credit: {
		type: Number,
		default: 0,
		required: true,
	},

	used: {
		type: Number,
		default: 0
	},

	pending: {
		type: Number,
		default: 0
	}

}, { timestamps: true, "strict": true  });

const LeaveCredit = module.exports = mongoose.model('LeaveCredit.js', schema);

