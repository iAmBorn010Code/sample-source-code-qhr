const mongoose = require('mongoose');
const validate = require('mongoose-validator');

// Schema
var schema = mongoose.Schema({
	
	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		validate: {
			validator: function(v) {
				return mongoose.Types.ObjectId.isValid(v)
			},
			message: 'Invalid ID.'
		}
	},

	desc: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 30],
			  message: 'Earning description should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
		]
	},


	price: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 100000
				},
				message: 'Invalid value. Cannot be more than a hundred thousand.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Invalid value. Cannot be less than 0.'
			}),
		]
    },

    total_amount: {
		type: Number,
		default: 0.00,
    },

    tax: {type: Number, default: 0.00, required: false },
}, { timestamps: true, "strict": true  });

const InvoiceItem = module.exports = mongoose.model('InvoiceItem', schema);

