const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];

// Schema
var schema = mongoose.Schema({

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},
	
	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},

	name: {
		type: String,
		default: null,
		required: true,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [2, 50],
				message: 'Team Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
				validator: 'matches',
				arguments: /^[a-z\d\-\ \s]+$/i,
				message: 'Team Name should contain alpha-numeric characters, spaces, and dashes only.',
			}),
		]
	},

	status: {
		type: String,
		default: 'active'
	},

	cnt: {
		type: Number,
		default: 0
	}
	
}, { timestamps: true, "strict": true  });

 
const Team = module.exports = mongoose.model('Team', schema);

