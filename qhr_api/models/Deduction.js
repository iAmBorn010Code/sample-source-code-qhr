const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Schema
var schema = mongoose.Schema({
	
	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
		validate: {
			validator: function(v) {
				return mongoose.Types.ObjectId.isValid(v)
			},
			message: 'Invalid ID.'
		}
	},

	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
	},

	description: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 30],
			  message: 'Deduction Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: 'Deduction Name should contain alpha-numeric characters and spaces only',
			}),
		]
	},


	amount: {
		type: Number,
		default: 0.00,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Invalid value. Cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v > 0
				},
				message: 'Invalid value. Cannot be less than 0.'
			}),

		]
	},

	type: {
		type: String,
		default: null,
		required: true
	}
	
}, { timestamps: true, "strict": true  });

const Deduction = module.exports = mongoose.model('Deduction', schema);

