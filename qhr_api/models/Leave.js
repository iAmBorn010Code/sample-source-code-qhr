const mongoose = require('mongoose');
const validate = require('mongoose-validator')

// Validators
var nameValidator = [
  
];

var leaveTypes = [
	'SL',
	'VL',
	'EL',
	'PL',
	'ML'
];

// Schema
var schema = mongoose.Schema({

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true
	},
	
	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},

	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null,
		required: true,
		validate: {
			validator: function(v) {
				return mongoose.Types.ObjectId.isValid(v)
			},
			message: 'Invalid ID.'
		}
	},

	employee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

	date: { 
		required: true,
		type: Date
	},

	reason: {
		type: String,
		default: null,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 200],
			  message: 'Reason should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
		],
	},

	type: {
		required: true,
		type: String,
		validate: {
			validator: function(v) {
				return ( leaveTypes.indexOf(v) < 0 ) ? false : true;
			},
			message: 'Invalid leave type.'
		}
	},

	status: {
		type: String,
		default: 'pending'
	},
	
}, { timestamps: true, "strict": true  });

 
const Leave = module.exports = mongoose.model('Leave', schema);

