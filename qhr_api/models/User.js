const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const validate = require('mongoose-validator')
const bcrypt = require('bcryptjs');
const config = require('../config/config');
const uniqueString = require('unique-string');
const moment = require('moment-timezone');
const sanitizeHtml = require('sanitize-html');

var LeaveCredit
	= require('../models/childs/LeaveCredit');


var salaryTypes = [
	'monthly-fixed',
	'bi-monthly-fixed',
	'weekly-fixed',
	'monthly',
	'bi-monthly',
	'weekly'
];



// Validators
var nameValidator = [
  
];
 

// Schema
var schema = mongoose.Schema({
	
	hash: {
		// select: false,
		type: String,
		default: null,
		passIfEmpty: true,
		allowNull: true
	},

	salary_type: {
		type: String,
		default: 'monthly-fixed',
		passIfEmpty: false,
		validate: [
			validate( {
				validator: function(v) {
					return ( salaryTypes.indexOf(v) < 0 ) ? false : true;
				},
				message: 'Invalid salary type.'
			})
		]
	},

	basic_salary: {
		type: Number,
		passIfEmpty: true,	
		validate: [
			validate( {
				validator: function(v) {
					return v < 1000000
				},
				message: 'Basic Salary: Cannot be more than a million.'
			}),
			validate( {
				validator: function(v) {
					return v >= 0
				},
				message: 'Basic Salary: Cannot be less than 0.'
			}),
		]	
	},

	// UserSchema.path('username').validate(function(value, done) {
	//     mongoose.model('User', UserSchema).count({ username: value }, function(err, count) {
	//         if (err) {
	//             return done(err);
	//         } 
	//         // If `count` is greater than zero, "invalidate"
	//         done(!count);
	//     });
	// }, 'Sorry but this username is already taken');

	username: {
		type: String,
		required: false,
		unique: true, // TODO
		validate: [
			validate({
				validator: 'isLength',
				arguments: [6, 15],
				message: 'Username should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
				validator: 'matches',
				arguments: /^[a-z\d\_]+$/i,
				message: 'Username should contain alpha-numeric characters and underscores only.',
			}),
			// validate({
			// 	validator: function(v) {

			// 		mongoose.model('User', schema).count({ username: v }, function(err, count) {
						
			// 			if (err) {
			// 				return false;
			// 			} 
			// 			// If `count` is greater than zero, "invalidate"
			// 			return !count;
			// 		});

			// 		// return false;
			// 	},
			// 	message: 'Username already exist. Please try another one.',
			// })
		]
	},	

	first_name: {
		type: String,
		required: true,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [2, 50],
				message: 'First Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
				validator: 'matches',
				arguments: /^[a-z\d\ \s]+$/i,
				message: 'First Name should contain alpha-numeric characters and spaces only',
			}),

		]
	},
	
	middle_name: {
		type: String,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [2, 50],
				message: 'Middle Name should be between {ARGS[0]} and {ARGS[1]} characters',
				passIfEmpty: true,
			  	allowNull: true
			}),
			validate({
				validator: 'matches',
				arguments: /^[a-z\d\ \s]+$/i,
				message: 'Middle Name should contain alpha-numeric characters and spaces only',
				passIfEmpty: true,
			  	allowNull: true
			}),
		]
	},

	last_name: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [2, 50],
			  message: 'Last Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \s]+$/i,
			  message: 'Last Name should contain alpha-numeric characters and spaces only',
			}),
		]
	},

	suffix: {
		type: String,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [1, 4],
			  message: 'Suffix should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\.\s]+$/i,
			  message: 'Suffix should contain alpha-numeric characters and spaces only',
			  passIfEmpty: true,
			  allowNull: true
			}),
		]
	},
	gender: {
		type: String,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v === 'male' || v === 'female'
				},
				message: 'Invalid gender.'
			})
		]
	},
	birthdate: {
		type: Date,
		default: null,
		passIfEmpty: true,
		validate: [
			validate({
				validator: function(v) {

					// Get birthdate
					var birthdate = moment(v).format('L'); 

					// At least 18 years old
					var less18Years = moment().subtract(18, 'years').format('L');
					var less150Years = moment().subtract(100, 'years').format('L');
					
					// check if minor
					var isAfter = moment(birthdate).isAfter(less18Years);
					var isBefore = moment(birthdate).isBefore(less150Years);

					return ( isAfter || isBefore ) ? false : true;
				},
				message: 'Birthdate: Invalid Date. \
					Make sure your age is not below 18 years \
					and not greater than 100 years.'
				,
			}),
		]
	},
	birth_month: {
		type: String,
		default: null
	},
	birth_day: {
		type: String,
		default: null
	},
	birth_year: {
		type: Number,
		default: null
	},
	email: {
		required: true,
		type: String,
		default: null,
		trim: true,
		lowercase: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: 'Employee email has too few characters.',
			  passIfEmpty: true,
			  allowNull: true
			}),
			validate({
			  validator: 'isEmail',
			  message: 'Please specify a valid employee email.',
			  passIfEmpty: false,
			  allowNull: false
			}),
		]
	},
	password: {
		type: String,
		default: null,
		select: false,
		trim: true,
	},
	contact_nos: {
		type: String,
		default: null,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: 'Phone Number should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
			validate({
				validator: 'matches',
				arguments: /^[\+\d\,/ ]+$/i,
				message: 'Phone Number should contain commas, plus sign, and digits only.',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},
	status: {
		type: String,
		default: 'active'
	},
	address: {
		type: String,
		required: false,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [10, 200],
			  message: 'Address should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
		]
	},
	job_title: {
		type: String,
		default: null,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [3, 50],
			  message: 'Job Title should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ \/\&\s]+$/i,
			  message: 'Job Title should contain alpha-numeric characters and spaces only',
			  passIfEmpty: true,
			  allowNull: true
			}),
		]
	},
	job_description: {
		type: String,
		default: null,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [20, 500],
			  message: 'Job Description should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
		],
	},
	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},
	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},
	department_id: {
		type: mongoose.Schema.Types.ObjectId,
		default: null
	},
	designation: {
		type: String,
		default: null,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [3, 30],
				message: 'Designation should be between {ARGS[0]} and {ARGS[1]} characters',
				passIfEmpty: true,
				allowNull: true
			}),
			validate({
				validator: 'matches',
				arguments: /^[a-z\d\ \s]+$/i,
				message: 'Designation should contain alpha-numeric characters and spaces only',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},
	employment_status: {
		type: String,
		default: 'Full-time',
		validate: [
			validate({
				validator: 'isLength',
				arguments: [3, 20],
				message: 'Job Title should be between {ARGS[0]} and {ARGS[1]} characters',
				passIfEmpty: true,
				allowNull: true
			}),
			validate({
				validator: 'matches',
				arguments: /^[a-z\d\- \s]+$/i,
				message: 'Job Title should contain alpha-numeric characters and spaces only',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},

	sss: {
		type: String,
		default: null,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [7, 15],
				message: 'SSS should be between {ARGS[0]} and {ARGS[1]} characters.',
				passIfEmpty: true,
				allowNull: true
			}),
			validate({
				validator: 'matches',
				arguments: /^[\d\-]+$/i,
				message: 'SSS should contain dashes and digits only.',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},

	philhealth: {
		type: String,
		default: null,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [7, 15],
				message: 'Philhealth should be between {ARGS[0]} and {ARGS[1]} characters.',
				passIfEmpty: true,
				allowNull: true
			}),
			validate({
				validator: 'matches',
				arguments: /^[\d\-]+$/i,
				message: 'Philhealth should contain dashes and digits only.',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},

	pagibig: {
		type: String,
		default: null,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [7, 15],
				message: 'PAG-IBIG should be between {ARGS[0]} and {ARGS[1]} characters.',
				passIfEmpty: true,
				allowNull: true
			}),
			validate({
				validator: 'matches',
				arguments: /^[\d\-]+$/i,
				message: 'PAG-IBIG should contain dashes and digits only.',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},

	tin: {
		type: String,
		default: null,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [7, 15],
				message: 'TIN should be between {ARGS[0]} and {ARGS[1]} characters.',
				passIfEmpty: true,
				allowNull: true
			}),
			validate({
				validator: 'matches',
				arguments: /^[\d\-]+$/i,
				message: 'TIN should contain dashes and digits only.',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},

	linkedin: {
		type: String,
		default: null,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [3, 100],
				message: 'Linkedin should be between {ARGS[0]} and {ARGS[1]} characters',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},

	facebook: {
		type: String,
		default: null,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [3, 100],
				message: 'Facebook should be between {ARGS[0]} and {ARGS[1]} characters',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},

	instagram: {
		type: String,
		default: null,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [3, 100],
				message: 'Instagram should be between {ARGS[0]} and {ARGS[1]} characters',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},

	twitter: {
		type: String,
		default: null,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [3, 100],
				message: 'Twitter should be between {ARGS[0]} and {ARGS[1]} characters',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},

	skype: {
		type: String,
		default: null,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [3, 100],
				message: 'Skype should be between {ARGS[0]} and {ARGS[1]} characters',
				passIfEmpty: true,
				allowNull: true
			}),
		]
	},


	food_allowance: {
		type: Number,
		default: 0,
		required: true,
		passIfEmpty: true,
	},

	travel_allowance: {
		type: Number,
		default: 0,
		required: true,
		passIfEmpty: true,
	},



	photo: {
		type: String,
		default: null,
		passIfEmpty: true,
		allowNull: true,
		
	},

	date_joined: {
		type: String,
		passIfEmpty: true,
		validate: [
			validate({
				validator: function(v) {

					let dateJoined = moment(v).format("YYYY-MM-DD");
					let today = moment().format("YYYY-MM-DD");

					return dateJoined < today;
				},
				message: 'Date Joined: Invalid Date',
			}),
		]
	},

	mood: {
		type: String,
		required: false,
		allowNull: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 255],
			  message: 'Comment should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
		]
	},

	allow_ot_late: {
		type: Boolean,
		default: false
	},

	leave_credits: { type: [LeaveCredit.schema], select:true },
	with_cola: { type: Boolean, default: false },
	cola_rate: { type: Number, default: 0.00 },
	qr_code: { type: String, default: null },
	emp_ID: { type: String, default: null }

}, { timestamps: true, "strict": true  });

// schema.pre('save', function (next) {
	
// 	// TODO: If no password, create a ramdom string for now.
// 	if (! this.password )
// 		this.password = uniqueString();
// 	else
// 		this.password = bcrypt.hashSync(this.password, config.secret );
	
// 	return next();
// })

schema.pre('findOneAndUpdate', function (next) {

	if ( this._update.birthdate ) {

		let birthdate = moment(this._update.birthdate).format('YYYY-MM-DD');

		this._update.birth_month = moment(birthdate).format('MM');
		this._update.birth_day = moment(birthdate).format('DD');
		this._update.birth_year = moment(birthdate).format('YYYY');
	}

	if ( this._update.job_description ) {
		this._update.job_description = sanitizeHtml(this._update.job_description);
	}

	return next();
})

schema.plugin(uniqueValidator, {message: '`{PATH}` already taken.'});

const User = module.exports = mongoose.model('User', schema);

