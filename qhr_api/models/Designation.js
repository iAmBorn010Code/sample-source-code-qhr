const mongoose = require('mongoose');
const validate = require('mongoose-validator');

// Validators
var nameValidator = [
  
];

// Schema
var schema = mongoose.Schema({

	branch_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},
	
	company_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
	},

	title: {
		type: String,
		required: true,
		validate: [
			validate({
				validator: 'isLength',
				arguments: [3, 30],
				message: 'Title should be between {ARGS[0]} and {ARGS[1]} characters',
			})
		]
	},

	with_time: { type: Boolean, default: false },
	with_break: { type: Boolean, default: false },

	time_in: { type:String, default:null },
	time_out: { type:String, default:null },
	time_in2: { type:String, default:null },
	time_out2: { type:String, default:null },

	rate: {
		type: Number,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v > 0;
				},
				message: 'Rate cannot be zero or less.'
			})
		]
	},

	ot_rate: {
		type: Number,
		required: true,
		validate: [
			validate( {
				validator: function(v) {
					return v > 0;
				},
				message: 'Overtime rate cannot be zero or less.'
			})
		]
	}

	
}, { timestamps: true, "strict": true  });
 
const Designation = module.exports = mongoose.model('Designation', schema);

