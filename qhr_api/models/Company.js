const mongoose = require('mongoose');
const validate = require('mongoose-validator');

var config = require('../config/config');

// Validators
var nameValidator = [
  
]; 


// Schema
var schema = mongoose.Schema({
	
	verification_hash: {
		type: String,
		default: null,
		passIfEmpty: true,
		allowNull: true
	},
	
	name: {
		type: String,
		required: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: 'Company Name should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
			validate({
			  validator: 'matches',
			  arguments: /^[a-z\d\ :/.-\s]+$/i,
			  message: 'Company Name should contain alpha-numeric characters, dash, period and spaces only',
			}),
		]
	},
	
	address: {
		type: String,
		required: false,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [10, 200],
			  message: 'Address should be between {ARGS[0]} and {ARGS[1]} characters',
			}),
		]
	},

	email: {
		type: String,
		default: null,
		lowercase: true,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: 'Company email is too few characters.',
			  passIfEmpty: true,
			  allowNull: true
			}),
			validate({
			  validator: 'isEmail',
			  message: 'Please specify a valid company email.',
			  passIfEmpty: true,
			  allowNull: true
			}),
		]
    },

    contact_nos: {
		type: String,
		default: null,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [5, 50],
			  message: 'Contact Nos. should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
		]
    },
    
    short_description: {
		type: String,
		default: null,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [10, 150],
			  message: 'Short Description should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
		],
    },
    
    long_description: {
		type: String,
		default: null,
		validate: [
			validate({
			  validator: 'isLength',
			  arguments: [10, 255],
			  message: 'Long Description should be between {ARGS[0]} and {ARGS[1]} characters',
			  passIfEmpty: true,
			  allowNull: true
			}),
		],
	},

	logo: {
		type: String,
		default: null,
		passIfEmpty: true,
		allowNull: true,
		
	},

	attd_updtd_at: {type:Date, default:Date.now},

	verified: { 
		type: Boolean, 
		default: false 
	},

	expired_at: {
		type: Date,
		required: false,
		allowNull: false
	},	

	trial_ends_at: {
		type: Date,
		default: Date.now,
		required: false,
		allowNull: false
	},

	ends_at: {
		type: Date,
		required: false,
		allowNull: false
	},	
	
	verification_exp_at: {
		type: Date,
		required: false,
		allowNull: false
	},	
	
}, { timestamps: true, "strict": true  });

const Company = module.exports = mongoose.model('Company', schema);

