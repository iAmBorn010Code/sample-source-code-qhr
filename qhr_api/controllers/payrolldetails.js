'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const PayrollDetailModel  = require('../models/PayrollDetail');
const SystemLogModel = require('../models/SystemLog');
const PayrollDetailContributionModel  = require('../models/childs/PayrollDetailContribution');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit;
	let payroll = req.query.payroll_id || '';
	let contributions = req.query.contributions || false ;
	let emp_id = req.query.emp_id || '';

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	var conditions = {
		company_id: req.user.company_id
	};

	if ( payroll ) {
		conditions.payroll = payroll;
	}

	if ( contributions ) {
		conditions.contributions = { $exists: true, $ne: [] };
	}

	if ( emp_id ) {
		conditions.employee = emp_id;
	}


	var query = PayrollDetailModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('employee')
		  	.exec('find', function (err, results) {
				if ( err ) {
					//console.log('err', err);
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});

});

// Get payroll details of an employee using payroll_id and user_id
router.get('/show', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	var conditions = {
		payroll: req.query.payroll_id,
		company_id: req.user.company_id,
		employee: req.query.user_id,
	};

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );
	PayrollDetailModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
	.select(fields);
});

router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );
	PayrollDetailModel.findOne({ _id: req.params.id }, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
	.select(fields);
});



// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Daily Time records starts here.
	var Payroll = require("./../libs/Payroll");

	let contributions = req.query.contributions || [];

	let params = {
		user: null,
		company_id: req.user.company_id,
		branch_id: req.user.branch_id,
		salary_type: req.body.salaryType,
		period: {
			from: req.body.from,
			to: req.body.to,
		},
		user_id: req.body.employeeId,
		payroll_id: req.body.payrollId,
		contributions: req.body.contributions || [],
		addtl_incomes: req.body.addtlIncomes || [],
		addtl_deductions: req.body.addtlDeductions || [],
		action: 'create',

		with_taxes: req.body.withTaxes ? req.body.withTaxes : false,
	 	with_benefits: req.body.withBenefits ? req.body.withBenefits : false,
	 	with_night_diff: req.body.withNightDiff ? req.body.withNightDiff : false,
	 	with_ot: req.body.withOvertime ? req.body.withOvertime : false,
	 	deduct_absences: req.body.deductAbsences ? req.body.deductAbsences : false,
	 	deduct_lates: req.body.deductLates ? req.body.deductLates : false,
	 	deduct_undertime: req.body.deductUndertime ? req.body.deductUndertime : false,
	}

	// Step 1: Find user from DB. 
	Payroll.getUser( {_id: req.body.employeeId} )

	// Step 2: If found, then do calculations.
	.then( function( user ){
		params.user = user;
		return Payroll.calculateTimeRecords( params );
	}) 

	.then( function( params ){
		return Payroll.createOrUpdatePayrollDetail( params );
	})

	.then( function( params ){
		Params.results_count = 1;
		Params.results = params;
		Params.msg = 'Successfully added to Payroll.';
		res.json( Params );
	})
	
	// Step 6: If it has, then blah blah blah.
	.catch(function(error){
		// Handle error
		//console.log( 'GOT an error', error );
		Params.error = true;
		Params.error_type = error.error_type || 'error';
		Params.msg = error.message || error;
		res.json( Params );
	});
});


// TODO: UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Daily Time records starts here.
	var Payroll = require("./../libs/Payroll");

	let params = {
		user: null,
		company_id: req.user.company_id,
		branch_id: req.user.branch_id,
		salary_type: req.body.salaryType,
		period: {
			from: req.body.from,
			to: req.body.to,
		},
		user_id: req.body.employeeId,
		payroll_id: req.body.payrollId,
		contributions: req.body.contributions || [],
		addtl_incomes: req.body.addtlIncomes || [],
		addtl_deductions: req.body.addtlDeductions || [],
		action: 'update',
		_id: req.body.payrollDetailId,

	 	with_taxes: req.body.withTaxes ? req.body.withTaxes : false,
	 	with_benefits: req.body.withBenefits ? req.body.withBenefits : false,
	 	with_night_diff: req.body.withNightDiff ? req.body.withNightDiff : false,
	 	with_ot: req.body.withOvertime ? req.body.withOvertime : false,
	 	deduct_absences: req.body.deductAbsences ? req.body.deductAbsences : false,
	 	deduct_lates: req.body.deductLates ? req.body.deductLates : false,
	 	deduct_undertime: req.body.deductUndertime ? req.body.deductUndertime : false,
	}

	// Step 1: Find user from DB. 
	Payroll.getUser( {_id: req.body.employeeId} )

	// Step 2: If found, then do calculations.
	.then( function( user ){
		params.user = user;
		return Payroll.calculateTimeRecords( params );
	}) 

	.then( function( params ){
		return Payroll.createOrUpdatePayrollDetail( params );
	})

	.then( function( params ){
		Params.results_count = 1;
		Params.results = params;
		Params.msg = 'Successfully Updated.';
		res.json( Params );
	})
	
	// Step 6: If it has, then blah blah blah.
	.catch(function(error){
		// Handle error
		//console.log( 'GOT an error', error );
		Params.error = true;
		Params.error_type = error.error_type || 'error';
		Params.msg = error.message || error;
		res.json( Params );
	});

});


// DELETE
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	PayrollDetailModel.findByIdAndRemove({_id: req.params.id}, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		let branch_id = req.query.branch_id || '';

		var SystemLog = new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id,
			ref_id: req.params.id,
			type: 'Payroll Details',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'deleted a payroll detail.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});


module.exports = router;