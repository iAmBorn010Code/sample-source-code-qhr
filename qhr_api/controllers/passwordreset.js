'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

const moment = require('moment-timezone');

// Models declarations
const UserModel  = require('../models/User');
const PasswordResetModel  = require('../models/PasswordReset');
const SystemLogModel  = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {
	Params = new ApiParams().params;
	next()
});

router.post('/reset/:hash', function (req, res) {

	// chech if hash exists
	PasswordResetModel.findOne({hash: req.params.hash}, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = 'Invalid hash.';
			res.json( Params );
			return;
		}

		// check if hash is not expired
		let currTime = moment().format("YYYY-MM-DD HH:mm:ss");
		let expire_at = moment(result.expire_at).format("YYYY-MM-DD HH:mm:ss");

		if ( currTime > expire_at ) {
			Params.error = true;
			Params.msg = 'This link is expired. Please request a new one.';
			res.json( Params );
			return;	
		}

		// get user && check if status is active
		let conditions = {
			_id: result.user_id,
			status: 'active'
		}

		UserModel.findOne(conditions, function(err, user) { 

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if (! user ) {
				Params.error = true ;
				Params.msg = 'User is not found or no longer active.';
				res.json( Params );
				return;	
			}

			// prepare data
			let new_pass = req.body.new_password;
			let confirm_pass = req.body.confirm_password;

			if ( !new_pass || !confirm_pass ) {
				Params.error = true ;
				Params.msg = 'Password cannot be empty.';
				res.json( Params );
				return;		
			}

			if ( new_pass != confirm_pass ) {
				Params.error = true ;
				Params.msg = 'Passwords do not match.';
				res.json( Params );
				return;			
			}

			if ( new_pass.length < 6 || new_pass.length > 30  ) {

				Params.error = true;
				Params.msg = 'Password should be between 6 and 30 characters';
				res.json( Params );
				return;
			}

			// Let's try to encrypt password.
			try {
				new_pass = bcrypt.hashSync(new_pass, config.secret );
			} catch(err) {
				//console.log( err );
				Params.error = true;
				Params.msg = 'Unable to change password at this time. Please try again later.';
				res.json( Params );
				return;
			}

			// update user password
			user.password = new_pass;
			user.save( function (error) {
	
				if ( error ) {
					//console.log('err', error);
					Params.form_errors = error;
					Params.error = true;
					Params.msg = 'Unable to change password at this time. Please try again later.';
					res.json( Params );
					return;
				}

				var SystemLog = new SystemLogModel({
					user_id: user._id, 
					employee: user._id, 
					ref_id: user._id,
					type: 'Users',
					company_id: user.company_id,
					branch_id: user.branch_id,
					action: 'requested for a new password.'
				}).save();

				var SesClient = require("../libs/SesClient");
				let link = config.APP_URL + 'login';
					
				// all fields here is required
				var data = {
					to: [user.email],
					subject: 'QloudHR Password Reset',
					clientName: 'Hi, ' + user.first_name + '!',
					messageBody: 'The password for your QloudHR Account ' + user.username + ' was recently changed.',
					linkhere: 'Click here to login: ' + link,
					messageEnd:'Thank you for using QloudHR.',
					from: 'no-reply@qloudhr.com'
				};
			    
			    SesClient.sendEmail( data )
				.then( function( results ){
					//console.log('successfully sent');
					Params.results_count = 1;
					Params.results = user;
					Params.msg = 'Password successfully updated.';
					res.json( Params );
				})
				.catch(function(error){
					//console.log( 'GOT an error', error );
					Params.error = true;
					Params.error_type = error.error_type || 'error';
					Params.msg = error.message || error;
					res.json( Params );
				});
		
			});

		});
		
	});

});



module.exports = router;
