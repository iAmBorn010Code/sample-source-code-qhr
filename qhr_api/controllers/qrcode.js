'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var Config = require('../config/config');
var QRCode = require('qrcode');

const UserModel  = require('../models/User');

var multer  = require('multer')
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');
const s3 = new aws.S3();

var path = require('path');
var fs = require('fs');
var sharp = require('sharp');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// Generate
router.get('/:user_id', VerifyToken, function (req, res) {
	Params.token = req.token;

	var hash = req.query.hash;
	var user_id = req.params.user_id;
	var filename = Date.now().toString() + '_' + user_id + '.png';
	var path = './temp_files/' + filename;

	QRCode.toFile(path, hash, {type: 'png'}, function (err, result) {

		if (err){
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		var bucket = Config.S3_BUCKET_FOLDER + 'companies/' + req.user.company_id + '/users/' + user_id + '/qr';

		if( path === undefined ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		var maxHeight = 400;
		var maxWidth = 400;
		var newHeight = 0;
		var newWidth = 0;
		var left = 0;
		var top = 0;

		var image = sharp(path);


		image
			.metadata()
			.then( function( metadata ) {

				if ( metadata.width < metadata.height ) {
					newHeight = metadata.width;
					newWidth = metadata.width;
					left = 0;
					top = Math.round((metadata.height - metadata.width) / 2);
				} else if ( metadata.height < metadata.width ) {
					newHeight = metadata.height;
					newWidth = metadata.height;
					left = Math.round((metadata.width - metadata.height) / 2);
					top = 0;
				} else {
					newHeight = newWidth = metadata.height;
					left = 0;
					top = 0;
				}

				return image
					.extract({ left: left, top: top, width: newWidth, height: newHeight })
				  	.resize( maxWidth, maxHeight )
				  	.toBuffer();
			})
			.then( function( data ) {
				var options = {
					Bucket:  bucket,
					Key: filename,
					Body: data,
					ACL: 'public-read',
					ContentLength: image.length,
					ContentType: "image/png"
				};

				s3.putObject(options, function (err, data) {
					if (!err) {
					  	fs.unlink(path, (err) => {
							if (err) {
								Params.msg = err.message;
								Params.error = true;
								res.json( Params );
								return;
							}

							var user_data = {
								qr_code :  filename
							};

							var opts = { runValidators: true, new: true };

							var conditions = {
								_id: user_id,
								company_id: req.user.company_id
							};

							UserModel.findOneAndUpdate(conditions, user_data, opts, function (error, user) {

								if ( error ) {
									// Delete qr when there is error when saving in the database has error
									var params = { Bucket: bucket, Key: filename };

									s3.deleteObjects(params, function (err, data) {
										if ( err ) {
											Params.msg = error.message;
											Params.error = true;
											res.json( Params );
											return;
										}
									});
								   
									Params.form_errors = error;
									Params.error = true;
									res.json( Params );
									return;
								}


								if ( !user ) {
									// Delete qr when there is no user found.
									var params = { Bucket: bucket, Key: filename };

									 s3.deleteObject(params, function (err, data) {
										if ( err ) {
											Params.form_errors = error.message;
											Params.error = true;
											res.json( Params );
											return;
										}
									});

									Params.error = true;
									Params.msg = ErrorMsgs.err_0004;
									res.json( Params );
									return;

								   
								}

								Params.msg = 'Successfully uploaded.';
								Params.results = user;
								Params.qr_code = data.qr_code;
								res.json( Params );
								return;

							});
					  		
						});
					}
				});
			})
			.catch(function(error){
				//console.log( 'GOR an error', error );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
			});
	  
	})




});


module.exports = router;

