'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const BranchModel  = require('../models/Branch');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	var conditions = {
		company_id: req.user.company_id
	};

	if ( req.query.s ) {
		//console.log('req query s');
		var conditions = {
			name: new RegExp('^'+ req.query.s, 'i'),
			company_id: req.user.company_id
		}
	}

	var query = BranchModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.error = false;
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(700) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var Branch = new BranchModel({
		name: req.body.name,
		company_id: req.user.company_id,
		status: req.body.status,
		address: req.body.address,
	});

	var name = req.body.name;
	var branch_name = new RegExp(["^", name, "$"].join(""), "i");

	var conditions = {
		name: req.body.name,
		company_id: req.user.company_id
	};

	var conditions = {
		name: branch_name,
		company_id: req.user.company_id,
	}

	BranchModel.findOne(conditions, function(err, branch) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( branch ) {
			Params.error = true;
			Params.msg = 'The branch you entered is already registered.';
			res.json( Params );
			return;
		}

		// Validate the input first.
		Branch.validate( function(error) {

			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}

			// Now save data into db.
			Branch.save();

			new SystemLogModel({
				user_id: req.user._id, 
				employee: req.user._id, 
				ref_id: Branch._id,
				type: 'Branches',
				company_id: req.user.company_id,
				branch_id: Branch._id,
				action: 'created '+ Branch.name + '.'
			}).save();

			Params.error = false;
			Params.msg = 'Successfully created.';
			Params.result = Branch;
			res.json( Params );

		});

	});


});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	// Select fields only.
	fields = TextUtil.toFields( fields );
	BranchModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	});
});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	
	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(700) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };

	var trim_name = req.body.name.trim();

	if ( trim_name === '' ) {
		Params.error = true;
		Params.msg = 'Please write a branch name.';
		res.json( Params );
		return;
	}

	// Sanitize html.
	if ( typeof data.job_description !== 'undefined' && data.job_description ) {
		data.job_description = sanitizeHtml(data.job_description);
	}

	var branch_conditions = {
		name: req.body.name,
		company_id: req.user.company_id,
		status: 'active'
	}

	BranchModel.findOne(branch_conditions, function(err, branch) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( branch && branch._id != req.params.id ) {
			Params.error = true;
			Params.msg = 'The branch you entered is already registered.';
			res.json( Params );
			return;
		}

		var conditions = {
			_id: req.params.id,
			company_id: req.user.company_id
		};

		BranchModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( !result ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0004;
				res.json( Params );
				return;
			}

			new SystemLogModel({
				user_id: req.user._id, 
				employee: req.user._id, 
				ref_id: req.params.id,
				type: 'Branches',
				company_id: req.user.company_id,
				branch_id: req.params.id,
				action: 'updated the ' + result.name + ' information.'
			}).save();

			Params.error = false;
			Params.msg = 'Successfully updated.';
			res.json( Params );
			return;
			
		})

		
		

	});


});


module.exports = router;
