'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const FreedomBoardModel  = require('../models/FreedomBoard');
const DepartmentModel  = require('../models/Department');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	var conditions = {
		company_id: req.user.company_id,
	};

	let branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	var query = FreedomBoardModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('department')  
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );
	FreedomBoardModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
	.select(fields)
	.populate('department');
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var branch_id = req.query.branch_id || req.user.branch_id;

	var FreedomBoard = new FreedomBoardModel({
		department: req.body.department_id,
		company_id: req.user.company_id,
		rating: req.body.rating,
		comment: req.body.comment,
		branch_id: branch_id
	});

	if ( !req.body.department_id ) {
		Params.error = true;
		Params.msg = 'Please select a department.';
		res.json( Params );
		return;	
	}

	// Validate the input first.
	FreedomBoard.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		// Now save data into db.
		FreedomBoard.save(function(err){
	    	
	    	if ( !err ) {
	    		
	    		var conditions = {
	    			_id: req.body.department_id,
	    			company_id: req.user.company_id,
	    			branch_id: branch_id
	    		};

	    		DepartmentModel.findOne(conditions, function (deptErr, result) {
	    		    
	    		    if ( deptErr ) {

	    		    	Params.error = true;
						Params.msg = 'Something went wrong. Please try again.';
						res.json( Params );
	    		    	
	    		    } else {

	    		    	let rating = result.rating || 0;
	    		    	let no_of_raters = result.no_of_raters || 0;
	    		    	
	    		    	result.rating = rating + parseInt(req.body.rating);
	    		    	result.no_of_raters = no_of_raters + 1;
	    		    	result.rating = result.rating / result.no_of_raters;

	    		    	result.save(function(err2){
	    		    		if ( err2 ) {
	    		    			//console.log('error', err2);
	    		    		} else {
	    		    			Params.error = false;
								Params.msg = 'Successfully created.';
								Params.result = FreedomBoard;
								res.json( Params );
	    		    		}
	    		    	});
	    		    }

	    		});
	    	}
	    });


	});

});

// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;

	if ( req.perm.indexOf(1500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	FreedomBoardModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});
module.exports = router;
