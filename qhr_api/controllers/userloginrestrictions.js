'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
const moment = require('moment-timezone');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');


// Models declarations
const LoginRestrictionModel  = require('../models/LoginRestriction');
const UserLoginRestrictionModel  = require('../models/UserLoginRestriction');
const SystemLogModel  = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var conditions = {
		company_id: req.user.company_id,
	};

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(2400) === -1 ) {
		conditions.employee = req.user._id;
	}

	let branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var skip = (page - 1) * limit;
	var restriction_id = req.query.restriction_id || '';

	if ( restriction_id ) {
		conditions.restriction = restriction_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields );

	var query = UserLoginRestrictionModel
		.find( conditions )
		.sort( { createdAt: 1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('employee', 'job_title email first_name last_name middle_name photo _id branch_id company_id')
		  	.populate('restriction')
		  	.exec('find', function (err, results) {
				if ( err ) {
					//console.log('err: ', err);
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(2400) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	var UserLoginRestriction = new UserLoginRestrictionModel({
		company_id: req.user.company_id,
		branch_id: branch_id,
		employee: req.body.emp_id,
		restriction: req.body.restriction_id,
	});

	// Validate the input first.
	UserLoginRestriction.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		var conditions = {
			employee: req.body.emp_id,
			restriction: req.body.restriction_id
		}

		UserLoginRestrictionModel.findOne(conditions, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( result ) {
				Params.error = true;
				Params.msg = 'The employee already has this restriction.';
				res.json( Params );
				return;
			}

			// Now save data into db.
			UserLoginRestriction.save();

			var SystemLog = new SystemLogModel({
				user_id: req.user._id,
				employee: req.user._id,  
				ref_id: UserLoginRestriction._id,
				type: 'User Login Restrictions',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'Created a new login restriction.'
			}).save();

			Params.error = false;
			Params.msg = 'Successfully created.';
			Params.result = UserLoginRestriction;
			res.json( Params );
			
		})


	});


});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;
	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(2400) === -1 ) {
		conditions.user_id = req.user._id;
	}

	UserLoginRestrictionModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		
		
	})
	.select(fields);
});

// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;
	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	if ( req.perm.indexOf(2400) === -1 ) {
		conditions.user_id = req.user._id;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	UserLoginRestrictionModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'User Login Restrictions',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'Deleted an employee login restriction.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});

module.exports = router;
