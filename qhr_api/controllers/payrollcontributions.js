'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const PayrollDetailModel  = require('../models/PayrollDetail');
const SystemLogModel = require('../models/SystemLog');
const PayrollDetailContributionModel  = require('../models/childs/PayrollDetailContribution');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit;
	let payroll = req.query.payroll_id || '';
	let emp_id = req.query.emp_id || '';
	let contributions = req.query.contributions || false;

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	var conditions = {
		company_id: req.user.company_id,
		payroll: payroll,
	};

	if ( contributions ) {
		conditions.contributions = { $exists: true, $ne: [] };
	}

	if ( emp_id ) {
		conditions.employee = emp_id;
	}


	var query = PayrollDetailModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('employee')
		  	.exec('find', function (err, results) {
				if ( err ) {
					//console.log('err', err);
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});

});


router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );
	PayrollDetailModel.findOne({ _id: req.params.id }, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
	.select(fields);
});


// update contribution
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };
	var branch_id = req.query.branch_id || '';


	PayrollDetailModel.findOne({ _id: req.params.id,  }, function(err, result) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0000;
			res.json( Params );
			return;
		}


		// result.contributions.id(req.body._id).name = data.name;
		// result.contributions.id(req.body._id).ee_amt = data.ee_amt;
		// result.contributions.id(req.body._id).er_amt = data.er_amt;
		// result.contributions.id(req.body._id).total = data.ee_amt + data.er_amt;	

		// if ( req.body.update_status ){
		result.contributions.id(req.body._id).name = data.name;
		result.contributions.id(req.body._id).ee_status = data.ee_status;
		result.contributions.id(req.body._id).er_status = data.er_status;
		result.contributions.id(req.body._id).ec_status = data.ec_status;
		// }

		// let total_contributions_ee = 0;
		// let total_contributions_er = 0;

		// for ( var c = 0; c < result.contributions.length; c++ ) {
		// 	total_contributions_ee += result.contributions[c].ee_amt;
		// 	total_contributions_er += result.contributions[c].er_amt;
		// }

		// result.total_contributions = total_contributions_ee + total_contributions_er;
		// result.total_contributions_ee = total_contributions_ee;
		// result.total_contributions_er = total_contributions_er;
		// let total_deductions = result.withholding_tax + result.total_tardiness + result.total_absences + result.total_addtl_deductions + total_contributions_ee;
		// result.total_deductions = total_deductions;
		// result.net_pay = result.total_incomes - total_deductions;
			


		result.save( function (err, payrollDetail){

			if ( err ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( !payrollDetail ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0000;
				res.json( Params );
				return;
			}


			Params.results = payrollDetail;
			Params.msg = 'Successfully updated.';
			res.json( Params );
			return;
		});

	});



});



module.exports = router;