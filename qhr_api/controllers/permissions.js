'use strict';

const express = require('express');
const router = express.Router();

var config = require('../config/config');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;
	Params.results = config.permissions;
	res.json( Params );
});

module.exports = router;
