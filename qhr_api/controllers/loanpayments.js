'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');
const moment = require('moment-timezone');
// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const LoanPaymentModel  = require('../models/LoanPayment');
const LoanModel  = require('../models/Loan');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		company: req.user.company_id,
		status: { $ne:'deleted' },
	};

	if ( req.query.status ) {
		conditions.status = req.query.status;
	}

	if ( req.query.emp_id ) {
		conditions.employee = req.query.emp_id;
	}

	if ( req.perm.indexOf(1400) === -1 ) {
		conditions.employee = req.user._id;
	}

	let branch_id = req.query.branch_id || '';

	if ( branch_id ) {
		conditions.branch = branch_id;
	}

	var query = LoanPaymentModel
		.find(conditions)
		.select(fields)
		.sort({ createdAt: -1 });

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit( limit )
		  	.populate('employee')
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(1400) === -1 &&
		req.body.employee_id != req.user._id
	) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var today = moment().format('YYYY-MM-DD');

	let branch_id = req.query.branch_id || '';

	
	var loanData = {
		_id: req.body.loan_id,
		company: req.user.company_id,
		amount_paid: req.body.amount
	};

	var LoanPaymentModule = require("./../libs/LoanPaymentModule");
	// find loan and update balance based on paymount
	LoanPaymentModule.createNewBalance( loanData )
		//Create loans payment history 
		.then( function( results ) {
			var today = moment().format('YYYY-MM-DD');
			//console.log(results);
			results.loanpayment = {
				loan: results.loan._id,
				company: req.user.company_id,
				branch: branch_id,
				amount: req.body.amount,
				payment_date: today,	
			}
		
			return LoanPaymentModule.createLoanPayment( results );
		})
		.then( function( results ) {
			Params.results = results;
			Params.error = false;
			Params.msg = 'Loans Payment Transaction is successful.';
			res.json( Params );
				return;
		})
		.catch(function(error){
			//console.log( 'GOR an error', error );
			Params.error = true;
			Params.msg = error.message || error;
			Params.form_errors = error.form_errors || null;
			res.json( Params );
		});

});



module.exports = router;