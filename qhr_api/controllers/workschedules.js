'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
const moment = require('moment-timezone');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');


// Models declarations
const WorkScheduleModel  = require('../models/WorkSchedule');
const SystemLogModel  = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {
	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var conditions = {
		company_id: req.user.company_id
	};

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3400) === -1 ) {
		conditions.employee = req.user._id;
	}

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var skip = (page - 1) * limit;
	var user_id = req.query.user_id || '';

	// Select fields only.
	fields = TextUtil.toFields( fields );

	let branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	if ( user_id ) {
		conditions.employee = user_id;
	}

	var query = WorkScheduleModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('employee')
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3400) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	let regexp = /^([01]\d|2[0-3]):?([0-5]\d)$/;
	let time_in = req.body.time_in || '';
	let time_out = req.body.time_out || '';
	let time_in2 = req.body.time_in2 || '';
	let time_out2 = req.body.time_out2 || '';
	let nd_start = req.body.nd_start || '';
	let nd_end = req.body.nd_end || '';

	if ( time_in != '' && !time_in.match(regexp) ) {
		Params.error = true;
		Params.msg = 'Invalid clock in.';
		res.json( Params );
		return;
	}

	if ( time_out != '' && !time_out.match(regexp) ) {
		Params.error = true;
		Params.msg = 'Invalid clock out.';
		res.json( Params );
		return;
	}

	if ( req.body.with_break && !time_in2.match(regexp) ) {
		Params.error = true;
		Params.msg = 'Invalid second clock in.';
		res.json( Params );
		return;
	}

	if ( req.body.with_break && !time_out2.match(regexp) ) {
		Params.error = true;
		Params.msg = 'Invalid second clock out.';
		res.json( Params );
		return;
	}

	if ( nd_start != '' && !nd_start.match(regexp) ) {
		Params.error = true;
		Params.msg = 'Invalid night differential start.';
		res.json( Params );
		return;
	}

	if ( nd_end != '' && !nd_end.match(regexp) ) {
		Params.error = true;
		Params.msg = 'Invalid night differential end.';
		res.json( Params );
		return;
	}

	if ( time_in != '' && time_in >= time_out ) {
		Params.error = true;
		Params.msg = "Clock in cannot be the same or more than clock out.";
		res.json( Params );
		return;
	}

	if ( time_in2 != '' && time_in2 <= time_out ) {
		Params.error = true;
		Params.msg = "Second clock in cannot be the same or less than the first clock out.";
		res.json( Params );
		return;
	}		

	if ( time_in2 != '' && time_out2 <= time_in2 ) {
		Params.error = true;
		Params.msg = "Second clock out cannot be the same or less than the second clock in.";
		res.json( Params );
		return;
	}	

	if ( nd_start != '' && nd_end == '' ) {
		Params.error = true;
		Params.msg = "Invalid night differential end.";
		res.json( Params );
		return;
	}

	if ( nd_end != '' && nd_start == '' ) {
		Params.error = true;
		Params.msg = "Invalid night differential start.";
		res.json( Params );
		return;
	}	

	if ( nd_start != '' && nd_start < time_in ) {
		Params.error = true;
		Params.msg = "Night differential start cannot be less than the clock in.";
		res.json( Params );
		return;
	}		

	if ( req.body.with_break && nd_end != '' && nd_end > time_out2 ) {
		Params.error = true;
		Params.msg = "Night differential end cannot be more than the second clock out.";
		res.json( Params );
		return;
	}		

	if ( !req.body.with_break && nd_end != '' && nd_end > time_out ) {
		Params.error = true;
		Params.msg = "Night differential end cannot be more than the clock out.";
		res.json( Params );
		return;
	}		

	if ( !req.body.with_break ) {
		time_in2 = null;
		time_out2 = null;
	}

	var WorkSchedule = new WorkScheduleModel({
		branch_id: branch_id,
		company_id: req.user.company_id,
		employee: req.body.user_id,
		rate: req.body.rate,
		enabled: req.body.enabled,
		with_break: req.body.with_break,
		time_in: time_in,
		time_out: time_out,
		time_in2: time_in2,
		time_out2: time_out2,
		nd_start: nd_start,
		nd_end: nd_end,
		mon_enabled: req.body.mon_enabled,
		tue_enabled: req.body.tue_enabled,
		wed_enabled: req.body.wed_enabled,
		thu_enabled: req.body.thu_enabled,
		fri_enabled: req.body.fri_enabled,
		sat_enabled: req.body.sat_enabled,
		sun_enabled: req.body.sun_enabled,
	});

	var conditions = {
		company_id: req.user.company_id,
		employee: req.body.user_id,
	};

	WorkScheduleModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( result ) {
			Params.error = true;
			Params.msg = "This employee has an existing work schedule. Update work schedule instead.";
			res.json( Params );
			return;
		}

		// Validate the input first.
		WorkSchedule.validate( function(error) {

			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}
			
			// Now save data into db.
			WorkSchedule.save();

			var SystemLog = new SystemLogModel({
				user_id: req.user._id,
				employee: req.user._id,  
				ref_id: WorkSchedule._id,
				type: 'Work Schedules',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'Created a work schedule.'
			}).save();

			Params.error = false;
			Params.msg = 'Successfully created.';
			Params.result = WorkSchedule;
			res.json( Params );

		});
				
		
	});


});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	WorkScheduleModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		
		
	})
	.select(fields)
	.populate('employee');
});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3400) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var status = req.body.status;
	var opts = { runValidators: true, new: true };
	var branch_id = req.query.branch_id || req.user.branch_id;

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	// Prepare data.
	let regexp = /^([01]\d|2[0-3]):?([0-5]\d)$/;
	let time_in = req.body.time_in || '';
	let time_out = req.body.time_out || '';
	let time_in2 = req.body.time_in2 || '';
	let time_out2 = req.body.time_out2 || '';
	let nd_start = req.body.nd_start || '';
	let nd_end = req.body.nd_end || '';

	if ( time_in != '' && !time_in.match(regexp) ) {
		Params.error = true;
		Params.msg = 'Invalid time in.';
		res.json( Params );
		return;
	}

	if ( time_out != '' && !time_out.match(regexp) ) {
		Params.error = true;
		Params.msg = 'Invalid time out.';
		res.json( Params );
		return;
	}

	if ( req.body.with_break && !time_in2.match(regexp) ) {
		Params.error = true;
		Params.msg = 'Invalid second time in.';
		res.json( Params );
		return;
	}

	if ( req.body.with_break && !time_out2.match(regexp) ) {
		Params.error = true;
		Params.msg = 'Invalid second time out.';
		res.json( Params );
		return;
	}

	if ( time_in != '' && time_in >= time_out ) {
		Params.error = true;
		Params.msg = "Time-in cannot be the same or more than time-out.";
		res.json( Params );
		return;
	}

	if ( time_in2 != '' && time_in2 <= time_out ) {
		Params.error = true;
		Params.msg = "Second time-in cannot be the same or less than the first time-out.";
		res.json( Params );
		return;
	}		

	if ( time_in2 != '' && time_in2 >= time_out2 ) {
		Params.error = true;
		Params.msg = "Second time-in cannot be the same or more than the second time-out.";
		res.json( Params );
		return;
	}		

	if ( nd_start != '' && nd_end == '' ) {
		Params.error = true;
		Params.msg = "Invalid night differential end.";
		res.json( Params );
		return;
	}

	if ( nd_end != '' && nd_start == '' ) {
		Params.error = true;
		Params.msg = "Invalid night differential start.";
		res.json( Params );
		return;
	}

	if ( nd_start != '' && nd_start < time_in ) {
		Params.error = true;
		Params.msg = "Night differential start cannot be less than the clock in.";
		res.json( Params );
		return;
	}		

	if ( req.body.with_break && nd_end != '' && nd_end > time_out2 ) {
		Params.error = true;
		Params.msg = "Night differential end cannot be more than the second clock out.";
		res.json( Params );
		return;
	}		

	if ( !req.body.with_break && nd_end != '' && nd_end > time_out ) {
		Params.error = true;
		Params.msg = "Night differential end cannot be more than the clock out.";
		res.json( Params );
		return;
	}		

	if ( !req.body.with_break ) {
		time_in2 = null;
		time_out2 = null;
	}

	var data = {
		time_in: time_in,
		time_out: time_out,
		time_in2: time_in2,
		time_out2: time_out2,
		nd_start: nd_start,
		nd_end: nd_end,
		with_break: req.body.with_break,
		enabled: req.body.enabled,
		rate: req.body.rate,
		mon_enabled: req.body.mon_enabled,
		tue_enabled: req.body.tue_enabled,
		wed_enabled: req.body.wed_enabled,
		thu_enabled: req.body.thu_enabled,
		fri_enabled: req.body.fri_enabled,
		sat_enabled: req.body.sat_enabled,
		sun_enabled: req.body.sun_enabled
	};

	WorkScheduleModel.findOneAndUpdate(conditions, data, opts, function (error, result) {

		if ( error ) {
			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'Work Schedules',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'updated an employee work schedule.'
		}).save();


		Params.msg = 'Successfully updated.';
		res.json( Params );
		return;

	});

});


// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3400) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	var branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	WorkScheduleModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'Work Schedules',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'Deleted a work schedule.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});

module.exports = router;
