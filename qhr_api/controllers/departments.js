'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const DepartmentModel  = require('../models/Department');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	var conditions = {
		company_id: req.user.company_id,
	};

	if ( req.query.s ) {
		//console.log('req query s');
		var conditions = {
			name: new RegExp('^'+ req.query.s, 'i'),
			company_id: req.user.company_id
		}
	}

	if ( req.query.w === 'get_ratings' ) {
		conditions.status = 'active';
	}

	let branch_id = req.query.branch_id || '';

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	var query = DepartmentModel
		.find( conditions )
		.sort( req.query.w === 'get_ratings' ? { rating: -1 } : { createdAt: -1 } );


	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});

});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(600) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	let branch_id = req.query.branch_id || '';

	var Department = new DepartmentModel({
		name: req.body.name,
		company_id: req.user.company_id,
		status: req.body.status,
		description: req.body.description,
		branch_id: branch_id
	});

	var name = req.body.name;
	var department_name = new RegExp(["^", name, "$"].join(""), "i");

	var conditions = {
		name: department_name,
		company_id: req.user.company_id,
		branch_id: req.user.branch_id
	}

	DepartmentModel.findOne(conditions, function(err, department) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( department ) {
			Params.error = true;
			Params.msg = 'The department you entered is already registered.';
			res.json( Params );
			return;
		}

		// Validate the input first.
		Department.validate( function(error) {

			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}

			// Now save data into db.
			Department.save();

			let branch_id = req.query.branch_id || '';

			new SystemLogModel({
				user_id: req.user._id, 
				employee: req.user._id, 
				ref_id: Department._id,
				type: 'Departments',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'created the department ' + Department.name + '.'
			}).save();

			Params.error = false;
			Params.msg = 'Successfully created.';
			Params.result = Department;
			res.json( Params );

		});

		

	});

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;	

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	let branch_id = req.query.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields );
	DepartmentModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	});
});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(600) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };

	var dept_conditions = {
		name: req.body.name,
		company_id: req.user.company_id,
		status: 'active',
	}

	DepartmentModel.findOne(dept_conditions, function(err, department) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( department && department._id != req.params.id ) {
			Params.error = true;
			Params.msg = 'The department you entered is already registered.';
			res.json( Params );
			return;
		}

		
		var conditions = {
			_id: req.params.id,
			company_id: req.user.company_id
		};

		DepartmentModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( !result ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0004;
				res.json( Params );
				return;
			}

			let branch_id = req.query.branch_id || '';

			new SystemLogModel({
				user_id: req.user._id, 
				employee: req.user._id, 
				ref_id: req.params.id,
				type: 'Departments',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'updated the ' + result.name + ' department information.'
			}).save();

			Params.msg = 'Successfully updated.';
			res.json( Params );
			return;
			
		})
		
		

	});

});


module.exports = router;
