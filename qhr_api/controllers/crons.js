'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const CompanyModel = require('../models/Company');
const NotificationModel = require('../models/Notification');
const moment = require('moment-timezone');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;


// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/attendances/update', function (req, res) {

	const moment = require('moment-timezone');
	
	let fromDate = moment().subtract(6, 'days').startOf('day');
	let toDate = moment().subtract(2, 'days').endOf('day');

	Params.token = req.token;

	let fields = (req.query.f) || '';

	let conditions = {
		attd_updtd_at: { 
			$gte: fromDate.format("YYYY-MM-DD HH:mm:ss"), 
			$lte: toDate.format("YYYY-MM-DD HH:mm:ss"),
		},
	};

	if ( req.query.company_id ) {
		conditions._id = req.query.company_id;
	}

	// //console.log(conditions);

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );
	CompanyModel.findOne(conditions, function(err, company) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}


		if ( !company ) {
			//console.log('No company records found.')

			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}


		fromDate = moment(company.attd_updtd_at).add(1, 'days').startOf('day');
		toDate = moment(fromDate).endOf('day');

		// //console.log('WHAAT:fromDate: ' + fromDate);
		// //console.log('WHAAT:toDate: ' + toDate);

		// Daily Time records starts here.
		var DTR = require("./../libs/DTR");
		// Step 1: update DTR for all employees from a specific company.
		DTR.getWorkingEmployees({
			'company': company,
			'date': {
				'from': fromDate,
				'to': toDate
			}
		})

		.then( function( results ){
			// Got employees with working schedule yesterday.
			return DTR.getAbsentsFromWorkingEmployees( results );
		})

		.then( function( results ){
			return DTR.isHolidayToday( results );
		})

		.then( function( results ){
			return DTR.isOnLeave( results );
		})

		.then( function( results ) {
			return DTR.recordAbsentEmployees( results );
		})

		.then( function( results ) {

			Params.error = false;
			Params.msg = results.company.name +  ' : Attendances has been successfully udpated.';
			res.json( Params );
			return;
		})

		.catch(function(error){
			// Handle error
			//console.log( 'GOT an error', error );
			Params.error = true;
			Params.error_type = error.error_type || 'error';
			Params.msg = error.message || error;
			res.json( Params );
			return;
		});
	})
	.select(fields);
});

router.get('/notifications/delete', function (req, res) {

	let threeMonthsLess = moment().subtract(3, 'months');

	let conditions = {
		createdAt: { 
			$lte: threeMonthsLess.format("YYYY-MM-DD HH:mm:ss"),
		},
	};

	NotificationModel.deleteMany(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		Params.error = false;
		Params.msg = result.n + ' record(s) successfully deleted.';
		Params.results = result;
		res.json( Params );
		return;
	})	

});

module.exports = router;
