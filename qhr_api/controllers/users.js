'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');
var slugify = require('slug');
var moment = require('moment-timezone');

var UserLib = require('../libs/UserLib');


// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const UserModel  = require('../models/User');
const CompanyModel  = require('../models/Company');
const BranchModel  = require('../models/Branch');
const SystemLogModel  = require('../models/SystemLog');
const NotificationModel = require('../models/Notification');
const UserSettingModel = require('../models/UserSetting');
const PasswordResetModel = require('../models/PasswordReset');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

const uniqueString = require('unique-string');
const crypto = require('crypto');


// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});


// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	let limit = parseInt(req.query.limit);
	let page = parseInt(req.query.page) || 1;
	let fields = (req.query.f) || '';
	let skip = (page - 1) * limit;
	let sort = req.query.sort || '';
	let status = req.query.status || 'active';
	let w = req.query.w || '';
	let branch_id = req.query.branch_id || req.user.branch_id;
	let salary_type = req.query.salary_type || '';

	// Prepare conditions.
	let conditions = {
		company_id: req.user.company_id,
	};

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(200) === -1 ) {
		conditions._id = req.user._id;
	}

	// Allow only access on specific branch.
	if ( req.perm.indexOf(200) === -1 ) { // No permission
		branch_id = req.user.branch_id;
	}

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	} 

	if ( salary_type ) {
		conditions.salary_type = salary_type;
	} 

	if ( status == 'active' ) {
		conditions.status = 'active';
	}


	if ( w === 'upcoming_birthdays' ) {
		let fromMonth = moment().format('M');
		let toMonth = moment(fromMonth).add(1, 'months').format('M');

		conditions.birth_month = { $gte: fromMonth, $lte: toMonth };
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );
	sort = TextUtil.toSort( sort, ' ' );

	let query = UserModel
		.find(conditions)
		.select(fields)
		.sort(sort);
	
	if ( req.query.s ) {
		let trim_search = req.query.s.trim();
		
		if ( trim_search === '' ) {
			Params.error = true;
			Params.msg = 'Please enter valid search query.';
			res.json( Params );
			return;
		}

		query.or([
			{ first_name: new RegExp('^'+ trim_search, 'i') }, 
			{ last_name: new RegExp('^'+ trim_search, 'i') }
		]);
	}

	if ( status != 'active' ) {
		query.or([
			{ status: 'inactive' }, 
			{ status: 'resigned' },
			{ status: 'awol' }
		]);	
	}

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Send reset link
router.get('/forgot-password', function (req, res) {
	// Prepare conditions.
	var conditions = {
		username: req.query.username
	};

	UserModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = 'Username not found!';
			res.json( Params );
			return;
		}


		let hash = Math.random().toString(36).substr(2);
		let resetLink = config.APP_URL + 'forgot-password/reset/' + hash;
		let expire_at = moment().add('days', 1);

		var PasswordReset = new PasswordResetModel({
			user_id: result._id,
			hash: hash,
			expire_at: expire_at,
		});

		PasswordReset.validate( function(error) { 
			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}

			PasswordReset.save(function(err){ 

				if (! err ) {

					var SesClient = require("../libs/SesClient");
					
					// all fields here is required
					var data = {
						to: [result.email],
						subject: 'QloudHR Password Reset',
						clientName: 'Hi, ' + result.first_name + '!',
						messageBody: 'You requested to reset your QloudHR Online password. Click on the link below to continue.',
						linkhere: 'Reset Password: ' + resetLink,
						messageEnd:'If you didn’t make this request, simply ignore this message. Please do not reply to this message. This is a system-generated email sent to ' + result.email + '.',
						from: 'no-reply@qloudhr.com'
					};
				    
				    SesClient.sendEmail( data )
					.then( function( results ){
						//console.log('successfully sent');
						Params.results_count = 1;
						Params.results = result;
						Params.msg = 'We have sent instructions to your email.';
						res.json( Params );
					})
					.catch(function(error){
						//console.log( 'GOT an error', error );
						Params.error = true;
						Params.error_type = error.error_type || 'error';
						Params.msg = error.message || error;
						res.json( Params );
					});
				}

			});
		});

		

	});
});

// Send Email
// router.get('/send-email', VerifyToken, function (req, res) {

// 	var SesClient = require("../libs/SesClient");

// 	var data = {
// 		email: 'cheryl.codenbrew@gmail.com',
// 		subject: 'Test Email',
// 		message: 'This is the body of the email',
// 		from: 'no-reply@qloudhr.com'
// 	};
    
//     SesClient.sendEmail( data )
// 	.then( function( results ){
// 		//console.log('successfully sent');
// 		Params.error = false;
// 		Params.msg = results.msg;
// 		res.json( Params );
// 		return;
// 	})
// 	.catch(function(error){
// 		//console.log( 'GOT an error', error );
// 		Params.error = true;
// 		Params.error_type = error.error_type || 'error';
// 		Params.msg = error.message || error;
// 		res.json( Params );
// 	});

// });

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(200) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Allow only access on specific branch.
	var branch_id = req.query.branch_id || req.user.branch_id;

	// Get birthdate
	var birthdate = moment(req.body.birthdate).format('L'); 
	var birth_month = null;
	var birth_day = null;
	var birth_year = null;

	// At least 18 years old
	var less_than_18 = moment().subtract(18, 'years').format('L');
	var less_than_150 = moment().subtract(150, 'years').format('L');
	
	// check if minor
	var isAfter = moment(birthdate).isAfter(less_than_18);

	if ( isAfter) {
		Params.error = true;
		Params.msg = 'You must be at least 18 years old.';
		res.json( Params );
		return;
	}

	// check if age is 150
	var isBefore = moment(birthdate).isBefore(less_than_150);

	if ( isBefore ) {
		Params.error = true;
		Params.msg = 'Invalid age. Please contact administrator for more info.';
		res.json( Params );
		return;
	}

	if ( req.body.birthdate ) {

		birthdate = moment(req.body.birthdate).format('YYYY-MM-DD');

		birth_month = moment(birthdate).format('MM');
		birth_day = moment(birthdate).format('DD');
		birth_year = moment(birthdate).format('YYYY');
	}

	var hash = bcrypt.hashSync(uniqueString(), config.secret );
	var tempPassword = uniqueString();

	var new_user = {
		first_name: req.body.first_name,
		middle_name: req.body.middle_name,
		last_name: req.body.last_name,
		suffix: req.body.suffix,
		gender: req.body.gender,
		birthdate: req.body.birthdate,
		birth_month: birth_month,
		birth_day: birth_day,
		birth_year: birth_year,
		branch_id: branch_id,
		email: req.body.email,
		username: req.body.username,
		address: req.body.address,
		contact_nos: req.body.contact_nos,
		company_id: req.user.company_id, // add to same company only.
		with_break: req.body.with_break,
		hash: hash,
		daily_work_schedules: config.daily_work_schedules,
		food_allowance: req.body.food_allowance,
		travel_allowance: req.body.travel_allowance,
		basic_salary: req.body.basic_salary,
		salary_type: req.body.salary_type,
		password: tempPassword
	};

	var UserLib = require("./../libs/UserLib");

	UserLib.countUser({ company_id: req.user.company_id })
	.then( function( params ){
		return UserLib.checkPlanType( params );
	})
	.then( function( params ){
		return UserLib.validateUser( new_user );
	})
	.then( function( params ){
		return UserLib.checkUsername( req.body.username );
	})
	.then( function( params ){

		let email_conditions = {
			email: req.body.email,
			company_id: req.user.company_id
		};

		return UserLib.checkEmail( email_conditions );
	})
	.then( function( params ){
		return UserLib.createUser( new_user, req.user._id );
	})
	.then( function( result ){
		Params.user = result.user;
		Params.error = false;
		Params.msg = 'You have successfully created an employee.';
		res.json( Params );
		return;
	})
	.catch(function(error){
		Params.error = true;
		Params.error_type = error.error_type || 'error';
		Params.msg = error.message || error;
		Params.form_errors = error.form_errors || null;
		res.json( Params );
	});

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( 
		req.perm.indexOf(200) === -1 &&
		req.params.id !== req.user._id // Only his own account is allowed.
	) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = '+leave_credits';
	
	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Allow only access on specific branch.
	let branch_id = req.query.branch_id || req.user.branch_id;

	// Prepare conditions.
	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	UserModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		//console.log('res: ', result);

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		
		
	})
	.select(fields);
});


// UPDATE PASSWORD
router.post('/:id/update/password', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( 
		req.perm.indexOf(200) === -1 &&
		req.params.id !== req.user._id // Only his own account is allowed.
	) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };

	// Allow only access on specific branch.
	var branch_id = req.query.branch_id || req.user.branch_id;

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(200) === -1 ) {
		conditions._id = req.user._id;
	}

	// Sanitize html.
	if ( typeof data.password !== 'undefined' && data.password ) {

		// Validate password.
		if ( data.password.length < 6 || data.password.length > 30  ) {

			Params.error = true;
			Params.msg = 'Password should be between 6 and 30 characters';
			res.json( Params );
			return;
		}

		// Let's try to encrypt password.
		try {
			data.password = bcrypt.hashSync(data.password, config.secret );
		} catch(err) {
			//console.log( err );
			Params.error = true;
			Params.msg = 'Unable to change password at this time. Please try again later.';
			res.json( Params );
			return;
		}
	}

	UserModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: req.params.id,
			type: 'Users',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'updated an employee password.'
		}).save();

		Params.msg = 'Password successfully updated.';
		res.json( Params );
		return;
		
	})

});

// update employee password
router.post('/:id/update/user/password', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( 
		req.perm.indexOf(200) === -1 &&
		req.params.id !== req.user._id // Only his own account is allowed.
	) {
		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true, context: 'query' };
	const ownRecord = req.params.id === req.user._id;

	// Verify if user don't have an access capability. 
	// Allow only own item to be created.
	if ( req.perm.indexOf(200) === -1 && !ownRecord ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Pre validation.
	if ( ownRecord ) req.checkBody('current_password', 'Current Password is required.').notEmpty();

	req.checkBody('new_password', 'New Password is required').notEmpty();
	req.checkBody('confirm_password')
		.notEmpty().withMessage('Confirm Password is required.')
		.equals(req.body.new_password).withMessage('New password do not match with confirm password.')
		.isLength({ min: 6 }).withMessage('Confirm password must be atleast 6 characters.')
		;
	// req.checkBody('confirm_password', 'Confirm password must be atleast 6 characters.')

	var validationErrors = req.validationErrors();

	if ( validationErrors ) {

		Params.error = true;
		Params.form_errors = validationErrors;
		res.json( Params );
		return;
	}

	// Allow only access on specific branch.
	var branch_id = req.query.branch_id || req.user.branch_id;

	// Prepare conditions.
	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	UserModel.findOne( conditions, function(err, user) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !user ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		// Only check validity of password if it is his own profile/record.
		if ( ownRecord ) {

			var valid = bcrypt.compareSync(data.current_password, user.password);

			if (! valid ) {
				Params.error = true;
				Params.msg = 'Current password is incorrect.';
				res.json( Params );
				return;
			}

			if ( data.current_password === data.new_password ) {
				Params.error = true;
				Params.msg = 'New password cannot be the same as current password';
				res.json( Params );
				return;
			}
		}

		// Let's try to encrypt password.
		try {
			data.new_password = bcrypt.hashSync(data.new_password, config.secret );
		} catch(err) {
			//console.log( err );
			Params.error = true;
			Params.msg = 'Unable to change password at this time. Please try again later.';
			res.json( Params );
			return;
		}

		// Now, update password field.
		user.password = data.new_password;
	
		user.save( function (error) {
	
			if ( error ) {
				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}

			var SystemLog = new SystemLogModel({
				user_id: req.user._id, 
				employee: req.user._id, 
				ref_id: req.params.id,
				type: 'Users',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'updated the employee password of '+ user.first_name +' '+ user.last_name +'.'
			}).save();

			Params.error = false;
			Params.msg = 'Password successfully updated.';
			res.json( Params );
			return;
	
		});
	})

});


// UPDATE emp details
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( 
		req.perm.indexOf(200) === -1 &&
		req.params.id != req.user._id
	) {
		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true, new: true, context: 'query' };
	var user_hash;
	var username;

	UserLib.getUser({
		_id: req.params.id,
		company_id: req.user.company_id,
	})
	.then( function( results ) {

		user_hash = results.hash;
		username = results.username;

		if ( !req.body.daily_work_schedules ) {
			delete req.body.email;
			return new Promise((resolve, reject) => {
				resolve( results );
			}); // End Promise
		} else {

			// check for invalid time
			return UserLib.isValidTime( results, req.body.daily_work_schedules );
		}

		
	})
	.then( function( results ) {

		// Remove username if no changes has been made.
		if ( results.username === req.body.username ) {
			delete req.body.username;
		}

		// Remove email if no changes has been made.
		if ( results.email === req.body.email ) {
			delete req.body.email;
			return new Promise((resolve, reject) => {
				resolve();
			}); // End Promise
		} else {
			// Is email already exist?
			return new Promise((resolve, reject) => {
				let conditions = {	
					company_id: req.user.company_id, 
					email: req.body.email 
				};

				UserModel.findOne(conditions, function(error, user) {
					if ( error ) {
						//console.log('ERROR(getUser): ', error);
						reject( {message: 'An error occurred. Please try again later.'} );
					}

					if ( user ) {
						reject( {message: 'Email already exist.'} );			
					}

					resolve();	
				});
			}); // End Promise
		}

	}).then( function( results ) {
		// Now, let's update user info.
		var conditions = {
			_id: req.params.id,
			company_id: req.user.company_id
		};

		return UserLib.updateUser( conditions, data, opts );
	})
	.then( function( results ) {

		if ( results.user.hash != user_hash ) {
			return UserLib.generateQR( results.user );
		} else {
			return new Promise((resolve, reject) => {
				resolve( results );
			}); // End Promise
		}
		
	})
	.then( function( results ) {

		if ( results.user.username != username ) {

			let link = config.APP_URL + 'login';

			let data = {
				to: [results.user.email],
				subject: 'Username Changed',
				clientName: 'Hi, ' + results.user.first_name + '!',
				messageBody: 'The username for your QloudHR Account was recently changed.',
				linkhere: 'Click here to login: ' + link,
				messageEnd:'Thank you for using QloudHR.',
				from: 'no-reply@qloudhr.com'
			};
			return UserLib.sendEmail({
				user: results.user,
				emailData: data
			});

		} else {
			return new Promise((resolve, reject) => {
				resolve( results );
			}); // End Promise
		}
		
	})
	.then( function( results ) {

		let branch_id = req.query.branch_id || req.user.branch_id;

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: results.user._id,
			type: 'Users',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'updated an employee information.'
		}).save();

		new NotificationModel({
			user_id: results.user._id, 
			type: 'Profile',
			ref_id: results.user._id,
			description: 'Your profile has been updated.'
		}).save();

		return new Promise((resolve, reject) => {
			resolve( results );
		}); // End Promise	
		
	})
	.then( function( results ) {
		Params.results_count = 1;
		Params.results = results;
		Params.msg = 'Successfully updated.';
		res.json( Params );
	})
	.catch(function(error){
		Params.error = true;
		Params.msg = !("message" in error) ? '' : error.message;
		Params.form_errors = error;
		res.json( Params );
	});


});


router.get('/:id/generate_hash', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(200) === -1 ) {
		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var emp_hash = bcrypt.hashSync(uniqueString(), config.secret );

	Params.results = emp_hash;
	res.json( Params );
	return;

});

router.get('/count_employees/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(200) === -1 ) {
		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var conditions = {
		company_id: req.params.id,
		status: 'active'
	}

	UserModel.count(conditions, function (err, count) {

		if ( err ) {
			Params.errors = err;
			Params.error = true;
			res.json( Params );
			return;
		}
		

		Params.results = count;
		res.json( Params );
		return;

	});


});


module.exports = router;
