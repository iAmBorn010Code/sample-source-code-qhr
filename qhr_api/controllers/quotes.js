'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const QuoteModel  = require('../models/Quote');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	var conditions = {
		company_id: req.user.company_id
	};

	let branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}


	var query = QuoteModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );
	QuoteModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
	.select(fields);
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(900) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	// count if more than 10
	var conditions = {
		company_id: req.user.company_id,
		branch_id: branch_id
	}

	QuoteModel.count(conditions, function (err, count) {

		if ( err ) {
			Params.errors = err;
			Params.error = true;
			res.json( Params );
			return;
		}

		if ( count >= 10 ) {
			Params.error = true;
			Params.msg = "You have reached the maximum number of quotes allowed for your company."
			res.json( Params );
			return;
		}
		
		var Quote = new QuoteModel({
			user_id: req.user._id,
			company_id: req.user.company_id,
			quote: req.body.quote,
			branch_id: branch_id
		});

		// Validate the input first.
		Quote.validate( function(error) {

			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}

			// Now save data into db.
			Quote.save();

			var SystemLog = new SystemLogModel({
				user_id: req.user._id, 
				employee: req.user._id, 
				ref_id: Quote._id,
				type: 'Quotes',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'created a new quote for the company.'
			}).save();

			Params.error = false;
			Params.msg = 'Successfully created.';
			Params.result = Quote;
			res.json( Params );

		});
		

	});


});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(900) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	QuoteModel.findOneAndUpdate(conditions, data, opts, function (error, result) {

		if ( error ) {
			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		let branch_id = req.query.branch_id || req.user.branch_id;

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id, 
			ref_id: req.params.id,
			type: 'Quotes',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'updated a company quote.'
		}).save();

		Params.msg = 'Successfully updated.';
		res.json( Params );
		return;

	});

});

// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;

	if ( req.perm.indexOf(900) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	var branch_id = req.query.branch_id || req.user.branch_id;

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
		branch_id: branch_id
	};

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	QuoteModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: req.params.id,
			type: 'Quotes',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'deleted a company quote.'
		}).save();


		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});

module.exports = router;
