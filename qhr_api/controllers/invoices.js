'use strict';

const express = require('express');
const router = express.Router();
const crypto = require('crypto');

var moment = require('moment-timezone');

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var config = require('../config/config');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const InvoiceModel  = require('../models/Invoice');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});


// SHOW
router.get('/:invoice_uid', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	var conditions = {
		invoice_uid: req.params.invoice_uid,
		company_id: req.user.company_id,
		payment_trans_id: req.query.payment_trans_id
	};

	let branch_id = req.query.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	InvoiceModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	});
});

module.exports = router;
