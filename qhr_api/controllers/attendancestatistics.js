'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var moment = require('moment');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const DailyTimeRecordModel = require('../models/DailyTimeRecord');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

const uniqueString = require('unique-string');

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {
	
	Params = new ApiParams().params;
	next()
});

router.get('/total', VerifyToken, function (req, res) {

	Params.token = req.token;

	var w = req.query.w || '' ;
	var filter_date = req.query.filter_date || '';
	var user_id = req.query.emp_id || '';
	var branch_id = req.query.branch_id || '';
	var overtime = req.query.ot || '';
	var fromDate = moment().format("YYYY-MM-DD 00:00:00");
	var toDate = moment().format("YYYY-MM-DD 23:59:59");
	var conditions = {
		company_id: req.user.company_id
	};

	if ( user_id ) {
		conditions.user_id = user_id;
	}

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	if ( overtime ) {
		conditions.with_ot = true;
	}

	if ( filter_date === 'today' ) {
		fromDate = moment().format("YYYY-MM-DD 00:00:00");
		toDate = moment().format("YYYY-MM-DD 23:59:59");
		conditions.for_date = { $gte: fromDate, $lte: toDate };
	} else if( filter_date === 'yesterday' ) {
		fromDate = moment().subtract(1, 'days').startOf('day');
		toDate = moment().subtract(1, 'days').endOf('day');
		conditions.for_date = { $gte: fromDate, $lte: toDate };
	} else if( filter_date === 'current_month' ) {
		fromDate = moment().format("YYYY-MM-01 00:00:00");
		toDate = moment().add(1, 'months').startOf('month').format("YYYY-MM-DD 00:00:00");
		conditions.for_date = { $gte: fromDate, $lte: toDate };
	} else if ( filter_date === 'custom' ) {
		fromDate = moment().format(req.query.date_from + " 00:00:00");
		toDate = moment().format(req.query.date_to + " 23:59:59");
		conditions.for_date = { $gte: fromDate, $lte: toDate };
	} 

	if ( w == 'absents' ) {

		conditions.is_absent = 1;	
	}

	if ( w == 'on_time' ) {

		if ( overtime ) {
			conditions.ot_in_status = 'On Time';	
		} else {
			conditions.time_in_status = 'On Time';	
		}
	}

	if ( overtime && w == 'lates' ) {
		conditions.ot_in_status = 'Late';
	}

	if ( overtime && w == 'undertime' ) {
		conditions.ot_out_status = 'Undertime';
	}

	//console.log('conditions', conditions);

	let query = DailyTimeRecordModel
		.find(conditions);

		if ( w == 'lates' && !overtime ) {
			query.or([
				{ time_in_status: 'Late' }, 
				{ time_in2_status: 'Late' }
			]);
		}

		if ( w == 'undertime' && !overtime ) {
			query.or([
				{ time_out_status: 'Undertime' }, 
				{ time_out2_status: 'Undertime' }
			]);
		}

		// if ( w == 'on_time' ) {
		// 	//console.log('-----------------');
		// 	//console.log('ontime');
		// 	query.or([
		// 		{ time_in_status: 'On Time' }, 
		// 		{ time_in2_status: 'On Time' }
		// 	]);
		// }

	query.count(function (err, count) {

		if ( err ) {
			Params.errors = err;
			Params.error = true;
			res.json( Params );
			return;
		}

		Params.w = w;
  		Params.results = count;
		res.json( Params );
		return;

	});
	
});

module.exports = router;
