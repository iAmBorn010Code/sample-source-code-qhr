'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
const moment = require('moment-timezone');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');


// Models declarations
const LoginRestrictionModel  = require('../models/LoginRestriction');
const UserLoginRestrictionModel  = require('../models/UserLoginRestriction');
const SystemLogModel  = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var conditions = {
		company_id: req.user.company_id,
	};

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(2400) === -1 ) {
		conditions.user_id = req.user._id;
	}

	let branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	var query = LoginRestrictionModel
		.find( conditions )
		.sort( { date: 1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(2400) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	var LoginRestriction = new LoginRestrictionModel({
		company_id: req.user.company_id,
		branch_id: branch_id,
		name: req.body.name,
		type: req.body.type,
		value: req.body.value
	});

	// Validate the input first.
	LoginRestriction.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}
		
		// Now save data into db.
		LoginRestriction.save();

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: LoginRestriction._id,
			type: 'Login Restrictions',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'Created a new login restriction.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully created.';
		Params.result = LoginRestriction;
		res.json( Params );


	});


});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;
	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(2400) === -1 ) {
		conditions.user_id = req.user._id;
	}

	LoginRestrictionModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		
		
	})
	.select(fields);
});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true, new: true };

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	if ( req.perm.indexOf(2400) === -1 ) {
		conditions.user_id = req.user._id;
	}

	LoginRestrictionModel.findOneAndUpdate(conditions, data, opts, function (error, result) {

		if ( error ) {
			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		let branch_id = req.query.branch_id || req.user.branch_id;

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'Login Restrictions',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'updated a login restriction.'
		}).save();


		Params.msg = 'Successfully updated.';
		res.json( Params );
		return;

	});

});


// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;
	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	if ( req.perm.indexOf(2400) === -1 ) {
		conditions.user_id = req.user._id;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	LoginRestrictionModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'Login Restrictions',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'Deleted a login restriction.'
		}).save();

		UserLoginRestrictionModel.deleteMany({ restriction: req.params.id }, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			Params.error = false;
			Params.msg = 'Successfully deleted.';
			Params.results_count = 1;
			Params.results = result;
			res.json( Params );
			return;
			
		});	

	})
	.select(fields);
});

module.exports = router;
