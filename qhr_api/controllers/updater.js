'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var config = require('../config/config');

const UserModel  = require('../models/User');
const BranchModel  = require('../models/Branch');
const CompanyModel  = require('../models/Company');
const DailyTimeRecordModel  = require('../models/DailyTimeRecord');
const SystemLogModel = require('../models/SystemLog');
const NotificationModel = require('../models/Notification');
const DepartmentModel = require('../models/Department');
const HolidayModel = require('../models/Holiday');
const LeaveModel = require('../models/Leave');
const PayrollModel = require('../models/Payroll');
const PayrollDetailModel = require('../models/PayrollDetail');




// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// Generate
router.get('/', function (req, res) {

	var conditions = {
		company_id: '5cca8be72b2f0201eeba7a71'
	};

	// branch ID
	var data = { branch_id: '5cca8be82b2f0201eeba7a72' };

	// DTR
	// DailyTimeRecordModel.update(conditions, data, { multi: true }, function(err, result) {
		
	// 	Params.results_count = 1;
	// 	Params.results = result;
	// 	res.json( Params );
	// 	return;
	// });

	// User
	// UserModel.update(conditions, data, { multi: true }, function(err, result) {
		
	// 	Params.results_count = 1;
	// 	Params.results = result;
	// 	res.json( Params );
	// 	return;
	// });

	// Department
	// DepartmentModel.update(conditions, data, { multi: true }, function(err, result) {
		
	// 	Params.results_count = 1;
	// 	Params.results = result;
	// 	res.json( Params );
	// 	return;
	// });
	
	// Holiday
	// HolidayModel.update(conditions, data, { multi: true }, function(err, result) {
		
	// 	Params.results_count = 1;
	// 	Params.results = result;
	// 	res.json( Params );
	// 	return;
	// });

	// Leave
	// LeaveModel.update(conditions, data, { multi: true }, function(err, result) {
		
	// 	Params.results_count = 1;
	// 	Params.results = result;
	// 	res.json( Params );
	// 	return;
	// });
	
	// Payroll
	// PayrollModel.update(conditions, data, { multi: true }, function(err, result) {
		
	// 	Params.results_count = 1;
	// 	Params.results = result;
	// 	res.json( Params );
	// 	return;
	// });

	// SystemLogModel
	// SystemLogModel.update(conditions, data, { multi: true }, function(err, result) {
		
	// 	Params.results_count = 1;
	// 	Params.results = result;
	// 	res.json( Params );
	// 	return;
	// });



	// Payroll Details
	PayrollDetailModel.update(conditions, data, { multi: true }, function(err, result) {
		
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	});
});


module.exports = router;

