'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');
const moment = require('moment-timezone');
// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const PayrollModel  = require('../models/Payroll');
const PayrollGroupModel  = require('../models/PayrollGroup');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	var conditions = {
		company_id: req.user.company_id
	};

	let branch_id = req.query.branch_id || '';

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	var query = PayrollGroupModel
		.find( conditions )
		.sort( { createdAt: 1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	let branch_id = req.query.branch_id || '';

	var conditions = {
		name: new RegExp('^'+ req.body.name, 'i'),
		company_id: req.user.company_id,	
	};

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	PayrollGroupModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( result ) {
			Params.error = true;
			Params.msg = 'The Payroll Group name you entered is already used.';
			res.json( Params );
			return;
		}

		var Payroll_Group = new PayrollGroupModel({
			name: req.body.name,
			total_incomes: 0,
			total_deductions: 0,
			total_taxes: 0,
			total_contributions: 0,
			overall_total: 0,
			company_id: req.user.company_id,
			branch_id: branch_id,
		});

		// Validate the input first.
		Payroll_Group.validate( function(error) {

			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}

			// Now save data into db.
			Payroll_Group.save();

			let branch_id = req.query.branch_id || '';

			new SystemLogModel({
				user_id: req.user._id, 
				employee: req.user._id, 
				ref_id: Payroll_Group._id,
				type: 'Payroll Groups',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'created the '+ Payroll_Group.name +'.'
			}).save();

			Params.error = false;
			Params.msg = 'Successfully created.';
			Params.result = Payroll_Group;
			res.json( Params );

		});
	});

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	let branch_id = req.query.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	PayrollGroupModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	});
});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {
	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	let branch_id = req.query.branch_id || '';

	var conditions = {
		name: new RegExp('^'+ req.body.name, 'i'),
		company_id: req.user.company_id,	
	};

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	PayrollGroupModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( result ) {
			Params.error = true;
			Params.msg = 'The Payroll Group name you entered is already used.';
			res.json( Params );
			return;
		}

		// Prepare data.
		var data = {
			name: req.body.name
		};
		var opts = { runValidators: true };


		var conditions = {
			_id: req.params.id,
			company_id: req.user.company_id
		};

		PayrollGroupModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.form_errors = err;
				Params.error = true;
				res.json( Params );
				return;
			}

			if ( !result ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0004;
				res.json( Params );
				return;
			}

			let branch_id = req.query.branch_id || '';

			new SystemLogModel({
				user_id: req.user._id, 
				employee: req.user._id, 
				ref_id: req.params.id,
				type: 'Payroll Groups',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'updated a payroll group.'
			}).save();

			Params.msg = 'Successfully updated.';
			res.json( Params );
			return;
			
		})

	});

});


// Add payroll to payroll_group
router.post('/:id/group', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };
	var branch_id = req.query.branch_id || '';

	var conditions = {
		_id: req.params.id, // payroll_group id
	};

	// Get Payroll Group previous total values
	PayrollGroupModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001+'group';
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004+'group';
			res.json( Params );
			return;
		}

		var total_incomes = result.total_incomes;
		var total_deductions = result.total_deductions;
		var total_taxes = result.total_taxes;
		var total_contributions = result.total_contributions;
		var overall_total = result.overall_total;

		var conditions = {
			_id: req.body.payroll_id, // payroll id
		};

		// Get Payroll total values then add to the group total
		PayrollModel.findOne(conditions, function(err, payroll) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001+'payroll';
				res.json( Params );
				return;
			}

			if ( !payroll ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0004+'payroll';
				res.json( Params );
				return;
			}

			data = {
				total_incomes : total_incomes + payroll.total_incomes,
				total_deductions : total_deductions + payroll.total_deductions,
				total_taxes : total_taxes + payroll.total_taxes,
				total_contributions : total_contributions + payroll.total_contributions,
				overall_total : overall_total + payroll.overall_total_netpay,
			};

			var conditions = {
				_id: req.params.id, // payroll group id
			};

			PayrollGroupModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

				if ( err ) {
					//console.log( err );
					Params.form_errors = err+'group';
					Params.error = true;
					res.json( Params );
					return;
				}

				if ( !result ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0004+'group';
					res.json( Params );
					return;
				}

				new SystemLogModel({
					user_id: req.user._id, 
					employee: req.user._id, 
					ref_id: req.params.id,
					type: 'Payroll Groups',
					company_id: req.user.company_id,
					branch_id: branch_id,
					action: 'updated the '+ result.name +'.'
				}).save();

				Params.msg = 'Successfully updated.';
				res.json( Params );
				return;
				
			})

		})

	});

});



module.exports = router;