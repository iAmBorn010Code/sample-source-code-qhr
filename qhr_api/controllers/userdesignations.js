'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
const moment = require('moment-timezone');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');


// Models declarations
const UserDesignationModel  = require('../models/UserDesignation');
const SystemLogModel  = require('../models/SystemLog');
const NotificationModel = require('../models/Notification');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var conditions = {
		company_id: req.user.company_id,
	};

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3300) === -1 ) {
		conditions.employee = req.user._id;
	}

	let branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	let designation_id = req.query.designation_id;

	if ( designation_id ) {
		conditions.designation = designation_id
	}

	let day = req.query.day;

	if ( day ) {
		conditions.day = day;
	}

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	var query = UserDesignationModel
		.find( conditions )
		.sort( { createdAt: 1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('employee')
		  	.populate('designation')
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3300) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id =  req.user.branch_id;

	var conditions = {
		employee: req.body.user_id,
		company_id: req.user.company_id,
		branch_id: branch_id,
		day: req.body.day,
	}

	UserDesignationModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( result ) {
			Params.error = true;
			Params.msg = "Designation(" + req.body.day + ") : Employee is already assigned in this designation and has "+result.status+" status.";
			res.json( Params );
			return;
		}

		var by = null;
		var by_id = null;

		if ( req.body.status != 'pending' ) {
			by = req.user.name;
			by_id = req.user._id;
		}

		var UserDesignation = new UserDesignationModel({
			employee: req.body.user_id,
			designation: req.body.designation_id,
			status: req.body.status,
			company_id: req.user.company_id,
			branch_id: branch_id,
			day: req.body.day,
			by: by,
			by_id: by_id
		});


		// Validate the input first.
		UserDesignation.validate( function(error) {

			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}
			
			// Now save data into db.
			UserDesignation.save();

			Params.error = false;
			Params.msg = 'Successfully created.';
			Params.result = UserDesignation;
			res.json( Params );


		});

	});


});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;
	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(3300) === -1 ) {
		conditions.employee = req.user._id;
	}

	UserDesignationModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		
		
	})
	.select(fields)
	.populate('employee')
	.populate('designation');
});

//UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3300) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var opts = { runValidators: true, new: true };
	var branch_id = req.query.branch_id || req.user.branch_id;

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	var data = {
		status: req.body.status,
		by: req.user.name,
		by_id: req.user._id
	};

	UserDesignationModel.findOneAndUpdate(conditions, data, opts, function (error, result) {

		if ( error ) {
			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'User Designations',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: req.body.status + ' ' + result.employee.first_name + ' ' + result.employee.last_name + '\'s designation.'
		}).save();


		Params.msg = 'Successfully updated.';
		res.json( Params );
		return;

	});

});


// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;
	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(3300) === -1 ) {
		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	UserDesignationModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});

module.exports = router;
