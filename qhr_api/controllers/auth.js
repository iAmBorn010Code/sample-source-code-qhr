'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config/config');

var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const UserModel  = require('../models/User');
const CompanyModel  = require('../models/Company');
const BranchModel  = require('../models/Branch');
const UserPermissionModel  = require('../models/UserPermission');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	Params = new ApiParams().params;
	next()
});

// Login
router.post('/login', function (req, res) {
	let password = req.body.password;
	let username = req.body.username;

	if (!username || !password) {
		Params.error = true;
		Params.msg = 'Username and password are required.';
		res.json( Params );
		return;
	}

	var conditions = {
		username: req.body.username,
		password: bcrypt.hashSync(password, config.secret ),
	};

	// Check if employee exists in a specific company.
	UserModel.findOne(conditions, function(err, user) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !user ) {
			Params.error = true;
			Params.msg = 'Username and password do not match.';
			res.json( Params );
			return;
		}

		// Now, let's get user permission.
		var conditions = {
			user_id: user._id
		};

		//console.log( user );

		// Select fields only.
		UserPermissionModel.findOne(conditions, function(err, permission) {
			if ( err ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			let perm = !permission ? [] : permission.perm;
			
			// Create a token and append some public data.
			let token = jwt.sign({ 
					id: user._id, 
					perm: perm, 
					user: {
						_id: user._id,
						email: user.email,
						company_id: user.company_id,
						branch_id: user.branch_id,
						name: user.first_name + ' ' + user.last_name
					},
				}, config.secret, {
			 	expiresIn: 86400 // expires in 24 hours : TODO: Put this under configurations
			});

			CompanyModel.findOne({_id: user.company_id}, function(err, company) {

				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
					return;
				}

				if ( !company ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0004;
					res.json( Params );
					return;
				}

				BranchModel.findOne({_id: user.branch_id, company_id: user.company_id}, function(err, branch) {

					if ( err ) {
						Params.error = true;
						Params.msg = ErrorMsgs.err_0001;
						res.json( Params );
						return;
					}

					if ( !branch ) {
						Params.error = true;
						Params.msg = ErrorMsgs.err_0004;
						res.json( Params );
						return;
					}

					// TODO: Return only needed fields.
					var avatarUrl = config.S3_BUCKET_FILE_URL 
						+ 'companies/' + user.company_id 
						+ '/users/' + user._id
						+ '/avatar/' + user.photo;

					Params.perm = perm;
					Params.user = {
						_id: user._id,
						company_id: user.company_id,
						email: user.email,
						first_name: user.first_name,
						middle_name: user.middle_name,
						last_name: user.last_name,
						suffix: user.suffix,
						credits: user.credits,
						contact_nos: user.contact_nos,
						job_title: user.job_title,
						avatar_url: avatarUrl,
						photo: user.photo,
						hash: user.hash
					};
					Params.token = token;
					Params.branch = {
						_id: branch._id,
						name: branch.name,
					};

					Params.company = {
						_id: company._id,
						name: company.name,
					};

					//console.log('Params', Params );

					res.json( Params );
					return;

				});



			});

		});

	});
	
});

// Verify Token
router.get('/me', VerifyToken, function (req, res) { 

	Params.token = req.token;

	// Prepare conditions.
	var conditions = {
		_id: req.user._id,
	};

	UserModel.findOne(conditions, function(err, user) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.forced_login = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !user ) {
			Params.error = true;
			Params.forced_login = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		var avatarUrl = config.S3_BUCKET_FILE_URL 
			+ 'companies/' + user.company_id 
			+ '/users/' + user._id
			+ '/avatar/' + user.photo;

		Params.user = {
			_id: user._id,
			company_id: user.company_id,
			email: user.email,
			first_name: user.first_name,
			middle_name: user.middle_name,
			last_name: user.last_name,
			suffix: user.suffix,
			credits: user.credits,
			contact_nos: user.contact_nos,
			job_title: user.job_title,
			avatar_url: avatarUrl,
			hash: user.hash
		};

		//console.log('Params: Auth:me', Params );
		res.json( Params );
		return;
	});
});

// Login
router.post('/logout', function (req, res) {
	res.json( Params );
});



module.exports = router;
