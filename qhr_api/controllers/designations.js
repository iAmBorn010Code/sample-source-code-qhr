'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
const moment = require('moment-timezone');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');


// Models declarations
const DesignationModel  = require('../models/Designation');
const UserDesignationModel  = require('../models/UserDesignation');
const SystemLogModel  = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {
	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3100) === -1 ) {
		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var skip = (page - 1) * limit;
	var conditions = {
		company_id: req.user.company_id
	};

	// Select fields only.
	fields = TextUtil.toFields( fields );

	let branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	var query = DesignationModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3100) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	let regexp = /^([01]\d|2[0-3]):?([0-5]\d)$/;
	let time_in = req.body.time_in || '';
	let time_out = req.body.time_out || '';
	let time_in2 = req.body.time_in2 || '';
	let time_out2 = req.body.time_out2 || '';
	let with_time = req.body.with_time || false;
	let with_break = req.body.with_break || false;

	if ( with_time ) {

		if ( time_in == '' || !time_in.match(regexp) ) {
			Params.error = true;
			Params.msg = 'Invalid time in.';
			res.json( Params );
			return;
		}

		if ( time_out == '' || !time_out.match(regexp) ) {
			Params.error = true;
			Params.msg = 'Invalid time out.';
			res.json( Params );
			return;
		}

		if ( time_in >= time_out ) {
			Params.error = true;
			Params.msg = "Time-in cannot be the same or more than time-out.";
			res.json( Params );
			return;
		}

		if ( with_break ) {

			if ( time_in2 == '' || !time_in2.match(regexp) ) {
				Params.error = true;
				Params.msg = 'Invalid second time in.';
				res.json( Params );
				return;
			}

			if ( time_out2 == '' || !time_out2.match(regexp) ) {
				Params.error = true;
				Params.msg = 'Invalid second time out.';
				res.json( Params );
				return;
			}


			if ( time_in2 <= time_out ) {
				Params.error = true;
				Params.msg = "Second time-in cannot be the same or less than the first time-out.";
				res.json( Params );
				return;
			}		

			if ( time_in2 >= time_out2 ) {
				Params.error = true;
				Params.msg = "Second time-in cannot be the same or more than the second time-out.";
				res.json( Params );
				return;
			}	
		}

	}

	if ( !with_time ) {
		time_in = null;
		time_out = null;
		time_in2 = null;
		time_out2 = null;
		with_break = false;
	}

	if ( !with_break ) {
		time_in2 = null;
		time_out2 = null;
	}

	var Designation = new DesignationModel({
		branch_id: branch_id,
		company_id: req.user.company_id,
		title: req.body.title,
		rate: req.body.rate,
		ot_rate: req.body.ot_rate,
		with_time: with_time,
		with_break: with_break,
		time_in: time_in,
		time_out: time_out,
		time_in2: time_in2,
		time_out2: time_out2,
	});

	// Validate the input first.
	Designation.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		var conditions = {
			company_id: req.user.company_id,
			branch_id: branch_id,
			title: req.body.title
		}

		DesignationModel.findOne(conditions, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( result ) {
				Params.error = true;
				Params.msg = "This designation already exists.";
				res.json( Params );
				return;
			}

			// Now save data into db.
			Designation.save();

			var SystemLog = new SystemLogModel({
				user_id: req.user._id,
				employee: req.user._id,  
				ref_id: Designation._id,
				type: 'Designations',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'Created a designation.'
			}).save();

			Params.error = false;
			Params.msg = 'Successfully created.';
			Params.result = Designation;
			res.json( Params );
			
			
		});

	});

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	DesignationModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		
		
	})
	.select(fields);
});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3100) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var status = req.body.status;
	var opts = { runValidators: true, new: true };
	var branch_id = req.query.branch_id || req.user.branch_id;

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	// Prepare data.
	let regexp = /^([01]\d|2[0-3]):?([0-5]\d)$/;
	let time_in = req.body.time_in || '';
	let time_out = req.body.time_out || '';
	let time_in2 = req.body.time_in2 || '';
	let time_out2 = req.body.time_out2 || '';
	let with_time = req.body.with_time || false;
	let with_break = req.body.with_break || false;


	if ( with_time ) {

		if ( time_in == '' || !time_in.match(regexp) ) {
			Params.error = true;
			Params.msg = 'Invalid time in.';
			res.json( Params );
			return;
		}

		if ( time_out == '' || !time_out.match(regexp) ) {
			Params.error = true;
			Params.msg = 'Invalid time out.';
			res.json( Params );
			return;
		}

		if ( time_in >= time_out ) {
			Params.error = true;
			Params.msg = "Time-in cannot be the same or more than time-out.";
			res.json( Params );
			return;
		}

		if ( with_break ) {
			if ( time_in2 == '' || !time_in2.match(regexp) ) {
				Params.error = true;
				Params.msg = 'Invalid second time in.';
				res.json( Params );
				return;
			}

			if ( time_out2 == '' || !time_out2.match(regexp) ) {
				Params.error = true;
				Params.msg = 'Invalid second time out.';
				res.json( Params );
				return;
			}


			if ( time_in2 <= time_out ) {
				Params.error = true;
				Params.msg = "Second time-in cannot be the same or less than the first time-out.";
				res.json( Params );
				return;
			}		

			if ( time_in2 >= time_out2 ) {
				Params.error = true;
				Params.msg = "Second time-in cannot be the same or more than the second time-out.";
				res.json( Params );
				return;
			}		
		}

	}

	if ( !with_time ) {
		time_in = null;
		time_out = null;
		time_in2 = null;
		time_out2 = null;
		with_break = false;
	}

	if ( !with_break ) {
		time_in2 = null;
		time_out2 = null;
	}

	var data = {
		time_in: time_in,
		time_out: time_out,
		time_in2: time_in2,
		time_out2: time_out2,
		with_time: with_time,
		with_break: with_break,
		title: req.body.title,
		rate: req.body.rate,
		ot_rate: req.body.ot_rate
	};

	var title_conditions = {
		company_id: req.user.company_id,
		branch_id: branch_id,
		title: req.body.title
	}

	DesignationModel.findOne( title_conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( result && req.params.id != result._id ) {
			Params.error = true;
			Params.msg = "This designation already exists.";
			res.json( Params );
			return;
		}

		DesignationModel.findOneAndUpdate(conditions, data, opts, function (error, result) {

			if ( error ) {
				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}

			if ( !result ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0004;
				res.json( Params );
				return;
			}

			var SystemLog = new SystemLogModel({
				user_id: req.user._id,
				employee: req.user._id,  
				ref_id: req.params.id,
				type: 'Designations',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'updated the designation ' + result.title + '.'
			}).save();


			Params.msg = 'Successfully updated.';
			res.json( Params );
			return;

		});

	});

});


// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(3100) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	var branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	DesignationModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'Designations',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'Deleted the designation ' + result.title + '.'
		}).save();

		UserDesignationModel.deleteMany({ designation: req.params.id }, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			Params.error = false;
			Params.msg = 'Successfully deleted.';
			Params.results_count = 1;
			Params.results = result;
			res.json( Params );
			return;

			
		});

	})
	.select(fields);
});

module.exports = router;
