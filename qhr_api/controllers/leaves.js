'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var moment = require('moment');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const LeaveModel  = require('../models/Leave');
const SystemLogModel = require('../models/SystemLog');
const NotificationModel = require('../models/Notification');
const UserModel = require('../models/User');
const HolidayModel = require('../models/Holiday');

var LeaveLib = require('../libs/Leave');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit;
	var w = (req.query.w) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	var conditions = {
		company_id: req.user.company_id,
	};

	if ( req.query.emp_id ) {
		conditions.user_id = req.query.emp_id;
	}

	if ( req.perm.indexOf(400) === -1 ) {
		conditions.user_id = req.user._id;
	}

	if ( req.query.w === 'upcoming_leaves' ) {
		let fromDate = moment().format("YYYY-MM-DD 00:00:00");
		let toDate = moment().endOf('month').format("YYYY-MM-DD 00:00:00");
		conditions.date =  { $gte: fromDate, $lte: toDate };
		conditions.status = 'approved';
	}

	let branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	var query = LeaveModel
		.find( conditions )
		.sort( req.query.w === 'upcoming_leaves' ? { date: 1 } : { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('employee')
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(400) === -1 &&
		req.body.user_id != req.user._id )
	{
		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	// get date today
	var today = moment().format('L');

	//get user leave date
	var date = moment(req.body.date).format('L'); 

	// check if date is same or before today
	var isSameOrBefore = moment(date).isSameOrBefore(today);

	if ( isSameOrBefore ){
		Params.error = true;
		Params.msg = 'Please enter valid date.';
		res.json( Params );
		return;
	}

	if ( !req.body.type ) {
		Params.error = true;
		Params.msg = 'Please select leave type.';
		res.json( Params );
		return;
	}

	if ( !req.body.reason ) {
		Params.error = true;
		Params.msg = 'Please enter reason for leave.';
		res.json( Params );
		return;
	}


	LeaveLib.isHoliday({
		date: req.body.date,
		company_id: req.user.company_id,
		branch_id: branch_id
	})
	.then( function( results ) {

		return LeaveLib.hasLeave({
			user_id: req.body.user_id,
			date: req.body.date
		});
	})
	.then( function( results ) {

		return LeaveLib.checkLeaveCredit({
			user_id: req.body.user_id,
			company_id: req.user.company_id,
			branch_id: branch_id,
			type: req.body.type
		});
	})
	.then( function( results ) {

		return LeaveLib.createLeave({
			user_id: req.body.user_id,
			company_id: req.user.company_id,
			branch_id: branch_id,
			type: req.body.type,
			date: req.body.date,
			reason: req.body.reason
		});
	})
	.then( function( results ) {

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: results._id,
			type:'Leaves',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'created new leave for an employee.'
		}).save();

		return LeaveLib.updateUserCredit({
			user_id: req.body.user_id,
			company_id: req.user.company_id,
			branch_id: branch_id,
			type: req.body.type,
		});
	})
	.then( function( results ) {
		Params.results_count = 1;
		Params.results = results;
		Params.msg = 'Successfully added.';
		res.json( Params );
	})
	.catch(function(error){
		Params.error = true;
		Params.msg = !("message" in error) ? '' : error.message;
		Params.form_errors = error;
		res.json( Params );
	});


});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;
	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	let branch_id = req.query.branch_id || '';

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	if ( req.perm.indexOf(400) === -1 ) {
		conditions.user_id = req.user._id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields );
	LeaveModel
	.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results = result;
		res.json( Params );
	})
	.populate('employee');


});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(400) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var leave_data = req.body;
	var opts = { runValidators: true };

	// Sanitize html.
	if ( typeof leave_data.reason !== 'undefined' && leave_data.reason ) {
		leave_data.reason = sanitizeHtml(leave_data.reason);
	}

	// check leaves for existing (same date, user)
	var leave_conditions = {
		user_id: req.body.user_id,
		date: req.body.date
	}

	LeaveModel.find(leave_conditions, function(err, leave) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( leave ) {

			for ( var i = 0; i < leave.length; i++ ) {

				if ( req.body.status != 'disapproved' ){

					if ( leave[i]._id != req.params.id && leave[i].status === 'approved' ) {
						Params.error = true;
						Params.msg = 'This employee has an existing leave on this date.';
						res.json( Params );	
						return;
					}	
				}
			}
		}

		// get user information
		var user_conditions = {
			_id: req.body.user_id,
			company_id: req.user.company_id
		};

		UserModel.findOne(user_conditions, function(err, user) {

			if ( err ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}
				
			var conditions = {
				_id: req.params.id,
				company_id: req.user.company_id
			};

			let leaves = user.leave_credits;

			for ( var i = 0; i < leaves.length; i++ ) {

				if ( leaves[i].type == leave_data.type && leaves[i].credit <= 0 && leave_data.status === 'approved' ) {
					Params.error = true;
					Params.msg = 'This employee does not have enough leave credit.';
					res.json( Params );
					return;
				}	
			}
			

			LeaveModel.findOneAndUpdate(conditions, leave_data, opts, function(err, result) {

				if ( err ) {
					//console.log( err );
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
					return;
				}

				if ( !result ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0004;
					res.json( Params );
					return;
				}			

				// update user credit
				for ( var i = 0; i < user.leave_credits.length; i++ ) {

					if ( user.leave_credits[i].type == leave_data.type ) {
						user.leave_credits[i].pending -= 1;

						if ( leave_data.status == 'approved' ) {
							user.leave_credits[i].credit -= 1;
							user.leave_credits[i].used += 1;
						}
					}	
				}

				user.save( function (err, user){

					if ( err ) {
						Params.error = true;
						Params.msg = ErrorMsgs.err_0001;
						res.json( Params );
						return;
					}

					if ( !user ) {
						Params.error = true;
						Params.msg = ErrorMsgs.err_0001;
						res.json( Params );
						return;
					}

					let branch_id = req.query.branch_id || '';

					new SystemLogModel({
						user_id: req.user._id, 
						employee: req.user._id, 
						ref_id: req.params.id,
						type:'Leaves',
						company_id: req.user.company_id,
						branch_id: branch_id,
						action: 'updated a leave information.'
					}).save();

					new NotificationModel({
						user_id: user._id, 
						type: 'Leave',
						ref_id: req.params.id,
						description: 'Your leave on ' + result.date + ' has been ' + leave_data.status + '.'
					}).save();


					Params.results = result;
					Params.msg = 'Successfully updated.';
					res.json( Params );
					return;
				});
				

			});


		})

		

	}); 


});


module.exports = router;
