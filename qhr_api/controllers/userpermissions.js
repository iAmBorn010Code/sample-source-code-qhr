'use strict';

const express = require('express');
const router = express.Router();

var config = require('../config/config');

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const UserPermissionModel  = require('../models/UserPermission');
const UserModel  = require('../models/User');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	//console.log('here');
	Params = new ApiParams().params;
	next()
});


// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	var conditions = {
		company_id: req.user.company_id
	};

	var query = UserPermissionModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.error = false;
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	var conditions = {
		user_id: req.params.id, // This is equevalent to user_id
		company_id: req.user.company_id,
	};

	// Select fields only.
	fields = TextUtil.toFields( fields );
	UserPermissionModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	});
});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	
	Params.token = req.token;

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };

	var conditions = {
		user_id: req.body.user_id,
	};

	//console.log('req.body.user_id', req.body.user_id);
	//console.log('req.user._id', req.user._id);
	// Do not allow self update of permission.
	if ( req.body.user_id === req.user._id ) {
		Params.error = true;
		Params.msg = 'You are not allowed to update your own permissions.';
		res.json( Params );
		return;
	}

	// Validate data permissions.
	// 1. Convert into integers.
	var perm = req.body.perm;
	perm = perm.split(',').map(function(item) {
	    return parseInt(item, 10);
	});

	// 2. Remove duplicate values.
	perm = perm.filter(function(elem, pos) {
	    return perm.indexOf(elem) == pos;
	});

	// 3. Return only matched items in an array.
	var matched = perm.filter( function(n) 
		{ 
			return this.has(n) 
		}, new Set(config.permission_keys) 
	);

	// Now set data to validated permissions.
	data.perm = matched;

	// All is good, start updating record.
	UserPermissionModel.findOneAndUpdate(conditions, data, opts, function(err, userPermission) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		/*
		Since no employee, first check if passed ID belongs to 
		specific employee in a specific company.
		*/

		if ( !userPermission ) {

			var conditions = {
				company_id: req.user.company_id,
				_id: req.body.user_id
			};

			// Check if valid employee under a company.
			UserModel.findOne(conditions, function(err, user) {

				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
					return;
				}

				if (! user ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0004;
					res.json( Params );
					return;
				}


				// If legit employee, create permission to that employee then.

				var UserPermission = new UserPermissionModel({
					user_id: req.body.user_id,
					company_id: req.user.company_id,
					perm: data.perm
				});

				// Validate the input first.
				UserPermission.validate( function(error) {

					if ( error ) {
						Params.error = true;
						Params.msg = ErrorMsgs.err_0001;
						res.json( Params );
						return;
					}

					// Now save data into db.
					UserPermission.save( function( err, permission) {

						if ( err ) {

							Params.error = true;
							Params.msg = ErrorMsgs.err_0001;
							res.json( Params );
							return;
						}

						new SystemLogModel({
							user_id: req.user._id, 
							employee: req.body.user_id, 
							ref_id: UserPermission.id,
							type: 'User Permissions',
							company_id: req.user.company_id,
							branch_id: user.branch_id,
							action: 'Permission has been updated.'
						}).save();
						

						Params.error = false;
						Params.msg = 'Permission successfully updated.';
						Params.result = permission;
						res.json( Params );
					});

				});
				
			});

			
		} else {

			// new SystemLogModel({
			// 	user_id: req.user._id, 
			// 	employee: req.body.user_id, 
			// 	ref_id: userPermission.id,
			// 	company_id: req.user.company_id,
			// 	action: 'Permission has been updated.'
			// }).save();

			Params.error = false;
			Params.msg = 'Permission successfully updated.';
			res.json( Params );
			return;
		}

		
	});

});


module.exports = router;
