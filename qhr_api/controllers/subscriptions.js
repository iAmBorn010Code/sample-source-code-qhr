'use strict';

const express = require('express');
const router = express.Router();

var moment = require('moment-timezone');

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var config = require('../config/config');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const SubscriptionModel  = require('../models/Subscription');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});


// INDEX
router.get('/', VerifyToken, function (req, res) {

	// Params.token = req.token;

	// var limit = parseInt(req.query.limit) || 10;
	// var page = parseInt(req.query.page) || 1;
	// var fields = (req.query.f) || '';
	// var conditions = {};
	// var skip = (page - 1) * limit
	// var type = req.query.type || 'debit';

	// // Select fields only.
	// fields = TextUtil.toFields( fields );

	// var conditions = {
	// 	company_id: req.user.company_id,
	// 	type: type
	// };

	// var query = BillingTransactionModel
	// 	.find( conditions )
	// 	.sort( { createdAt: -1 } );

	// // TODO: Find a better way to get total count for large data set.
	// query.count(function (err, count) {
	//   	query
	// 	  	.skip(skip)
	// 		.limit(limit)
	// 	  	.exec('find', function (err, results) {
	// 			if ( err ) {
	// 				Params.error = true;
	// 				Params.msg = ErrorMsgs.err_0001;
	// 				res.json( Params );
	// 			} else {
	// 				Params.total_count = count;
	// 				Params.results_count = results.length;
	// 				Params.results = results;
	// 				res.json( Params );
	// 			}
	// 	   	});
	// });
});

// Get subscription list
router.get('/get', VerifyToken, function (req, res) {

	Params.token = req.token;

	Params.results = config.plan_type;
	res.json( Params );

});


// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	var paid = false; 

	if ( req.query.paid == 'true' ) {
		paid = true;
	}

	var conditions = {
		company_id: req.user.company_id,
		paid: paid
	};

	// Select fields only.
	fields = TextUtil.toFields( fields );
	SubscriptionModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		if ( result.company_id._id != req.user.company_id){
			Params.error = true;
			Params.msg = 'You are not allowed to access this page.';
			res.json( Params );
			return;
		}

		var today = moment().format('YYYY-MM-DD');
		var expired_at = moment(result.expired_at).format('YYYY-MM-DD');
		var isAfter = moment(today).isAfter(expired_at);

		if ( isAfter ) {
			var isExpired = true;
		}

		Params.results_count = 1;
		Params.results = result;
		Params.isExpired = isExpired;
		res.json( Params );
		return;
	})
	// .populate('company_id')
	.sort( { createdAt: -1 } )
	.populate('company_id');
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var conditions = {
		company_id: req.user.company_id,
		paid: false
	};

	// Delete previous pending subscription
	SubscriptionModel.findOneAndDelete(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}
	});

	var plan_type = req.body.plan_type;

	var paid = false; 
	var interval_count = req.body.interval_count;
	if ( plan_type === 'A' ) {
		paid = true;

		var fromDate = moment().format('YYYY-MM-DD');
		var toDate = moment(fromDate).add(interval_count, 'months').format('YYYY-MM-DD');

	}

	// If new sub plan type is A then find current and delete
	if ( paid ) {
		var conditions = {
			company_id: req.user.company_id,
			paid: true
		};

		SubscriptionModel.findOne(conditions, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( result ) {
				SubscriptionModel.deleteOne(conditions, function(err, result) {

					if ( err ) {
						//console.log( err );
						Params.error = true;
						Params.msg = ErrorMsgs.err_0001;
						res.json( Params );
						return;
					}

				});

			}
		});
	}

	// Save subscription 
	var total_amount = (req.body.price * interval_count) || 0;

	var Subscription = new SubscriptionModel({
		user_id: req.user._id,
		company_id: req.user.company_id,
		interval: 'monthly',
		interval_count: interval_count,
		plan_type: req.body.plan_type,
		total_amount: total_amount,
		paid: paid,
		payment_method: req.body.payment_method,
		start_at: fromDate,
		expired_at: toDate,	
	});

	// Validate the input first.
	Subscription.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		// Now save data into db.
		Subscription.save();
		let branch_id = req.query.branch_id || '';

		var action = 'requested to subscribe to plan type ' + Subscription.plan_type +' for ₱' + req.body.price+'.00/month.';
		if (Subscription.plan_type == 'A') {
			action = 'subscribed to plan type A for free';
		}
		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: Subscription._id,
			type: 'Subscriptions',
			company_id: req.user.company_id,
			branch_id:branch_id,
			action: action
		}).save();

		// If subscription plan type is A then create payment history and invoice
		if ( Subscription.plan_type == 'A' ) {
			var BillingModule = require("./../libs/BillingModule");
			var invoice_uid =  Math.floor(100000000 + Math.random() * 900000000);
			var today = moment().format('YYYY-MM-DD');
			var params = {};

			params.paymentData = {
				transaction: 'subscription',
				trans_type: 'credit',
				payment_method: Subscription.payment_method,
				payment_date: today,
				amount: 0,
				company_id: Subscription.company_id,
				ref_id: Subscription._id,
				invoice_uid: invoice_uid
			};

			BillingModule.createPaymentTransaction( params )
				.then( function( results ) {
					var items = {
						company_id: results.payment.company_id,
						desc: 'Subscription - Plan Type A',
						price: config.plan_type[plan_type].price,
						total: results.payment.amount
					};  
					var invoiceData = {
						payment_method: results.payment.payment_method,
						payment_date: results.payment.payment_date,
						total_amount: 0,
						company_id: results.payment.company_id,
						items: items,
						invoice_uid: results.payment.invoice_uid,
						payment_trans_id: results.payment._id
					}

					return BillingModule.createInvoice( invoiceData );
				})
				.then( function( results ) {
					Params.error = false;
					Params.msg = 'Your account has successfully subscribed for free.';
					Params.result = Subscription;
					res.json( Params );
				})
				.catch(function(error){
					//console.log( 'GOR an error', error );
					Params.error = true;
					Params.msg = error.message || error;
					Params.form_errors = error.form_errors || null;
					res.json( Params );
				});
		} else {
			Params.error = false;
			Params.msg = 'You have successfully requested to upgrade your account.';
			Params.result = Subscription;
			res.json( Params );
		}

	});

});

// Cancel Subscription
router.post('/cancel', VerifyToken, function (req, res) { 
	Params.token = req.token;

	var paid = req.body.paid;

	if ( paid ) {
		var conditions = {
			_id: req.body._id,
			company_id: req.user.company_id,
			paid: true
		};

		SubscriptionModel.findOneAndDelete(conditions, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}
		});

		var Subscription = new SubscriptionModel({
			user_id: req.user._id,
			company_id: req.user.company_id,
			interval: 'monthly',
			interval_count: 0,
			plan_type: 'A',
			total_amount: 0,
			paid: true,
		});

		// Validate the input first.
		Subscription.validate( function(error) {

			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}

			// Now save data into db.
			Subscription.save();

			Params.error = false;
			Params.msg = 'Successfully canceled your plan.';
			Params.result = Subscription;
			res.json( Params );

		});

	} else {

		var conditions = {
			_id: req.body._id,
			company_id: req.user.company_id,
			paid: false
		};

		SubscriptionModel.findOneAndDelete(conditions, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}
		});

		var conditions = {
			company_id: req.user.company_id,
			paid: true
		};
		SubscriptionModel.findOne(conditions, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( result ) {
				Params.error = false;
				Params.msg = 'Successfully canceled your plan.';
				Params.result = result;
				res.json( Params );
			}
		});
	}

});


module.exports = router;
