'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
const moment = require('moment-timezone');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');


// Models declarations
const TeamMemberModel  = require('../models/TeamMember');
const TeamModel  = require('../models/Team');
const SystemLogModel  = require('../models/SystemLog');
const NotificationModel = require('../models/Notification');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;
	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {
		team_id: req.query.team_id,
	};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	var query = TeamMemberModel
		.find( conditions )
		.sort( { createdAt: 1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('employee')
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(1300) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var TeamMember = new TeamMemberModel({
		user_id: req.body.user_id,
		employee: req.body.user_id,
		team_id: req.body.team_id,
	});

	var conditions = {
		user_id: req.body.user_id,
		team_id: req.body.team_id
	}

	TeamMemberModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( result ) {
			Params.error = true;
			Params.msg = "This employee is already in this team.";
			res.json( Params );
			return;
		}


		// Validate the input first.
		TeamMember.validate( function(error) {

			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}
			
			// Now save data into db.
			TeamMember.save();

			var opts = { runValidators: true, new: true };

			TeamModel.findOneAndUpdate( { _id: req.body.team_id }, { $inc: { cnt: 1 }}, opts, function (error, result) {


				if ( error ) {
					Params.error = ErrorMsgs.err_0001;
					res.json( Params );
					return;
				}

				if ( !result ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0004;
					res.json( Params );
					return;
				}

				new NotificationModel({
					user_id: req.body.user_id, 
					type: 'Teams',
					ref_id: TeamMember._id,
					description: 'You are now a member of team ' + result.name
				}).save();


				Params.error = false;
				Params.msg = 'Successfully added.';
				Params.result = TeamMember;
				res.json( Params );

			});

			


		});
		
	});


});


// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(1300) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
	};

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	TeamMemberModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		var opts = { runValidators: true, new: true };

		TeamModel.findOneAndUpdate( { _id: result.team_id }, { $inc: { cnt: -1 }}, opts, function (error, team) {

			if ( error ) {
				Params.error = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( !team ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0004;
				res.json( Params );
				return;
			}

			new NotificationModel({
				user_id: result.user_id, 
				type: 'Teams',
				ref_id: result._id,
				description: 'You have been removed from the team ' + team.name
			}).save();


			Params.error = false;
			Params.msg = 'Successfully deleted.';
			Params.results_count = 1;
			Params.results = result;
			res.json( Params );
			return;

		});
		

	});
});

module.exports = router;
