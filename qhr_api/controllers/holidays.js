'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
const moment = require('moment-timezone');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const HolidayModel  = require('../models/Holiday');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {
		company_id: req.user.company_id
	};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Allow only access on specific branch.
	let branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	var query = HolidayModel
		.find( conditions )
		.sort( { date: 1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(1000) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// get date today
	var today = moment().format('L');

	//get date
	var date = moment(req.body.date).format('L'); 


	// check if date is same or before today
	var isSameOrBefore = moment(date).isSameOrBefore(today);

	if ( isSameOrBefore ){
		Params.error = true;
		Params.msg = 'Please enter valid date.';
		res.json( Params );
		return;
	}

	let branch_id = req.query.branch_id || req.user.branch_id;

	var Holiday = new HolidayModel({
		name: req.body.name,
		date: moment(req.body.date).format('YYYY-MM-DD'),
		type: req.body.type,
		description: req.body.description,
		company_id: req.user.company_id,
		branch_id: branch_id
	});

	// Validate the input first.
	Holiday.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}
		
		// Now save data into db.
		Holiday.save();

		Params.error = false;
		Params.msg = 'Successfully created.';
		Params.result = Holiday;
		res.json( Params );


	});


});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;
	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	let branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields );
	HolidayModel
	.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results = result;
		res.json( Params );
	});


});


// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;

	if ( req.perm.indexOf(1000) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	HolidayModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});

module.exports = router;
