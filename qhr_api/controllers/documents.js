'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const DocumentModel = require('../models/Document');
const SystemLogModel = require('../models/SystemLog');
const NotificationModel = require('../models/Notification');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(200) === -1 &&
		req.query.emp_id != req.user._id
	) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	var conditions = {
		user_id: req.query.emp_id,
		company_id: req.user.company_id
	};

	var query = DocumentModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;
	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(200) === -1 ) {
		conditions.user_id = req.user._id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );
	DocumentModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
	.select(fields);
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(200) === -1 &&
		req.body.user_id != req.user._id
	) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var Document = new DocumentModel({
		user_id: req.body.user_id,
		company_id: req.user.company_id,
		name: req.body.name,
		filename: req.body.filename
	});

	// Validate the input first.
	Document.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		// Now save data into db.
		Document.save();

		let branch_id = req.query.branch_id || '';

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: Document._id,
			type: 'Documents',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'uploaded a document.'
		}).save();

		new NotificationModel({
			user_id: Document.user_id, 
			type: 'Document',
			ref_id: Document._id,
			description: Document.name + ' was uploaded into your documents.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully created.';
		Params.result = Document;
		res.json( Params );

	});

});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(200) === -1 ) {
		conditions.user_id = req.user._id;
	}

	DocumentModel.findOneAndUpdate(conditions, data, opts, function (error, result) {

		if ( error ) {
			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		let branch_id = req.query.branch_id || '';

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: req.params.id,
			type: 'Documents',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'updated an employee document.'
		}).save();

		new NotificationModel({
			user_id: result.user_id, 
			type: 'Document',
			ref_id: req.params.id,
			description: 'Your ' + result.name + ' document has been updated.'
		}).save();

		Params.msg = 'Successfully updated.';
		res.json( Params );
		return;

	});

});

// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;
	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(200) === -1 ) {
		conditions.user_id = req.user._id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	DocumentModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		let branch_id = req.query.branch_id || '';

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: req.params.id,
			type: 'Documents',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'deleted an employee document.'
		}).save();

		new NotificationModel({
			user_id: result.user_id, 
			type: 'Document',
			ref_id: req.params.id,
			description: result.name + ' was removed from your documents.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});

// upload file
router.post('/file_upload', VerifyToken, function (req, res) {
	
	Params.token = req.token;

	//console.log('file upload here.');

	// Select fields only.

});

module.exports = router;
