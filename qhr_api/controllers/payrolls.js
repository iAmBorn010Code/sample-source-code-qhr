'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');
const moment = require('moment-timezone');
// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const PayrollModel  = require('../models/Payroll');
const SystemLogModel = require('../models/SystemLog');
const PayrollDetailModel  = require('../models/PayrollDetail');
const PayrollApproverModel  = require('../models/PayrollApprover');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;
const mongoose = require('mongoose');

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var limit = parseInt(req.query.limit) || 10;
		limit =  req.query.w === 'latest_payroll' ? 1 : limit;

	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		company_id: req.user.company_id,
		status: { $ne:'deleted' }, // TODO
	};

	if ( req.query.w === 'total_payroll' ) {
		conditions.status = 'approved';

		let fromDate = moment().format("YYYY-MM-01 00:00:00");
		let toDate = moment().add(1, 'months').startOf('month').format("YYYY-MM-DD 00:00:00");
		conditions.pay_period_from =  { $gte: fromDate, $lte: toDate };
		conditions.pay_period_to = { $gte: fromDate, $lte: toDate };
	}

	if ( req.query.w === 'latest_payroll' ) {
		conditions.status = 'approved';
	}

	if ( req.query.payroll_group_id ) {
		conditions.payroll_group_id = req.query.payroll_group_id;
	}

	if ( req.query.status ) {
		conditions.status = req.query.status;
	}

	if ( req.query.branch_id ) {
		conditions.branch_id = req.query.branch_id
	}

	//console.log( conditions );

	// conditions = { 
	// 	company_id: '5bd474acf4cf66547e80c42d',
 //  		status: { '$ne': 'deleted' },
 //  		branch_id: '5cc67b2ce70ab70248d3527b'
 //  	};

	var query = PayrollModel
		.find(conditions)
		.select(fields)
		.sort({ createdAt: -1 });

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {

					// Loop
					// for (var i = 0; i < results.length; i++) {

					// 	PayrollModel.findOneAndUpdate(
					// 		{
					// 			_id: results[i]._id
					// 		}, 
					// 		{
					// 			$set:{company_id:mongoose.Types.ObjectId( results[i].company_id )}
					// 		}, {new: true}, (err, doc) => {
					// 	    if (err) {
					// 	        //console.log("Something wrong when updating data!");
					// 	    }

					// 	    //console.log(doc);
					// 	});
					// }
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var pay_period_from = req.body.pay_period_from;
	var pay_period_to = req.body.pay_period_to;

	// Check if the pay period from date is after pay period to date.
	var isAfter = moment(pay_period_from).isAfter(pay_period_to);

	if ( isAfter ) {
		Params.error = true;
		Params.msg = 'Invalid dates for payroll.';
		res.json( Params );
		return;
	}

	// Check if the pay period from date is the same as the pay period to date.
	var isSame = moment(pay_period_from).isSame(pay_period_to);

	if ( isSame ) {
		Params.error = true;
		Params.msg = 'Dates for payroll cannot be the same.';
		res.json( Params );
		return;
	}

	let branch_id = req.query.branch_id || req.user.branch_id;

	var Payroll = new PayrollModel({
		name: req.body.name,
		salary_type: req.body.salary_type, // TODO: allow only specified salary types in config.
		pay_period_from: pay_period_from,
		pay_period_to: pay_period_to,
		company_id: req.user.company_id,
		branch_id: branch_id,
		payroll_group_id: req.body.payroll_group_id ? req.body.payroll_group_id : null, // TODO: Check if group id exist or not.
		total_incomes: 0,
		total_deductions: 0,
		total_taxes: 0,
		total_contributions: 0,
		overall_total_netpay: 0,
	});

	// Validate the input first.
	Payroll.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		// Now save data into db.
		Payroll.save();

		let branch_id = req.query.branch_id || '';

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: Payroll._id,
			type: 'Payrolls',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'created the '+ Payroll.name +'.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully created.';
		Params.result = Payroll;
		res.json( Params );

	});

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	let branch_id = req.query.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	PayrollModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
	.select(fields);
});


// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };
	var branch_id = req.query.branch_id || '';

	// Check if the pay period from date is after pay period to date.
	var isAfter = moment(data.pay_period_from).isAfter(data.pay_period_to);

	if ( isAfter ) {
		Params.error = true;
		Params.msg = 'Invalid dates for payroll.';
		res.json( Params );
		return;
	}

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	// TODO: ???
	if ( data.status ){

		let approver_conditions = {
			employee: req.user._id,
			company_id: req.user.company_id,
			branch_id: branch_id
		}

		PayrollApproverModel.findOne(approver_conditions, function(err, result) {
			if ( err ) {
				//console.log( err );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( !result ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0000;
				res.json( Params );
				return;
			}

			PayrollModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

				if ( err ) {
					//console.log( err );
					Params.form_errors = err;
					Params.error = true;
					res.json( Params );
					return;
				}

				if ( !result ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0004;
					res.json( Params );
					return;
				}

				new SystemLogModel({
					user_id: req.user._id, 
					employee: req.user._id, 
					ref_id: req.params.id,
					type: 'Payrolls',
					company_id: req.user.company_id,
					branch_id: branch_id,
					action: 'updated the '+ result.name +'.'
				}).save();

				Params.results = result;
				Params.msg = 'Successfully updated.';
				res.json( Params );
				return;
				
			})


		});

	} else {
		
		PayrollModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

			if ( err ) {
				//console.log( err );
				Params.form_errors = err;
				Params.error = true;
				res.json( Params );
				return;
			}

			if ( !result ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0004;
				res.json( Params );
				return;
			}

			new SystemLogModel({
				user_id: req.user._id, 
				employee: req.user._id, 
				ref_id: req.params.id,
				type: 'Payrolls',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'updated the '+ result.name +'.'
			}).save();

			Params.results = result;
			Params.msg = 'Successfully updated.';
			res.json( Params );
			return;
			
		})		
	}





});


// CALCULATE
router.get('/user/:userId/calculate', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(500) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Daily Time records starts here.
	var Payroll = require("./../libs/Payroll");

	// let contributions = req.query.contributions || '';
	// let params = {
	// 	user: null,
	// 	company_id: req.user.company_id,
	// 	salary_type: req.query.salary_type,
	// 	period: {
	// 		from: req.query.from,
	// 		to: req.query.to,
	// 	}
	// }

	let params = {
		user: null,
		company_id: req.user.company_id,
		branch_id: req.query.branch_id || req.user.branch_id,
		salary_type: req.query.salary_type,
		period: {
			from: req.query.from,
			to: req.query.to,
		},
		user_id: req.query.employee_id,
		payroll_id: req.query.payroll_id,

	 	with_taxes: req.query.with_taxes ? req.query.with_taxes : true,
	 	with_benefits: req.query.with_benefits ? req.query.with_taxes : true,
	 	with_night_diff: req.query.with_night_diff ? req.query.with_night_diff : true,
	 	with_ot: req.query.with_ot ? req.query.with_ot : true,
	 	deduct_absences: req.query.deduct_absences ? req.query.deduct_absences : true,
	 	deduct_lates: req.query.deduct_lates ? req.query.deduct_lates : true,
	 	deduct_undertime: req.query.deduct_undertime ? req.query.deduct_undertime : true,
	 	contributions: [],
		addtl_incomes: [],
		addtl_deductions: [],
	}


	// Step 1: Find user from DB. 
	Payroll.getUser( {_id: req.params.userId} )

	// Step 2: If found, then do calculations.
	.then( function( user ){
		params.user = user;
		return Payroll.calculateTimeRecords( params );
	}) 

	.then( function( results ){
		Params.error = false;
		Params.results_count = 1;
		Params.results = results;
		res.json( Params );
	})
	
	// Step 6: If it has, then blah blah blah.
	.catch(function(error){
		// Handle error
		//console.log( 'GOT an error', error );
		Params.error = true;
		Params.error_type = error.error_type || 'error';
		Params.msg = error.message || error;
		res.json( Params );
	});

	// res.json( Params );
	// return;
});


// Calculate total net pay, deductions, gross incomes
// router.get('/:payrollId/calculate', VerifyToken, function (req, res) {

// 	Params.token = req.token;

// 	if ( req.perm.indexOf(500) === -1 ) {

// 		Params.error = true;
// 		Params.msg = ErrorMsgs.err_0000;
// 		res.json( Params );
// 		return;
// 	}

// 	var conditions = {
// 		payroll: req.params.payrollId,
// 		company_id: req.user.company_id,
// 	};

// 		var query = PayrollDetailModel
// 			.find( conditions )
// 			.sort( { createdAt: -1 } );

// 		query
// 			.exec('find', function (error, results) {
				
// 				if ( error ) {
// 					Params.error = true;
// 					Params.msg = ErrorMsgs.err_0001;
// 					res.json( Params );
// 				}

// 				let d = {
// 					'total_incomes' : 0,
// 					'total_deductions' : 0,
// 					'total_taxes' : 0,
// 					'total_contributions' : 0,
// 					'overall_total_netpay': 0,

// 				}

// 				// //console.log('Payroll:',results);
// 				if ( results.length ) {

// 					for ( var i = results.length - 1; i >= 0; i-- ) {

// 						if ( results[i].total_incomes ) d.total_incomes += results[i].total_incomes;
// 						if ( results[i].total_deductions ) d.total_deductions += results[i].total_deductions;
// 						if ( results[i].withholding_tax ) d.total_taxes += results[i].total_tax;
// 						if ( results[i].total_contributions ) d.total_contributions += results[i].total_contributions;
// 						if ( results[i].total_net_pay ) d.overall_total_netpay += results[i].total_net_pay;

// 					}

// 					// SAVE TO DATABASE: TODO: why is it there's saving here?
// 					var opts = { runValidators: true };

// 					var conditions = {
// 						_id: req.params.payrollId,
// 						company_id: req.user.company_id
// 					};

// 					var data = {
// 						total_incomes: d.total_incomes,
// 						total_deductions: d.total_deductions,
// 						total_taxes: d.total_taxes,
// 						total_contributions: d.total_contributions,
// 						overall_total_netpay: d.overall_total_netpay
// 					};

// 					PayrollModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

// 						if ( err ) {
// 							//console.log( err );
// 							Params.form_errors = err;
// 							Params.error = true;
// 							res.json( Params );
// 							return;
// 						}

// 						if ( !result ) {
// 							Params.error = true;
// 							Params.msg = ErrorMsgs.err_0004;
// 							res.json( Params );
// 							return;
// 						}

// 						Params.msg = 'Successfully calculate total net pay.';
// 						Params.result = result;
// 						Params.result.overall_total_netpay = d.overall_total_netpay;
// 						res.json( Params );
// 						return;
						
// 					})
// 				}
// 			});
	
// });



module.exports = router;