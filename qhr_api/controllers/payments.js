'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');

const uniqueString = require('unique-string');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const PaymentModel  = require('../models/Payment');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	var conditions = {
		user_id: req.user._id,
		company_id: req.user.company_id
	};

	//console.log( conditions );

	var query = PaymentModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});

});


// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	//console.log('payments show', req.params.id);

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );
	PaymentModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
	.select(fields);
});

// Store
router.post('/', VerifyToken, function (req, res) {


	Params.token = req.token;

	let stripe_charge_id = bcrypt.hashSync(uniqueString(), config.secret );

	var Payment = new PaymentModel({
		user_id: req.user._id,
		company_id: req.user.company_id,
		stripe_charge_id: stripe_charge_id,
		charge_type: req.body.charge_type,
		plan_type: req.body.plan_type,
		paid_amount: req.body.paid_amount,
		received_amount: req.body.paid_amount
	});

	// Validate the input first.
	Payment.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		// Now save data into db.
		Payment.save();

		var SystemLog = new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: Payment._id,
			type: 'Payments',
			company_id: req.user.company_id,
			branch_id: req.user.branch_id,
			action: 'created a payment.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully created.';
		Params.result = Payment;
		res.json( Params );

	});

});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	PaymentModel.findOneAndUpdate(conditions, data, opts, function (error, result) {

		if ( error ) {
			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'Payments',
			company_id: req.user.company_id,
			branch_id: req.user.branch_id,
			action: 'updated a payment.'
		}).save();

		Params.msg = 'Successfully updated.';
		res.json( Params );
		return;

	});

});

// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	PaymentModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: req.params.id,
			type: 'Payments',
			company_id: req.user.company_id,
			branch_id: req.user.branch_id,
			action: 'deleted a payment.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});

module.exports = router;
