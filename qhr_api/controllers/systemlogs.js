'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const SystemLogModel  = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(2000) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields );

	conditions.company_id = req.user.company_id;
	
	let branch_id = req.query.branch_id || '';

	if ( branch_id ) {
		conditions.branch_id = branch_id;	
	}

	var query = SystemLogModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
			.limit(limit)
			.populate('employee')  
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});


// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';
	var branch_id = req.query.branch_id || '';

	// Select fields only.
	fields = TextUtil.toFields( fields );
	SystemLogModel.findOne({_id: req.params.id}, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		if ( result.company_id !== req.user.company_id || result.branch_id !== branch_id){
			Params.error = true;
			Params.msg = 'You are not allowed to access this page.'
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	});
});


module.exports = router;
