'use strict';

const express = require('express');
const router = express.Router();
const crypto = require('crypto');

var moment = require('moment-timezone');

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var config = require('../config/config');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const SubscriptionModel  = require('../models/Subscription');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});


// INDEX
router.get('/', VerifyToken, function (req, res) {

	// Params.token = req.token;

	// var limit = parseInt(req.query.limit) || 10;
	// var page = parseInt(req.query.page) || 1;
	// var fields = (req.query.f) || '';
	// var conditions = {};
	// var skip = (page - 1) * limit
	// var type = req.query.type || 'debit';

	// // Select fields only.
	// fields = TextUtil.toFields( fields );

	// var conditions = {
	// 	company_id: req.user.company_id,
	// 	type: type
	// };

	// var query = BillingTransactionModel
	// 	.find( conditions )
	// 	.sort( { createdAt: -1 } );

	// // TODO: Find a better way to get total count for large data set.
	// query.count(function (err, count) {
	//   	query
	// 	  	.skip(skip)
	// 		.limit(limit)
	// 	  	.exec('find', function (err, results) {
	// 			if ( err ) {
	// 				Params.error = true;
	// 				Params.msg = ErrorMsgs.err_0001;
	// 				res.json( Params );
	// 			} else {
	// 				Params.total_count = count;
	// 				Params.results_count = results.length;
	// 				Params.results = results;
	// 				res.json( Params );
	// 			}
	// 	   	});
	// });
});

// Get subscription list
router.get('/get', VerifyToken, function (req, res) {

	Params.token = req.token;

	Params.results = config.plan_type;
	res.json( Params );

});


// Change sub status to paid.
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var conditions = {
		company_id: req.body.company_id,
		paid: true
	};

	// find current subscription 
	SubscriptionModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		// check if current sub is same as parameter
		if ( result && result._id == req.params.id ) {
			Params.msg = 'Subscription is already paid.';
			res.json( Params );
			return;
		}

		// delete current sub
		if ( result ) {
			SubscriptionModel.deleteOne(conditions, function(err, result) {

				if ( err ) {
					//console.log( err );
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
					return;
				}

			});
		} 
			
		// change pending sub to paid and create payment history and invoice.
		var subscirptionConditions = {
			_id: req.params.id,
			company_id: req.body.company_id,
			paid: false
		};

		var BillingModule = require("./../libs/BillingModule");

		BillingModule.getSubscription( subscirptionConditions )
			.then( function( results ) {
					// //console.log( results);
					var interval_count = results.subscription.interval_count;

					var fromDate = moment().format('YYYY-MM-DD');
					var toDate = moment(fromDate).add(interval_count, 'months').format('YYYY-MM-DD');

					var subscriptionData = {
						paid: true,
						start_at: fromDate,
						expired_at: toDate,
					};

					return BillingModule.updateSubscriptionStatus( subscriptionData, subscirptionConditions );
				})
			.then( function( results ) {
				//console.log('1',results);
				var invoice_uid =  Math.floor(100000000 + Math.random() * 900000000);
				var today = moment().format('YYYY-MM-DD');
				var params = {};
				
				params.plan_type = results.subscription.plan_type;
				// //console.log(results);
				params.paymentData = {
					transaction: 'subscription',
					trans_type: 'credit',
					payment_method: results.subscription.payment_method,
					payment_date: today,
					amount: results.subscription.total_amount,
					company_id: results.subscription.company_id,
					ref_id: results.subscription._id,
					invoice_uid: invoice_uid
				};
				return BillingModule.createPaymentTransaction( params );
			})
			.then( function( results ) {
				//console.log('2',results);
				var plan_type = results.plan_type;
				var items = {
					company_id: results.payment.company_id,
					desc: 'Subscription - Plan Type ' + plan_type,
					price: config.plan_type[plan_type].price,
					total: results.payment.amount
				};  
				//console.log('items',items);
				var invoiceData = {
					payment_method: results.payment.payment_method,
					payment_date: results.payment.payment_date,
					total_amount: results.payment.amount,
					company_id: results.payment.company_id,
					items: items,
					invoice_uid: results.payment.invoice_uid,
					payment_trans_id: results.payment._id
				}
				

				return BillingModule.createInvoice( invoiceData );
			})
			.then( function( results ) {
				//console.log(results);
				Params.results = results;
				Params.error = false;
				Params.msg = 'Billing is successful.';
				res.json( Params );
					return;
			})
			.catch(function(error){
				//console.log( 'GOR an error', error );
				Params.error = true;
				Params.msg = error.message || error;
				Params.form_errors = error.form_errors || null;
				res.json( Params );
			});
	});
});


module.exports = router;
