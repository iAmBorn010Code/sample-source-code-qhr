'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var moment = require('moment');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');
var bcrypt = require('bcryptjs');
var config = require('../config/config');



// Models declarations
const CompanyModel  = require('../models/Company');
const SystemLogModel = require('../models/SystemLog');
const UserModel = require('../models/User');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

const uniqueString = require('unique-string');

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

router.get('/users', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.query.w === 'total_users') {

		var conditions = {
			company_id: req.user.company_id,
			status: 'active'
		}

		UserModel.count(conditions, function (err, count) {

			if ( err ) {
				Params.errors = err;
				Params.error = true;
				res.json( Params );
				return;
			}
			

			Params.results = count;
			res.json( Params );
			return;

		});

	}

	if ( req.query.w === 'birthdays' ) {

		var conditions = {
			company_id: req.user.company_id,
			status: 'active'
		}

		var getMonth = moment().format("M");
		var fromDate = moment().format("D");

		conditions.birth_month =  getMonth;
		conditions.birth_day = { $gte: fromDate };

		var query = UserModel
			.find( conditions )
			.sort( { birthdate: 1 } );

		// TODO: Find a better way to get total count for large data set.
		query.count(function (err, count) { 
		  	query
			  	.exec('find', function (err, results) {
					if ( err ) {
						Params.error = true;
						Params.msg = ErrorMsgs.err_0001;
						res.json( Params );
					} else {
						Params.total_count = count;
						Params.results_count = results.length;
						Params.results = results;
						res.json( Params );
					}
			   	});
		});
	}


	if ( req.query.w === 'new_emp') {

		var conditions = {
			company_id: req.user.company_id,
			status: 'active'
		}

		let fromDate = moment().format("YYYY-MM-01 00:00:00");
		let toDate = moment().add(1, 'months').startOf('month').format("YYYY-MM-DD 00:00:00");
		conditions.createdAt =  { $gte: fromDate, $lte: toDate };

		var query = UserModel
			.find( conditions )
			.sort( { createdAt: -1 } );

		// TODO: Find a better way to get total count for large data set.
		query.count(function (err, count) {
		  	query
		  		.limit(10)
			  	.exec('find', function (err, results) {
					if ( err ) {
						Params.error = true;
						Params.msg = ErrorMsgs.err_0001;
						res.json( Params );
					} else {
						Params.total_count = count;
						Params.results_count = results.length;
						Params.results = results;
						res.json( Params );
					}
			   	});
		});

	}


});

module.exports = router;
