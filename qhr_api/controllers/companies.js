'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');
var bcrypt = require('bcryptjs');
var config = require('../config/config');
var slugify = require('slug');
var moment = require('moment');



// Models declarations
const CompanyModel  = require('../models/Company');
const UserModel  = require('../models/User');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

const uniqueString = require('unique-string');

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		_id: req.user.company_id
	};


	var query = CompanyModel
		.find( conditions )
		.select(fields)
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	// .populate('employee')
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', function (req, res) {


	var SignupModule = require("./../libs/SignupModule");

	var slug = slugify(req.body.name, {
		replacement: '',
		remove: null,
		lower: true
	})


	var company_email = req.body.email.toLowerCase();

	var companyData = {
		name: req.body.name,
		address: req.body.address,
		email: company_email,
		slug: slug,
		verification_hash: uniqueString(),
		verified: false,
		plan_type: 'A',
		ends_at: moment().add(5, 'years').format("YYYY-MM-DD HH:mm:ss"),

	};

		// Step 1: Create a company
	SignupModule.createCompany( companyData )
		// Step 2: Create Main Branch
		.then( function( results ) {
			return SignupModule.createMainBranch( results );
		})
		.then ( function (results) {
			return SignupModule.createCompanySettings( results );
		})
		// Step 3: Create User
		.then( function( results ) {

			var username = req.body.username.toLowerCase();
			var user_email = req.body.user_email.toLowerCase();
			var hash = bcrypt.hashSync(uniqueString(), config.secret );

			var userData = {
				first_name: req.body.first_name,
				middle_name: req.body.middle_name,
				last_name: req.body.last_name,
				gender: req.body.gender,
				email: user_email,
				username: username,
				password: req.body.password,
				job_title: 'Owner',
				designation: 'Company Management',
				employment_status: 'Full-time',
				status: 'active',
				hash: hash,
				company_id: results.company.id,
				branch_id: results.branch.id,
			};

			return SignupModule.createUser( results, userData );
		})
		// Step 4: Create a new permission for user.
		.then( function( results ) {
			return SignupModule.createUserPermissions( results );
		})
		// Step 5: Notify employee that he successfully signed up.
		.then( function( results ) {
			//console.log( 'GOT Final Results', results );
			// Params.results = results;
			Params.error = false;
			Params.msg = 'You have successfully created a new company.';
			res.json( Params );
			return;
		})
		// Step 6: If it has an error, then show it.
		// TODO: Remove all record if an error occured.
		.catch(function(error){
			//console.log( 'GOR an error', error );
			Params.error = true;
			Params.msg = error.message || error;
			Params.form_errors = error.form_errors || null;
			res.json( Params );
		});

	// Params.token = req.token;

	// var Company = new CompanyModel({
	// 	name: req.body.name,
	// 	address: req.body.address,
	// 	email: req.body.email,
	// 	contact_nos: req.body.contact_nos,
	// 	short_description: req.body.short_description,
	// 	long_description: req.body.long_description,
	// });

	// // Validate the input first.
	// Company.validate( function(error) {

	// 	if ( error ) {

	// 		Params.form_errors = error;
	// 		Params.error = true;
	// 		res.json( Params );
	// 		return;
	// 	}

	// 	// Now save data into db.
	// 	Company.save();

	// 	new SystemLogModel({
	// 		user_id: req.user._id, 
	// 		employee: req.user._id, 
	// 		ref_id: Company._id,
	// 		type: 'companies',
	// 		company_id: req.user.company_id,
	// 		action: 'created ' + Company.name + '.'
	// 	}).save();

	// 	Params.err = false;
	// 	Params.msg = 'Successfully created.';
	// 	Params.result = Company;
	// 	res.json( Params );

	// });

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
	};

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );
	CompanyModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		if (! result._id.equals( req.user.company_id )){
			Params.error = true;
			Params.msg = 'You are not allowed to access this page.'
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
	.select(fields);
});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };

	var conditions = {
		_id: req.params.id,
		_id: req.user.company_id
	};

	// Sanitize html.
	if ( typeof data.job_description !== 'undefined' && data.job_description ) {
		data.job_description = sanitizeHtml(data.job_description);
	}

	CompanyModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: req.params.id,
			type: 'Companies',
			company_id: req.user.company_id,
			branch_id: req.user.branch_id,
			action: 'updated the ' + result.name + ' information.'
		}).save();

		Params.msg = 'Successfully updated.';
		res.json( Params );
		return;
		
	})

});

router.get('/confirmation/:hash', function (req, res) {

	var opts = { runValidators: true };

	CompanyModel.findOne( {verification_hash: req.params.hash}, function(err, result) {
		

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}
		
		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		// Check if verification is expired
		var expired_at = moment(result.verification_exp_at).format('YYYY-MM-DD'); 
		var today = moment().format('YYYY-MM-DD');
		var isAfter = moment(expired_at).isAfter(today);

		// check if the company is already verified or the verification has expired
		if ( result.verified || !isAfter ) {
			//console.log('this is working');
			Params.error = true;
			Params.msg = "The link you are trying to access is no longer available.";
			Params.companyName = result.name;
			//console.log(Params);
			res.json( Params );
			return;
		}

		// get company employee details 
		UserModel.findOne({company_id: result._id}, function(err, user) {

			if ( err ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}
	
			if ( !user ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0004;
				res.json( Params );
				return;
			}

			Params.results_count = 1;
			Params.userFullName = user.first_name + ' ' + user.last_name;
			Params.msg = 'Company Confirmed.';
			res.json( Params );
			
		});
	
	})

});

//show company name via slug
router.get('/i/:slug', function (req, res) {

	//console.log('company slug');

	var conditions = {
		slug: req.params.slug,
	};

	var fields = TextUtil.toFields( 'slug,name' );

	var query = CompanyModel
		.find(conditions)
		.select(fields);

	// Select fields only.
	query.findOne( function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
});

router.get('/:id/generate_hash', VerifyToken, function (req, res) {

	Params.token = req.token;

	var company_hash = bcrypt.hashSync(uniqueString(), config.secret );

	Params.results = company_hash;
	res.json( Params );
	return;

});

router.get('/subscribe/:type', VerifyToken, function (req, res) {

	Params.token = req.token;

	var opts = { runValidators: true };

	var conditions = {
		_id: req.user.company_id
	};

	let ends_at = moment().add(1, 'months').format("YYYY-MM-DD HH:mm:ss");
	let trial_ends_at = moment().format("YYYY-MM-DD HH:mm:ss");

	var data = {
		plan_type: req.params.type,
		ends_at: ends_at,
		trial_ends_at: trial_ends_at
	};


	CompanyModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: req.params.id,
			type: 'Companies',
			company_id: req.user.company_id,
			branch_id: req.user.branch_id,
			action: 'updated the ' + result.name + ' information.'
		}).save();

		Params.msg = 'Subscription successful!';
		res.json( Params );
		return;
		
	})

});

module.exports = router;
