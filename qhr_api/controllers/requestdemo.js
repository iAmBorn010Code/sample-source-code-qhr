'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var config = require('../config/config');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const RequestDemoModel  = require('../models/RequestDemo');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;


// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// Store
router.post('/', function (req, res) {

	var RequestDemo = new RequestDemoModel({
		full_name: req.body.full_name,
		company_name: req.body.company_name,
		email: req.body.email,
		company_size: req.body.company_size,
		contact_no: req.body.contact_no
	});

	var conditions = {
		email: req.body.email
	}

	RequestDemoModel.findOne(conditions, function(err, result) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( result ) {
			Params.error = true;
			Params.msg = 'The email you entered is already registered.';
			res.json( Params );
			return;
		}

		// Validate the input first.
		RequestDemo.validate( function(error) {

			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}

			// Now save data into db.
			RequestDemo.save();

			var SesClient = require("../libs/SesClient");
			let loginLink = config.APP_URL + 'login';
					
			// all fields here is required
			var data = {
				to: [req.body.email],
				subject: 'QloudHR Free Demo',
				clientName: 'Hi, ' + req.body.full_name + '!',
				messageBody: 'Thank you for requesting a demo of QloudHR.',
				linkhere: 'Login link: ' + loginLink,
				messageEnd:'If you didn’t make this request, simply ignore this message. Please do not reply to this message. This is a system-generated email sent to ' + req.body.email + '.',
				from: 'demo@qloudhr.com'
			};
		    
		    SesClient.sendEmail( data )
			.then( function( results ){
				//console.log('successfully sent');
				Params.results_count = 1;
				Params.results = results;
				Params.msg = 'We have sent instructions to your email.';
				res.json( Params );
			})
			.catch(function(error){
				//console.log( 'GOT an error', error );
				Params.error = true;
				Params.error_type = error.error_type || 'error';
				Params.msg = error.message || error;
				res.json( Params );
			});

			Params.error = false;
			Params.msg = 'Successfully created.';
			Params.result = RequestDemo;
			res.json( Params );

		});

	});

});

router.post('/contact-us', function (req, res) {

	var SesClient = require("../libs/SesClient");
			
	// all fields here is required
	var data = {
		to: ['contact@qloudhr.com'], // contact@qloudhr.com
		subject: 'QloudHR Contact Us',
		clientName: 'From: ' + req.body.name,
		messageBody: req.body.message,
		linkhere: 'Client Email: '+ req.body.email,
		messageEnd: '',
		from: 'demo@qloudhr.com' // if user email, error ( not verified )
	}; 
    
    SesClient.sendEmail( data )
	.then( function( results ){
		//console.log('successfully sent');
		Params.results_count = 1;
		Params.results = results;
		Params.msg = 'Your message has been sent. We will get back to you as soon as we can. Have a nice day!';
		res.json( Params );
	})
	.catch(function(error){
		//console.log( 'GOT an error', error );
		Params.error = true;
		Params.error_type = error.error_type || 'error';
		Params.msg = error.message || error;
		res.json( Params );
	});	

	

});


module.exports = router;
