'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const UserModel  = require('../models/User');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || 'first_name,last_name,photo,_id,job_title,leave_credits';
	var conditions = {};
	var skip = (page - 1) * limit;
	let emp_id = req.query.emp_id || '';
	let leave_type = req.query.leave_type || '';

	// Select fields only.
	fields = TextUtil.toFields( fields );

	//console.log('here???');

	// Prepare conditions.
	var conditions = {
		company_id: req.user.company_id,
		leave_credits: { $exists: true, $ne: [] }
	};

	if ( req.perm.indexOf(400) === -1 ) {
		conditions._id = req.user._id;
	}

	if ( emp_id ) {
		conditions._id = emp_id;
	}

	var query = UserModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.exec('find', function (err, results) {
				if ( err ) {
					//console.log('err', err);
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	}).select(fields);

});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(400) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = '+leave_credits';
	var branch_id = req.query.branch_id || '';

	var data = {
		type: req.body.type,
		credit: req.body.credit
	};


	UserModel.findOne({ _id: req.body.user_id }, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		for ( var i = 0; i < result.leave_credits.length; i++ ) {

			let leaveType = [];
			leaveType['SL'] = 'Sick Leave';
			leaveType['VL'] = 'Vacation Leave';
			leaveType['EL'] = 'Emergency Leave';
			leaveType['PL'] = 'Paternity Leave';
			leaveType['ML'] = 'Maternity Leave';

			if ( result.leave_credits[i].type == data.type ) {
				Params.error = true;
				Params.msg = 'This employee has an existing ' + leaveType[data.type];
				res.json( Params );
				return;		
			}
		}

		result.leave_credits.push(data);

		result.save( function (err, user){

			if ( err ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( !user ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}


			Params.results = user;
			Params.msg = 'Successfully added.';
			res.json( Params );
			return;
		});

	}).select(fields);

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var conditions = {
		_id: req.query.user_id,
		company_id: req.user.company_id,
	};

	//console.log('here???show');

	var fields = (req.query.f) || 'first_name,last_name,photo,_id,job_title,leave_credits';
	fields = TextUtil.toFields( fields );

	UserModel.findOne( conditions, function(err, result) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		let data = {
			user: result,
			leave_credits: result.leave_credits.id(req.params.id)
		};

		Params.results = data;
		res.json( Params );
	}).select( fields );
});


// update leave credits
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(400) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var fields = '+leave_credits';

	UserModel.findOne({ _id: req.params.id,  }, function(err, result) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0000;
			res.json( Params );
			return;
		}

		
		result.leave_credits.id(data._id).credit = data.credit;

		result.save( function (err, credit){

			if ( err ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}

			if ( !credit ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0000;
				res.json( Params );
				return;
			}


			Params.results = credit;
			Params.msg = 'Successfully updated.';
			res.json( Params );
			return;
		});

	}).select(fields);

});

module.exports = router;