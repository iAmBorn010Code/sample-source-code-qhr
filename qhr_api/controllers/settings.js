'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const SettingModel  = require('../models/Setting');
const SystemLogModel  = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(2200) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}
	
	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields );
	SettingModel.findOne({company_id: req.user.company_id}, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;
	
	var Setting = new SettingModel({
		field1: req.body.field1,
		field2: req.body.field2,
		field3: req.body.field3,
		field4: req.body.field4,
		company_id: req.user.company_id
	});

	// Validate the input first.
	Setting.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		// Now save data into db.
		Setting.save();

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: Setting._id,
			type: 'Settings',
			company_id: req.user.company_id,
			branch_id: req.user.branch_id,
			action: 'created new setting.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully created.';
		Params.result = Setting;
		res.json( Params );

	});

});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(2200) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}
	
	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	// Sanitize html.
	if ( typeof data.job_description !== 'undefined' && data.job_description ) {
		data.job_description = sanitizeHtml(data.job_description);
	}

	SettingModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.form_errors = err;
			Params.error = true;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: req.params.id,
			type: 'Settings',
			company_id: req.user.company_id,
			branch_id: req.user.branch_id,
			action: 'updated a setting.'
		}).save();

		Params.msg = 'Successfully updated.';
		res.json( Params );
		return;
		
	})

});


module.exports = router;
