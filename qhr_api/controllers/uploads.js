'use strict';

const express = require('express');
const router = express.Router();

var crypto = require('crypto');

var multer  = require('multer')
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');
const s3 = new aws.S3();

var path = require('path');
var fs = require('fs');
var sharp = require('sharp');

var Config = require('../config/config');



// temporary Employee Avatar storage
var userAvatarStorage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, './temp_files');
	},
	filename: function(req, file, cb){
		let fileExt = path.extname( file.originalname );
		let filenamemd5 = crypto.createHash('md5').update(file.originalname).digest('hex');
		let filename = Date.now().toString() + '_' + filenamemd5 + fileExt; // .JPEG ? PNG ? 
		
		cb(null, filename);

	}
});

// company logo temporary storage
var companyLogoStorage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, './temp_files');
	},
	filename: function(req, file, cb){
		let fileExt = path.extname( file.originalname );
		let filenamemd5 = crypto.createHash('md5').update(file.originalname).digest('hex');
		let filename = Date.now().toString() + '_' + filenamemd5 + fileExt; // .JPEG ? PNG ? 
		
		cb(null, filename);

	}
});

var branchLogoStorage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, './temp_files');
	},
	filename: function(req, file, cb){
		let fileExt = path.extname( file.originalname );
		let filenamemd5 = crypto.createHash('md5').update(file.originalname).digest('hex');
		let filename = Date.now().toString() + '_' + filenamemd5 + fileExt; // .JPEG ? PNG ? 
		
		cb(null, filename);

	}
});

// User Avatar Upload
var uploadUserAvatar = multer({ storage : userAvatarStorage }).single('avatar');


// Company Logo Upload
var uploadCompanyLogo = multer({ storage : companyLogoStorage }).single('logo');

// Branch Logo Upload
var uploadBranchLogo = multer({ storage: branchLogoStorage }).single('logo');

//Upload Document
var uploadUserDoc = multer({

	limits: {
		fileSize: 1024 * 1024 * 5 // we are allowing only 5 MB files
	},
	storage: multerS3({
	s3: s3,
	bucket: Config.S3_BUCKET_FOLDER + 'companies',
	acl: 'public-read',
	contentType: function (req, file, cb) {
		cb(null, 'image/png');
	},
	metadata: function (req, file, cb) {

		cb(null, {fieldName: file.fieldname});
	},
	key: function (req, file, cb) {

		if ( 
			file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' ||
			file.mimetype === 'application/pdf' || file.mimetype === 'application/msword' ||
			file.mimetype === 'application/pdf' || file.mimetype === 'application/msword' ||
			file.mimetype === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || 
			file.mimetype === 'application/vnd.ms-excel' ||
			file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		) {

			let fileExt = path.extname( file.originalname );
			let filenamemd5 = crypto.createHash('md5').update(file.originalname).digest('hex');
			let filename = Date.now().toString()+ '_' + filenamemd5 + fileExt; // .JPEG ? PNG ? 
			let uploadPath = req.user.company_id + '/users/' + req.query.user_id + '/documents/' + filename;
			req.newfilename = filename;

			cb(null, uploadPath);
		} else {
			cb(new Error('File type not supported.'))
		}
	}
	
	}),

}).single('file');

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const UserModel  = require('../models/User');
const CompanyModel  = require('../models/Company');
const BranchModel  = require('../models/Branch');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {
	
	Params = new ApiParams().params;
	next()
});


// User Avatar Upload
router.post('/user/avatar', VerifyToken, function (req, res) {

	Params.token = req.token;


	uploadUserAvatar( req, res, function(err) {

		var avatar = req.file;
		var bucket = Config.S3_BUCKET_FOLDER + 'companies/' + req.user.company_id + '/users/' + req.body.user_id + '/avatar';

		if( err ) {
			Params.error = true;
			Params.msg = err.message;
			res.json( Params );
			return;

		}

		if( avatar === undefined ) {
			Params.error = true;
			Params.msg = 'No file selected.';
			res.json( Params );
			return;
		}

		var maxHeight = 400;
		var maxWidth = 400;
		var newHeight = 0;
		var newWidth = 0;
		var left = 0;
		var top = 0;

		var image = sharp(avatar.path);


		image
			.metadata()
			.then( function( metadata ) {

				if ( metadata.width < metadata.height ) {
					newHeight = metadata.width;
					newWidth = metadata.width;
					left = 0;
					top = Math.round((metadata.height - metadata.width) / 2);
				} else if ( metadata.height < metadata.width ) {
					newHeight = metadata.height;
					newWidth = metadata.height;
					left = Math.round((metadata.width - metadata.height) / 2);
					top = 0;
				} else {
					newHeight = newWidth = metadata.height;
					left = 0;
					top = 0;
				}

				return image
					.extract({ left: left, top: top, width: newWidth, height: newHeight })
				  	.resize( maxWidth, maxHeight )
				  	.toBuffer();
			})
			.then( function( data ) {
				// var image = Buffer.concat(chunks);
				var options = {
					Bucket:  bucket,
					Key: avatar.filename,
					Body: data,
					ACL: 'public-read',
					ContentLength: image.length,
					ContentType: "image/png"
				};

				s3.putObject(options, function (err, data) {
					if (!err) {
					  	fs.unlink(avatar.path, (err) => {
							if (err) {
								Params.msg = err.message;
								Params.error = true;
								res.json( Params );
								return;
							}

							var user_id = req.body.user_id;

							var data = {
								photo :  avatar.filename
							};

							var opts = { runValidators: true };

							var conditions = {
								_id: user_id,
								company_id: req.user.company_id
							};

							UserModel.findOneAndUpdate(conditions, data, opts, function (error, result) {

								// //console.log(avatar);

								if ( error ) {
									// Delete uploaded avatar when there is error when saving in the database has error
									var params = { Bucket: bucket, Key: avatar.filename };

									s3.deleteObjects(params, function (err, data) {
										if ( err ) {
											Params.msg = error.message;
											Params.error = true;
											res.json( Params );
											return;
										}
									});
								   
									Params.form_errors = error;
									Params.error = true;
									res.json( Params );
									return;
								}


								if ( !result ) {
									// Delete uploaded avatar when there is no user found.
									var params = { Bucket: bucket, Key: avatar.filename };

									 s3.deleteObject(params, function (err, data) {
										if ( err ) {
											Params.form_errors = error.message;
											Params.error = true;
											res.json( Params );
											return;
										}
									});

									Params.error = true;
									Params.msg = ErrorMsgs.err_0004;
									res.json( Params );
									return;

								   
								}

								Params.msg = 'Successfully uploaded.';
								Params.photo = data.photo;
								res.json( Params );
								return;

							});
					  		
						});
					}
				});
			})
			.catch(function(error){
				//console.log( 'GOR an error', error );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
			});


		})
	
});


// Company Logo Upload 
router.post('/company/logo', VerifyToken, function (req, res) {

	Params.token = req.token;

	uploadCompanyLogo( req, res, function(err) {

		var logo = req.file;
		var bucket = Config.S3_BUCKET_FOLDER + 'companies/' + req.user.company_id + '/logo';

		if( err ) {
			Params.error = true;
			Params.msg = err.message;
			res.json( Params );
			return;

		}

		if( logo === undefined ) {
			Params.error = true;
			Params.msg = 'No file selected.';
			res.json( Params );
			return;
		}

		var maxHeight = 400;
		var maxWidth = 400;
		var newHeight = 0;
		var newWidth = 0;
		var left = 0;
		var top = 0;

		var image = sharp(logo.path);

		image.metadata()
			.then( function( metadata ) {
				if ( metadata.width < metadata.height ) {
					newHeight = metadata.width;
					newWidth = metadata.width;
					left = 0;
					top = Math.round((metadata.height - metadata.width) / 2);
				} else if ( metadata.height < metadata.width ) {
					newHeight = metadata.height;
					newWidth = metadata.height;
					left = Math.round((metadata.width - metadata.height) / 2);
					top = 0;
				} else {
					newHeight = newWidth = metadata.height;
					left = 0;
					top = 0;
				}

				return image
					.extract({ left: left, top: top, width: newWidth, height: newHeight })
				  	.resize( maxWidth, maxHeight )
				  	.toBuffer();
			})
			.then( function( data ) {
				// var image = Buffer.concat(chunks);
				var options = {
					Bucket:  bucket,
					Key: logo.filename,
					Body: data,
					ACL: 'public-read',
					ContentLength: image.length,
					ContentType: "image/png"
				};

					
				s3.putObject(options, function (err, data) {
					if (!err) {
					  	fs.unlink(logo.path, (err) => {
							if (err) {
								Params.msg = err.message;
								Params.error = true;
								res.json( Params );
								return;
							}

							var company_id = req.body.company_id;

							var data = {
								logo :  logo.filename
							};

							var opts = { runValidators: true };

							var conditions = {
								_id: req.params.id,
								_id: req.user.company_id
							};

							CompanyModel.findOneAndUpdate(conditions, data, opts, function (error, result) {

								if ( error ) {
									// Delete uploaded logo when there is error when saving in the database
									var params = { Bucket: bucket, Key: logo.filename };

									s3.deleteObjects(params, function (err, data) {
										if ( err ) {
											Params.msg = error.message;
											Params.error = true;
											res.json( Params );
											return;
										}
									});

									Params.form_errors = error;
									Params.error = true;
									res.json( Params );
									return;
								}

								if ( !result ) {
									// Delete uploaded logo when there is no company found.
									var params = { Bucket: bucket, Key: logo.filename };

									 s3.deleteObject(params, function (err, data) {
										if ( err ) {
											Params.form_errors = error.message;
											Params.error = true;
											res.json( Params );
											return;
										}
									});

									Params.error = true;
									Params.msg = ErrorMsgs.err_0004;
									res.json( Params );
									return;
								}


								Params.msg = 'Successfully uploaded.';
								Params.logo = data.logo;
								res.json( Params );
								return;

							});
					  		
						});
					}
				});

			})
			.catch(function(error){
				//console.log( 'GOR an error', error );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
			});
		})
	
});

// Branch Logo Upload 
router.post('/branch/logo', VerifyToken, function (req, res) {

	Params.token = req.token;

	uploadBranchLogo( req, res, function(err) {

		var logo = req.file;
		var bucket = Config.S3_BUCKET_FOLDER + 'companies/' + req.user.company_id + '/branches/' + req.body.branch_id + '/logo';

		if( err ) {
			Params.error = true;
			Params.msg = err.message;
			res.json( Params );
			return;

		}

		if( logo === undefined ) {
			Params.error = true;
			Params.msg = 'No file selected.';
			res.json( Params );
			return;
		}

		var maxHeight = 400;
		var maxWidth = 400;
		var newHeight = 0;
		var newWidth = 0;
		var left = 0;
		var top = 0;

		var image = sharp(logo.path);

		image.metadata()
			.then( function( metadata ) {
				if ( metadata.width < metadata.height ) {
					newHeight = metadata.width;
					newWidth = metadata.width;
					left = 0;
					top = Math.round((metadata.height - metadata.width) / 2);
				} else if ( metadata.height < metadata.width ) {
					newHeight = metadata.height;
					newWidth = metadata.height;
					left = Math.round((metadata.width - metadata.height) / 2);
					top = 0;
				} else {
					newHeight = newWidth = metadata.height;
					left = 0;
					top = 0;
				}

				return image
					.extract({ left: left, top: top, width: newWidth, height: newHeight })
				  	.resize( maxWidth, maxHeight )
				  	.toBuffer();

			})
			.then( function( data ) {
				// var image = Buffer.concat(chunks);
				var options = {
					Bucket:  bucket,
					Key: logo.filename,
					Body: data,
					ACL: 'public-read',
					ContentLength: image.length,
					ContentType: "image/png"
				};

					
				s3.putObject(options, function (err, data) {
					if (!err) {
					  	fs.unlink(logo.path, (err) => {
							if (err) {
								Params.msg = err.message;
								Params.error = true;
								res.json( Params );
								return;
							}

							var branch_id = req.body.branch_id;

							var data = {
								logo :  logo.filename
							};

							var opts = { runValidators: true };

							var conditions = {
								_id: branch_id,
								company_id: req.user.company_id
							};

							BranchModel.findOneAndUpdate(conditions, data, opts, function (error, result) {

								if ( error ) {
									// Delete uploaded logo when there is error when saving in the database
									var params = { Bucket: bucket, Key: logo.filename };

									s3.deleteObjects(params, function (err, data) {
										if ( err ) {
											Params.msg = error.message;
											Params.error = true;
											res.json( Params );
											return;
										}
									});

									Params.form_errors = error;
									Params.error = true;
									res.json( Params );
									return;
								}

								if ( !result ) {
									// Delete logo avatar when there is no branch found.
									var params = { Bucket: bucket, Key: logo.filename };

									 s3.deleteObject(params, function (err, data) {
										if ( err ) {
											Params.form_errors = error.message;
											Params.error = true;
											res.json( Params );
											return;
										}
									});

									Params.error = true;
									Params.msg = ErrorMsgs.err_0004;
									res.json( Params );
									return;
								}

								Params.msg = 'Successfully uploaded.';
								Params.logo = data.logo;
								res.json( Params );
								return;

							});
					  		
						});
					}
				});
			})
			.catch(function(error){
				//console.log( 'GOR an error', error );
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
			});
	})
	
});

//Upload Document
router.post('/user/documents', VerifyToken, function (req, res) {

	Params.token = req.token;

	uploadUserDoc( req, res, function(err) {

		var file = req.file;

		//console.log(err);

		if( err ) {
			Params.error = true;
			Params.msg = err.message;
			res.json( Params );
			return;

		}

		Params.msg = 'Successfully uploaded.';
		Params.filename = req.newfilename;
		res.json( Params );
		return;
	})
	
});



module.exports = router;
