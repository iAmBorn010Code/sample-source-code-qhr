'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
const moment = require('moment-timezone');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');


// Models declarations
const OvertimeModel  = require('../models/Overtime');
const SupervisorModel  = require('../models/Supervisor');
const SystemLogModel  = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {
	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var skip = (page - 1) * limit;
	var conditions = {
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(1900) === -1 ) {
		conditions.employee = req.user._id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields );

	let branch_id = req.query.branch_id || req.user.branch_id;
	let user_id = req.query.emp_id;
	let time_in = req.query.time_in;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	if ( user_id ) {
		conditions.employee = user_id;
	}

	if ( time_in ) {

		let fromDate = moment(time_in).format("YYYY-MM-DD 00:00:00");
   		let toDate = moment(time_in).format("YYYY-MM-DD 23:59:59");
   		conditions.time_in = { $gte: fromDate, $lte: toDate };
   		conditions.status = 'approved';
	}

	var query = OvertimeModel
		.find( conditions )
		.sort( { createdAt: -1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('employee')
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(1900) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	let currTime = moment().format("YYYY-MM-DD HH:mm:ss");
	let currTimeUnix = moment(currTime, "YYYY-MM-DD HH:mm:ss").unix();
	let time_in = moment(req.body.time_in).format("YYYY-MM-DD HH:mm:ss");
	let time_in_unix = moment(time_in).unix();
	let time_out = moment(req.body.time_out).format("YYYY-MM-DD HH:mm:ss");
	let time_out_unix = moment(time_out).unix();
	let for_date = moment(req.body.time_in).format("YYYY-MM-DD");
	let added12Hours = moment(req.body.time_in).add(12, 'hours').format("YYYY-MM-DD HH:mm:ss");

	if ( time_in_unix <= currTimeUnix || time_out_unix <= currTimeUnix ) {
		Params.error = true;
		Params.msg = "Time-in or time-out cannot be the same or less than the current time and date.";
		res.json( Params );
		return;
	}

	if ( time_in_unix >= time_out_unix ) {
		Params.error = true;
		Params.msg = "Time-in cannot be the same or more than time-out.";
		res.json( Params );
		return;
	}

	if ( time_out > added12Hours ) {
		Params.error = true;
		Params.msg = "Overtime cannot be more than 12 hours.";
		res.json( Params );
		return;	
	}

	if ( req.body.with_ot_rate && req.body.ot_rate <= 0 ) {
		Params.error = true;
		Params.msg = "Overtime rate cannot be 0 or less.";
		res.json( Params );
		return;	
	}

	if ( !req.body.with_ot_rate ) {
		req.body.ot_rate = 0;
	}

	var Overtime = new OvertimeModel({
		branch_id: branch_id,
		company_id: req.user.company_id,
		employee: req.body.employee_id,
		for_date: for_date,
		time_in: time_in,
		time_in_unix: time_in_unix,
		time_out: time_out,
		time_out_unix: time_out_unix,
		with_ot_rate: req.body.with_ot_rate,
		ot_rate: req.body.ot_rate
	});

	// Validate the input first.
	Overtime.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}
		
		// Now save data into db.
		Overtime.save();

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: Overtime._id,
			type: 'Overtimes',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'Created overtime.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully created.';
		Params.result = Overtime;
		res.json( Params );


	});


});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	OvertimeModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		
		
	})
	.select(fields)
	.populate('employee');
});

// UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(1900) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var status = req.body.status;
	var opts = { runValidators: true, new: true };
	var branch_id = req.query.branch_id || req.user.branch_id;

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
		branch_id: branch_id
	};

	var OTModule = require("./../libs/OTModule");

	if ( status ) {

		let supervisor_conditions = {
			employee: req.body.employee,
			supervisor: req.user._id,
			company_id: req.user.company_id,
			branch_id: branch_id
		}

		let data = {
			status: req.body.status,
			approved_by: req.user.name
		}

		let ot_status = status == 'approved' ? 'approve' : 'disapprove';

		OTModule.checkApprover( supervisor_conditions, ot_status )
		.then( function( result ){
			return OTModule.updateOT( conditions, data );
		})
		.then( function( result ){

			var SystemLog = new SystemLogModel({
				user_id: req.user._id,
				employee: req.user._id,  
				ref_id: req.params.id,
				type: 'Overtimes',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: req.body.status + ' overtime of ' + result.employee.first_name + ' ' + result.employee.last_name + '.'
			}).save();

			if ( req.body.status == 'approved' ) {

				return OTModule.updateDTR( result );

			} else {
				return new Promise((resolve, reject) => {
					resolve( result );
				}); // End Promise	
			}

		})
		.then( function( results ){
			Params.results_count = 1;
			Params.results = results;
			Params.msg = 'Successfully updated.';
			res.json( Params );
		})
		.catch(function(error){
			Params.error = true;
			Params.msg = !("message" in error) ? '' : error.message;
			Params.form_errors = error;
			res.json( Params );
		});


	} else {

		OTModule.validateOT( req.body )
		.then( function( result ){
			return OTModule.updateOT( conditions, result );
		})
		.then( function( result ){

			var SystemLog = new SystemLogModel({
				user_id: req.user._id,
				employee: req.user._id,  
				ref_id: req.params.id,
				type: 'Overtimes',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'updated overtime of ' + result.employee.first_name + ' ' + result.employee.last_name + '.'
			}).save();

			return new Promise((resolve, reject) => {
				resolve( result );
			}); // End Promise
		})
		.then( function( results ){
			Params.results_count = 1;
			Params.results = results;
			Params.msg = 'Successfully updated.';
			res.json( Params );
		})
		.catch(function(error){
			Params.error = true;
			Params.msg = !("message" in error) ? '' : error.message;
			Params.form_errors = error;
			res.json( Params );
		});


		
	}

});


// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(1900) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
	};

	var branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	OvertimeModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		var SystemLog = new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'Overtimes',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'Deleted the overtime of ' + result.employee.first_name + ' ' + result.employee.last_name + '.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});

module.exports = router;
