'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');
var slugify = require('slug');
var moment = require('moment-timezone');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');



// Models declarations
const UserModel  = require('../models/User');
const CompanyModel  = require('../models/Company');
const BranchModel  = require('../models/Branch');
const SystemLogModel  = require('../models/SystemLog');
const NotificationModel = require('../models/Notification');
const UserSettingModel = require('../models/UserSetting');
const PasswordResetModel = require('../models/PasswordReset');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

const uniqueString = require('unique-string');
const crypto = require('crypto');


// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

router.post('/', function (req, res) {

	var SignupModule = require("./../libs/SignupModule");
	var BillingModule = require("./../libs/BillingModule");

	var company_email = req.body.company_email.toLowerCase();
	var verification_exp_at = moment().add(24, 'hours').format('YYYY-MM-DD');

	var companyData = {
		name: req.body.company_name,
		email: company_email,
		verification_hash: 
			uniqueString() + 
			crypto.createHash('md5').update(uniqueString()).digest("hex"),
		verified: false,
		verification_exp_at: verification_exp_at,
	};

	let conditions = {
		email: company_email,
		verified: true
	};

	//Check if there is a verified company with the same company email
	CompanyModel.findOne(conditions, function(err, company) {
		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( company ) {
			Params.error = true;
			Params.msg = 'The company email you entered is already registered.';
			res.json( Params );
			return;
		}

			// Step 1: Create a company
		SignupModule.createCompany( companyData )
			// Step 2: Create Main Branch
			.then( function( results ) {
				return SignupModule.createMainBranch( results );
			})
			// Step 3: Create User
			.then( function( results ) {

				var user_email = req.body.user_email.toLowerCase();
				var hash = bcrypt.hashSync(uniqueString(), config.secret );

				var tempUsername = Math.random().toString(36).substring(2, 8);
				var tempPassword = uniqueString();

				results.user = {
					first_name: req.body.first_name,
					middle_name: req.body.middle_name,
					last_name: req.body.last_name,
					gender: req.body.gender.toLowerCase(),
					email: user_email,
					contact_nos: req.body.contact_nos,
					job_title: req.body.job_title,
					designation: 'Company Management',
					employment_status: 'Full-time',
					status: 'active',
					company_id: results.company.id,
					branch_id: results.branch.id,
					daily_work_schedules: config.daily_work_schedules,
					leaves: config.leaves,
					hash: hash,
					username: tempUsername,
					password: tempPassword
				};

				return SignupModule.createUser( results );
			})
			// Step 4: Create a new subscription for user.
			.then( function( results ) {

				var plan_type = req.body.plan_type || 'A';
				var paid = false; 
				var interval_count = req.body.interval_count;

				if ( plan_type === 'A' ) {
					paid = true;	

					var fromDate = moment().format('YYYY-MM-DD');
					var toDate = moment(fromDate).add(interval_count, 'months').format('YYYY-MM-DD');
				}
				var total_amount = ( config.plan_type[plan_type].price * interval_count) || 0;

				results.subscription = {
					user_id: results.user._id,
					company_id: results.company._id,
					interval: 'monthly',
					interval_count: interval_count,
					plan_type: req.body.plan_type,
					total_amount: total_amount,
					paid: paid,
					payment_method: req.body.payment_method,
					start_at: fromDate,
					expired_at: toDate,
				};

				return SignupModule.createSubscription( results );
			})
			// Step 5: Create a new permission for user.
			.then( function( results ) {
				return SignupModule.createUserPermissions( results );
			})
			// Step 6: If plan type is A create invoice and payment.
			.then( function( results ) {
				//console.log('results: ' + results.subscription.plan_type)
				var plan_type =  results.subscription.plan_type;
				if ( plan_type == 'A' ) {
					var BillingModule = require("./../libs/BillingModule");
					var invoice_uid =  Math.floor(100000000 + Math.random() * 900000000);
					var today = moment().format('YYYY-MM-DD');
					var params = {};

					params.paymentData = {
						transaction: 'subscription',
						trans_type: 'credit',
						payment_method: results.subscription.payment_method,
						payment_date: today,
						amount: 0,
						company_id: results.subscription.company_id,
						ref_id: results.subscription._id,
						invoice_uid: invoice_uid
					};

					BillingModule.createPaymentTransaction( params )
						.then( function( billingResults ) {
							var items = {
								company_id: billingResults.payment.company_id,
								desc: 'Subscription - Plan Type A',
								price: config.plan_type[plan_type].price,
								total: billingResults.payment.amount
							};  
							var invoiceData = {
								payment_method: billingResults.payment.payment_method,
								payment_date: billingResults.payment.payment_date,
								total_amount: 0,
								company_id: billingResults.payment.company_id,
								items: items,
								invoice_uid: billingResults.payment.invoice_uid,
								payment_trans_id: billingResults.payment._id
							}

							return BillingModule.createInvoice( invoiceData );
						})
						.catch(function(error){
							//console.log( 'GOR an error', error );
							Params.error = true;
							Params.msg = error.message || error;
							Params.form_errors = error.form_errors || null;
							res.json( Params );
						});
				}

				return results;
				
				
			}).then( function( results ) {
				//console.log( 'GOT Final Results', results );
				Params.results = results;
				Params.error = false;
				Params.msg = 'You have successfully signed up.';
				res.json( Params );
				return;
			})
			// Step 7: If it has an error, then show it.
			// TODO: Remove all record if an error occured.
			.catch(function( result ){
				//console.log( 'GOR an error', result );
				Params.error = true;
				Params.msg = result.message || result;
				Params.form_errors = result.form_errors || null;

				// Check if failed, delete company.
				if ( result.company) {

					var conditions = {
						_id: result.company._id
					};

					CompanyModel.findByIdAndRemove(conditions, function(err, result) {});
				} 

				// Delete branch
				if ( result.branch ) {
					var conditions = {
						_id: result.branch._id
					};

					BranchModel.findByIdAndRemove(conditions, function(err, result) {});
				}


				res.json( Params );
				return;
			});

	});


		
});

// resend verification link
router.post('/resend-verification-link', function (req, res) {
	// Prepare data.
	var data = {
		username: req.body.username,
		password: req.body.password,
	}
	var opts = { runValidators: true, context: 'query', new: true};

	// Check if username exists
	UserModel.findOne({username: data.username}, function(err, user) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !user ) {
			Params.error = true;
			Params.msg = 'Username not found.';
			res.json( Params );
			return;
		}

		var verification_hash = uniqueString() + crypto.createHash('md5').update(uniqueString()).digest("hex");
		var verification_exp_at = moment().add(24, 'hours').format('YYYY-MM-DD');

		var company_data = {
			verification_hash: verification_hash,
			verification_exp_at: verification_exp_at
		};

		console.log('company data: ', company_data);

		CompanyModel.findOneAndUpdate({_id: user.company_id, verified: false}, company_data , opts, function (error, company) {

			if ( error ) {
				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}

			if ( !company ) {
				Params.error = true;
				Params.msg = 'Your company is already verified.';
				res.json( Params );
				return;
			}

			console.log('companyData new: ', company);


			var SesClient = require("../libs/SesClient");
			let link = config.APP_URL + 'companies/verify/' + verification_hash;
					
			// all fields here is required
			var data = {
				to: [company.email, user.email],
				subject: 'QloudHR - Company Verification',
				clientName: 'Hello!',
				messageBody: company.name + '\'s new verification link is now available. Please click the link below.',
				linkhere: 'Verification link: ' + link,
				messageEnd:'Thank you for using QloudHR.',
				from: 'QloudHR Support <support@qloudhr.com>'
			};
		    
		    SesClient.sendEmail( data )
				.then( function( results ){
					Params.results_count = 1;
					Params.userFullName = user.first_name + ' ' +  user.last_name;
					Params.msg = company.name + ' has been successfully verified.';
					res.json( Params );
				})
				.catch(function(error){
					Params.error = true;
					Params.error_type = error.error_type || 'error';
					Params.msg = error.message || error;
					res.json( Params );
				});
		});

	});

});


//Get username and password
router.post('/verified/:hash', function (req, res) {
	// Prepare data.
	var data = {
		username: req.body.username,
		password: req.body.password,
	};
	var opts = { runValidators: true, context: 'query'};

	var conditions = {
		verification_hash: req.params.hash,
		verified: false
	};

	// find company with same verification hash
	CompanyModel.findOne( conditions, function(err, company) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !company ) {
			Params.error = true;
			Params.msg = 'The verification link provided is already expired or does not exist.';
			res.json( Params );
			return;
		}


		// Sanitize html.
		if ( typeof data.password !== 'undefined' && data.password ) {
			// Validate password.
			if ( data.password.length < 6 || data.password.length > 30  ) {

				Params.error = true;
				Params.msg = 'Password should be between 6 and 30 characters.';
				res.json( Params );
				return;
			}

			// Let's try to encrypt password.
			try {
				data.password = bcrypt.hashSync(data.password, config.secret );
			} catch(err) {
				//console.log( err );
				Params.error = true;
				Params.msg = 'Unable to add password at this time. Please try again later.';
				res.json( Params );
				return;
			}
		}

		if ( typeof data.username !== 'undefined' && data.username ) {
			// Validate username.
			if ( data.username.length < 2 || data.username.length > 15  ) {
				Params.error = true;
				Params.msg = 'Username should be between 2 and 15 characters.';
				res.json( Params );
				return;
			}

			var regexp = /^[a-z\d\_]+$/i;

			if ( !data.username.match(regexp) ) {
				Params.error = true;
				Params.msg = 'Username should contain alpha-numeric characters and underscores only.';
				res.json( Params );
				return;					
			}
		}

		// TODO: Add this in the module. Check if username already exists.
		UserModel.findOne({username: data.username}, function(err, user) {

			if ( err ) {
				Params.error = true;
				Params.msg = ErrorMsgs.err_0001;
				res.json( Params );
				return;
			}
	
			if ( user ) {
				Params.error = true;
				Params.msg = 'The username you entered is already taken.';
				res.json( Params );
				return;
			}

			//console.log('user', user);

			var conditions = {
				company_id: company._id
			};

			// Update user name and password
			UserModel.findOneAndUpdate(conditions, data, {new: true}, function(err, employee) {

				if ( err ) {
					//console.log( err );
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
					return;
				}

				if ( !employee ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0004;
					res.json( Params );
					return;
				}

				// Now, it's time to verify the company.
				var data = {
					verified: true,
					verification_hash: null,
				}

				CompanyModel.update({_id: company._id}, data , opts, function (error) {

					if ( error ) {
						Params.form_errors = error;
						Params.error = true;
						res.json( Params );
						return;
					}

					var SesClient = require("../libs/SesClient");
					let link = config.APP_URL + 'login';
							
					// all fields here is required
					var data = {
						to: [company.email, employee.email],
						subject: 'QloudHR - Company Verification',
						clientName: 'Hello '+employee.first_name+'!',
						clientUserName: 'Username: '+employee.username+'',
						messageBody: company.name + ' has been successfully verified! Click on the link below to login now.',
						linkhere: 'Login link: ' + link,
						messageEnd:'Thank you for using QloudHR.',
						from: 'QloudHR Support <support@qloudhr.com>'
					};
				    
				    SesClient.sendVerifiedEmail( data )
						.then( function( results ){
							Params.results_count = 1;
							Params.userFullName = employee.first_name + ' ' +  employee.last_name;
							Params.msg = company.name + ' has been successfully verified.';
							res.json( Params );
						})
						.catch(function(error){
							Params.error = true;
							Params.error_type = error.error_type || 'error';
							Params.msg = error.message || error;
							res.json( Params );
						});
				});
			});

		});
	});

});

module.exports = router;