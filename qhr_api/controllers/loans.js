'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
var bcrypt = require('bcryptjs');
var config = require('../config/config');
const moment = require('moment-timezone');
// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const LoanModel  = require('../models/Loan');
const SystemLogModel = require('../models/SystemLog');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		company: req.user.company_id,
		status: { $ne:'deleted' },
	};

	if ( req.query.status ) {
		conditions.status = req.query.status;
	}

	if ( req.query.emp_id ) {
		conditions.employee = req.query.emp_id;
	}

	if ( req.perm.indexOf(1400) === -1 ) {
		conditions.employee = req.user._id;
	}

	let branch_id = req.query.branch_id || '';

	if ( branch_id ) {
		conditions.branch = branch_id;
	}

	var query = LoanModel
		.find(conditions)
		.select(fields)
		.sort({ createdAt: -1 });

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit( limit )
		  	.populate('employee')
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(1400) === -1 &&
		req.body.employee_id != req.user._id
	) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// var start_date = req.body.start_date;

	// // Check if the start_date is after the end_date
	// var isAfter = moment(start_date).isAfter(end_date);

	// if ( isAfter ) {
	// 	Params.error = true;
	// 	Params.msg = 'Invalid dates for loan.';
	// 	res.json( Params );
	// 	return;
	// }

	var branch_id = req.query.branch_id || req.user.branch_id;

	var Loan = new LoanModel({
		employee: req.body.employee_id,
		company: req.user.company_id,
		branch: branch_id,
		amount: req.body.amount,
		balance: req.body.amount,
		payment_type: req.body.payment_type,
		period: req.body.period,
		with_interest: req.body.with_interest,
		interest: req.body.interest,
		interest_type: req.body.interest_type,
		status: 'pending',
		start_date: req.body.start_date,
		percentage: req.body.percentage,
		// payment_rcv: req.body.payment_rcv,
		
	});

	// Validate the input first.
	Loan.validate( function(error) {

		if ( error ) {

			Params.form_errors = error;
			Params.error = true;
			res.json( Params );
			return;
		}

		// Now save data into db.
		Loan.save();

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: Loan._id,
			type: 'Loans',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'created a loan.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully created.';
		Params.result = Loan;
		res.json( Params );

	});

});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company: req.user.company_id,
	};

	if ( req.perm.indexOf(1400) === -1 ) {
		conditions.employee = req.user._id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	LoanModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;
	})
	.populate('employee');
	// .select(fields);
});


// UPDATE
router.post('/:id', VerifyToken, function (req, res) {
	Params.token = req.token;

	if ( req.perm.indexOf(1400) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// Prepare data.
	var data = req.body;
	var opts = { runValidators: true };

	var start_date = req.body.start_date;
	var end_date = req.body.end_date;

	if ( start_date &&  end_date) {
		// Check if the start_date is after the end_date
		var isAfter = moment(start_date).isAfter(end_date);

		if ( isAfter ) {
			Params.error = true;
			Params.msg = 'Invalid dates for loan.';
			res.json( Params );
			return;
		}

		// Check if the start_date is the same as the end_date.
		var isSame = moment(start_date).isSame(end_date);

		if ( isSame ) {
			Params.error = true;
			Params.msg = 'Dates for loan cannot be the same.';
			res.json( Params );
			return;
		}
	}

	var conditions = {
		_id: req.params.id,
		company: req.user.company_id
	};


	LoanModel.findOneAndUpdate(conditions, data, opts, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.form_errors = err;
			Params.error = true;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		let branch_id = req.query.branch_id || '';

		new SystemLogModel({
			user_id: req.user._id, 
			employee: req.user._id, 
			ref_id: req.params.id,
			type: 'Loans',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'updated a loan.'
		}).save();

		Params.msg = 'Successfully updated.';
		res.json( Params );
		return;
		
	})


});




module.exports = router;