'use strict';

const express = require('express');
const router = express.Router();
var config = require('../config/config');
// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');

// Models declarations
const DailyTimeRecordModel  = require('../models/DailyTimeRecord');
const SystemLogModel = require('../models/SystemLog');
const NotificationModel = require('../models/Notification');
const UserModel = require('../models/User');
const moment = require('moment-timezone');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var conditions = {};
	var skip = (page - 1) * limit;
	var overtime = req.query.ot || '';

	var fromDate = moment().format("YYYY-MM-DD 00:00:00");
	var toDate = moment().format("YYYY-MM-DD 23:59:59");

	// Select fields only.
	fields = TextUtil.toFields( fields );

	// Prepare conditions.
	if ( req.query.user_id ) {
		conditions.user_id = req.query.user_id;
	}

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(300) === -1 ) {
		conditions.user_id = req.user._id;
	}

	if ( req.query.filter_date ) {

		if ( req.query.filter_date === 'today' ) {
			fromDate = moment().format("YYYY-MM-DD 00:00:00");
			toDate = moment().format("YYYY-MM-DD 23:59:59");
			conditions.for_date = { $gte: fromDate, $lte: toDate };
		} else if( req.query.filter_date === 'yesterday' ) {
			fromDate = moment().subtract(1, 'days').startOf('day');
			toDate = moment().subtract(1, 'days').endOf('day');
			conditions.for_date = { $gte: fromDate, $lte: toDate };
		} else if( req.query.filter_date === 'current_month' ) {
			fromDate = moment().format("YYYY-MM-01 00:00:00");
			toDate = moment().add(1, 'months').startOf('month').format("YYYY-MM-DD 00:00:00");
			conditions.for_date = { $gte: fromDate, $lte: toDate };
		} else if ( req.query.filter_date === 'custom' ) {
			fromDate = moment().format(req.query.date_from + " 00:00:00");
			toDate = moment().format(req.query.date_to + " 23:59:59");
			conditions.for_date = { $gte: fromDate, $lte: toDate };
		}
	} else {
		//console.log('no filter date');
	}

	if ( req.query.emp_id ) {
		conditions.user_id = req.query.emp_id;
	}

	if ( overtime ) {
		conditions.with_ot = true;
	}

	conditions.branch_id = req.query.branch_id || req.user.branch_id;
	conditions.company_id = req.user.company_id;

	var query = DailyTimeRecordModel
		.find( conditions )
		.sort( { for_date: -1 } );

		// if ( !overtime ) {
		// 	query.or([
		// 		{ time_in: { $gt: 0 } }, 
		// 		{ time_in2: { $gt: 0 } }
		// 	]);	
		// }

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('employee', 'job_title email first_name last_name middle_name designation photo _id branch_id company_id')
		  	.populate('designation', 'title')
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;


					// for (var i = 0; i < results.length; i++) {
					// 	// //console.log(results[i].time_in);

					// 	var opts = { runValidators: false };
					// 	let time_in = moment(results[i].time_in).format("YYYY-MM-DD HH:mm:ss");
					// 	var c = {
					// 		_id: results[i].id,
					// 	};

					// 	let d = {
					// 		time_in: time_in
					// 	}

					// 	DailyTimeRecordModel.findOneAndUpdate(c, d, opts, function (error, r) {});
					// }

					// udpate
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', function (req, res) {

	Params.token = req.token;

	if ( !req.body.hash || !req.body.action ) {
		Params.error = true;
		Params.msg = 'Invalid request.';
		res.json( Params );
		return;
	}

	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

	let currDate = moment().format('YYYY-MM-DD');
	let fromDate = moment(currDate).format("YYYY-MM-DD 00:00:00");
	let toDate = moment(currDate).format("YYYY-MM-DD 23:59:59");

	// Daily Time records starts here.
	var DTR = require("./../libs/DTR");
	let params = {
		user: null,
		req: req.body,
		action: req.body.action,
	}
	// Find user from DB. 
	DTR.getUser({ hash: params.req.hash })
	// Check IP Address/Device ID

	// TODO enable this later.
	// .then( function( user ) {
	// 	return DTR.checkIP( user, ip );
	// })
	// If found, then check if employee has a work schedule TODAY.
	.then( function( user ){
		params.user = user;
		return DTR.hasWorkSchedule( params );
	})

	.then( function( params ){
		return DTR.hasDesignation( params );
	})

	// If it has, check if employee already loggedin or not.
	.then( function( params ){
		if ( params.action === 'time-in' || params.action === 'time-in2' || params.action === 'overtime-in')
			return DTR.isAlreadyLoggedIn( params );
		else
			return DTR.isAlreadyLoggedOut( params );
	})
	// .then ( function( params ){
	// 	return DTR.isOnLeave({
	// 		'user': user,
	// 		'date': {
	// 			'from': fromDate,
	// 			'to': toDate
	// 		},
	// 		'w': 'create'
	// 	});
	// }) 
	// .then ( function( results ){
	// 	if ( results.onLeaveEmployees.length > 0 ) {
	// 		Params.error = true;
	// 		Params.msg = "Sorry. You are not allowed to login on your leave.";
	// 		res.json( Params );
	// 		return;
	// 	} else {
	// 		return DTR.isHoliday({
	// 			'user': params.user,
	// 			'time_in': currDate
	// 		});
	// 	}
	// }) 
	// .then( function( params ){
	// 	return DTR.hasOT( params );
	// })
	// Employee is not logged yet, Now, try to log employee.
	.then( function( params ){
		if ( params.action === 'time-in' || params.action === 'time-in2' || params.action === 'overtime-in')
			return DTR.login( params );
		else
			return DTR.logout( params );
	})


	// SUCCESS: Notify employee that employee successfully logged in.
	.then( function( params ){
		console.log('FINAL:', params);
		Params.error = false;
		Params.user = {
			photo: config.S3_BUCKET_FILE_URL + 'companies/' 
				+ params.user.company_id + '/users/' + params.user._id 
				+ '/avatar/' + params.user.photo,
			fullname: params.user.first_name + ' ' + params.user.last_name,
			job_title: params.user.job_title,
		};
		Params.msg = params.msg;
		res.json( Params );
		return;
	})
	// FAILED: If it has, then blah blah blah.
	.catch(function(error){
		// Handle error
		console.log( 'GOR an error1', error );
		Params.error = true;
		Params.error_type = error.error_type || 'error';
		if ( typeof error.user !== 'undefined' ) {
			Params.user = {
				photo: config.S3_BUCKET_FILE_URL + 'companies/' 
				+ error.user.company_id + '/users/' + error.user._id 
				+ '/avatar/' + error.user.photo,
				fullname: error.user.first_name + ' ' + error.user.last_name,
				job_title: error.user.job_title,
			};
		}
		Params.msg = error.message || error;
		res.json( Params );
	});
});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {
	Params.token = req.token;

	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
		branch_id: req.query.branch_id || req.user.branch_id
	};

	if ( req.perm.indexOf(300) === -1 ) {
		conditions.user_id = req.user._id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields );
	DailyTimeRecordModel
	.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results = result;
		res.json( Params );
	})
	.populate('employee');


});

// create
router.post('/create', VerifyToken, function (req, res) {

	Params.token = req.token;

	if ( req.perm.indexOf(300) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var DTR = require("./../libs/DTR");
	var dtrDataChecking = require("./../libs/DTRDataChecking");
	var LeaveLib = require('../libs/Leave');
	var userConditions = {
		_id: req.body.user_id,
		company_id: req.user.company_id
	};

	if ( req.body.status != 'Absent' ) {

		DTR.getUser( userConditions )
			.then( function( user ){

				var params = {
					user: user,
					data: req.body,
					action: 'create',
					id: ''
				};

				return dtrDataChecking.completeData( params );
			})
			// .then( function( params ){
			// 	return dtrDataChecking.getNightDifferential( params );
			// })
			.then( function( params ){
				return dtrDataChecking.validateReferences( params );
			})
			.then( function( params ){
				return dtrDataChecking.hasDTR( params );
			})
			.then ( function( params ){
				params.leave_action = 'create';
				return DTR.isOnLeave( params );
			}) 
			.then ( function( params ){
				return DTR.isHoliday( params );
			})
			.then ( function( params ){
				if ( params.data.on_leave ) {

					params.user_id = params.user._id;
					params.company_id = params.user.company_id;
					params.branch_id = params.user.branch_id;
					params.type = params.data.leave_type;

					return LeaveLib.checkLeaveCredit( params );

				} else {
					return new Promise((resolve, reject) => {
						resolve( params );
					}); 
				}
			})
			.then ( function( params ){
				if ( params.data.on_leave ) {
					return LeaveLib.updateUserLeave( params );
				} else {
					return new Promise((resolve, reject) => {
						resolve( params );
					}); 
				}
			})
			.then( function( params ){
				return dtrDataChecking.dtrValidation( params );
			})
			.then( function( params ){
				return DTR.createOrUpdateAttendance( params );
			})
			.then( function( result ) {

				let branch_id = req.query.branch_id || req.user.branch_id;

				new SystemLogModel({
					user_id: req.user._id, 
					employee: req.user._id, 
					ref_id: result.dtr._id,
					type: 'Daily Time Records',
					company_id: req.user.company_id,
					branch_id: branch_id,
					action: 'created an employee attendance.'
				}).save();

				return new Promise((resolve, reject) => {
					resolve( result );
				}); 
				
			})
			.then( function( result ){
				Params.dtr = result.dtr;
				Params.error = false;
				Params.msg = 'You have successfully created an attendance.';
				res.json( Params );
				return;
			})
			.catch(function(error){
				Params.error = true;
				Params.error_type = error.error_type || 'error';
				Params.msg = error.message || error;
				res.json( Params );
			});

	} else {

		DTR.getUser( userConditions )
			.then( function( user ){

					var params = {
						user: user,
						data: req.body,
						action: 'create',
						id: ''
					};

					return dtrDataChecking.completeData( params );
				})
			.then( function( params ){
				return dtrDataChecking.validateReferences( params );
			})
			.then( function( params ){
				return dtrDataChecking.hasDTR( params );
			})
			.then ( function( params ){
				params.leave_action = 'create';
				return DTR.isOnLeave( params );
			})
			.then ( function( params ){
				return DTR.isHoliday( params );
			})
			.then( function( params ){
				return dtrDataChecking.dtrValidation( params );
			})
			.then( function( params ){
				return DTR.createOrUpdateAttendance( params );
			})
			.then( function( result ) {

				let branch_id = req.query.branch_id || req.user.branch_id;

				new SystemLogModel({
					user_id: req.user._id, 
					employee: req.user._id, 
					ref_id: result.dtr._id,
					type: 'Daily Time Records',
					company_id: req.user.company_id,
					branch_id: branch_id,
					action: 'created an employee attendance.'
				}).save();

				return new Promise((resolve, reject) => {
					resolve( result );
				}); 
				
			})
			.then( function( result ){
				Params.dtr = result.dtr;
				Params.error = false;
				Params.msg = 'You have successfully created an attendance.';
				res.json( Params );
				return;
			})
			.catch(function(error){
				Params.error = true;
				Params.error_type = error.error_type || 'error';
				Params.msg = error.message || error;
				res.json( Params );
			});

	}


});


//UPDATE
router.post('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;
	var opts = { runValidators: true };

	if ( req.perm.indexOf(300) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	// check for reason
	let reason = req.body.reason_for_update;

	if ( !reason ) {
		Params.error = true;
		Params.msg = "Please state your reason for updating this attendance.";
		res.json( Params );
		return;
	}			

	// get attendance details
	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id,
		branch_id: req.query.branch_id || req.user.branch_id,
	};
 
	DailyTimeRecordModel.findOne(conditions, function(err, result) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		var userConditions = {
			_id: result.user_id,
			company_id: result.company_id
		};

		var DTR = require("./../libs/DTR");
		var dtrDataChecking = require("./../libs/DTRDataChecking");
		var LeaveLib = require('../libs/Leave');
		var user = null;

		DTR.getUser( userConditions )
		.then( function( user ){

			var params = {
				user: user,
				data: req.body,
				dtr: result,
				action: 'update',
				id: result._id
			};

			return dtrDataChecking.completeData( params );
		})
		// .then( function( params ){
		// 	return dtrDataChecking.getNightDifferential( params );
		// })
		.then( function( params ){
				return dtrDataChecking.validateReferences( params );
		})
		.then( function( params ){
			return dtrDataChecking.hasDTR( params );
		})
		.then ( function( params ){
			params.leave_action = 'create';
			return DTR.isOnLeave( params );
		}) 
		.then ( function( params ){
			return DTR.isHoliday( params );
		})
		.then ( function( params ){
			if ( params.data.on_leave ) {

				params.user_id = params.user._id;
				params.company_id = params.user.company_id;
				params.branch_id = params.user.branch_id;
				params.type = params.data.leave_type;

				return LeaveLib.checkLeaveCredit( params );
				
			} else {
				return new Promise((resolve, reject) => {
					resolve( params );
				}); 
			}
		})
		.then ( function( params ){
			if ( params.data.on_leave ) {
				return LeaveLib.updateUserLeave( params );
			} else {
				return new Promise((resolve, reject) => {
					resolve( params );
				}); 
			}
		})
		.then( function( params ){
			return dtrDataChecking.dtrValidation( params );
		})
		.then( function( params ){
			return DTR.createOrUpdateAttendance( params );
		})
		.then( function( result ) {

			let branch_id = req.query.branch_id || req.user.branch_id;

			new SystemLogModel({
				user_id: req.user._id, 
				employee: req.user._id, 
				ref_id: result.dtr._id,
				type: 'Daily Time Records',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'created an employee attendance.'
			}).save();

			return new Promise((resolve, reject) => {
				resolve( result );
			}); 
			
		})
		.then( function( result ){
			Params.dtr = result.dtr;
			Params.error = false;
			Params.msg = 'You have successfully updated an attendance.';
			res.json( Params );
			return;
		})
		.catch(function(error){
			Params.error = true;
			Params.error_type = error.error_type || 'error';
			Params.msg = error.message || error;
			res.json( Params );
		});
		
	});

});

// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;
	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(300) === -1 ) {
		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id = req.query.branch_id || req.user.branch_id;

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	DailyTimeRecordModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'Daily Time Records',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'deleted an attendance.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});


module.exports = router;
