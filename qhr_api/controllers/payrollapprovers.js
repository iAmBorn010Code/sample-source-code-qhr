'use strict';

const express = require('express');
const router = express.Router();

// Libs
var ErrorMsgs = require('../constants/ErrorMsgs');
var TextUtil = require('../libs/TextUtil');
var sanitizeHtml = require('sanitize-html');
const moment = require('moment-timezone');

// Middlewares
var VerifyToken = require('../middlewares/VerifyToken');


// Models declarations
const PayrollApproverModel  = require('../models/PayrollApprover');
const SystemLogModel  = require('../models/SystemLog');
const NotificationModel = require('../models/Notification');

// Variables
var ApiParams = require('../constants/ApiParams');
var Params = new ApiParams().params;

// Middleware that is specific to this router.
router.use( function timeLog (req, res, next) {

	
	Params = new ApiParams().params;
	next()
});

// INDEX
router.get('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	var conditions = {
		company_id: req.user.company_id,
	};

	var limit = parseInt(req.query.limit) || 10;
	var page = parseInt(req.query.page) || 1;
	var fields = (req.query.f) || '';
	var skip = (page - 1) * limit;
	var branch_id = req.query.branch_id || '';
	let w = req.query.w || '';

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(1800) === -1 ) {
		conditions.employee = req.user._id;
	}

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	if ( w == 'payroll_approver' ) {
		conditions.employee = req.user._id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields );

	var query = PayrollApproverModel
		.find( conditions )
		.sort( { date: 1 } );

	// TODO: Find a better way to get total count for large data set.
	query.count(function (err, count) {
	  	query
		  	.skip(skip)
		  	.limit(limit)
		  	.populate('employee')
		  	.exec('find', function (err, results) {
				if ( err ) {
					Params.error = true;
					Params.msg = ErrorMsgs.err_0001;
					res.json( Params );
				} else {
					Params.total_count = count;
					Params.results_count = results.length;
					Params.results = results;
					res.json( Params );
				}
		   	});
	});
});

// Store
router.post('/', VerifyToken, function (req, res) {

	Params.token = req.token;

	// Verify if employee don't have an access capability.
	if ( req.perm.indexOf(1800) === -1 ) {

		Params.error = true;
		Params.msg = ErrorMsgs.err_0000;
		res.json( Params );
		return;
	}

	var branch_id = req.query.branch_id || '';

	var PayrollApprover = new PayrollApproverModel({
		employee: req.body.user_id,
		company_id: req.user.company_id,
		branch_id: branch_id,
	});

	var conditions = {
		employee: req.body.user_id,
		company_id: req.user.company_id,
		branch_id: branch_id
	}

	PayrollApproverModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( result ) {
			Params.error = true;
			Params.msg = "This employee is already allowed to approve payrolls.";
			res.json( Params );
			return;
		}

		// Validate the input first.
		PayrollApprover.validate( function(error) {

			if ( error ) {

				Params.form_errors = error;
				Params.error = true;
				res.json( Params );
				return;
			}
			
			// Now save data into db.
			PayrollApprover.save();

			new SystemLogModel({
				user_id: req.user._id,
				employee: req.body.user_id,  
				ref_id: PayrollApprover._id,
				type: 'Payroll Approver',
				company_id: req.user.company_id,
				branch_id: branch_id,
				action: 'added an employee allowed to approve/disapprove payrolls.'
			}).save();

			new NotificationModel({
				user_id: req.body.user_id, 
				type: 'Payroll Approver',
				ref_id: PayrollApprover._id,
				description: 'You are now allowed to approve payrolls.'
			}).save();

			Params.error = false;
			Params.msg = 'Successfully created.';
			Params.result = PayrollApprover;
			res.json( Params );


		});

	});


});

// SHOW
router.get('/:id', VerifyToken, function (req, res) {

	Params.token = req.token;
	var fields = (req.query.f) || '';

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	// Prepare conditions.
	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(1800) === -1 ) {
		conditions.employee = req.user._id;
	}

	PayrollApproverModel.findOne(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}

		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		
		
	})
	.select(fields)
	.populate('employee');
});

// Delete
router.delete('/:id', VerifyToken, function (req, res) {
	
	Params.token = req.token;
	var fields = (req.query.f) || '';

	var conditions = {
		_id: req.params.id,
		company_id: req.user.company_id
	};

	if ( req.perm.indexOf(2400) === -1 ) {
		conditions.user_id = req.user._id;
	}

	var branch_id = req.query.branch_id || '';

	if ( branch_id ) {
		conditions.branch_id = branch_id;
	}

	// Select fields only.
	fields = TextUtil.toFields( fields, ' ' );

	PayrollApproverModel.findByIdAndRemove(conditions, function(err, result) {

		if ( err ) {
			//console.log( err );
			Params.error = true;
			Params.msg = ErrorMsgs.err_0001;
			res.json( Params );
			return;
		}

		if ( !result ) {
			Params.error = true;
			Params.msg = ErrorMsgs.err_0004;
			res.json( Params );
			return;
		}		

		new SystemLogModel({
			user_id: req.user._id,
			employee: req.user._id,  
			ref_id: req.params.id,
			type: 'Payroll Approver',
			company_id: req.user.company_id,
			branch_id: branch_id,
			action: 'removed an employee from payroll approvers.'
		}).save();

		new NotificationModel({
			user_id: result.employee, 
			type: 'Payroll Approver',
			ref_id: result._id,
			description: 'Your permission as a payroll approver has been removed.'
		}).save();

		Params.error = false;
		Params.msg = 'Successfully deleted.';
		Params.results_count = 1;
		Params.results = result;
		res.json( Params );
		return;

	})
	.select(fields);
});

module.exports = router;
